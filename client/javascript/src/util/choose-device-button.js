import * as WebFont from 'webfontloader'

WebFont.load({
  google: {
    families: [ 'Ubuntu' ]
  }
});

export const ChooseDeviceButton = {
  show(action, text) {
    this.hide()

    const button = document.createElement('button')
    button.id = 'zeus_choose_device_button'
    button.onclick = action
    button.style.cssText =
      'color: white;' +
      'background-color: #3DB4A1;' +
      'padding: 0.5em 1.5em;' +
      'border-radius: 0.5em;' +
      'border: none;' +
      'font-family: Ubuntu, sans-serif;' +
      'font-size: 12pt;' +
      'font-weight: 600;' +
      'cursor: pointer;' +
      'outline: none;' +
      'z-index: 10000;'
    button.onmouseover = () => {
      button.style.backgroundColor = '#41bfaa'
    }
    button.onmouseleave = () => {
      button.style.backgroundColor = '#3DB4A1'
    }
    button.innerText = text

    const dimmer = document.createElement('div')
    dimmer.id = 'zeus_choose_device_button_background'
    dimmer.style.cssText =
      'position: fixed;' +
      'display: flex;' +
      'align-items: center;' +
      'justify-content: center;' +
      'width: 100%;' +
      'height: 100%;' +
      'top: 0;' +
      'left: 0;' +
      'background-color: rgba(20, 30, 50, 0.9);' +
      'text-align: center;' +
      'z-index: 9999;'
    dimmer.appendChild(button)

    document.body.appendChild(dimmer)
  },
  hide() {
    const existing = document.getElementById('zeus_choose_device_button_background')
    if (existing) {
      existing.outerHTML = ''
    }
  }
}