import axios from 'axios/index'

export const ZeusApi = (apiUrl) => ({
  submitCommand: async (device, auth, command, value, meta) => {
    const response = await axios.post(`${apiUrl}/device/command/submit`, {
      deviceFilter: {
        ...device,
        auth: auth
      },
      meta: meta,
      command: command,
      value: value
    })
    return response.data
  },

  getDeviceStatus: async (device, auth) => {
    const response = await axios.post(`${apiUrl}/device/status`, {
      device: {
        ...device,
        auth: auth
      }
    })
    return response.data
  }
})