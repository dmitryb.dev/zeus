export const ZeusWebsocket = {
  subscribe: (path, request, reconnectionRequestProvider, callback) => {
    let connectionHolder = {}

    let wsCallback = {
      onMessage: msg => callback.onMessage(JSON.parse(msg)),
      onOpen: () => {
        connectionHolder.connection.send(JSON.stringify(request));
        callback.onConnected()
      },
      onReopen: () => {
        connectionHolder.connection.send(JSON.stringify(reconnectionRequestProvider()))
        callback.onConnected();
      },
      onClose: () => {
        callback.onConnectionLost()
      }
    }
    keepWebSocketConnected(path, connectionHolder, wsCallback)

    return {
      unsubscribe: () => {
        connectionHolder.isClosed = true
        connectionHolder.connection.close()
      }
    }
  }
}

const keepWebSocketConnected = (url, connectionHolder, callback, isInitial = true) => {
  connectionHolder.connection = new WebSocket(url)
  connectionHolder.connection.onopen = isInitial ? callback.onOpen : callback.onReopen
  connectionHolder.connection.onclose = () => {
    callback.onClose();
    if (!connectionHolder.isClosed) {
      console.error('WebSocket for ' + url + ' was closed/failed. Connection will be retried after 1 second')
      setTimeout(() => keepWebSocketConnected(url, connectionHolder, callback, false), 1000)
    }
  };
  connectionHolder.connection.onmessage = (event) => callback.onMessage(event.data)
}