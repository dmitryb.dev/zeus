import { ZeusWebsocket } from './websocket'

export const ZeusWsApi = (wsApiUrl) => ({
  deviceEventStream: (device, auth, callback, ...valueMappers) => {
    let request = {
      device: {
        ...device,
        auth: auth
      },
      valueTransform: valueMappers
    }
    let lastEventIdHolder = {}
    let reconnectionRequestProvider = () => ({
        startFrom: lastEventIdHolder.lastEventId != null ? lastEventIdHolder.lastEventId + 1 : null,
        device: {
          ...device,
          auth: auth
        },
        valueTransform: valueMappers
    })
    let onMessage = (event) => {
      lastEventIdHolder.lastEventId = event.eventId
      callback.onEvent(event)
    }
    let wsCallback = {
      onMessage: onMessage,
      onConnected: callback.onConnected || (() => {}),
      onConnectionLost: callback.onConnectionLost || (() => {})
    }
    return ZeusWebsocket.subscribe(`${wsApiUrl}/device/event`, request, reconnectionRequestProvider, wsCallback)
  }
})