import { ZeusApi } from '../api/api'
import { ZeusWsApi } from '../api/ws-api'

export const SingleDeviceDsl = (apiUrl, wsApiUrl, device, auth) => ({
  _httpApi: ZeusApi(apiUrl),
  _wsApi: ZeusWsApi(wsApiUrl),

  submit(command, value, meta) {
    return this._httpApi.submitCommand(device, auth, command, value, meta)
  },
  status() {
    return this._httpApi.getDeviceStatus(device, auth)
  },
  subscribe(callback, ...valueMappers) {
    this._wsApi.deviceEventStream(device, auth, callback, valueMappers)
  }
})