import popupCentered from 'popup-centered'
import * as queryString from 'query-string'
import { ChooseDeviceButton } from '../util/choose-device-button'

export const DeviceChooser = (authServer) => {
  let onDeviceChosenCallbacks = []
  window.addEventListener('message', event => {
    if (event.origin === authServer) {
      onDeviceChosenCallbacks.forEach(listener => listener(event.data))
      onDeviceChosenCallbacks = []
    }
  })

  const getCurrentDevice = () => {
    const currentDeviceJson = localStorage.getItem('zeus_current_device')
    if (currentDeviceJson) {
      return JSON.parse(currentDeviceJson)
    } else {
      return null
    }
  }

  const chooseDevice = (props) => new Promise(resolve => {
    const deviceChosenCallback = device => {
      localStorage.setItem('zeus_current_device', JSON.stringify(device))
      ChooseDeviceButton.hide()
      resolve(device)
      if (props.redirect) {
        location.replace(
          props.redirect +
          (props.redirect.includes('?') ? '&' : '?') +
          'zeus_device=' +
          JSON.stringify(device)
        )
      }
    }

    if (props.eager === false && props.button) {
      ChooseDeviceButton.show(() => chooseDevice({ ...props, eager: true }).then(deviceChosenCallback), props.button)
      return
    }

    const usePopUp = (props.popup !== false && window.innerWidth >= 600) || props.popup === true

    const redirectUrl = props.redirect ? props.redirect : location.href

    const args = [
      props.header !== undefined ? `headerText=${props.header}` : '',

      props.manufacturerId !== undefined ? `manufacturerId=${props.manufacturerId}` : '',
      props.deviceNumber !== undefined ? `deviceNumber=${props.deviceNumber}` : '',
      props.tags !== undefined ? `tags=${props.tags.join(',')}` : '',

      props.role !== undefined ? `role=${props.role}` : '',
      props.pass !== undefined ? `pass=${props.pass}` : '',
      usePopUp ? undefined : `redirectUrl=${props.redirect ? props.redirect : redirectUrl}`,
      props.dark !== undefined ? `darkTheme=${props.dark}` : ''
    ]
      .filter(arg => arg)
      .join('&')

    const authUrl = `${authServer}?${args}`

    if (usePopUp) {
      if (props.button && !getCurrentDevice()) {
        ChooseDeviceButton.show(() => chooseDevice({ ...props, eager: true }).then(deviceChosenCallback), props.button)
      }
      onDeviceChosenCallbacks.push(deviceChosenCallback)
      popupCentered(authUrl, props.header || 'Devices', 500, 700)
    } else {
      location.replace(authUrl)
    }
  })

  const saveDeviceFromUrl = () => {
    const args = queryString.parse(location.search)
    if (args.zeus_device) {
      const base = location.href.split('?')[0]
      const query = queryString.stringify({ ...args, zeus_device: undefined })

      history.replaceState(
        {},
        null,
        base + (query ? `?${query}` : '')
      )

      localStorage.setItem('zeus_current_device', args.zeus_device.toString())
    }
  }

  return {
    currentDevice() {
      saveDeviceFromUrl()
      getCurrentDevice()
    },
    requestDevice: async ({ rerequest, ...props }) => {
      saveDeviceFromUrl()

      const currentDevice = getCurrentDevice()
      if (currentDevice && rerequest !== true) {
        return currentDevice
      } else {
        return await chooseDevice(props)
      }
    }
  }
}