import { SingleDeviceDsl } from './dsl/single-device'
import { DeviceChooser } from './dsl/auth'

const ZeusClient = (config = { host: 'localhost:8080', authServer: 'http://localhost:8082' }) => {
  const deviceChooser = DeviceChooser(config.authServer)

  return {
    device(device, auth) {
      if ((auth.role == null || auth.pass == null) && auth.secret == null) {
        throw 'Auth object must contain role + pass or secret'
      }
      return SingleDeviceDsl(
        `http://${config.host}/api/v1`,
        `ws://${config.host}/api/v1`,
        device,
        auth
      )
    },
    currentDevice: (preset = {}) => deviceChooser.currentDevice(preset),
    requestDevice: (preset = {}, onSuccess = () => {}) =>
      deviceChooser.requestDevice(preset)
        .then(device => {
          onSuccess(device)
          return device
        })
  }
}

export const { device, requestDevice, currentDevice } = ZeusClient()
export default ZeusClient