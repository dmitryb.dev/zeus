
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <zeus_dsl.h>
#include <zconf.h>

ZRoleMask USER_ROLE = Z_ROLE_ID(0);
ZRoleMask ADMIN_ROLE = Z_ROLE_ID(1);

RoleId DEVELOPER_ROLE = ROLE_ID(0);
RoleId MANUFACTURER_ROLE = ROLE_ID(1);

void on_command(Command command) {
    printf("Command received: id = %lli, type = (%d) %d, %d\n", command.meta.command_id, command.type_length,
            command.type[0], command.type[1]);
    printf(" value types: (%d) %d, %d, %d\n", command.value_length, command.value_type[0], command.value_type[1],
            command.value_type[2]);
    printf(" values: %d, %f\n", command.value[0].int_value, command.value[1].float_value);

    ZEventValue value[] = {
            zeus_int_value(1),
            zeus_float_value(4.7),
            Z_END_OF_VALUES
    };

    int type[] = { 1, 2, 3, Z_END_OF_TYPES };

    ZEvent event = {
            .meta.subscribe = true,
            .meta.command.ack = true,
            .type = type,
            .value = value,
            .meta.event_id = 2
    };
    zeus_send_event(command.meta.client, &event);

    sleep(5);
}

int main()
{
    sigaction(SIGPIPE, &(struct sigaction) {SIG_IGN}, NULL);

    DeviceId dev_id = device_id(42, 21, "scrt");

    Tags tags = {"test", "v1"};

    Roles roles = {
            role(DEVELOPER_ROLE, "DEV", "12345"),
            role(MANUFACTURER_ROLE, "MNF", "abc")
    };

    ZeusClient client = zeus_client(dev_id, tags, roles, on_command);

    run_zeus_blocking(&client);

    return 0;
}

int main2()
{
    sigaction(SIGPIPE, &(struct sigaction){SIG_IGN}, NULL);

    ZDeviceId device_id = { .manufacturer_id = 12, .device_number = 21, .secret = "scrt" };

    const char *tags[] = { "test", "v1", Z_END_OF_TAGS };

    ZRole roles[] = {
            zeus_role(USER_ROLE, "USER", "PASS1"),
            zeus_role(ADMIN_ROLE, "ADMIN", "PASS2"),
            Z_END_OF_ROLES
    };

    ZConfig configuration = {
            .deviceId = device_id,
            .tags = tags,
            .roles = roles,
            .on_command = on_command
    };

    ZeusClient client = zeus_create_client(configuration);

    for (int i = 0; i < 1000000000; i++) {
        zeus_run(&client);
    }

    return 0;
}

