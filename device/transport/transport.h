#ifndef CLIENT_TRANSPORT_H
#define CLIENT_TRANSPORT_H

#include "zeus_api.h"


/**
 * Provides current time from some point
 */
int zeus_current_time_seconds();



/**
 * Implement it to support SSL connection
 *
 * @param server_address - dns name of the server (will be automatically provided by client)
 * @param port - port of server to connect
 */
void zeus_open_secure_channel(ZeusClient *client, const char* server_address, int port);

/**
 * Implement it to support plain tcp connection
 *
 * @param server_address - dns name of the server (will be automatically provided by client)
 * @param port - port of server to connect
 */
void zeus_open_insecure_channel(ZeusClient *client, const char* server_address, int port);

/**
 * Call it when channel is ready to send data
 */
void zeus_channel_ready(ZeusClient *client); // Body is provided by library

/**
 * Call it when some error occurs
 */
void zeus_channel_failed(ZeusClient *client, const char* error_type, const char* error_msg); // Body is provided by library



/**
 * Implement it to allow sending data to server
 *
 * @param data - array of data bytes
 * @param data_length_bytes - length of array of data
 */
void zeus_send_data(ZeusClient *client, const char* data, unsigned int data_length_bytes);



/**
 * Call it, when data received on your channel implementation
 * Must not be called concurrently
 *
 * @param data - array of data bytes
 * @param data_length_bytes - length of array of data
 */
void zeus_data_received(ZeusClient *client, char* data, int data_length_bytes); // Body is provided by library



/**
 * Implement it to support closing of channel
 * It should work correcty, when library tries to close already closed channel, and anyway call `zeus_channel_closed`
 */
void zeus_close_channel(ZeusClient *client);

/**
 * Call it when connection to the server is closed
 */
void zeus_channel_closed(ZeusClient *client); // Body is provided by library



/**
 * Called on every event loop step within `run_zeus` function
 */
void zeus_on_step(ZeusClient *client);



/**
 * Prints error message
 */
void zeus_log_error(const char* type, const char* msg);

void zeus_log_debug(const char* type, const char* msg);


#endif //CLIENT_TRANSPORT_H
