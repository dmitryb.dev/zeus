#define ZEUS_BYTE_ORDER_LITTLE_ENDIAN

typedef struct {
    int opened_socket;
} LinuxTransportState;

#define ZeusTransportState LinuxTransportState

#include <transport.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>

const int ZEUS_BUFFER_SIZE = 64;
char buffer[ZEUS_BUFFER_SIZE];

/**
 * Implement it to support SSL connection
 *
 * @param server_address - ascii encoded dns name of the server (will be automatically provided by client)
 * @param port - port of server to connect
 */
void zeus_open_secure_channel(ZeusClient *client, const char* server_address, int port)
{
    zeus_open_insecure_channel(client, server_address, port);
}

/**
 * Implement it to support plain tcp connection
 *
 * @param server_address - ascii encoded dns name of the server (will be automatically provided by client)
 * @param port - port of server to connect
 */
void zeus_open_insecure_channel(ZeusClient *client, const char* server_address, int port)
{
    struct hostent *he;
    if ((he = gethostbyname(server_address)) == NULL)
    {
        zeus_channel_failed(client, "Get host by name", strerror(errno));
        return;
    }

    if ((client->transport.opened_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        zeus_channel_failed(client, "Socket", strerror(errno));
        return;
    }

    struct sockaddr_in their_addr;
    their_addr.sin_family = AF_INET;      /* host byte order */
    their_addr.sin_port = htons(port);    /* short, network byte order */
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

    fcntl(client->transport.opened_socket, F_SETFL, O_NONBLOCK);

    if (connect(client->transport.opened_socket, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1
            && errno != EINPROGRESS)
    {
        zeus_channel_failed(client, "Connect", strerror(errno));
        return;
    }
}




/**
 * Implement it to allow sending data to server
 *
 * @param data - array of data bytes
 * @param data_length_bytes - length of array of data
 */
void zeus_send_data(ZeusClient *client, const char* data, unsigned int data_length_bytes)
{
    if (send(client->transport.opened_socket, data, data_length_bytes, 0) == -1 && errno != EWOULDBLOCK)
    {
        zeus_channel_failed(client, "Send", strerror(errno));
    }
}

/**
 * Implement it to allow closing of channel
 */
void zeus_close_channel(ZeusClient *client)
{
    close(client->transport.opened_socket);
    zeus_channel_closed(client);
}

int zeus_current_time_seconds()
{
    return (int) time(NULL);
}




void zeus_on_step(ZeusClient *client)
{
    if (client->connection.state == ZEUS_CONNECTING)
    {
        struct timeval timeout = { 0, 0 };

        fd_set write_fds;
        FD_ZERO(&write_fds);
        FD_SET(client->transport.opened_socket, &write_fds);

        select(client->transport.opened_socket + 1, NULL, &write_fds, NULL, &timeout);

        if (FD_ISSET(client->transport.opened_socket, &write_fds))
        {
            int error = 1;
            unsigned int error_length = sizeof error;
            getsockopt(client->transport.opened_socket, SOL_SOCKET, SO_ERROR, &error, &error_length);

            if (error == 0)
            {
                zeus_channel_ready(client);
            }
            else
            {
                zeus_channel_failed(client, "Connecting", strerror(error));
            }
        }
    }
    if (client->connection.state == ZEUS_CONNECTED)
    {
        int read = (int) recv(client->transport.opened_socket, buffer, ZEUS_BUFFER_SIZE, 0);
        if (read < 0)
        {
            if (errno != EAGAIN)
            {
                zeus_channel_failed(client, "Read", strerror(errno));
            }
        }
        else
        {
            zeus_data_received(client, buffer, read);
        }
    }
}

void zeus_log_error(const char* type, const char* msg)
{
    fprintf(stderr, "%s: %s\n", type, msg);
}

void zeus_log_debug(const char* type, const char* msg)
{
    fprintf(stdout ,"%s: %s\n", type, msg);
}