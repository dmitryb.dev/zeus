#include <stdbool.h>
#include <zeus_dsl.h>
#include <zeus_api.h>

Role role(int id, char* name, char* pass)
{
    return zeus_role(id, name, pass);
}

DeviceId device_id(int manufacturer_id, int device_number, char* secret)
{
    DeviceId device_id = { .manufacturer_id = manufacturer_id, .device_number = device_number, .secret = secret };
    return device_id;
}

void run_zeus(ZeusClient *client)
{
    zeus_run(client);
}

void run_zeus_blocking(ZeusClient *client)
{
    while (true)
    {
        zeus_run(client);
    }
}

ZeusClient zeus_client(DeviceId device_id, Tags tags, Roles roles, void (*on_command)(ZCommand))
{
    ZDeviceId z_device_id = {
            .manufacturer_id = device_id.manufacturer_id,
            .device_number = device_id.device_number,
            .secret = device_id.secret
    };

    tags.as_array[0] = tags.tag0;
    tags.as_array[1] = tags.tag1;
    tags.as_array[2] = tags.tag2;
    tags.as_array[3] = tags.tag3;
    tags.as_array[4] = tags.tag4;
    tags.as_array[5] = tags.tag5;
    tags.as_array[6] = tags.tag6;
    tags.as_array[7] = tags.tag7;
    tags.as_array[8] = Z_END_OF_TAGS;

    roles.as_array[0] = roles.role0;
    roles.as_array[1] = roles.role1;
    roles.as_array[2] = roles.role2;
    roles.as_array[3] = roles.role3;
    roles.as_array[4] = roles.role4;
    roles.as_array[5] = roles.role5;
    roles.as_array[6] = roles.role6;
    roles.as_array[7] = roles.role7;
    roles.as_array[8] = Z_END_OF_ROLES;

    ZConfig configuration = {
            .deviceId = z_device_id,
            .tags = (const char **) tags.as_array,
            .roles = (const ZRole *) roles.as_array,
            .on_command = on_command
    };

    return zeus_create_client(configuration);
}