#ifndef CORE_DSL_H
#define CORE_DSL_H

#include "zeus_api.h"

typedef ZDeviceId DeviceId;

DeviceId device_id(int manufacturer_id, int device_number, char* secret);


typedef ZCommand Command;


typedef struct {
    char* tag0;
    char* tag1;
    char* tag2;
    char* tag3;
    char* tag4;
    char* tag5;
    char* tag6;
    char* tag7;
    char* as_array[9];
} Tags;

Tags NO_TAGS;


typedef ZRole Role;

typedef struct{
    Role role0;
    Role role1;
    Role role2;
    Role role3;
    Role role4;
    Role role5;
    Role role6;
    Role role7;
    Role as_array[9];
} Roles;

Role role(int id, char* name, char* pass);

typedef const int RoleId;
#define ROLE_ID(id) Z_ROLE_ID(id)
#define ANY_ROLE Z_ROLE_ANY

ZeusClient zeus_client(DeviceId device_id, Tags tags, Roles roles, void (*on_command)(ZCommand));

void run_zeus(ZeusClient *client);
void run_zeus_blocking(ZeusClient *client);


#endif //CORE_DSL_H
