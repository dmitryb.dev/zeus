cmake_minimum_required(VERSION 3.13)
project(zeus_api C)

include_directories(generated_bundle/api/include)

add_library(zeus_api generated_bundle/api/src/zeus.c)
