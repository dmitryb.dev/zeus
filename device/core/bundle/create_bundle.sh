#!/usr/bin/env bash

BUNDLE_DIR=${1-generated_bundle}

echo Bundle dir: ${BUNDLE_DIR}

rm -rf ${BUNDLE_DIR}

echo "Creating sources..."

kotlinc -script create-bundle.kts -- \
    --src ../src --src ../lib/nanopb --src ../proto/generated \
    --file ../../transport/transport.h:transport.h \
    --prefixAll ../lib/nanopb:zeus_npb \
    --prefix \
            PB_GEN_FIELD_INFO_,PB_GEN_FIELD_INFO_ASSERT_,PB_FIELDINFO_WIDTH_,PB_FIELDINFO_,PB_ATYPE_,PB_HTYPE_, \
            PB_LTYPE_MAP_,PB_DATA_OFFSET_,PB_DATA_SIZE_,PB_SIZE_OFFSET_,PB_ARRAY_SIZE_,PB_SUBMSG_INFO_,PB_ONEOF_NAME_, \
            PB_FIELDINFO_ASSERT_,FIELDINFO_DOES_NOT_FIT_,PB_STATIC_ASSERT,PB_DATA_SIZE_PTR_,PB_SIZE_OFFSET_PTR_, \
            PB_WT_VARINT,PB_WT_64BIT,PB_WT_STRING,PB_WT_32BIT,PB_WITHOUT_64BIT,PB_ONEOF_,PB_DATAOFFSET_,PB_ \
        :zeus_npb \
    --prefix \
            pb_wire_type_t,pb_fields_seen_t \
        :zeus_npb \
    --out ${BUNDLE_DIR}/api/src/zeus.c

echo
echo "Creating headers..."

kotlinc -script create-bundle.kts -- \
    --src ../include \
    --exclude ../include/zeus_config.h \
    --out ${BUNDLE_DIR}/api/include/zeus_api.h

cp ../include/zeus_config.h ${BUNDLE_DIR}/api/include/