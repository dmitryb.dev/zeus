#include <transport.h>
#include "connection.h"
#include "connection_events.h"
#include <zeus_config.h>

const int ZEUS_CONNECTION_RETRY_DELAY_SECONDS = 10;

void zeus_connect(const char* host, int port, ZeusClient *client);

ZeusConnectionState zeus_keep_connection(ZeusClient *client)
{
    if (client->connection.state == ZEUS_NOT_CONNECTED
            && client->connection.next_connection_attempt_seconds <= zeus_current_time_seconds())
    {
        zeus_connect(Z_SERVER_HOST, Z_SERVER_PORT, client);
    }

    return client->connection.state;
}

void zeus_connect(const char *host, const int port, ZeusClient *client)
{
    client->connection.state = ZEUS_CONNECTING;
    if (client->connection.option == ZEUS_USE_SSL)
    {
        zeus_open_secure_channel(client, host, port);
    }
    else
    {
        zeus_open_insecure_channel(client, host, port);
    }
}


void zeus_connection_ready(ZeusClient *client)
{
    client->connection.state = ZEUS_CONNECTED;
    zeus_log_debug ("Connection", "connected to server");
}

void zeus_connection_failed(ZeusClient *client, const char* error_type, const char* error_msg)
{
    client->connection.next_connection_attempt_seconds =
            zeus_current_time_seconds() + ZEUS_CONNECTION_RETRY_DELAY_SECONDS;
    zeus_log_error(error_type, error_msg);
    zeus_close_channel(client);
}

void zeus_connection_closed(ZeusClient *client)
{
    zeus_log_debug ("Connection", "connection closed");
    client->connection.state = ZEUS_NOT_CONNECTED;
    zeus_on_connection_lost(client);
}
