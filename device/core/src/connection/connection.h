#ifndef CORE_CONNECTION_H
#define CORE_CONNECTION_H

#include <transport.h>

ZeusConnectionState zeus_keep_connection(ZeusClient *client);

void zeus_connection_ready(ZeusClient *client);
void zeus_connection_failed(ZeusClient *client, const char* error_type, const char* error_msg);
void zeus_connection_closed(ZeusClient *client);

#endif //CORE_CONNECTION_H
