#include <zeus_api.h>
#include "connection/connection.h"
#include "outbound/auth/auth.h"
#include "inbound/buffer/receive_buffer.h"
#include "outbound/ping/ping.h"

void zeus_run(ZeusClient *client)
{
    if (zeus_keep_connection(client) == ZEUS_CONNECTED)
    {
        if (zeus_keep_authenticated(client) == ZEUS_AUTHENTICATED)
        {
        }
        zeus_handle_if_message_received(client);
        zeus_check_ping(client);
    }
    zeus_on_step(client);
}

void zeus_on_connection_lost(ZeusClient *client)
{
    zeus_reset_authentication(client);
    zeus_buffer_clear(client);
    zeus_reset_ping(client);
}



void zeus_channel_ready(ZeusClient *client)
{
    zeus_connection_ready(client);
}

void zeus_channel_failed(ZeusClient *client, const char* error_type, const char* error_msg)
{
    zeus_connection_failed(client, error_type, error_msg);
}

void zeus_channel_closed(ZeusClient *client)
{
    zeus_connection_closed(client);
}