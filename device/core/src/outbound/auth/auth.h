#ifndef CORE_AUTH_H
#define CORE_AUTH_H

#include <zeus_api.h>

ZeusAuthState zeus_keep_authenticated(ZeusClient *client);
void zeus_reset_authentication(ZeusClient *client);

#endif //CORE_AUTH_H
