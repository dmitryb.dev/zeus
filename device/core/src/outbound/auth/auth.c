#include <zeus_api.h>
#include <transport.h>
#include <device/messaging/device/v1/inbound.pb.h>
#include "../send.h"

const int ZEUS_AUTH_TIMEOUT_SECONDS = 90;

ZeusAuthState zeus_keep_authenticated(ZeusClient *client)
{
    switch (client->authentication.state)
    {
        case ZEUS_NOT_AUTHENTICATED:
            client->authentication.auth_deadline_seconds = zeus_current_time_seconds() + ZEUS_AUTH_TIMEOUT_SECONDS;
            client->authentication.state = ZEUS_WAITING_FOR_AUTHENTICATION;
            zeus_send_auth(client);
            break;
        case ZEUS_WAITING_FOR_AUTHENTICATION:
            if (client->authentication.auth_deadline_seconds <= zeus_current_time_seconds())
            {
                zeus_channel_failed(client, "Authentication", "timeout");
            }
            break;
        case ZEUS_AUTHENTICATED:break;
    }
    return client->authentication.state;
}

void zeus_reset_authentication(ZeusClient *client)
{
    client->authentication.state = ZEUS_NOT_AUTHENTICATED;
}

void zeus_on_authorized(ZeusClient *client, device_messaging_device_v1_AuthResult *msg)
{
    if (msg->is_authorized)
    {
        zeus_log_debug ("Authorization", "device authorized");
        client->authentication.state = ZEUS_AUTHENTICATED;
//        zeus_send_subscribe(client);
    }
    else
    {
        zeus_channel_failed(client, "Authentication", "failed");
    }
}
