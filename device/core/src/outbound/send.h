#ifndef CORE_SENDER_H
#define CORE_SENDER_H

#include "zeus_api.h"

void zeus_send_auth(ZeusClient *client);
void zeus_send_pong(ZeusClient *client);
void zeus_send_subscribe(ZeusClient *client);
void zeus_send_event_internal(ZeusClient *client, ZEvent *event);

#endif //CORE_SENDER_H
