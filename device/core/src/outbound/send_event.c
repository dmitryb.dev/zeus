#include <device/messaging/device/v1/outbound.pb.h>
#include <device/messaging/device/messages.pb.h>
#include <transport.h>
#include <pb_encode.h>
#include <device/messaging/device/v1/common.pb.h>

bool zeus_send_proto(ZeusClient *client, const device_messaging_device_DeviceOutboundMessage*);

bool zeus_encode_event_type(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);
bool zeus_encode_event_value(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

void zeus_send_event_internal(ZeusClient *client, ZEvent *event)
{
    device_messaging_device_DeviceOutboundMessage msg = {};
    msg.which_version = device_messaging_device_DeviceOutboundMessage_v1_tag;

    msg.version.v1.which_type = device_messaging_device_v1_OutboundV1_event_tag;

    #ifdef Z_FORCE_INT_32
        msg.version.v1.type.event.meta.command.command_id.which_value = device_messaging_device_v1_CommandId_pair_of_int32_tag;
        msg.version.v1.type.event.meta.command.command_id.value.pair_of_int32.high = event->meta.command.id.high;
        msg.version.v1.type.event.meta.command.command_id.value.pair_of_int32.low = event->meta.command.id.low;
    #else
        msg.version.v1.type.event.meta.command.command_id.which_value = device_messaging_device_v1_CommandId_int64_value_tag;
        msg.version.v1.type.event.meta.command.command_id.value.int64_value = event->meta.command.id;
    #endif

    msg.version.v1.type.event.meta.event_id = event->meta.event_id;
    msg.version.v1.type.event.meta.command.ack = event->meta.command.ack;
    msg.version.v1.type.event.meta.roles_mask = event->meta.roles;
    msg.version.v1.type.event.meta.history_size = event->meta.history_size;
    msg.version.v1.type.event.meta.subscribe = event->meta.subscribe;

    msg.version.v1.type.event.type.funcs.encode = zeus_encode_event_type;
    msg.version.v1.type.event.type.arg = event->type;

    msg.version.v1.type.event.value.funcs.encode = zeus_encode_event_value;
    msg.version.v1.type.event.value.arg = event;

    if (zeus_send_proto(client, &msg))
    {
        zeus_log_debug("Event", "event sent");
    }
}

bool zeus_encode_event_type(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    int* types = (int*) *arg;

    if (!types)
    {
        return true;
    }

    int I_MAX = 2048;
    int i = 0;
    for (; i < I_MAX; i++)
    {
        if (types[i] == Z_END_OF_TYPES)
        {
            break;
        }

        if (!pb_encode_tag_for_field(stream, field) || !pb_encode_varint(stream, types[i]))
        {
            return false;
        }
    }

    if (i == I_MAX)
    {
        zeus_log_error("Send", "Z_END_OF_TYPES is not found, see documentation for correct usage");
        return false;
    }

    return true;
}

bool zeus_encode_event_value(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    ZEvent* event = (ZEvent*) *arg;

    if (!event || !event->value)
    {
        return true;
    }

    int I_MAX = 2048;
    int i = 0;
    for (; i < I_MAX; i++)
    {
        if (event->value[i].value_type == Z_END_OF_VALUES)
        {
            break;
        }

        if (!pb_encode_tag_for_field(stream, field))
        {
            return false;
        }

        device_messaging_device_v1_Value value_msg = {};

        switch (event->value[i].value_type)
        {
            case Z_INT:
                value_msg.which_type = device_messaging_device_v1_Value_int32_tag;
                value_msg.type.int32 = event->value[i].value.int_value;
                break;

            case Z_FLOAT:
                value_msg.which_type = device_messaging_device_v1_Value_float32_tag;
                value_msg.type.float32 = event->value[i].value.float_value;
                break;

            case Z_END_OF_VALUES:break;
        }

        if (!pb_encode_submessage(stream, device_messaging_device_v1_Value_fields, &value_msg))
        {
            return false;
        }
    }

    if (i == I_MAX)
    {
        zeus_log_error("Send", "Z_END_OF_VALUES is not found, see documentation for correct usage");
        return false;
    }


    return true;
}