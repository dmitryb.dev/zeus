#include <zeus_api.h>
#include <transport.h>
#include <device/messaging/device/v1/inbound.pb.h>
#include "../send.h"

const int ZEUS_PING_TIMEOUT_SECONDS = 150;

void zeus_check_ping(ZeusClient *client)
{
    if (client->ping_pong.ping_deadline_seconds == 0)
    {
        client->ping_pong.ping_deadline_seconds = zeus_current_time_seconds() + ZEUS_PING_TIMEOUT_SECONDS;
    }
    else if (zeus_current_time_seconds() > client->ping_pong.ping_deadline_seconds)
    {
        zeus_channel_failed(client, "Ping", "server hasn't send ping for a long time");
    }
}

void zeus_on_ping(ZeusClient *client, device_messaging_device_v1_Ping *msg)
{
    zeus_log_debug ("Ping", "ping received");
    client->ping_pong.ping_deadline_seconds = zeus_current_time_seconds() + ZEUS_PING_TIMEOUT_SECONDS;
    zeus_send_pong(client);
}

void zeus_reset_ping(ZeusClient *client)
{
    client->ping_pong.ping_deadline_seconds = 0;
}