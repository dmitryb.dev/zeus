#ifndef CORE_PING_H
#define CORE_PING_H

#include <zeus_api.h>

void zeus_check_ping(ZeusClient *client);
void zeus_reset_ping(ZeusClient *client);

#endif //CORE_PING_H
