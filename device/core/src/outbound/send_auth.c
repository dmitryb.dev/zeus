#include <zeus_api.h>
#include <pb_encode.h>
#include <device/messaging/device/messages.pb.h>
#include <device/messaging/device/v1/outbound.pb.h>
#include <transport.h>
#include "../util/nanpb_util.h"

bool zeus_send_proto(ZeusClient *client, const device_messaging_device_DeviceOutboundMessage*);

bool zeus_encode_tags(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);
bool zeus_encode_role(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

void zeus_send_auth(ZeusClient *client)
{
    ZConfig configuration = client->configuration;

    device_messaging_device_DeviceOutboundMessage msg = {};
    msg.which_version = device_messaging_device_DeviceOutboundMessage_v1_tag;

    msg.version.v1.which_type = device_messaging_device_v1_OutboundV1_auth_tag;

    device_messaging_device_v1_Auth_DeviceId device_id = {};
    device_id.manufacturer_id = configuration.deviceId.manufacturer_id;
    device_id.device_number = configuration.deviceId.device_number;
    device_id.tags.funcs.encode = zeus_encode_tags;
    device_id.tags.arg = configuration.tags;
    msg.version.v1.type.auth.device_id = device_id;

    msg.version.v1.type.auth.roles.funcs.encode = zeus_encode_role;
    msg.version.v1.type.auth.roles.arg = (void *) configuration.roles;

    strcpy(msg.version.v1.type.auth.secret, configuration.deviceId.secret);
    msg.version.v1.type.auth.subscribe = true;

    #ifdef Z_FORCE_INT_32
        msg.version.v1.type.auth.force_int32 = true;
    #else
        msg.version.v1.type.auth.force_int32 = false;
    #endif

    if (zeus_send_proto(client, &msg))
    {
        zeus_log_debug("Authorization", "authorization message sent");
    }
}

bool zeus_encode_role(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    ZRole* roles = (ZRole*) *arg;

    if (!roles)
    {
        return true;
    }

    int I_MAX = 2048;
    int i = 0;
    for (; i < I_MAX; i++)
    {
        if (roles[i].name == Z_END_OF_ROLES.name)
        {
            break;
        }

        if (!pb_encode_tag_for_field(stream, field))
        {
            return false;
        }

        device_messaging_device_v1_Auth_RoleDescription proto = {};
        proto.role_mask = roles[i].mask;
        proto.role_name.funcs.encode = zeus_encode_string;
        proto.role_name.arg = (void *) roles[i].name;

        strcpy(proto.role_pass, roles[i].pass);

        if (!pb_encode_submessage(stream, device_messaging_device_v1_Auth_RoleDescription_fields, &proto))
        {
            return false;
        }
    }

    if (i == I_MAX)
    {
        zeus_log_error("Send", "Z_END_OF_ROLES is not found, see documentation for correct usage");
        return false;
    }

    return true;
}

bool zeus_encode_tags(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    char** tags = (char **) (char*) *arg;

    if (!tags)
    {
        return true;
    }

    int I_MAX = 2048;
    int i = 0;
    for (; i < I_MAX; i++)
    {
        if (tags[i] == Z_END_OF_TAGS)
        {
            break;
        }
        if (!zeus_encode_string(stream, field, (void *const *) &tags[i]))
        {
            return false;
        }
    }

    if (i == I_MAX)
    {
        zeus_log_error("Send", "Z_END_OF_TAGS is not found, see documentation for correct usage");
        return false;
    }

    return true;
}