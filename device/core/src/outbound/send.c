#include <device/messaging/device/v1/outbound.pb.h>
#include <device/messaging/device/messages.pb.h>
#include <transport.h>
#include <pb_encode.h>
#include "../util/size_util.h"


const int ZEUS_SEND_BUFFER_SIZE = 256;

bool zeus_send_proto(ZeusClient *client, const device_messaging_device_DeviceOutboundMessage*);

void zeus_send_pong(ZeusClient *client)
{
    device_messaging_device_DeviceOutboundMessage msg = {};
    msg.which_version = device_messaging_device_DeviceOutboundMessage_v1_tag;

    msg.version.v1.which_type = device_messaging_device_v1_OutboundV1_pong_tag;
    msg.version.v1.type.pong.dummy_field = 0;

    if (zeus_send_proto(client, &msg))
    {
        zeus_log_debug("Ping", "pong sent");
    }
}

void zeus_send_subscribe(ZeusClient *client)
{
    device_messaging_device_DeviceOutboundMessage msg = {};
    msg.which_version = device_messaging_device_DeviceOutboundMessage_v1_tag;

    msg.version.v1.which_type = device_messaging_device_v1_OutboundV1_subscribe_tag;

    if (zeus_send_proto(client, &msg))
    {
        zeus_log_debug("Command", "subscribe sent");
    }
}

bool zeus_send_proto(ZeusClient *client, const device_messaging_device_DeviceOutboundMessage* msg)
{
    static unsigned char zeus_send_buffer[ZEUS_SEND_BUFFER_SIZE];

    pb_ostream_t stream = pb_ostream_from_buffer(
            zeus_send_buffer + ZEUS_LENGTH_PREFIX_SIZE,
            ZEUS_SEND_BUFFER_SIZE - ZEUS_LENGTH_PREFIX_SIZE);

    if (!pb_encode(&stream, device_messaging_device_DeviceOutboundMessage_fields, msg))
    {
        zeus_channel_failed(client, "NanoPB", stream.errmsg);
        return false;
    }

    zeus_write_16_bit_size((zeus_length_t) stream.bytes_written, zeus_send_buffer);

    zeus_send_data(client, (char*) &zeus_send_buffer, (unsigned int) (stream.bytes_written + 2));

    return true;
}