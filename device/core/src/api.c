#include <zeus_api.h>
#include "outbound/send.h"

char* Z_END_OF_TAGS;
ZRole Z_END_OF_ROLES;

ZRole zeus_role(int mask, char *name, char *pass)
{
    ZRole role = { .mask = mask, .name = name, .pass = pass };
    return role;
}

ZeusClient zeus_create_client(ZConfig configuration)
{
    ZeusClient client = { .configuration = configuration };
    return client;
}

ZEventStatus zeus_send_event(ZeusClient *client, ZEvent *event)
{
    if (client->connection.state != ZEUS_CONNECTED || client->authentication.state != ZEUS_AUTHENTICATED)
    {
        return Z_NOT_CONNECTED;
    }

    zeus_send_event_internal(client, event);

    return Z_EVENT_SENT;
}

ZEventValue zeus_int_value(int value)
{
    ZEventValue value_description = {
            .value_type = Z_INT,
            .value.int_value = value
    };
    return value_description;
}

ZEventValue zeus_float_value(float value)
{
    ZEventValue value_description = {
            .value_type = Z_FLOAT,
            .value.float_value = value
    };
    return value_description;
}