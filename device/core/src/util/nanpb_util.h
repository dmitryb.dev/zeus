#ifndef CORE_NANPB_UTIL_H
#define CORE_NANPB_UTIL_H

#include <stdbool.h>
#include <pb.h>

bool zeus_encode_string(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

#endif //CORE_NANPB_UTIL_H
