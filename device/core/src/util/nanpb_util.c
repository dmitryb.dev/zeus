#include "nanpb_util.h"
#include <pb_encode.h>

bool zeus_encode_string(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    char *value = *arg;
    if (!pb_encode_tag_for_field(stream, field))
    {
        return false;
    }

    return pb_encode_string(stream, (uint8_t*) value, strlen(value));
}
