#ifndef CORE_SIZE_UTIL_H
#define CORE_SIZE_UTIL_H

typedef short zeus_length_t;
#define ZEUS_LENGTH_PREFIX_SIZE 2

zeus_length_t zeus_read_16bit_size(const char *data);

void zeus_write_16_bit_size(zeus_length_t size, unsigned char *data);

#endif //CORE_SIZE_UTIL_H
