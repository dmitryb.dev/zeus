#include "size_util.h"

const int ZEUS_SHIFT_SIZE = sizeof(zeus_length_t) - ZEUS_LENGTH_PREFIX_SIZE;

zeus_length_t zeus_parse_big_endian(zeus_length_t);


zeus_length_t zeus_read_16bit_size(const char *data)
{
    zeus_length_t size = *((zeus_length_t *) data) >> ZEUS_SHIFT_SIZE;
    return zeus_parse_big_endian(size);
}

void zeus_write_16_bit_size(zeus_length_t size, unsigned char *data)
{
    zeus_length_t size_big_endian = zeus_parse_big_endian(size);

    unsigned char* size_data = (unsigned char *) &size_big_endian;

    for (int i_data = 0, i_size = ZEUS_SHIFT_SIZE; i_data < 2; i_data++, i_size++)
    {
        data[i_data] = size_data[i_size];
    }
}

zeus_length_t zeus_parse_big_endian(zeus_length_t value)
{
    #ifndef ZEUS_BYTE_ORDER_LITTLE_ENDIAN
        char* value_bytes = (char *) &value;
        for (int i = 0; i < sizeof(value) / 2; i++)
        {
            char tmp = value_bytes[i];
            value_bytes[i] = value_bytes[sizeof(value) - i - 1];
            value_bytes[sizeof(value) - i - 1] = tmp;
        }
        return *((zeus_length_t *) value_bytes);
    #else
        return value;
    #endif
}

