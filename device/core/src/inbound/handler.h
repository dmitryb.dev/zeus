#ifndef CORE_HANDLER_H
#define CORE_HANDLER_H

#include <zeus_api.h>

void zeus_handle_msg(ZeusClient *client, device_messaging_device_DeviceInboundMessage *msg);

#endif //CORE_HANDLER_H
