#ifndef CORE_HANDLER_EVENTS_H
#define CORE_HANDLER_EVENTS_H

#include <device/messaging/device/v1/inbound.pb.h>
#include <zeus_api.h>

void zeus_on_authorized(ZeusClient *client, device_messaging_device_v1_AuthResult *msg);
void zeus_on_ping(ZeusClient *client, device_messaging_device_v1_Ping *msg);
void zeus_on_command(ZeusClient *client, device_messaging_device_v1_Command *msg);

#endif //CORE_HANDLER_EVENTS_H
