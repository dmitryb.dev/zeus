#ifndef CORE_RECEIVE_BUFFER_H
#define CORE_RECEIVE_BUFFER_H


void zeus_buffer_clear(ZeusClient *client);

void zeus_handle_if_message_received(ZeusClient *client);

#endif //CORE_RECEIVE_BUFFER_H
