#include <transport.h>
#include <pb.h>
#include <pb_decode.h>
#include <device/messaging/device/messages.pb.h>
#include <device/messaging/device/v1/inbound.pb.h>
#include "receive_buffer.h"
#include "../../util/size_util.h"
#include "../handler.h"

void zeus_data_received(ZeusClient *client, char* data, int size)
{
    ZeusBuffer *buffer = &client->inbound_buffer;
    switch (buffer->state)
    {
        case ZEUS_WAITING_FOR_NEW_MSG:
            if (size == 0) return;

            if (size < ZEUS_LENGTH_PREFIX_SIZE)
            {
                zeus_channel_failed(client, "Receive buffer",
                        "can't read message size, received data is too small to contain message length prefix");
                return;
            }

            buffer->incoming_msg_size = buffer->bytes_to_read = zeus_read_16bit_size(data);
            if (buffer->bytes_to_read <= Z_RECEIVE_BUFFER_SIZE_BYTES)
            {
                buffer->data_end_pointer = 0;
                buffer->state = ZEUS_WAITING_FOR_MSG_DATA;
            }
            else
            {
                zeus_channel_failed(client, "Receive buffer", "received message length has exceeded buffer size");
                return;
            }
            zeus_data_received(client, &data[ZEUS_LENGTH_PREFIX_SIZE], size - ZEUS_LENGTH_PREFIX_SIZE);
            break;

        case ZEUS_WAITING_FOR_MSG_DATA:
        {
            int bytes_to_copy = size < buffer->bytes_to_read ? size : buffer->bytes_to_read;
            for (int i_data = 0; i_data < bytes_to_copy; buffer->data_end_pointer++, i_data++)
            {
                buffer->data[buffer->data_end_pointer] = data[i_data];
            }

            buffer->bytes_to_read -= size;
            size -= bytes_to_copy;

            if (buffer->bytes_to_read == 0)
            {
                buffer->contains_unhandled_msg = true;
                buffer->state = ZEUS_WAITING_FOR_MSG_HANDLED;
            }

            if (size > 0)
            {
                zeus_data_received(client, &data[bytes_to_copy], size);
            }
            break;
        }

        case ZEUS_WAITING_FOR_MSG_HANDLED:
            if (buffer->contains_unhandled_msg)
            {
                if (size < ZEUS_LENGTH_PREFIX_SIZE)
                {
                    zeus_channel_failed(client, "Receive buffer",
                            "can't read message size, received data is too small to contain message length prefix");
                    return;
                }
                buffer->bytes_to_skip = zeus_read_16bit_size(data);
                buffer->state = ZEUS_MSG_SKIPPING;
                zeus_data_received(client, &data[ZEUS_LENGTH_PREFIX_SIZE], size - ZEUS_LENGTH_PREFIX_SIZE);
            }
            else
            {
                buffer->state = ZEUS_WAITING_FOR_NEW_MSG;
                zeus_data_received(client, data, size);
            }
            break;

        case ZEUS_MSG_SKIPPING:
            if (buffer->bytes_to_skip > size)
            {
                buffer->bytes_to_skip -= size;
            }
            else
            {
                buffer->state = ZEUS_WAITING_FOR_MSG_HANDLED;
                if (buffer->bytes_to_skip < size)
                {
                    zeus_data_received(client, &data[buffer->bytes_to_skip], size - buffer->bytes_to_skip);
                }
            }
            break;
    }
}

void zeus_buffer_clear(ZeusClient *client)
{
    ZeusBuffer *buffer = &client->inbound_buffer;
    buffer->state = ZEUS_WAITING_FOR_NEW_MSG;
    buffer->data_end_pointer = 0;
    buffer->contains_unhandled_msg = false;
}

void zeus_handle_if_message_received(ZeusClient *client)
{
    ZeusBuffer *buffer = &client->inbound_buffer;
    if (buffer->contains_unhandled_msg)
    {
        device_messaging_device_DeviceInboundMessage received_msg = {};

        pb_istream_t stream = pb_istream_from_buffer((const pb_byte_t *) buffer->data, (size_t) buffer->incoming_msg_size);

        bool is_decoded = pb_decode(&stream, device_messaging_device_DeviceInboundMessage_fields, &received_msg);
        buffer->contains_unhandled_msg = false;

        if (is_decoded)
        {
            zeus_handle_msg(client, &received_msg);
        }
        else
        {
            zeus_channel_failed(client, "NanoPB", stream.errmsg);
        }
    }
}