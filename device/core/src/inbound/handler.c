#include <device/messaging/device/v1/inbound.pb.h>
#include <device/messaging/device/messages.pb.h>
#include <transport.h>
#include <zeus_api.h>
#include <device/messaging/device/v1/common.pb.h>
#include "handler.h"
#include "handler_events.h"


void zeus_handle_msg(ZeusClient *client, device_messaging_device_DeviceInboundMessage *msg)
{
    switch (msg->version.v1.which_type)
    {
        case device_messaging_device_v1_InboundV1_auth_result_tag:
            zeus_on_authorized(client, &msg->version.v1.type.auth_result);
            break;

        case device_messaging_device_v1_InboundV1_ping_tag:
            zeus_on_ping(client, &msg->version.v1.type.ping);
            break;

        case device_messaging_device_v1_InboundV1_command_tag:
            zeus_on_command(client, &msg->version.v1.type.command);
            break;

        default: return;
    }
}

void zeus_on_command(ZeusClient *client, device_messaging_device_v1_Command *msg)
{
    zeus_log_debug("Command", "command received");

    if (!client->configuration.on_command) {
        zeus_log_debug("Command", "command lost, no on_command handler set");
        return;
    }

    ZCommand command = {
            .meta.client = client,
            .meta.role = msg->meta.user_role,
    };

    #ifdef Z_FORCE_INT_32
        command.meta.command_id.high = msg->meta.command_id.value.pair_of_int32.high;
        command.meta.command_id.low = msg->meta.command_id.value.pair_of_int32.low;
    #else
        command.meta.command_id = msg->meta.command_id.value.int64_value;
    #endif

    for (int i = 0; i < Z_MAX_COMMAND_TYPES; i++)
    {
        if (msg->type[i] != 0)
        {
            command.type[i] = msg->type[i];
            command.type_length = i + 1;
        }
    }

    for (int i = 0; i < Z_MAX_COMMAND_VALUES; i++)
    {
        switch (msg->value[i].which_type)
        {
            case device_messaging_device_v1_Value_int32_tag:
                command.value[i].int_value = msg->value[i].type.int32;
                command.value_type[i] = Z_INT;
                break;

            case device_messaging_device_v1_Value_float32_tag:
                command.value[i].float_value = msg->value[i].type.float32;
                command.value_type[i] = Z_FLOAT;
                break;
        }
        if (msg->value[i].which_type != 0)
        {
            command.value_length = i + 1;
        }
    }

    client->configuration.on_command(command);
}