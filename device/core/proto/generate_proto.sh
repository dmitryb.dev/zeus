#!/usr/bin/env bash

NANOPB_GEN=$1

if [ -z "${NANOPB_GEN}" ]
then
    mkdir .temp
    cd .temp

    git clone https://github.com/nanopb/nanopb
    cd nanopb
    git checkout 3c69a905b16df149e1bda12f25e0522073a24678
    cd generator/proto
    make

    cd ../../../../

    NANOPB_GEN=./.temp/nanopb/generator/protoc-gen-nanopb
fi

BASE_DIR=$(dirname "$0")/../../../backend
DOMAIN_DIR=${BASE_DIR}/lib/domain/src/main/proto
DEVICE_DIR=${BASE_DIR}/device-messaging/src/main/proto

OUTPUT_DIR=generated

generate_proto() {
    PROTO_DIR=$1
    PROTO_FILE=$2

    mkdir -p $(dirname "$0")/${OUTPUT_DIR}/$(dirname "$PROTO_FILE")
    protoc --plugin="${NANOPB_GEN}" \
        -I=${DOMAIN_DIR} -I=${DEVICE_DIR} ${PROTO_DIR}/${PROTO_FILE}.proto \
        --nanopb_out=$(dirname "$0")/${OUTPUT_DIR} \
        -o$(dirname "$0")/${OUTPUT_DIR}/${PROTO_FILE}.pb
}

rm -rf ${OUTPUT_DIR}

generate_proto ${DEVICE_DIR} device/messaging/device/v1/common
generate_proto ${DEVICE_DIR} device/messaging/device/v1/inbound
generate_proto ${DEVICE_DIR} device/messaging/device/v1/outbound
generate_proto ${DEVICE_DIR} device/messaging/device/messages


# Dirty hack to replace fixed size for fields with compile time constant
sed -i '' 's/-1234567001/Z_MAX_COMMAND_TYPES/g' generated/device/messaging/device/v1/inbound.pb.h
sed -i '' 's/-1234567002/Z_MAX_COMMAND_VALUES/g' generated/device/messaging/device/v1/inbound.pb.h
sed -i '' 's/#define device_messaging_device_v1_Command_siz.*//g' generated/device/messaging/device/v1/inbound.pb.h
sed -i '' 's/#include <pb\.h>/#include <pb.h>\
#include <zeus_config.h>/g' generated/device/messaging/device/v1/inbound.pb.h

rm -rf .temp
