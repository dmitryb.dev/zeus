/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.3 at Mon May 13 01:04:51 2019. */

#ifndef PB_DEVICE_MESSAGING_DEVICE_V1_INBOUND_PB_H_INCLUDED
#define PB_DEVICE_MESSAGING_DEVICE_V1_INBOUND_PB_H_INCLUDED
#include <pb.h>
#include <zeus_config.h>

#include "device/messaging/device/v1/common.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _device_messaging_device_v1_Ping {
    char dummy_field;
/* @@protoc_insertion_point(struct:device_messaging_device_v1_Ping) */
} device_messaging_device_v1_Ping;

typedef struct _device_messaging_device_v1_AuthResult {
    bool is_authorized;
/* @@protoc_insertion_point(struct:device_messaging_device_v1_AuthResult) */
} device_messaging_device_v1_AuthResult;

typedef struct _device_messaging_device_v1_Command_Meta {
    device_messaging_device_v1_CommandId command_id;
    int32_t user_role;
/* @@protoc_insertion_point(struct:device_messaging_device_v1_Command_Meta) */
} device_messaging_device_v1_Command_Meta;

typedef struct _device_messaging_device_v1_Command {
    device_messaging_device_v1_Command_Meta meta;
    pb_size_t type_count;
    int32_t type[Z_MAX_COMMAND_TYPES];
    pb_size_t value_count;
    device_messaging_device_v1_Value value[Z_MAX_COMMAND_TYPES];
/* @@protoc_insertion_point(struct:device_messaging_device_v1_Command) */
} device_messaging_device_v1_Command;

typedef struct _device_messaging_device_v1_InboundV1 {
    pb_size_t which_type;
    union {
        device_messaging_device_v1_Ping ping;
        device_messaging_device_v1_AuthResult auth_result;
        device_messaging_device_v1_Command command;
    } type;
/* @@protoc_insertion_point(struct:device_messaging_device_v1_InboundV1) */
} device_messaging_device_v1_InboundV1;

/* Default values for struct fields */

/* Initializer values for message structs */
#define device_messaging_device_v1_InboundV1_init_default {0, {device_messaging_device_v1_Ping_init_default}}
#define device_messaging_device_v1_Ping_init_default {0}
#define device_messaging_device_v1_AuthResult_init_default {0}
#define device_messaging_device_v1_Command_init_default {device_messaging_device_v1_Command_Meta_init_default, 0, {}, 0, {}}
#define device_messaging_device_v1_Command_Meta_init_default {device_messaging_device_v1_CommandId_init_default, 0}
#define device_messaging_device_v1_InboundV1_init_zero {0, {device_messaging_device_v1_Ping_init_zero}}
#define device_messaging_device_v1_Ping_init_zero {0}
#define device_messaging_device_v1_AuthResult_init_zero {0}
#define device_messaging_device_v1_Command_init_zero {device_messaging_device_v1_Command_Meta_init_zero, 0, {}, 0, {}}
#define device_messaging_device_v1_Command_Meta_init_zero {device_messaging_device_v1_CommandId_init_zero, 0}

/* Field tags (for use in manual encoding/decoding) */
#define device_messaging_device_v1_AuthResult_is_authorized_tag 1
#define device_messaging_device_v1_Command_Meta_command_id_tag 1
#define device_messaging_device_v1_Command_Meta_user_role_tag 2
#define device_messaging_device_v1_Command_meta_tag 1
#define device_messaging_device_v1_Command_type_tag 2
#define device_messaging_device_v1_Command_value_tag 3
#define device_messaging_device_v1_InboundV1_ping_tag 1
#define device_messaging_device_v1_InboundV1_auth_result_tag 2
#define device_messaging_device_v1_InboundV1_command_tag 3

/* Struct field encoding specification for nanopb */
extern const pb_field_t device_messaging_device_v1_InboundV1_fields[4];
extern const pb_field_t device_messaging_device_v1_Ping_fields[1];
extern const pb_field_t device_messaging_device_v1_AuthResult_fields[2];
extern const pb_field_t device_messaging_device_v1_Command_fields[4];
extern const pb_field_t device_messaging_device_v1_Command_Meta_fields[3];

/* Maximum encoded size of messages (where known) */
#define device_messaging_device_v1_InboundV1_size 4
#define device_messaging_device_v1_Ping_size     0
#define device_messaging_device_v1_AuthResult_size 2

#define device_messaging_device_v1_Command_Meta_size 32

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define INBOUND_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
