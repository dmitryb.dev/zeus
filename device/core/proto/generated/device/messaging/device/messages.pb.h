/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.3 at Mon May 13 01:04:55 2019. */

#ifndef PB_DEVICE_MESSAGING_DEVICE_MESSAGES_PB_H_INCLUDED
#define PB_DEVICE_MESSAGING_DEVICE_MESSAGES_PB_H_INCLUDED
#include <pb.h>

#include "device/messaging/device/v1/inbound.pb.h"

#include "device/messaging/device/v1/outbound.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _device_messaging_device_DeviceInboundMessage {
    pb_size_t which_version;
    union {
        device_messaging_device_v1_InboundV1 v1;
    } version;
/* @@protoc_insertion_point(struct:device_messaging_device_DeviceInboundMessage) */
} device_messaging_device_DeviceInboundMessage;

typedef struct _device_messaging_device_DeviceOutboundMessage {
    pb_size_t which_version;
    union {
        device_messaging_device_v1_OutboundV1 v1;
    } version;
/* @@protoc_insertion_point(struct:device_messaging_device_DeviceOutboundMessage) */
} device_messaging_device_DeviceOutboundMessage;

/* Default values for struct fields */

/* Initializer values for message structs */
#define device_messaging_device_DeviceInboundMessage_init_default {0, {device_messaging_device_v1_InboundV1_init_default}}
#define device_messaging_device_DeviceOutboundMessage_init_default {0, {device_messaging_device_v1_OutboundV1_init_default}}
#define device_messaging_device_DeviceInboundMessage_init_zero {0, {device_messaging_device_v1_InboundV1_init_zero}}
#define device_messaging_device_DeviceOutboundMessage_init_zero {0, {device_messaging_device_v1_OutboundV1_init_zero}}

/* Field tags (for use in manual encoding/decoding) */
#define device_messaging_device_DeviceInboundMessage_v1_tag 1
#define device_messaging_device_DeviceOutboundMessage_v1_tag 1

/* Struct field encoding specification for nanopb */
extern const pb_field_t device_messaging_device_DeviceInboundMessage_fields[2];
extern const pb_field_t device_messaging_device_DeviceOutboundMessage_fields[2];

/* Maximum encoded size of messages (where known) */
#define device_messaging_device_DeviceInboundMessage_size (5 + sizeof(union{char f0[4]; char f3[device_messaging_device_v1_CommandId_size];}))
#define device_messaging_device_DeviceOutboundMessage_size (5 + device_messaging_device_v1_OutboundV1_size)

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define MESSAGES_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
