#ifndef CORE_API_H
#define CORE_API_H

#include <zeus_config.h>
#include "src/zeus_configuration.h"
#include "src/zeus_communication.h"
#include "src/zeus_client_state.h"

typedef struct ZeusClient ZeusClient;

ZeusClient zeus_create_client(ZConfig configuration);

void zeus_run(ZeusClient *client);

ZEventStatus zeus_send_event(ZeusClient *client, ZEvent *event);

#endif //CORE_API_H
