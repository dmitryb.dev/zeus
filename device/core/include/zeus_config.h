#ifndef CORE_CONFIG_H
#define CORE_CONFIG_H

// For compilers, that doesn't support int64
//#define Z_FORCE_INT_32

//#define Z_SERVER_HOST "ec2-3-16-151-248.us-east-2.compute.amazonaws.com"
#define Z_SERVER_HOST "localhost"
#define Z_SERVER_PORT 7890

#define Z_RECEIVE_BUFFER_SIZE_BYTES 256

#define Z_MAX_COMMAND_TYPES 8
#define Z_MAX_COMMAND_VALUES 8

#endif //CORE_CONFIG_H
