#ifndef CORE_CLIENT_STATE_H
#define CORE_CLIENT_STATE_H

#include <zeus_config.h>
#include "zeus_configuration.h"

#ifndef ZeusTransportState
    #define ZeusTransportState int
#endif

typedef enum {
    ZEUS_NOT_CONNECTED,
    ZEUS_CONNECTING,
    ZEUS_CONNECTED
} ZeusConnectionState;

typedef enum {
    ZEUS_PLAIN_TEXT,
    ZEUS_USE_SSL
} ZeusConnectionOption;

typedef struct {
    const ZeusConnectionOption option;
    volatile ZeusConnectionState state;
    volatile int next_connection_attempt_seconds;
} ZeusConnection;


typedef enum {
    ZEUS_NOT_AUTHENTICATED,
    ZEUS_WAITING_FOR_AUTHENTICATION,
    ZEUS_AUTHENTICATED
} ZeusAuthState;

typedef struct {
    ZeusAuthState state;
    int auth_deadline_seconds;
} ZeusAuthentication;


typedef struct {
    int ping_deadline_seconds;
} ZeusPingPong;

typedef struct {
    volatile enum {
        ZEUS_WAITING_FOR_NEW_MSG,
        ZEUS_WAITING_FOR_MSG_DATA,
        ZEUS_WAITING_FOR_MSG_HANDLED,
        ZEUS_MSG_SKIPPING
    } state;

    char data[Z_RECEIVE_BUFFER_SIZE_BYTES < 64 ? 64 : Z_RECEIVE_BUFFER_SIZE_BYTES];

    volatile int data_end_pointer;

    volatile int incoming_msg_size;
    volatile int bytes_to_read;
    volatile int bytes_to_skip;

    volatile bool contains_unhandled_msg;
} ZeusBuffer;


struct ZeusClient {
    ZConfig configuration;

    ZeusConnection connection;
    ZeusAuthentication authentication;
    ZeusPingPong ping_pong;
    ZeusBuffer inbound_buffer;

    ZeusTransportState transport;
};


ZeusClient zeus_create_client(ZConfig configuration);


void zeus_run(ZeusClient *client);

#endif //CORE_CLIENT_STATE_H
