#ifndef CORE_COMMUNICATION_H
#define CORE_COMMUNICATION_H

#include <zeus_config.h>
#include <stdbool.h>
#include <stdint.h>

struct ZeusClient;
typedef struct ZeusClient ZeusClient;


#define Z_END_OF_TYPES 0


typedef enum {
    Z_END_OF_VALUES = 0,
    Z_INT,
    Z_FLOAT
} ZValueType;
typedef union {
    int int_value;
    float float_value;
} ZValue;


#ifdef Z_FORCE_INT_32
    typedef struct {
        int32_t high;
        uint32_t low;
    } ZCommandId;
#else
    typedef int64_t ZCommandId;
#endif


struct ZCommand {
    struct {
        ZeusClient *client;
        ZCommandId command_id;
        int role;
    } meta;

    int type[Z_MAX_COMMAND_TYPES];
    int type_length;

    ZValueType value_type[Z_MAX_COMMAND_VALUES];
    ZValue value[Z_MAX_COMMAND_VALUES];
    int value_length;
};


typedef struct {
    ZValueType value_type;
    ZValue value;
} ZEventValue;

typedef struct {
    struct {
        int event_id;
        struct {
            ZCommandId id;
            bool ack;
        } command;

        int roles;
        int history_size;
        bool subscribe;
    } meta;

    int *type;
    ZEventValue *value;
} ZEvent;

typedef enum {
    Z_EVENT_SENT = 0,
    Z_NOT_CONNECTED
} ZEventStatus;

ZEventValue zeus_int_value(int value);
ZEventValue zeus_float_value(float value);


#endif //CORE_COMMUNICATION_H
