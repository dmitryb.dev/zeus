#ifndef CORE_CONFIGURATION_H
#define CORE_CONFIGURATION_H

#include <zeus_config.h>

struct ZCommand;
typedef struct ZCommand ZCommand;

typedef struct {
    const int manufacturer_id;
    const int device_number;
    const char* secret;
} ZDeviceId;
extern char* Z_END_OF_TAGS;


typedef const int ZRoleMask;

typedef struct {
    ZRoleMask mask;
    const char* name;
    const char* pass;
} ZRole;
extern ZRole Z_END_OF_ROLES;

#define Z_ROLE_ID(id) (1 << id)
#define Z_ROLE_ANY (~0)

ZRole zeus_role(int mask, char *name, char *pass);


typedef struct {
    const ZDeviceId deviceId;
    const char** tags;
    const ZRole* roles;

    void (*on_command)(ZCommand);
} ZConfig;

#endif //CORE_CONFIGURATION_H
