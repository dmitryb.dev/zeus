package zeus.plugin.grpcjson

import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.node.*
import com.google.protobuf.*
import com.google.protobuf.util.*
import io.github.classgraph.*
import io.grpc.*
import io.grpc.stub.*
import io.grpc.stub.annotations.*
import org.gradle.api.*
import org.gradle.internal.impldep.org.codehaus.plexus.util.*
import spark.*
import java.net.*
import java.time.Duration
import java.util.concurrent.*
import java.util.concurrent.atomic.*
import kotlin.reflect.*
import kotlin.reflect.full.*


class GrpcJsonProxyPlugin : Plugin<Project> {

    open class Extension {
        var grpcHost = "localhost"
        var grpcPort = 8080
        var jsonPort: Int? = null
        var timeoutSeconds = 10
    }

    private val jsonMapper = ObjectMapper()

    override fun apply(project: Project) {
        val extension = project.extensions.create("grpcJsonProxy", Extension::class.java)

        project.task("runGrpcJsonProxy").run {
            group = "grpc-json proxy"
            doLast {
                val port = extension.jsonPort ?: findFreePort(9000..9050)
                setupSpark(port)

                val channel = ManagedChannelBuilder.forAddress(extension.grpcHost, extension.grpcPort)
                        .usePlaintext()
                        .keepAliveTimeout(1, TimeUnit.SECONDS)
                        .build()

                Runtime.getRuntime().addShutdownHook(Thread(Runnable { channel.shutdown(); Spark.stop() }))

                val httpEndpoints = findGrpcEndpoints("${project.buildDir.path}/classes/java/main")
                        .map { createHttpEndpoint(it, channel, extension.timeoutSeconds) }

                val listEndpointsRoute = { _: Request, res: Response ->
                    res.type("application/json")
                    jsonMapper.writeValueAsString(httpEndpoints)
                }
                Spark.get("/", listEndpointsRoute)
                Spark.notFound(listEndpointsRoute)

                printManual(port)
                Thread.sleep(Duration.ofHours(2).toMillis())
            }
        }
    }

    private fun findGrpcEndpoints(classPath: String): List<GrpcEndpointInfo> {
        val classes = ClassGraph()
                .enableAllInfo()
                .overrideClasspath(classPath)
                .scan()
                .getClassesWithMethodAnnotation(RpcMethod::class.qualifiedName)

        return sequence {
            for (classInfo in classes) {
                val methods = classInfo.methodInfo
                        .filter { it.hasAnnotation(RpcMethod::class.qualifiedName) }

                for (methodInfo in methods) {
                    val rpcMethodAnnotation = methodInfo
                            .getAnnotationInfo(RpcMethod::class.qualifiedName)
                            .loadClassAndInstantiate()
                            as RpcMethod

                    val grpcMethodName = rpcMethodAnnotation.fullMethodName.substringAfter('/')

                    yield(GrpcEndpointInfo(
                            rpcMethodAnnotation.fullMethodName,
                            classInfo.loadClass().kotlin,
                            grpcMethodName,
                            ProtobufEntity(
                                    createProtobufBuilder(rpcMethodAnnotation.requestType),
                                    rpcMethodAnnotation.methodType == MethodDescriptor.MethodType.CLIENT_STREAMING
                                            || rpcMethodAnnotation.methodType == MethodDescriptor.MethodType.BIDI_STREAMING
                            ),
                            ProtobufEntity(
                                    createProtobufBuilder(rpcMethodAnnotation.responseType),
                                    rpcMethodAnnotation.methodType == MethodDescriptor.MethodType.SERVER_STREAMING
                                            || rpcMethodAnnotation.methodType == MethodDescriptor.MethodType.BIDI_STREAMING
                            )
                    ))
                }
            }
        }.toList()
    }

    private fun createHttpEndpoint(grpcEndpoint: GrpcEndpointInfo, channel: Channel, timeoutSeconds: Int): String {
        val requestExample = if (grpcEndpoint.request.isStreaming)
            "[ ${getJsonExample(grpcEndpoint.request.builder)} ]"
        else
            getJsonExample(grpcEndpoint.request.builder)

        Spark.get(grpcEndpoint.path) { _, res ->
            res.type("application/json")
            requestExample
        }

        Spark.post(grpcEndpoint.path) { req, res ->
            res.type("application/json")
            callGrpcMethod(grpcEndpoint, channel, req.body(), timeoutSeconds)
        }

        return grpcEndpoint.path
    }

    private fun createProtobufBuilder(type: KClass<*>): Message.Builder = type.staticFunctions
            .find { it.name == "newBuilder" && it.parameters.isEmpty() }!!
            .call()
            as Message.Builder

    @Suppress("UNCHECKED_CAST")
    private fun callGrpcMethod(grpcEndpoint: GrpcEndpointInfo, channel: Channel, request: String, timeoutSeconds: Int): Any {
        val stub = (
                grpcEndpoint.grpcClass.staticFunctions
                        .find { it.name == "newStub" }!!
                        .call(channel)!! as AbstractStub<*>
                ).withDeadline(Deadline.after(timeoutSeconds.toLong(), TimeUnit.SECONDS))


        println("Call: stub: ${stub::class.simpleName}, method: ${grpcEndpoint.method}, channel: ${channel.authority()}\n")

        val responseObserver = BlockingStreamObserver()

        val stubMethod = stub::class.functions
                .find { it.name.equals(grpcEndpoint.method, true) }!!

        if (grpcEndpoint.request.isStreaming) {
            val jsonNode = jsonMapper.readValue(request, JsonNode::class.java)
            val requests = (jsonNode as ArrayNode)
                    .map { parseJsonGrpcRequest(it.toString(), grpcEndpoint.request.builder) }

            val requestObserver: StreamObserver<Message> =
                    stubMethod.call(stub, responseObserver) as StreamObserver<Message>

            requests.forEach { requestObserver.onNext(it) }
            requestObserver.onCompleted()
        } else {
            stubMethod.call(stub, parseJsonGrpcRequest(request, grpcEndpoint.request.builder), responseObserver)
        }

        return responseObserver.getResponses()
                .map { JsonFormat.printer().includingDefaultValueFields().print(it) }
                .let { values -> if (!grpcEndpoint.response.isStreaming && values.size == 1)
                        values[0]
                    else
                        values
                }
    }

    private fun parseJsonGrpcRequest(request: String, messageBuilder: Message.Builder): Message {
        val builder = messageBuilder.clone()
        JsonFormat.parser().merge(request, builder)
        return builder.build()
    }

    private fun findFreePort(portsToTry: IntRange): Int {
        for (port in portsToTry) {
            try {
                ServerSocket(port).close()
                return port
            } catch (_: BindException) {}
        }
        throw IllegalStateException("Ports from ${portsToTry.first} to ${portsToTry.last} are used")
    }

    private fun printManual(port: Int) {
        println()
        println("Grpc-Json proxy is run on $port")
        println()
        println("GET / - list endpoints")
        println("GET /{service}/{method} - endpoint JSON request example")
        println("POST /{service}/{method} - execute endpoint method")
    }

    private fun getJsonExample(builder: Message.Builder): String {
        val exampleBuilder = builder.clone()

        fun initEmptyValues(mb: Message.Builder): Message.Builder {
            mb::class.functions
                    .filter { it.name.startsWith("set") || it.name.startsWith("add") }
                    .filter { it.parameters.size == 2
                            && (it.parameters[1].type.classifier as? KClass<*>)
                            ?.isSubclassOf(Message::class)
                            ?: false
                    }
                    .forEach { setter ->
                        val subMessage = createProtobufBuilder(setter.parameters[1].type.classifier as KClass<*>)
                        setter.call(mb, initEmptyValues(subMessage).build())
                    }
            mb::class.functions
                    .filter { it.name.startsWith("add") }
                    .filter { it.parameters.size == 2 }
                    .forEach { addMethod ->
                        val paramType = addMethod.parameters[1].type.classifier as KClass<*>
                        when {
                            paramType.isSubclassOf(Number::class) && addMethod.returnType.classifier == mb::class ->
                                addMethod.call(mb, 0)

                            paramType.isSubclassOf(String::class) -> addMethod.call(mb, "")
                    }}
            return mb
        }

        initEmptyValues(exampleBuilder)

        return JsonFormat.printer()
                .includingDefaultValueFields()
                .print(exampleBuilder.build())
    }

    private fun setupSpark(port: Int) {
        Spark.port(port)
        Spark.exception(Exception::class.java) { ex, _, res ->
            res.type("text/plain")

            when(ex) {
                IllegalArgumentException::class -> res.status(400)
                else -> res.status(500)
            }

            res.body(ExceptionUtils.getStackTrace(ex))
        }
    }

    private class GrpcEndpointInfo(
            val path: String,
            val grpcClass: KClass<*>,
            val method: String,
            val request: ProtobufEntity,
            val response: ProtobufEntity
    )

    private class ProtobufEntity(val builder: Message.Builder, val isStreaming: Boolean)

    private class BlockingStreamObserver : StreamObserver<Message> {
        private val responses = ConcurrentLinkedQueue<Message>()
        private var exception = AtomicReference<Throwable>()
        private var isCompleted = AtomicBoolean()

        override fun onNext(value: Message?) {
            responses.add(value)
        }

        override fun onError(t: Throwable?) {
            exception.set(t)
        }

        override fun onCompleted() {
            isCompleted.set(true)
        }

        fun getResponses(): Collection<Message> {
            while (true) {
                if (exception.get() != null) {
                    throw exception.get()
                }
                if (isCompleted.get()) {
                    return responses
                }
            }
        }

    }

}
