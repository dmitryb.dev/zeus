package zeus.common.domain.device

import common.domain.device.*


fun DeviceIds.DeviceId.toPrettyString() = "${this.manufacturerId}:${this.deviceNumber}"

fun Filters.SingleDeviceFilter.toPrettyString() = "${this.manufacturerId}:${this.deviceNumber}:[" +
        this.tagsList.joinToString(",") + "]"

fun Filters.DeviceFilter.toPrettyString() = (
        "${this.manufacturerId}:"
                + (if (this.hasDeviceNumber()) { "${this.deviceNumber.number}:" } else "")
                + "[${this.tagsList.joinToString(",")}]"
        )