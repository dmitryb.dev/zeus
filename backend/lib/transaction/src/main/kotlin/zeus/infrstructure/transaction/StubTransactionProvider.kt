package zeus.infrstructure.transaction

import javax.inject.*

@Singleton
class StubTransactionProvider @Inject constructor() : TransactionProvider {
    override suspend fun <T> transactional(action: suspend () -> T): T = action()
}