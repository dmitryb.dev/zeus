package zeus.infrstructure.transaction

interface TransactionProvider {
    suspend fun <T> transactional(action: suspend () -> T): T
}