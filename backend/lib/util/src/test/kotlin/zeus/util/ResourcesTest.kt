package zeus.util

import zeus.util.resources.Resources
import org.junit.Test

import org.junit.Assert.assertArrayEquals

class ResourcesTest {

    @Test
    fun readFolder() {
        val files = Resources.getAll("resources-test")

        assertArrayEquals(
                arrayOf("1\n2\n3", "a\nb", "q\nw\ne"),
                files.toTypedArray()
        )
    }

}