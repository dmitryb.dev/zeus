package zeus.util.mysql.mapper

import com.mysql.cj.xdevapi.Row
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import zeus.util.mysql.mapper.annotation.Column
import zeus.util.mysql.mapper.annotation.EntityCreator
import java.sql.Timestamp
import java.time.Instant

class XMapperTest {

    @Test
    fun instantiation() {
        val row = Mockito.mock<Row>(Row::class.java)
        Mockito.`when`(row.getInt("cA")).thenReturn(5)
        Mockito.`when`(row.getString("cB")).thenReturn("Str")
        Mockito.`when`(row.getInt("cC")).thenReturn(7)
        Mockito.`when`(row.getTimestamp("cD")).thenReturn(Timestamp(12345))

        val entityExample = XMapper.read(row, EntityExample::class.java)
        Assert.assertEquals(5, entityExample.a.toLong())
        Assert.assertEquals("Str", entityExample.b)
        Assert.assertEquals(7, entityExample.c.toLong())
        Assert.assertEquals(Instant.ofEpochMilli(12345), entityExample.d)
    }

    @Test
    fun mapping() {
        val values = XMapper.columnValues(
                EntityExample(7, null, 2, Instant.ofEpochMilli(123)), "pr."
        )
        values.removeValue("pr.gB")

        Assert.assertEquals(2, values.size.toLong())
        Assert.assertEquals(7, values["pr.gA"])
        Assert.assertEquals(Timestamp(123), values["pr.gD"])
    }

    class EntityExample @EntityCreator
    constructor(@param:Column("cA") @get:Column("gA") val a: Int,
                @param:Column("cB") @get:Column("gB") val b: String?,
                @param:Column("cC") val c: Int,
                @param:Column("cD") @get:Column("gD") val d: Instant?) {

        constructor() : this(1, null, 2, null) {}
    }

}