package zeus.util.mysql.mapper;

import com.mysql.cj.xdevapi.Row;
import zeus.util.mysql.mapper.ValuesMap;

/**
 * No generics here, because neither javassist nor janino supports generics
 */
public interface Extractor {
    Object rowToObject(Row row);
    ValuesMap objectToMap(Object obj, String prefix);
}
