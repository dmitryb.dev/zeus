package zeus.util.mysql.mapper.types;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;

public class Converters {

    public static Timestamp convertFromInstant(Instant instant) {
        return new Timestamp(instant.toEpochMilli());
    }

    public static Instant convertToInstant(Timestamp timestamp) {
        return Instant.ofEpochMilli(timestamp.getTime());
    }


    public static byte[] convertToBytes(String string) {
        return string.getBytes(StandardCharsets.US_ASCII);
    }

    public static String convertFromBytes(byte[] bytes) {
        return new String(bytes, StandardCharsets.US_ASCII);
    }

}
