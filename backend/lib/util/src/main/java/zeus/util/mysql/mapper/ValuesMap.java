package zeus.util.mysql.mapper;

import java.util.Arrays;
import java.util.HashMap;

public class ValuesMap extends HashMap<String, Object> {

    public ValuesMap removeValue(String... keys) {
        Arrays.stream(keys).forEach(this::remove);
        return this;
    }

    public ValuesMap keepOnly(String... keys) {
        ValuesMap map = new ValuesMap();
        Arrays.stream(keys)
                .forEach(key -> map.put(key, this.get(key)));
        return map;
    }

}
