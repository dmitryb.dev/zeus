package zeus.util.resources

import com.typesafe.config.*
import zeus.util.log.*
import java.io.*

object ConfigBundle {

    fun resource(bundlePath: String, bundleSpecification: String): Config {
        fun getResource(path: String) = try {
            Thread.currentThread().contextClassLoader.getResource(path)!!.readText()
        } catch (ex: Exception) {
            log.warn("Can't load resource '{}'", path)
            null
        }

        try {
            val defaultConfig = ConfigFactory.parseString(getResource("$bundlePath.conf"))

            return getResource(bundlePath + "_" + bundleSpecification + ".conf")?.let {
                ConfigFactory.parseString(it).withFallback(defaultConfig)
            } ?: defaultConfig
        } catch (ex: NullPointerException) {
            throw FileNotFoundException("Resource $bundlePath.conf not found")
        }
    }

    fun directory(bundlePath: String, bundleSpecification: String): Config {
        if (bundlePath.startsWith("resource://")) {
            return resource(bundlePath.replaceFirst("resource://", ""), bundleSpecification)
        }

        try {
            val defaultConfig = ConfigFactory.parseFile(File("$bundlePath.conf"))

            return File(bundlePath + "_" + bundleSpecification + ".conf").takeIf { it.exists() }?.let {
                ConfigFactory.parseFile(it).withFallback(defaultConfig)
            } ?: defaultConfig
        } catch (ex: NullPointerException) {
          throw FileNotFoundException("File $bundlePath.conf not found")
        }
    }

    private val log = logger<ConfigBundle>()

}
