package zeus.util.redis

import com.fasterxml.jackson.databind.*
import io.lettuce.core.codec.*
import java.nio.*
import java.nio.charset.*
import kotlin.reflect.*

object Codecs {

    interface Codec<V> {
        fun encode(value: V): ByteBuffer
        fun decode(buffer: ByteBuffer): V
    }

    fun <K, V> create(keyCodec: Codec<K>, valueCodec: Codec<V>): RedisCodec<K, V> {
        return create(keyCodec::encode, keyCodec::decode, valueCodec::encode, valueCodec::decode)
    }

    fun <V> create(valueEncoder: (V) -> ByteBuffer,
                   valueDecoder: (ByteBuffer) -> V): RedisCodec<V, V> {
        return create(valueEncoder, valueDecoder, valueEncoder, valueDecoder)
    }

    fun <K, V> create(keyEncoder: (K) -> ByteBuffer,
                      keyDecoder: (ByteBuffer) -> K,
                      valueEncoder: (V) -> ByteBuffer,
                      valueDecoder: (ByteBuffer) -> V): RedisCodec<K, V> {

        return object : RedisCodec<K, V> {
            override fun decodeKey(bytes: ByteBuffer): K {
                return keyDecoder(bytes)
            }

            override fun decodeValue(bytes: ByteBuffer): V {
                return valueDecoder(bytes)
            }

            override fun encodeKey(key: K): ByteBuffer {
                return keyEncoder(key)
            }

            override fun encodeValue(value: V): ByteBuffer {
                return valueEncoder(value)
            }
        }
    }

    class Json<V : Any>(private val type: KClass<V>) : Codec<V> {
        private val mapper = ObjectMapper()

        override fun encode(value: V): ByteBuffer = ByteBuffer.wrap(mapper.writeValueAsBytes(value))

        override fun decode(buffer: ByteBuffer): V = mapper.readValue(toByteArray(buffer), type.java)
    }

    object String : Codec<kotlin.String> {
        override fun encode(value: kotlin.String): ByteBuffer = ByteBuffer.wrap(value.toByteArray(StandardCharsets.UTF_8))

        override fun decode(buffer: ByteBuffer): kotlin.String = String(toByteArray(buffer), StandardCharsets.UTF_8)
    }

    object Long : Codec<kotlin.Long> {
        override fun encode(value: kotlin.Long): ByteBuffer = String.encode(value.toString())

        override fun decode(buffer: ByteBuffer): kotlin.Long = String.decode(buffer).toLong()
    }

    fun toByteArray(buffer: ByteBuffer): ByteArray {
        val byteArray = ByteArray(buffer.remaining())
        buffer.get(byteArray, 0, buffer.remaining())
        return byteArray
    }

}
