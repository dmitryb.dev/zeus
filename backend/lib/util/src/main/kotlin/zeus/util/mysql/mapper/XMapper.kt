package zeus.util.mysql.mapper

import com.mysql.cj.xdevapi.Row
import com.mysql.cj.xdevapi.RowResult
import zeus.util.mysql.mapper.generation.ExtractorGenerator
import java.util.*

object XMapper {

    private val extractors = HashMap<Class<*>, Extractor>()

    @JvmStatic
    fun <T> readAll(rowResult: RowResult, clazz: Class<T>): Collection<T> {
        return rowResult.fetchAll()
                .map { r -> read(r, clazz) }
    }
    @JvmStatic
    fun <T> readOne(rowResult: RowResult, clazz: Class<T>): T {
        return read(rowResult.fetchOne(), clazz)
    }

    @JvmStatic
    fun columnValues(obj: Any): ValuesMap {
        return getExtractorFor(obj.javaClass).objectToMap(obj, "")
    }

    @JvmStatic
    fun columnValues(obj: Any, prefix: String): ValuesMap {
        return getExtractorFor(obj.javaClass).objectToMap(obj, prefix)
    }

    internal fun <T> read(row: Row, clazz: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return getExtractorFor(clazz).rowToObject(row) as T
    }

    private fun getExtractorFor(clazz: Class<*>): Extractor =
            extractors.getOrPut(clazz) { ExtractorGenerator.createExtractor(clazz) }

}

fun Any.xmap() = XMapper.columnValues(this)
