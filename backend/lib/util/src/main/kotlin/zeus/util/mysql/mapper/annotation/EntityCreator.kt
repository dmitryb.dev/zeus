package zeus.util.mysql.mapper.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CONSTRUCTOR)
annotation class EntityCreator
