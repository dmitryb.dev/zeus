package zeus.util.mysql.extension

import com.mysql.cj.xdevapi.*
import kotlinx.coroutines.future.*
import zeus.util.mysql.mapper.*

suspend inline fun <reified MR> SqlStatement.findOne(): MR? =
        this.executeAsync().await()
                ?.takeIf { it.hasNext() }
                ?.let { XMapper.readOne(it, MR::class.java) }

suspend inline fun <reified MR> SqlStatement.findAll(): Collection<MR> =
        XMapper.readAll(this.executeAsync().await(), MR::class.java)

suspend inline fun SqlStatement.await() = this.executeAsync().await()