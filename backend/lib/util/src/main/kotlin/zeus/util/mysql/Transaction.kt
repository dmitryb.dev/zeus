package zeus.util.mysql

import com.mysql.cj.xdevapi.*
import kotlinx.coroutines.*
import zeus.util.log.*
import kotlin.coroutines.*

class SessionContext(val session: Session): CoroutineContext.Element {
    override val key: CoroutineContext.Key<*>
        get() = SessionContextKey
}

object SessionContextKey: CoroutineContext.Key<SessionContext>

suspend fun <T> Client.withSessionContext(block: suspend (Session) -> T): T {
    val existingSession = coroutineContext[SessionContextKey]?.session

    val session = existingSession ?: this.session

    try {
        return if (existingSession != null) {
            log.trace("Session already exists")
            block(session)
        } else {
            withContext(SessionContext(session)) {
                log.trace("Session is not exists, created new")
                block(session)
            }
        }
    } finally {
        if (existingSession == null) {
            session.close()
            log.trace("Session is closed")
        } else {
            log.trace("Session doesn't belong to current context, therefore it isn't closed")
        }
    }
}

suspend fun <T> Client.transactional(action: suspend () -> T): T = withSessionContext { session ->
    session.startTransaction()

    log.trace("Transaction started")

    try {
        val result = action()
        session.commit()

        log.trace("Transaction committed")

        result
    } catch(th: Throwable) {
        log.debug("Transaction failed, rollback")
        session.rollback()
        throw th
    }
}

private val log = logger<Clients>()