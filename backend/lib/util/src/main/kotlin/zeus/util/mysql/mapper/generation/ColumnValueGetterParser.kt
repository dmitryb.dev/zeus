package zeus.util.mysql.mapper.generation

import zeus.util.mysql.mapper.annotation.Column
import zeus.util.mysql.mapper.types.SupportedTypes
import java.lang.reflect.Method

object ColumnValueGetterParser {

    fun getValueGetters(entityClass: Class<*>): Iterable<GetterInfo> {
        return entityClass.methods
                .filter { it.getAnnotation(Column::class.java) != null }
                .map { parseMethod(it) }
    }

    private fun parseMethod(method: Method): GetterInfo {
        val columnName = method.getAnnotation(Column::class.java).value

        if (SupportedTypes.rowGetters.containsKey(method.returnType)) {
            return GetterInfo(null, method.name, columnName)
        }

        val converter = SupportedTypes.convertersToRowSupportedType
                .get(method.returnType)
                .orNull
        
        if (converter != null) {
            return GetterInfo(
                    converter.methodName,
                    method.name,
                    columnName
            )
        }

        throw IllegalArgumentException("Unsupported return type " + method.returnType.simpleName + " of method " + method.name)
    }

    class GetterInfo(val converterMethodName: String?, val methodName: String, val columnName: String)

}
