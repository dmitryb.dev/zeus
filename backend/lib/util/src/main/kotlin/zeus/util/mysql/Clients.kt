package zeus.util.mysql

import com.mysql.cj.xdevapi.*
import com.typesafe.config.Config
import java.util.*

object Clients {

    fun fromConfig(config: Config): Client {
        return ClientFactory().getClient(String.format(
                "mysqlx://%s:%s/%s?user=%s&password=%s",
                config.getString("mysql.host"),
                config.getInt("mysql.portx"),
                config.getString("mysql.schema"),
                config.getString("mysql.user"),
                config.getString("mysql.pass")
        ), Properties().apply {
            setProperty(Client.ClientProperty.POOLING_ENABLED.keyName, "true")
            setProperty(Client.ClientProperty.POOLING_MAX_SIZE.keyName, "50")
            setProperty(Client.ClientProperty.POOLING_MAX_IDLE_TIME.keyName, "60000")
            setProperty(Client.ClientProperty.POOLING_QUEUE_TIMEOUT.keyName, "2500")
        })
    }

}
