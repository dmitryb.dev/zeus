package zeus.util

import io.grpc.*
import kotlinx.coroutines.*
import kotlinx.coroutines.slf4j.*
import org.slf4j.*
import zeus.util.log.*

class GrpcServer(port: Int, vararg services: BindableService) {
    private val server: Server

    init {
        val serverBuilder = ServerBuilder.forPort(port)
                .directExecutor()
                .intercept(Tracer)

        services.forEach { serverBuilder.addService(it) }

        this.server = serverBuilder.build()
    }

    fun start() {
        Runtime.getRuntime().addShutdownHook(Thread(Runnable { this.stop() }))

        server.start()
        log.info("Grpc server started on {}", server.port)

        server.awaitTermination()
    }

    private fun stop() {
        this.server.shutdown()
    }

    companion object {
        private val log = logger<GrpcServer>()
    }
}

suspend fun <T> Logger.uncaught(block: suspend () -> T): T {
    try {
        return block()
    } catch (ex: StatusException) {
        throw ex
    } catch (th: Throwable) {
        error("Uncaught exception", th)
        throw th
    }
}

private object Tracer : ServerInterceptor {

    override fun <ReqT : Any?, RespT : Any?> interceptCall(
            call: ServerCall<ReqT, RespT>?, headers: Metadata?, next: ServerCallHandler<ReqT, RespT>?
    ): ServerCall.Listener<ReqT> {
        setupTraceId(
                headers?.get(Metadata.Key.of("traceId", Metadata.ASCII_STRING_MARSHALLER))
        )
        return next!!.startCall(call, headers)
    }
}