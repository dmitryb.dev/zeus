package zeus.util.mysql.mapper.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(
        AnnotationTarget.VALUE_PARAMETER,
        AnnotationTarget.PROPERTY_GETTER
)
annotation class Column(val value: String)
