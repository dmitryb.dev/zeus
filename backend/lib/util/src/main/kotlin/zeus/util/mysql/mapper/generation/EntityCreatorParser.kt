package zeus.util.mysql.mapper.generation

import zeus.util.mysql.mapper.annotation.Column
import zeus.util.mysql.mapper.annotation.EntityCreator
import zeus.util.mysql.mapper.types.SupportedTypes
import java.lang.reflect.Parameter

object EntityCreatorParser {

    fun getConstructorArguments(entityClass: Class<*>): Iterable<ArgInfo>? {
        val constructor = entityClass.constructors
                .firstOrNull { it.getAnnotation(EntityCreator::class.java) != null }

        return constructor?.parameters?.map{ parseParameter(it) }
    }

    private fun parseParameter(parameter: Parameter): ArgInfo {
        if (parameter.getAnnotation<Column>(Column::class.java) == null) {
            throw IllegalArgumentException(String.format("Can't find %s annotation for parameter of class \$s",
                    Column::class.java.simpleName, parameter.type.simpleName
            ))
        }

        val columnName = parameter.getAnnotation<Column>(Column::class.java).value

        if (SupportedTypes.rowGetters.containsKey(parameter.type)) {
            return ArgInfo(null, SupportedTypes.rowGetters.get(parameter.type).get(), columnName)
        }

        val converter = SupportedTypes.convertersToRowUnsupportedType
                .get(parameter.type)
                .orNull
        if (converter != null) {
            return ArgInfo(
                    converter.methodName,
                    SupportedTypes.rowGetters.get(converter.convertsType).get(),
                    columnName
            )
        }

        throw IllegalArgumentException("Unsupported type " + parameter.type.simpleName)
    }

    class ArgInfo(val converterMethodName: String?, val columnValueGetter: String, val columnName: String)

}
