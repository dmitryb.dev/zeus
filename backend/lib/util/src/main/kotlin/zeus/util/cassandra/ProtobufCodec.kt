package zeus.util.cassandra

import com.datastax.driver.core.DataType
import com.datastax.driver.core.ProtocolVersion
import com.datastax.driver.core.TypeCodec
import com.google.protobuf.Message
import com.google.protobuf.TextFormat
import java.nio.ByteBuffer

class ProtobufCodec<T : Message>(private val defaultInstance: T) : TypeCodec<T>(DataType.blob(), defaultInstance.javaClass) {

    override fun serialize(value: T, protocolVersion: ProtocolVersion): ByteBuffer {
        return ByteBuffer.wrap(value.toByteArray())
    }

    override fun deserialize(bytes: ByteBuffer, protocolVersion: ProtocolVersion): T {
        val byteArray = ByteArray(bytes.remaining())
        bytes.get(byteArray)

        @Suppress("UNCHECKED_CAST")
        return defaultInstance.toBuilder()
                .mergeFrom(byteArray)
                .build() as T
    }

    override fun parse(value: String): T {
        val builder = defaultInstance.toBuilder()
        TextFormat.getParser().merge(value, builder)

        @Suppress("UNCHECKED_CAST")
        return builder.build() as T
    }

    override fun format(value: T): String {
        return TextFormat.printToString(value)
    }
}
