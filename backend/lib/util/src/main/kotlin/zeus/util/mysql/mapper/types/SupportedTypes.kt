package zeus.util.mysql.mapper.types

import com.mysql.cj.xdevapi.Row
import io.vavr.collection.HashMap
import io.vavr.collection.Map
import java.lang.reflect.Method

import java.util.Arrays

object SupportedTypes {

    private val boxedForPrimitives = HashMap.empty<Class<*>, Class<*>>()
            .put(Boolean::class.javaPrimitiveType, Boolean::class.java)
            .put(Byte::class.javaPrimitiveType, Byte::class.java)
            .put(Double::class.javaPrimitiveType, Double::class.java)
            .put(Int::class.javaPrimitiveType, Int::class.java)
            .put(Long::class.javaPrimitiveType, Long::class.java)

    val rowGetters: Map<Class<*>, String>

    val convertersToRowUnsupportedType: Map<Class<*>, Converter>

    val convertersToRowSupportedType: Map<Class<*>, Converter>

    init {
        val foundGetters = java.util.HashMap<Class<*>, String>()
        Row::class.java.methods
                .filter { method -> method.name.startsWith("get") }
                .forEach { method ->
                    foundGetters[method.returnType] = method.name
                    boxedForPrimitives.get(method.returnType)
                            .forEach { cl -> foundGetters[cl] = method.name }
                }
        rowGetters = HashMap.ofAll(foundGetters)
    }

    init {
        val foundConverters = java.util.HashMap<Class<*>, Converter>()
        Arrays.stream<Method>(Converters::class.java.declaredMethods)
                .filter { method -> rowGetters.containsKey(method.parameterTypes[0]) }
                .forEach { method ->
                    foundConverters[method.returnType] = Converter(method.name, method.parameterTypes[0])
                    boxedForPrimitives.get(method.returnType)
                            .forEach { cl ->
                                foundConverters[cl] = Converter(method.name, method.parameterTypes[0])
                            }
                }

        convertersToRowUnsupportedType = HashMap.ofAll(foundConverters)
    }

    init {
        val foundConverters = java.util.HashMap<Class<*>, Converter>()
        Arrays.stream<Method>(Converters::class.java.declaredMethods)
                .filter { method -> rowGetters.containsKey(method.returnType) }
                .forEach { method ->
                    foundConverters[method.parameterTypes[0]] = Converter(method.name, method.returnType)
                    boxedForPrimitives.get(method.parameterTypes[0])
                            .forEach { cl ->
                                foundConverters[cl] = Converter(method.name, method.returnType)
                            }
                }

        convertersToRowSupportedType = HashMap.ofAll(foundConverters)
    }

    class Converter internal constructor(val methodName: String, val convertsType: Class<*>)
}
