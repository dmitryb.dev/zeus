package zeus.util.pulsar

import com.github.benmanes.caffeine.cache.*
import org.apache.pulsar.client.api.*
import java.util.concurrent.*

class ProducerCache<T>(pulsarClient: PulsarClient, schema: Schema<T>) {

    private val clientCache = Caffeine.newBuilder()
            .expireAfterAccess(5, TimeUnit.MINUTES)
            .removalListener<Any, Producer<*>> { _, producer, _ -> producer?.closeAsync() }
            .build<String, Producer<T>> { topic ->
                pulsarClient.newProducer(schema)
                        .topic(topic)
                        .create()
            }

    fun getClient(topic: String) = clientCache[topic]!!

}