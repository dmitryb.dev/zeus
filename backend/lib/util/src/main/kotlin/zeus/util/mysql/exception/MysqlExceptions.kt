package zeus.util.mysql.exception

import com.mysql.cj.protocol.x.*

open class MysqlException(message: String) : Exception(message) {
    companion object {
        suspend fun <T> wrapMysqlException(action: suspend () -> T): T = try {
            action()
        } catch (th: Throwable) {
            when (th) {
                is XProtocolError -> when (th.errorCode) {
                    1062 -> throw DuplicatedException(th.message ?: "")
                    1452 -> throw ForeignKeyViolated(th.message ?: "")
                    else -> throw th
                }
                else -> throw th
            }
        }
    }
}

class DuplicatedException(message: String) : MysqlException(message)
class ForeignKeyViolated(message: String) : MysqlException(message)
