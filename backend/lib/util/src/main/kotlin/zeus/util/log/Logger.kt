package zeus.util.log

import org.slf4j.*

inline fun <reified T> logger(): Logger = LoggerFactory.getLogger(T::class.java)