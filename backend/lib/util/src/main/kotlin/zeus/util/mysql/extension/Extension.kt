package zeus.util.mysql.extension

import com.mysql.cj.xdevapi.*
import com.mysql.cj.xdevapi.Statement
import kotlinx.coroutines.future.*
import zeus.util.mysql.mapper.*
import java.sql.*
import java.time.*

suspend inline fun <reified MR> Statement<SelectStatement, RowResult>.findOne(): MR? =
    this.executeAsync().await()
            ?.takeIf { it.hasNext() }
            ?.let { XMapper.readOne(it, MR::class.java) }

suspend inline fun <reified MR> Statement<SelectStatement, RowResult>.findAll(): Collection<MR> =
        XMapper.readAll(this.executeAsync().await(), MR::class.java)

fun <T : Any> Table.insert(value: T) = this.insert(XMapper.columnValues(value))
fun <T : Any> Table.update(value: T) = this.update().set(XMapper.columnValues(value))

suspend fun <T : Any> Table.save(value: T) = this.insert(XMapper.columnValues(value)).executeAsync().await()

suspend inline fun InsertStatement.await() = this.executeAsync().await()
suspend inline fun UpdateStatement.await() = this.executeAsync().await()
suspend inline fun DeleteStatement.await() = this.executeAsync().await()

fun timestamp() = Timestamp.from(Instant.now())