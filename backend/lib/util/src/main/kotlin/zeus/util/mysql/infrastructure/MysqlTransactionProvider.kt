package zeus.util.mysql.infrastructure

import com.mysql.cj.xdevapi.*
import zeus.infrstructure.transaction.*
import zeus.util.mysql.*
import javax.inject.*

class MysqlTransactionProvider @Inject constructor(private val client: Client) : TransactionProvider {
    override suspend fun <T> transactional(action: suspend () -> T): T = client.transactional(action)
}