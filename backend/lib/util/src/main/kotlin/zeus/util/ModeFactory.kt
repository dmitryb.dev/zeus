package zeus.util

import zeus.util.log.*
import java.util.*

class ModeFactory<T> private constructor(private val defaultEnv: String) {
    private val modeSuppliers = HashMap<String, () -> T>()

    fun withMode(name: String, envSupplier: () -> T): ModeFactory<T> {
        this.modeSuppliers[name] = envSupplier
        return this
    }

    fun create(args: Array<String>): T {
        val modeArg = args
                .filter { arg -> arg.startsWith("--") }
                .map { arg -> arg.replaceFirst("--".toRegex(), "") }
                .map {arg -> arg.toLowerCase() }
                .firstOrNull { modeSuppliers.containsKey(it) }

        val arg = modeArg ?: when {
            modeSuppliers.containsKey(System.getenv("MODE")?.toLowerCase()) -> System.getenv("MODE")
            modeSuppliers.containsKey(System.getenv("mode")?.toLowerCase()) -> System.getenv("mode")
            else -> defaultEnv
        }

        log.info("Using mode: {}", arg)

        return modeSuppliers[arg]!!()
    }

    companion object {
        private val log = logger<ModeFactory<Any>>()

        fun <T> withDefaultMode(name: String, envSupplier: () -> T): ModeFactory<T> {
            return ModeFactory<T>(name)
                    .withMode(name, envSupplier)
        }
    }

}
