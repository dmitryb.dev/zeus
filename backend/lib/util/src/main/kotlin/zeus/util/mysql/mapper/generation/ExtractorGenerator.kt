package zeus.util.mysql.mapper.generation

import zeus.util.mysql.mapper.Extractor
import io.vavr.control.Try
import org.codehaus.janino.SimpleCompiler
import org.jtwig.JtwigModel
import org.jtwig.JtwigTemplate

object ExtractorGenerator {

    fun createExtractor(clazz: Class<*>): Extractor {
        return createExtractorInternal(clazz)
    }

    @Throws(Exception::class)
    private fun createExtractorInternal(clazz: Class<*>): Extractor {
        val packageName = "xmapper." + clazz.getPackage().name
        val extractorClassName = clazz.simpleName + "_Extractor"

        val compiler = SimpleCompiler()
        compiler.cook(extractorSourceCode(clazz, packageName, extractorClassName))
        val extractorClass = compiler.classLoader.loadClass("$packageName.$extractorClassName")

        return extractorClass.getConstructor().newInstance() as Extractor
    }

    private fun extractorSourceCode(clazzToExtract: Class<*>, packageName: String, extractorClassName: String): String {
        val template = JtwigTemplate.classpathTemplate("xmapper/extractor.twig")
        val model = JtwigModel.newModel()
                .with("package", packageName)
                .with("extractorClassName", extractorClassName)
                .with("clazz", clazzToExtract.name.replace('$', '.'))
                .with("className", clazzToExtract.simpleName)
                .with("columnValueGetters", ColumnValueGetterParser.getValueGetters(clazzToExtract))
                .with("entityCreatorArgs", EntityCreatorParser.getConstructorArguments(clazzToExtract))

        return template.render(model)
    }

}
