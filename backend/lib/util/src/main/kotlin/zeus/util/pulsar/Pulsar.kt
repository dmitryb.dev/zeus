package zeus.util.pulsar

import com.google.protobuf.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.future.*
import org.apache.pulsar.client.api.*
import org.apache.pulsar.client.api.Message
import java.util.concurrent.*
import kotlin.coroutines.*
import kotlin.reflect.*

val protobufSchemas: MutableMap<KClass<out GeneratedMessageV3>, Schema<out GeneratedMessageV3>> = ConcurrentHashMap()

@Suppress("UNCHECKED_CAST")
@ExperimentalCoroutinesApi
suspend inline fun <reified T : GeneratedMessageV3> PulsarClient.read(
    topic: String,
    messageId: MessageId
): ReceiveChannel<T> = read(
        topic,
        messageId,
        protobufSchemas.getOrPut(T::class) { Schema.PROTOBUF(T::class.java)} as Schema<T>
)

@ExperimentalCoroutinesApi
suspend inline fun <T> PulsarClient.read(
        topic: String,
        messageId: MessageId,
        schema: Schema<T>
): ReceiveChannel<T> = GlobalScope.produce(coroutineContext) {
    val reader = newReader(schema)
            .topic(topic)
            .startMessageId(messageId)
            .create()

    try {
        while (!isClosedForSend) {
            @Suppress("UNCHECKED_CAST")
            send((reader.readNextAsync().await() as Message<T>).value)
        }
    } finally {
        reader.closeAsync()
    }
}



