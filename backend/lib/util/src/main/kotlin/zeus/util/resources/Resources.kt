package zeus.util.resources

import io.vavr.control.*
import zeus.util.log.*
import java.io.*
import java.net.*
import java.nio.file.*
import java.util.*
import java.util.Arrays.*
import java.util.Comparator.*

object Resources {

    private val log = logger<Resources>()

    private val resourcesBaseDirectory = Try.of { Resources::class.java.classLoader.getResource(".") }
            .mapTry { url -> File(url.toURI()) }
            .map { file -> file.parentFile.toURI() }
            .peek { uri -> log.info("Base folder: {}", uri.path) }
            .get()

    @JvmStatic
    fun get(file: String): String {
        return tryAll(file).first()
    }

    @JvmStatic
    fun getAll(folder: String): Collection<String> {
        return tryAll(folder)
    }

    private fun tryAll(folder: String): Collection<String> {
        val fileUrls = Resources::class.java.classLoader.getResources(folder)

        val contents = ArrayList<String>()
        while (fileUrls.hasMoreElements()) {
            val url = fileUrls.nextElement()

            val file = File(url.file)
            addAllFilesContentRecursively(file, contents)
        }

        return contents
    }


    private fun addAllFilesContentRecursively(file: File, contents: ArrayList<String>) {
        if (file.isDirectory) {
            val files = file.listFiles()
            sort(files!!, comparing<File, String> { it.name })
            for (f in files) {
                addAllFilesContentRecursively(f, contents)
            }
        } else {
            val content = String(Files.readAllBytes(file.toPath()))
            contents.add(content)
            log.info("Found resource: {}, {} lines", relativeResourcePath(file.toURI()), content.split("\n".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray().size)
        }
    }

    private fun relativeResourcePath(uri: URI): String =
            resourcesBaseDirectory
                .relativize(uri)
                .path

}
