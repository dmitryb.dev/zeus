package zeus.util.pulsar

import org.apache.pulsar.client.admin.*
import zeus.util.log.*
import java.util.*

object PulsarBootstrap {

    private val log = logger<PulsarBootstrap>()

    @JvmStatic
    fun create(pulsarAdmin: PulsarAdmin, vararg namespaces: String) {
        for (namespace in namespaces) {
            try {
                pulsarAdmin.namespaces().createNamespace(namespace)
                pulsarAdmin.namespaces().setNamespaceReplicationClusters(
                        namespace,
                        HashSet(pulsarAdmin.clusters().clusters)
                )
            } catch (e: PulsarAdminException) {
                if (e.statusCode == 409) {
                    log.info("Pulsar namespace \"{}\" already exists", namespace)
                } else {
                    log.error("Can't bootstrap pulsar namespace \"{}\": {}", namespaces, e)
                }
            }

        }
    }
}
