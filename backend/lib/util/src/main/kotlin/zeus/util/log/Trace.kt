package zeus.util.log

import kotlinx.coroutines.slf4j.*
import org.slf4j.*
import kotlin.coroutines.*

fun traceContext() = MDCContext()

fun newTraceContext(): MDCContext {
    setupTraceId()
    return traceContext()
}

fun getTraceId(): String? = MDC.get("traceId")

fun setupTraceId(existingTraceId: String? = null) {
    val traceId = existingTraceId ?: (1..5).map { ID_CHARS.random() }.joinToString("")

    MDC.put("traceId", traceId)
}

private const val ID_CHARS = "abcdefghiklmnopqrstuvwxyz1234567890"