package zeus.util.mysql.infrastructure

import com.mysql.cj.xdevapi.*
import zeus.util.mysql.*
import zeus.util.mysql.exception.*

abstract class MysqlStore(private val client: Client, private val schemaName: String, private val tableName: String) {

    protected suspend fun <T> withSchema(block: suspend (Schema) -> T) = client.withSessionContext { session ->
        MysqlException.wrapMysqlException {
            block(session.getSchema(schemaName))
        }
    }

    protected suspend fun <T> withTable(block: suspend (Table) -> T) = client.withSessionContext { session ->
        MysqlException.wrapMysqlException {
            block(session.getSchema(schemaName).getTable(tableName))
        }
    }

}