package zeus.security.device

import common.domain.device.*
import org.apache.commons.codec.digest.*

object DevicePass {

    private const val CONST_SALT = "a3f85vlsf43n"

    fun hash(filter: Filters.SingleDeviceFilter, pass: String): String {
        return DigestUtils.sha256Hex("$pass:${filter.manufacturerId}:${filter.deviceNumber}:$CONST_SALT")
    }
}
