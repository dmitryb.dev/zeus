package zeus.id.generator.local

interface LocalIdGenerator {
    suspend fun nextLocalId(outerEntity: String): Long
}
