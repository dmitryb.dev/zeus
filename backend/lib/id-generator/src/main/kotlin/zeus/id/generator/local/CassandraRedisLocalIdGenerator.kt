package zeus.id.generator.local

import com.datastax.driver.core.*
import com.datastax.driver.core.querybuilder.*
import io.lettuce.core.*
import kotlinx.coroutines.*
import kotlinx.coroutines.guava.*
import kotlinx.coroutines.reactive.*
import zeus.util.log.*
import zeus.util.redis.*

class CassandraRedisLocalIdGenerator(
        private val cassandraSession: Session,
        private val cassandraTable: CassandraTable,
        redisClient: RedisClient
) : LocalIdGenerator {

    private val redisCommands = redisClient.connect(Codecs.create(Codecs.String, Codecs.Long)).reactive()

    override suspend fun nextLocalId(outerEntity: String): Long {
        if (redisCommands.exists(outerEntity).awaitFirst() == 0L) {
            initRedisWithLocalIdStoredInCassandra(outerEntity)
        }

        val nextId = redisCommands.incr(outerEntity).awaitFirst()
        return saveLocalIdToCassandra(outerEntity, nextId)
    }

    private suspend fun initRedisWithLocalIdStoredInCassandra(outerEntity: String) {
        log.debug("Initializing redis with local id by cassandra for outer entity: {}", outerEntity)

        val lastId = findLastLocalIdInCassandra(outerEntity)
        redisCommands.setnx(outerEntity, lastId).awaitFirst()

        log.debug("Redis id store was initialized with last local id: {}",
                lastId, outerEntity)
    }

    private suspend fun findLastLocalIdInCassandra(outerEntity: String): Long {
        val query = QueryBuilder.select(cassandraTable.localIdColumn)
                .from(cassandraTable.name)
                .where(QueryBuilder.eq(cassandraTable.outerEntityIdColumn, outerEntity))
                .limit(1)

        val row = cassandraSession.executeAsync(query)
                .await()
                .one()

        return row?.getLong(cassandraTable.localIdColumn) ?: 0L
    }

    private suspend fun saveLocalIdToCassandra(outerEntity: String, localId: Long): Long {
        cassandraSession.executeAsync(QueryBuilder.insertInto(cassandraTable.name)
                .value(cassandraTable.outerEntityIdColumn, outerEntity)
                .value(cassandraTable.localIdColumn, localId))
                .await()

        log.debug("Local id: {} for outer entity: {} stored in cassandra", localId, outerEntity)

        GlobalScope.launch { withContext(newTraceContext()) {
            removeOldIdsFromCassandra(outerEntity, localId)
        }}

        return localId
    }

    private suspend fun removeOldIdsFromCassandra(outerEntity: String, localId: Long) {
        if (localId % OLD_ID_DELETE_FREQUENCY == 0L) {
            try {
                log.debug("Removing Local ids before id: {}, for outer entity {}", localId, outerEntity)

                cassandraSession.executeAsync(QueryBuilder.delete().from(cassandraTable.name)
                        .where(QueryBuilder.eq(cassandraTable.outerEntityIdColumn, outerEntity))
                        .and(QueryBuilder.lt(cassandraTable.localIdColumn, localId)))
                        .await()

                log.debug("Old local ids was removed")
            } catch (th: Throwable) {
                // It's just regular task for cleaning, so fail of this is not important
                log.warn("Can't remove old local ids", th)
            }
        }
    }

    companion object {
        private const val OLD_ID_DELETE_FREQUENCY = 100

        val log = logger<CassandraRedisLocalIdGenerator>()
    }

    class CassandraTable(val name: String, val outerEntityIdColumn: String, val localIdColumn: String)
}
