package zeus.id.generator.local

import java.util.*
import javax.inject.*

@Singleton
class StubLocalIdGenerator @Inject constructor() : LocalIdGenerator {

    private val ids = HashMap<String, Long>()

    override suspend fun nextLocalId(outerEntity: String): Long =
            ids.compute(outerEntity) { _, cId -> cId?.let { it.inc() } ?: 0L }!!
}
