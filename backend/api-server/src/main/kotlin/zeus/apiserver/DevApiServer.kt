package zeus.apiserver;

import dagger.Component
import zeus.apiserver.module.EntryPointModule
import zeus.apiserver.module.dev.DevConfigModule
import zeus.apiserver.module.prod.*
import javax.inject.Singleton

@Singleton
@Component(modules = [
    EntryPointModule::class,
    DevConfigModule::class,
    InfrastructureModule::class
])
interface DevApiServer : ApiServer
