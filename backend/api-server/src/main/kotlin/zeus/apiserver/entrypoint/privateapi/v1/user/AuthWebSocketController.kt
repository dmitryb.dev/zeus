package zeus.apiserver.entrypoint.privateapi.v1.user

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import zeus.apiserver.infrastructure.user.auth.stream.*
import javax.inject.*

class WaitForEmailRequest(val login: String, val pass: String)
class WaitForEmailResponse(val signInToken: String)

class WaitForPassRequest(val email: String)
class WaitForPassResponse

class AuthWebSocketController @Inject constructor(private val userAuthServerStreams: UserAuthServerStreams) {

    suspend fun waitForEmailConfirmation(
            receiveChannel: ReceiveChannel<WaitForEmailRequest>
    ): ReceiveChannel<WaitForEmailResponse> = GlobalScope.produce {
        val request = receiveChannel.receive()

        val token = userAuthServerStreams.waitForEmailConfirmation(request.login, request.pass)

        send(WaitForEmailResponse(token))

        receiveChannel.cancel()
        close()
    }

    suspend fun waitForPassChange(
            receiveChannel: ReceiveChannel<WaitForPassRequest>
    ): ReceiveChannel<WaitForPassResponse> = GlobalScope.produce {
        val request = receiveChannel.receive()

        userAuthServerStreams.waitForPassChange(request.email)

        send(WaitForPassResponse())

        receiveChannel.cancel()
        close()
    }

}