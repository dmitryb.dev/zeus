package zeus.apiserver.entrypoint.publicapi.v1.device.event.subscribe

import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.node.*
import common.domain.device.*
import io.ktor.features.*
import zeus.apiserver.entrypoint.publicapi.v1.device.common.*

class SubscribeRequest(
        val device: SingleAuthorizedDeviceFilter,
        val startFrom: Long?,
        val valueTransform: Collection<ValueTransform> = emptyList()
)

class ValueTransform(val event: Collection<Int>, val mapping: JsonNode) {

    init {
        validateMapping(mapping)
    }

    private fun validateMapping(mapping: JsonNode) { when {
        mapping.isContainerNode -> (mapping as ContainerNode<*>).forEach(this::validateMapping)
        mapping.isInt -> {}
        else ->  throw BadRequestException("Mapping can be only an int, array of ints or object containing int values "
                + "(nested objects and arrays are allowed)")
    }}
}

class UnsubscribeRequest(val request: SubscribeRequest)