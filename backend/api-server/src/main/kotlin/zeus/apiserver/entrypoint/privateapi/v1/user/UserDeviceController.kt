package zeus.apiserver.entrypoint.privateapi.v1.user

import user.account.dto.*
import zeus.apiserver.infrastructure.user.account.device.*
import javax.inject.*

class UserDeviceController @Inject constructor(
        private val deviceServerClient: UserDeviceServerClient
) {
    suspend fun createDevice(userId: Long, request: DeviceValue): DeviceCreatedResponse {
        val deviceId = deviceServerClient.addDevice(AddDeviceRequest {
            this.userId = userId
            filter {
                manufacturerId = request.manufacturerId
                deviceNumber = request.deviceNumber
                addAllTags(request.tags)
            }
            role = request.role
            pass = request.pass
            name = request.name
        })

        return DeviceCreatedResponse(deviceId)
    }

    suspend fun findDevices(userId: Long, request: FindDevicesRequest): FoundDevicesResponse {
        val devices = deviceServerClient.findDevices(FindUserDevicesRequest {
            this.userId = userId
            request.manufacturerId?.let { this.manufacturer { id = it } }
            request.deviceNumber?.let { this.device { number = it } }
            request.tags?.let { this.addAllTags(request.tags) }
            request.role?.let { this.role { value = it } }
            request.pass?.let { this.pass { value = it } }
        })

        return FoundDevicesResponse(
                devices.map { FoundDevice(
                        it.deviceId,
                        it.filter.manufacturerId,
                        it.filter.deviceNumber,
                        it.filter.tagsList,
                        it.role,
                        it.name,
                        false
                )}
        )
    }

    suspend fun updateDevice(userId: Long, deviceId: Long, request: DeviceValue) {
        deviceServerClient.updateDevice(UpdateDeviceRequest {
            this.userId = userId
            this.deviceId = deviceId
            filter {
                manufacturerId = request.manufacturerId
                deviceNumber = request.deviceNumber
                addAllTags(request.tags)
            }
            role = request.role
            request.pass?.let { pass { value = it } }
            name = request.name
        })
    }

    suspend fun removeDevice(userId: Long, deviceId: Long) {
        deviceServerClient.removeDevice(userId, deviceId)
    }
}


class DeviceValue(
        val manufacturerId: Long,
        val deviceNumber: Long,
        val tags: Collection<String>,
        val role: String,
        val pass: String?,
        val name: String
)

class FindDevicesRequest(
        val manufacturerId: Long?,
        val deviceNumber: Long?,
        val tags: Collection<String>?,
        val role: String?,
        val pass: String?
)

class FoundDevicesResponse(val devices: Collection<FoundDevice>)
class FoundDevice(
        val deviceId: Long,
        val manufacturerId: Long,
        val deviceNumber: Long,
        val tags: Collection<String>,
        val role: String,
        val name: String,
        val online: Boolean
)

class DeviceCreatedResponse(val deviceId: Long)
