package zeus.apiserver.entrypoint.publicapi.v1.device.status

import zeus.apiserver.entrypoint.publicapi.v1.device.common.*

class StatusRequest(val device: SingleAuthorizedDeviceFilter)
