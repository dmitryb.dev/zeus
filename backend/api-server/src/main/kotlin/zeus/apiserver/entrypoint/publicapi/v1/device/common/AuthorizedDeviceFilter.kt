package zeus.apiserver.entrypoint.publicapi.v1.device.common

class AuthorizedDeviceFilter(
        val manufacturerId: Long,
        val deviceNumber: Long?,
        val tags: Collection<String> = emptyList(),
        val auth: Auth
)