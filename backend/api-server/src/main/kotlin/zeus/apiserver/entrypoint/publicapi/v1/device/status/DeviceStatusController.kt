package zeus.apiserver.entrypoint.publicapi.v1.device.status

import zeus.apiserver.infrastructure.device.messaging.*
import javax.inject.*

class DeviceStatusController @Inject constructor(
        private val deviceMessagingServerClient: DeviceMessagingServerClient
) {

    suspend fun getStatus(request: StatusRequest): StatusResponse {
        val filter = request.device.toDomain()

        val status = deviceMessagingServerClient.getStatuses(listOf(filter)).first()

        return StatusResponse(status.isOnline)
    }

}