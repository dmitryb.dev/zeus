package zeus.apiserver.entrypoint.publicapi.v1.device.common

import com.fasterxml.jackson.databind.node.*
import common.domain.device.*
import common.domain.device.Values.Value.TypeCase.*

fun Values.Value.toJsonNode() = when (this.typeCase) {
    INT32 -> IntNode(this.int32)
    FLOAT -> FloatNode(this.float)
    STRING -> TextNode(this.string)
    else -> NullNode.instance
}