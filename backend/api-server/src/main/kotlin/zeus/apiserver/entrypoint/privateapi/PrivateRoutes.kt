package zeus.apiserver.entrypoint.privateapi

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.websocket.*
import zeus.apiserver.domain.*
import zeus.apiserver.entrypoint.privateapi.v1.user.*
import zeus.apiserver.entrypoint.util.*
import zeus.apiserver.infrastructure.user.auth.proxy.*
import javax.inject.*

class PrivateRoutes @Inject constructor(
        private val userAuthServerProxy: UserAuthServerProxy,
        private val authWebSocketController: AuthWebSocketController,
        private val userDeviceController: UserDeviceController,
        private val manufacturerController: ManufacturerController
) {
    fun v1Route(routing: Routing) = routing.apply {
        route("private/api/v1") {
            route("auth") {
                webSocket("wait-for-email-confirmation") {
                    handleWS<WaitForEmailRequest> { receiveChannel ->
                        authWebSocketController.waitForEmailConfirmation(receiveChannel)
                    }
                }
                webSocket("wait-for-pass-change") {
                    handleWS<WaitForPassRequest> { receiveChannel ->
                        authWebSocketController.waitForPassChange(receiveChannel)
                    }
                }
                route("{path...}") {
                    handle {
                        val uri = call.request.uri.replaceFirst("private/api/v1/auth", "api/v1/")
                        val response = userAuthServerProxy.proxy(
                                uri,
                                call.request.httpMethod.value,
                                call.receiveText(),
                                mapOf(
                                        Pair("Authorization", call.request.authorization()),
                                        Pair("Accept-Language", call.request.acceptLanguage())
                                )
                        )
                        call.respondText(response, ContentType.Application.Json)
                    }
                }
            }
            authenticate {
                route("device") {
                    post {
                        call.receive<DeviceValue>().let { request ->
                            userDeviceController.createDevice(call.userId, request)
                        }.also { call.respond(it) }
                    }
                    put("{deviceId}") {
                        call.receive<DeviceValue>().let { request ->
                            userDeviceController.updateDevice(
                                    call.userId,
                                    call.parameters["deviceId"]!!.toLong(),
                                    request
                            )
                        }.also { call.respond("") }
                    }
                    post("find") {
                        call.receive<FindDevicesRequest>().let { request ->
                            userDeviceController.findDevices(call.userId, request)
                        }.also { call.respond(it) }
                    }
                    delete("{deviceId}") {
                        userDeviceController.removeDevice(call.userId, call.parameters["deviceId"]!!.toLong())
                        call.respond("")
                    }
                }
                route("manufacturer") {
                    get {
                        manufacturerController.getManufacturer(call.userId).also { call.respond(it) }
                    }
                    post {
                        manufacturerController.createManufacturer(call.userId).also { call.respond(it) }
                    }
                }
            }
        }
    }
}