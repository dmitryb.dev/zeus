package zeus.apiserver.entrypoint.publicapi

import com.fasterxml.jackson.databind.*
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.websocket.*
import zeus.apiserver.entrypoint.publicapi.v1.device.command.*
import zeus.apiserver.entrypoint.publicapi.v1.device.command.submit.*
import zeus.apiserver.entrypoint.publicapi.v1.device.event.*
import zeus.apiserver.entrypoint.publicapi.v1.device.status.*
import zeus.apiserver.entrypoint.util.*
import javax.inject.*

class PublicRoutes @Inject constructor(
        private val commandController: CommandController,
        private val webSocketControllerProvider: Provider<WebSocketController>,
        private val deviceStatusController: DeviceStatusController
) {
    fun v1Route(routing: Routing) = routing.apply {
        route("/api/v1") {
            route("device") {
                route("command") {
                    post("unacked") {

                    }
                    post("submit") {
                        call.receive<SubmitCommandRequest>().let {
                            commandController.submit(it)
                        }.also { call.respond(it) }
                    }
                }
                webSocket("event") {
                    val controller = webSocketControllerProvider.get()
                    handleWS<JsonNode> { receiveChannel -> controller.handle(receiveChannel) }
                }
                post("status") {
                    call.receive<StatusRequest>().let {
                        deviceStatusController.getStatus(it)
                    }.also { call.respond(it) }
                }
            }
        }
    }
}