package zeus.apiserver.entrypoint.publicapi.v1.device.status

class StatusResponse(val online: Boolean)