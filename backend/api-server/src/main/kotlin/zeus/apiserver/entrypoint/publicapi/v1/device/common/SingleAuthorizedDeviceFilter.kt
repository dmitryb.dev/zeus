package zeus.apiserver.entrypoint.publicapi.v1.device.common

import common.domain.device.*

class SingleAuthorizedDeviceFilter(
        val manufacturerId: Long,
        val deviceNumber: Long,
        val tags: Collection<String> = emptyList(),
        val auth: Auth
) {
    fun toDomain() = SingleDeviceFilter {
        manufacturerId = this@SingleAuthorizedDeviceFilter.manufacturerId
        deviceNumber = this@SingleAuthorizedDeviceFilter.deviceNumber
        addAllTags(this@SingleAuthorizedDeviceFilter.tags)
    }
}