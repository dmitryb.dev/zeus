package zeus.apiserver.entrypoint.publicapi.v1.device.command.submit

class SubmitCommandResponse(
        val commandId: Long,
        val commnad: Collection<Int>,
        val value: Collection<Value>
) {
    val status = "Waiting for device"
}

class Value(val position: Int, val type: ValueType, val value: Any?)

enum class ValueType {
    int32, float, string, valueNotSet
}