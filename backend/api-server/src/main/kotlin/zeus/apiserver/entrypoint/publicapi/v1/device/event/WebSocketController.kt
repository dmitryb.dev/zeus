package zeus.apiserver.entrypoint.publicapi.v1.device.event

import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.node.*
import common.domain.device.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import zeus.apiserver.entrypoint.publicapi.v1.device.common.*
import zeus.apiserver.entrypoint.publicapi.v1.device.event.subscribe.*
import zeus.apiserver.entrypoint.publicapi.v1.device.event.subscribe.Device
import zeus.apiserver.entrypoint.util.*
import zeus.apiserver.infrastructure.device.event.*
import javax.inject.*

class WebSocketController @Inject constructor(private val deviceEventServerClient: DeviceEventServerClient) {

    private var subscription: ReceiveChannel<device.event.domain.Event.DeviceEventEntity>? = null

    suspend fun handle(receiveChannel: ReceiveChannel<JsonNode>): ReceiveChannel<Event> = GlobalScope.produce {
        for (msg in receiveChannel) {
            val request = msg.asType<SubscribeRequest>()

            val eventChannel = request.startFrom?.let { startFrom ->
                deviceEventServerClient.subscribe(request.device.toDomain(), startFrom)
            } ?: deviceEventServerClient.subscribe(request.device.toDomain())

            subscription?.cancel()
            subscription = eventChannel

            val transformers = request.valueTransform
                    .associateBy { it.event }
                    .mapValues { it.value::transform }

            GlobalScope.launch {
                for (eventEntity in eventChannel) {
                    send(Event(
                            eventEntity.eventId,
                            eventEntity.event.meta.deviceId.let {
                                Device(it.manufacturerId, it.deviceNumber, it.tagsList)
                            },
                            eventEntity.event.typeList,
                            (transformers[eventEntity.event.typeList] ?: ::valuesAsJsonArray)(eventEntity.event.valuesList)
                    ))
                }
            }
        }
    }
}

private fun valuesAsJsonArray(values: List<Values.Value>): JsonNode {
    val array = JsonNodeFactory.instance.arrayNode()
    values.forEach { array.add(it.toJsonNode()) }
    return array
}

private fun ValueTransform.transform(values: List<Values.Value>): JsonNode = valuesAsJsonNode(mapping, values)

private fun valuesAsJsonNode(mapping: JsonNode, values: List<Values.Value>): JsonNode = when {
    mapping.isObject -> {
        val node = JsonNodeFactory.instance.objectNode()
        mapping.fields().forEach { fieldMapping ->
            node.set(fieldMapping.key, valuesAsJsonNode(fieldMapping.value, values))
        }
        node
    }

    mapping.isArray -> {
        val node = JsonNodeFactory.instance.arrayNode()
        mapping.forEach { item ->
            node.add(valuesAsJsonNode(item, values))
        }
        node
    }

    values.size > mapping.asInt() -> values[mapping.asInt()].toJsonNode()

    else -> NullNode.instance
}