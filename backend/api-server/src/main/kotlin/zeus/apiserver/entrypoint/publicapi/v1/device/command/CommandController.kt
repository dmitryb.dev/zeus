package zeus.apiserver.entrypoint.publicapi.v1.device.command

import com.fasterxml.jackson.databind.*
import common.domain.device.*
import device.command.domain.*
import io.ktor.features.*
import zeus.apiserver.entrypoint.publicapi.v1.device.command.submit.*
import zeus.apiserver.infrastructure.device.command.*
import javax.inject.*

class CommandController @Inject constructor(private val commandServerClient: DeviceCommandServerClient) {

    suspend fun submit(request: SubmitCommandRequest): SubmitCommandResponse {
        val command = DeviceCommandValue {
            deviceFilter = DeviceFilter {
                manufacturerId = request.deviceFilter.manufacturerId
                request.deviceFilter.deviceNumber?.let { deviceNumber {
                    number = it
                }}
                addAllTags(request.deviceFilter.tags)
            }
            addAllType(request.command)
            timeoutMs = request.meta.timeoutMs
            request.value?.let { addAllValues(it.toValueDomain(request.fieldOrder)) }
        }

        val commandId = commandServerClient.push(command)

        fun convertValue(position: Int, value: Values.Value) = when (value.typeCase) {
            Values.Value.TypeCase.INT32 -> Value(position, ValueType.int32, value.int32)
            Values.Value.TypeCase.FLOAT -> Value(position, ValueType.float, value.float)
            Values.Value.TypeCase.STRING -> Value(position, ValueType.string, value.string)
            Values.Value.TypeCase.TYPE_NOT_SET -> Value(position, ValueType.valueNotSet, null)
            else -> throw IllegalStateException("Can't convert value of type ${value.typeCase.name}")
        }

        return SubmitCommandResponse(
                commandId,
                command.typeList,
                command.valuesList.mapIndexed(::convertValue)
        )
    }

}

private fun JsonNode.toValueDomain(
        fieldOrder: Collection<String>?
): Collection<Values.Value> = when {

    isMissingNode || isNull -> listOf(Value {})
    isTextual -> listOf(Value { string = textValue() })
    isFloatingPointNumber -> listOf(Value { float = floatValue() })
    isInt -> listOf(Value { int32 = intValue() })
    isArray -> flatMap { node -> node.toValueDomain(fieldOrder) }
    isObject -> {
        if (fieldOrder == null) {
            fields().asSequence()
                    .flatMap { it.value.toValueDomain(null).asSequence() }
                    .toList()
        } else {
            val fields = flatFields(null)
            fieldOrder.flatMap { fields[it]?.toValueDomain(fieldOrder) ?: listOf(Value {}) }
        }
    }

    else -> throw BadRequestException("Unsupported value type. It's allowed to use only int32, float, strings, arrays " +
            "and objects")
}

private fun JsonNode.flatFields(parentField: String?): Map<String, JsonNode> {
    val basePath = parentField?.let { "$it." } ?: ""

    return this.fields().asSequence()
            .flatMap { entry -> when {
                entry.value.isObject -> entry.value.flatFields(basePath + entry.key)
                        .entries.map { Pair(it.key, it.value) }
                        .asSequence()
                else -> sequenceOf(Pair(basePath + entry.key, entry.value))
            }}
            .toMap()
}