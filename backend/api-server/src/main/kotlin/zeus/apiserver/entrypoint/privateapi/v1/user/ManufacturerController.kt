package zeus.apiserver.entrypoint.privateapi.v1.user

import io.ktor.features.*
import zeus.apiserver.infrastructure.user.account.manufacturer.*
import javax.inject.*

class ManufacturerController @Inject constructor(
        private val manufacturerServerClient: ManufacturerServerClient
) {
    suspend fun getManufacturer(userId: Long): ManufacturerResponse {
        val manufacturerId = manufacturerServerClient.getManufacturer(userId)
        return manufacturerId?.let { ManufacturerResponse(it) } ?: throw NotFoundException()
    }

    suspend fun createManufacturer(userId: Long): ManufacturerResponse {
        val manufacturerId = manufacturerServerClient.createManufacturer(userId)
        return ManufacturerResponse(manufacturerId)
    }
}


class ManufacturerResponse(val manufacturerId: Long)