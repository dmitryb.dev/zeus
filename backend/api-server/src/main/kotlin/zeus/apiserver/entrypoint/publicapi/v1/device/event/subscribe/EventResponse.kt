package zeus.apiserver.entrypoint.publicapi.v1.device.event.subscribe

import com.fasterxml.jackson.databind.*
import common.domain.device.*

class Event(
        val eventId: Long,
        val device: Device,
        val event: Collection<Int>,
        val value: JsonNode
)

class Device(val manufacturerId: Long, val deviceNumber: Long, val tags: Collection<String>)