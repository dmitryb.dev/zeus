package zeus.apiserver.entrypoint.util

import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.routing.*
import io.ktor.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import org.slf4j.*

val mapper = jacksonObjectMapper()
val log = LoggerFactory.getLogger(WebSocketSession::class.java)

suspend inline fun <reified Req> WebSocketSession.handleWS(
        crossinline controller: suspend (ReceiveChannel<Req>) -> ReceiveChannel<*>
) {
    val channel = GlobalScope.produce<Req> {
        for (frame in incoming) {
            when (frame) {
                is Frame.Text -> catchJsonErrors {
                    send(mapper.readValue(frame.readText()))
                }
            }
        }
    }

    while (!channel.isClosedForReceive) {
        try {
            catchJsonErrors {
                for (msg in controller(channel)) outgoing.sendJson(msg)
            }
        } catch (e: BadRequestException) {
            outgoing.sendJson(WSError(HttpStatusCode.BadRequest, e.message ?: "Incorrect request"))
        } catch (e: Exception) {
            log.error("Unhandled error", e)
            outgoing.sendJson(WSError(HttpStatusCode.InternalServerError, "Internal server error"))
        }
    }


}

inline fun <reified T> JsonNode.asType() = mapper.treeToValue<T>(this)

suspend fun SendChannel<Frame>.sendJson(value: Any?) {
    send(Frame.Text(mapper.writeValueAsString(value)))
}

class WSError(val code: Int, val error: String) {
    constructor(code: HttpStatusCode, error: String) : this(code.value, error)
}

suspend fun WebSocketSession.catchJsonErrors(action: suspend () -> Unit) {
    try {
        action()
    } catch (e: JsonParseException) {
        outgoing.sendJson(WSError(
                HttpStatusCode.BadRequest,
                "Incorrect JSON, syntax error at ${e.location.lineNr}:${e.location.columnNr}"
        ))
    } catch (e: JsonMappingException) {
        outgoing.sendJson(WSError(HttpStatusCode.BadRequest, e.cause?.message ?: "Incorrect request"))
    }
}