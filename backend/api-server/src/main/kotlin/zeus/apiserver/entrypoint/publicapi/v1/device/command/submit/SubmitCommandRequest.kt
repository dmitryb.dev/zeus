package zeus.apiserver.entrypoint.publicapi.v1.device.command.submit

import com.fasterxml.jackson.databind.*
import common.domain.device.*
import device.command.domain.*
import io.ktor.features.*
import zeus.apiserver.entrypoint.publicapi.v1.device.common.*

class SubmitCommandRequest(
        val deviceFilter: AuthorizedDeviceFilter,
        val meta: Meta = Meta(),
        val command: Collection<Int>,
        val fieldOrder: Collection<String>?,
        val value: JsonNode?
)

class Meta(
        val delivery: Delivery = Delivery.AT_LEAST_ONCE,
        val timeoutMs: Long = 60000
)

enum class Delivery {
    AT_LEAST_ONCE,
    AT_MOST_ONCE
}

