package zeus.apiserver.entrypoint.publicapi.v1.device.common

import io.ktor.features.*

data class Auth(val role: String?, val pass: String?, val secret: String?) {

    init {
        if ((role == null || pass == null) && secret == null) {
            throw BadRequestException("role + pass or secret must be provided in auth object")
        }
    }

}