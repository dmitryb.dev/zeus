package zeus.apiserver

import dagger.Component
import zeus.apiserver.module.EntryPointModule
import zeus.apiserver.module.prod.*
import javax.inject.Singleton

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ConfigModule::class,
    InfrastructureModule::class
])
interface ProdApiServer : ApiServer
