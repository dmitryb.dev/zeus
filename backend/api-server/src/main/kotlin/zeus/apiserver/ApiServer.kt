package zeus.apiserver;

import io.ktor.server.engine.*

interface ApiServer {
    fun httpServer(): ApplicationEngine
}