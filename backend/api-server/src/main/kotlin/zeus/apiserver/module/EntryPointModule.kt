package zeus.apiserver.module

import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import com.fasterxml.jackson.core.*
import com.fasterxml.jackson.databind.*
import com.typesafe.config.*
import dagger.*
import dagger.Module
import io.grpc.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.client.features.*
import io.ktor.client.response.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.websocket.*
import zeus.apiserver.domain.*
import zeus.apiserver.entrypoint.privateapi.*
import zeus.apiserver.entrypoint.publicapi.*
import java.time.*


@Module
object EntryPointModule {

    @JvmStatic
    @Provides fun provideApiServer(
            config: Config,
            jwtSignAlgorithm: Algorithm,
            publicRoutes: PublicRoutes,
            privateRoutes: PrivateRoutes
    ) : ApplicationEngine = embeddedServer(Netty, config.getInt("http.server.port")) {
        install(Authentication) {
            jwt {
                verifier(JWT.require(jwtSignAlgorithm).build())
                validate { credential ->
                    if (credential.payload.getClaim("type").asString() == "signIn") {
                        UserPrincipal(credential.payload.getClaim("userId").asLong())
                    } else {
                        null
                    }
                }
            }
        }
        install(ContentNegotiation) {
            register(ContentType.Application.Json, JacksonConverter())
        }
        install(StatusPages) {
            exception<JsonParseException> { cause ->
                call.respond(HttpStatusCode.BadRequest, cause.cause?.message ?: "Incorrect JSON")
            }
            exception<JsonMappingException> { cause ->
                call.respond(HttpStatusCode.BadRequest, cause.cause?.message ?: "Incorrect request")
            }
            exception<NotFoundException> { call.respond(HttpStatusCode.NotFound) }
            exception<BadRequestException> { cause ->
                call.respond(HttpStatusCode.BadRequest, cause.message ?: "Incorrect request")
            }
            exception<BadResponseStatusException> { cause ->
                call.respond(cause.statusCode, cause.response.readText())
            }
            exception<StatusRuntimeException> { when (it.status.code) {
                Status.Code.CANCELLED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.INVALID_ARGUMENT -> call.respond(HttpStatusCode.BadRequest)
                Status.Code.DEADLINE_EXCEEDED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.NOT_FOUND -> call.respond(HttpStatusCode.NotFound)
                Status.Code.ALREADY_EXISTS -> call.respond(HttpStatusCode.Conflict)
                Status.Code.PERMISSION_DENIED -> call.respond(HttpStatusCode.Forbidden)
                Status.Code.FAILED_PRECONDITION -> call.respond(HttpStatusCode.PreconditionFailed)
                Status.Code.ABORTED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.OUT_OF_RANGE -> call.respond(HttpStatusCode.BadRequest)
                Status.Code.UNAUTHENTICATED -> call.respond(HttpStatusCode.Unauthorized)
                else -> call.respond(HttpStatusCode.InternalServerError)
            }}
        }
        install(WebSockets) {
            pingPeriod = Duration.ofSeconds(10)
        }
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Delete)
            method(HttpMethod.Put)
            method(HttpMethod.Patch)
            header(HttpHeaders.Authorization)
            anyHost()
        }
        routing {
            publicRoutes.v1Route(this)
            privateRoutes.v1Route(this)
        }
    }

}
