package zeus.apiserver.module.dev

import com.typesafe.config.*
import dagger.*

@Module
object DevConfigModule {

    @JvmStatic
    @Provides
    fun provideConfig() = ConfigFactory
            .load("application-dev.conf")
            .withFallback(ConfigFactory.load())

}
