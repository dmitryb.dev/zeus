package zeus.apiserver.module.prod

import com.auth0.jwt.algorithms.*
import com.typesafe.config.*
import dagger.*
import device.command.rpc.*
import device.event.rpc.*
import device.messaging.server.rpc.*
import io.grpc.*
import user.account.rpc.*
import user.auth.loginpass.*
import zeus.apiserver.infrastructure.device.command.*
import zeus.apiserver.infrastructure.device.event.*
import zeus.apiserver.infrastructure.device.messaging.*
import zeus.apiserver.infrastructure.user.account.device.*
import zeus.apiserver.infrastructure.user.account.manufacturer.*
import zeus.apiserver.infrastructure.user.auth.proxy.*
import zeus.apiserver.infrastructure.user.auth.stream.*
import java.security.*
import java.security.interfaces.*
import java.security.spec.*
import java.util.*

@Module
object InfrastructureModule {

    @JvmStatic
    @Provides
    fun provideSignAlgorithm(config: Config): Algorithm {
        val keyFactory = KeyFactory.getInstance("RSA")
        val privateKey = keyFactory.generatePrivate(
                PKCS8EncodedKeySpec(Base64.getDecoder().decode(
                        config.getString("jwt.private")
                                .replace("\\n".toRegex(), "")
                                .replace("-----BEGIN PRIVATE KEY-----", "")
                                .replace("-----END PRIVATE KEY-----", "")
                                .replace(" ".toRegex(), "")
                ))
        ) as RSAPrivateKey
        val publicKey = keyFactory.generatePublic(
                X509EncodedKeySpec(Base64.getDecoder().decode(
                        config.getString("jwt.public")
                                .replace("\\n".toRegex(), "")
                                .replace("-----BEGIN PUBLIC KEY-----", "")
                                .replace("-----END PUBLIC KEY-----", "")
                                .replace(" ".toRegex(), "")
                ))
        ) as RSAPublicKey

        return Algorithm.RSA256(publicKey, privateKey)
    }

    @JvmStatic
    @Provides
    fun provideDeviceCommandStub(config: Config) = DeviceCommandProducerGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("device-command.client.host"),
                    config.getInt("device-command.client.port")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideDeviceEventStub(config: Config) = DeviceEventBrokerGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("device-event.client.host"),
                    config.getInt("device-event.client.port")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideDeviceStatusStub(config: Config) = DeviceStatusGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("device-messaging.client.host"),
                    config.getInt("device-messaging.client.port")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideUserAuthLoginPassStub(config: Config) = LoginPassAuthGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("user-auth.client.host"),
                    config.getInt("user-auth.client.grpcPort")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideUserDeviceStub(config: Config) = DeviceGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("user-account.client.host"),
                    config.getInt("user-account.client.port")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideManufacturerStub(config: Config) = ManufacturerGrpc.newStub(
            ManagedChannelBuilder.forAddress(
                    config.getString("user-account.client.host"),
                    config.getInt("user-account.client.port")
            )
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideDeviceCommandServerClient(client: GrpcDeviceCommandServerClient): DeviceCommandServerClient = client

    @JvmStatic
    @Provides
    fun provideDeviceEventServerClient(client: GrpcDeviceEventServerClient): DeviceEventServerClient = client

    @JvmStatic
    @Provides
    fun provideDeviceMessagingServerClient(
            client: GrpcDeviceMessagingServerClient
    ): DeviceMessagingServerClient = client

    @JvmStatic
    @Provides
    fun provideUserAuthServerProxy(config: Config): UserAuthServerProxy = HttpUserAuthServerProxy(
            config.getString("user-auth.client.host") + ":" + config.getString("user-auth.client.httpPort")
    )

    @JvmStatic
    @Provides
    fun provideUserAuthServerStreams(
            client: GrpcUserAuthServerStreams
    ): UserAuthServerStreams = client

    @JvmStatic
    @Provides
    fun provideUserDeviceServerClient(client: GrpcUserDeviceServerClient): UserDeviceServerClient = client

    @JvmStatic
    @Provides
    fun provideManufacturerServerClient(client: GrpcManufacturerServerClient): ManufacturerServerClient = client

}