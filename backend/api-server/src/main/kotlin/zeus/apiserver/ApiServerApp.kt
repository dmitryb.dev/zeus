package zeus.apiserver

import zeus.util.*

fun main(args: Array<String>) {
    val apiServer = ModeFactory
            .withDefaultMode<ApiServer>("prod", DaggerProdApiServer::create)
            .withMode("dev", DaggerDevApiServer::create)
            .create(args)

    apiServer.httpServer().start(wait = true)
}
