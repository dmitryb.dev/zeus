package zeus.apiserver.domain

import io.ktor.application.*
import io.ktor.auth.*

class UserPrincipal(val userId: Long) : Principal

val ApplicationCall.userId get() = this.principal<UserPrincipal>()?.userId!!