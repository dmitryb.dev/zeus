package zeus.apiserver.domain

import common.domain.device.*

class DeviceStatus(val filter: Filters.SingleDeviceFilter, val isOnline: Boolean)