package zeus.apiserver.infrastructure.user.auth.stream

typealias SignInToken = String

interface UserAuthServerStreams {
    suspend fun waitForEmailConfirmation(login: String, pass: String): SignInToken
    suspend fun waitForPassChange(email: String)
}