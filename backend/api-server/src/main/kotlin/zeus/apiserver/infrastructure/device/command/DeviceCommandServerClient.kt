package zeus.apiserver.infrastructure.device.command

import device.command.domain.*

interface DeviceCommandServerClient {
    suspend fun push(command: Command.DeviceCommandValue): Long
}