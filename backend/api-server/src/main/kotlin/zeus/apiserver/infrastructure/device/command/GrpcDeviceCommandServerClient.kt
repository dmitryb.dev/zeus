package zeus.apiserver.infrastructure.device.command

import device.command.domain.*
import device.command.rpc.*
import javax.inject.*

class GrpcDeviceCommandServerClient @Inject constructor(
        private val client: DeviceCommandProducerGrpc.DeviceCommandProducerStub
) : DeviceCommandServerClient {

    override suspend fun push(command: Command.DeviceCommandValue): Long {
        return client.push {
            this.command = command
        }.commandId
    }
}