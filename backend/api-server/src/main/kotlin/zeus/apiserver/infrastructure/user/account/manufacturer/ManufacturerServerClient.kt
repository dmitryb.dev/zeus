package zeus.apiserver.infrastructure.user.account.manufacturer

typealias ManufacturerId = Long

interface ManufacturerServerClient {

    suspend fun createManufacturer(userId: Long): ManufacturerId
    suspend fun getManufacturer(userId: Long): ManufacturerId?

}