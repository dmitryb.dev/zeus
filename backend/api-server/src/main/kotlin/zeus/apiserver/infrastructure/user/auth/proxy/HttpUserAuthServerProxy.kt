package zeus.apiserver.infrastructure.user.auth.proxy

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.content.*

class HttpUserAuthServerProxy(private val userAuthServerUrl: String) : UserAuthServerProxy {

    override suspend fun proxy(uri: String, method: String, body: String, headers: Map<String, String?>): String {
        return httpClient.request {
            url("http://$userAuthServerUrl/$uri")
            this.method = HttpMethod(method)
            this.body = TextContent(body, ContentType.Application.Json)
            headers.forEach { (header, value) -> this.header(header, value) }
        }
    }

    companion object {
        private val httpClient = HttpClient(CIO)
    }
}