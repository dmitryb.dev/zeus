package zeus.apiserver.infrastructure.user.account.manufacturer

import user.account.rpc.*
import javax.inject.*

class GrpcManufacturerServerClient @Inject constructor(
        private val client: ManufacturerGrpc.ManufacturerStub
) : ManufacturerServerClient {

    override suspend fun createManufacturer(
            userId: Long
    ): ManufacturerId = client.createManufacturer { this.userId = userId }.manufacturerId

    override suspend fun getManufacturer(
            userId: Long
    ): ManufacturerId = client.getManufacturer { this.userId = userId }.manufacturerId
}