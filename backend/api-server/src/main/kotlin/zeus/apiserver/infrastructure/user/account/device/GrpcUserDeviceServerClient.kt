package zeus.apiserver.infrastructure.user.account.device

import kotlinx.coroutines.channels.*
import user.account.dto.*
import user.account.rpc.*
import javax.inject.*

class GrpcUserDeviceServerClient @Inject constructor(
        private val client: DeviceGrpc.DeviceStub
) : UserDeviceServerClient {

    override suspend fun addDevice(
            request: DeviceDtos.AddDeviceRequest
    ): DeviceId = client.addDevice(request).deviceId

    override suspend fun findDevices(
            request: DeviceDtos.FindUserDevicesRequest
    ): Collection<DeviceDtos.UserDeviceResponse> = client.findUserDevices(request).toList()

    override suspend fun updateDevice(request: DeviceDtos.UpdateDeviceRequest) {
        client.updateDevice(request)
    }

    override suspend fun removeDevice(userId: Long, deviceId: Long) {
        client.removeDevice {
            this.userId = userId
            this.userDeviceId = deviceId
        }
    }
}