package zeus.apiserver.infrastructure.user.auth.proxy

interface UserAuthServerProxy {

    suspend fun proxy(uri: String, method: String, body: String, headers: Map<String, String?>): String
}