package zeus.apiserver.infrastructure.device.event

import common.domain.device.*
import device.event.domain.*
import device.event.rpc.*
import kotlinx.coroutines.channels.*
import javax.inject.*

class GrpcDeviceEventServerClient @Inject constructor(
        private val client: DeviceEventBrokerGrpc.DeviceEventBrokerStub
) : DeviceEventServerClient {

    override suspend fun subscribe(filter: Filters.SingleDeviceFilter): ReceiveChannel<Event.DeviceEventEntity> {
        return client.subscribe(SubscribeRequest {
            this.filter = filter
        })
    }

    override suspend fun subscribe(
            filter: Filters.SingleDeviceFilter,
            startFromEventId: Long
    ): ReceiveChannel<Event.DeviceEventEntity> {
        return client.subscribe(SubscribeRequest {
            this.filter = filter
            this.startFromEventId = startFromEventId
        })
    }
}