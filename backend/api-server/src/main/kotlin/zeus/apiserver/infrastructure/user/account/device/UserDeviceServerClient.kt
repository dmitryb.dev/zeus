package zeus.apiserver.infrastructure.user.account.device

import user.account.dto.*

typealias DeviceId = Long

interface UserDeviceServerClient {

    suspend fun addDevice(request: DeviceDtos.AddDeviceRequest): DeviceId
    suspend fun findDevices(request: DeviceDtos.FindUserDevicesRequest): Collection<DeviceDtos.UserDeviceResponse>
    suspend fun updateDevice(request: DeviceDtos.UpdateDeviceRequest)
    suspend fun removeDevice(userId: Long, deviceId: Long)

}