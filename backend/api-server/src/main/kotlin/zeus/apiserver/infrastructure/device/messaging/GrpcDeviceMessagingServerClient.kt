package zeus.apiserver.infrastructure.device.messaging

import common.domain.device.*
import device.messaging.server.rpc.*
import kotlinx.coroutines.channels.*
import zeus.apiserver.domain.*
import javax.inject.*

class GrpcDeviceMessagingServerClient @Inject constructor(
        private val client: DeviceStatusGrpc.DeviceStatusStub
) : DeviceMessagingServerClient {

    override suspend fun getStatuses(filters: Collection<Filters.SingleDeviceFilter>): Collection<DeviceStatus> {
        val channel = client.getOnlineStatuses()

        for (filter in filters) {
            channel.requestChannel.send(IsDeviceOnlineRequest {
                deviceFilter = filter
            })
        }
        channel.requestChannel.close()

        return channel.responseChannel.map { response ->
            DeviceStatus(response.deviceFilter, response.isOnline)
        }.toList()
    }
}