package zeus.apiserver.infrastructure.device.event

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.channels.*

interface DeviceEventServerClient {

    suspend fun subscribe(filter: Filters.SingleDeviceFilter): ReceiveChannel<Event.DeviceEventEntity>
    suspend fun subscribe(
            filter: Filters.SingleDeviceFilter,
            startFromEventId: Long
    ): ReceiveChannel<Event.DeviceEventEntity>

}