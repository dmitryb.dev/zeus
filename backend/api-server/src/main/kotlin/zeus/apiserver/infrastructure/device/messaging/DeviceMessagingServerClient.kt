package zeus.apiserver.infrastructure.device.messaging

import common.domain.device.*
import zeus.apiserver.domain.*

interface DeviceMessagingServerClient {

    suspend fun getStatuses(filters: Collection<Filters.SingleDeviceFilter>): Collection<DeviceStatus>

}