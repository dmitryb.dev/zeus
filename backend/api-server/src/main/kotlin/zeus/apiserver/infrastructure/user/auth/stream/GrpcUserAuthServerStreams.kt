package zeus.apiserver.infrastructure.user.auth.stream

import io.grpc.*
import kotlinx.coroutines.*
import user.auth.loginpass.*
import zeus.util.log.*
import javax.inject.*

class GrpcUserAuthServerStreams @Inject constructor(
        private val loginPassStub: LoginPassAuthGrpc.LoginPassAuthStub
) : UserAuthServerStreams {

    override suspend fun waitForEmailConfirmation(login: String, pass: String): SignInToken {
        return try {
            loginPassStub.waitForEmailConfirmation {
                this.login = login
                this.pass = pass
            }.signInToken
        } catch (ex: StatusRuntimeException) {
            throw ex
        } catch (ex: Exception) {
            log.error("Failed to handle wait for email confirmation request", ex)
            delay(1000)
            waitForEmailConfirmation(login, pass)
        }
    }

    override suspend fun waitForPassChange(email: String) {
        try {
            loginPassStub.waitForPassChange { this.email = email }
        } catch (ex: StatusRuntimeException) {
            throw ex
        } catch (ex: Exception) {
            log.error("Failed to handle wait for pass change request", ex)
            delay(1000)
            waitForPassChange(email)
        }
    }

    companion object {
        private val log = logger<GrpcUserAuthServerStreams>()
    }
}