package zeus.device.command.service

import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.channels.*
import zeus.device.command.infrastructure.command.arrived.*
import zeus.device.command.infrastructure.command.handled.*
import zeus.device.command.infrastructure.command.queue.*
import zeus.id.generator.local.*
import zeus.util.log.*
import java.util.*
import javax.inject.*

class CommandManager @Inject constructor(
        @Named("commandIdGenerator") private val commandIdGenerator: LocalIdGenerator,
        private val commandStore: CommandStore,
        private val handledCommandStore: HandledCommandStore,
        private val commandStream: CommandStream
) {

    suspend fun push(command: Command.DeviceCommandValue): Command.DeviceCommandEntity {
        val commandEntity = DeviceCommandEntity {
            commandId = commandIdGenerator.nextLocalId(command.deviceFilter.manufacturerId.toString())
            this.command = command
        }

        commandStore.save(commandEntity)
        commandStream.notifySubscribers(commandEntity)

        return commandEntity
    }

    suspend fun getNext(deviceId: DeviceIds.DeviceId): Command.DeviceCommandEntity {
        fun isMatchesFilter(commandEntity: Command.DeviceCommandEntity) =
                HashSet(deviceId.tagsList).containsAll(commandEntity.command.deviceFilter.tagsList)

        val commandsStream = commandStream.commandsStream(deviceId)

        suspend fun commandFromRepository() = commandStore.find(
                deviceId,
                handledCommandStore.getLastHandledCommandId(deviceId)?.let { it.inc() } ?: 0L
        )
                .find(::isMatchesFilter)

        val command = commandFromRepository()
                ?: commandsStream.map { commandFromRepository() }.find { it != null }

        commandsStream.cancel()
        return command ?: throw IllegalStateException()
    }

    suspend fun ack(deviceId: DeviceIds.DeviceId, commandId: Long) {
        handledCommandStore.saveHandledCommand(deviceId, commandId)
    }

    suspend fun allUnacked(deviceFilter: Filters.SingleDeviceFilter): ReceiveChannel<Command.DeviceCommandEntity> {
        fun isMatchesFilter(commandEntity: Command.DeviceCommandEntity) =
                commandEntity.command.deviceFilter.tagsList.containsAll(HashSet(deviceFilter.tagsList))

        val deviceId = DeviceId {
            manufacturerId = deviceFilter.manufacturerId
            deviceNumber = deviceFilter.deviceNumber
        }

        return commandStore.find(
                deviceId,
                handledCommandStore.getLastHandledCommandId(deviceId)?.let { it.inc() } ?: 0L
        )
                .filter { isMatchesFilter(it) }
    }

    companion object {
        val log = logger<CommandManager>()
    }
}
