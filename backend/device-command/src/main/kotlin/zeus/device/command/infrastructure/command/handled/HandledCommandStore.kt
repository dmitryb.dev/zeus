package zeus.device.command.infrastructure.command.handled

import common.domain.device.*

interface HandledCommandStore {

    suspend fun saveHandledCommand(deviceId: DeviceIds.DeviceId, commandId: Long)
    suspend fun getLastHandledCommandId(deviceId: DeviceIds.DeviceId): Long?

}
