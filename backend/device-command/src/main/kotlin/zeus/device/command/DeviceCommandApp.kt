package zeus.device.command

import zeus.util.*

fun main(args: Array<String>) {

    val deviceCommand = ModeFactory
            .withDefaultMode<DeviceCommand>("prod", DaggerProdDeviceCommand::create)
            .withMode("dev", DaggerDevDeviceCommand::create)
            .withMode("stub", DaggerStubDeviceCommand::create)
            .create(args)

    deviceCommand.initializer().init()
    deviceCommand.grpcServer().start()
}