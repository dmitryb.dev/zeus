package zeus.device.command

import dagger.*
import zeus.device.command.module.*
import zeus.device.command.module.dev.*
import zeus.device.command.module.dev.DevInfrastructureInitModule
import zeus.device.command.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    DevInfrastructureInitModule::class,
    DevConfigModule::class
])
interface DevDeviceCommand : DeviceCommand
