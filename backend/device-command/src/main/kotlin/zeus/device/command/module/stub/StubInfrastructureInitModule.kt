package zeus.device.command.module.stub

import dagger.*
import zeus.device.command.module.*
import javax.inject.*


@Module
object StubInfrastructureInitModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideInfrastructureInitializer(): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {}
    }

}
