package zeus.device.command.infrastructure.command.arrived

import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.future.*
import org.apache.pulsar.client.api.*
import zeus.util.log.*
import zeus.util.pulsar.*
import javax.inject.*
import kotlin.coroutines.*

class PulsarCommandStream @Inject constructor(private val pulsarClient: PulsarClient) : CommandStream {

    private val producerCache = ProducerCache(pulsarClient, DEVICE_COMMAND_SCHEMA)

    override suspend fun commandsStream(deviceId: DeviceIds.DeviceId): ReceiveChannel<Command.DeviceCommandEntity> =
            GlobalScope.produce(coroutineContext) {
                val deviceCommands = pulsarClient.read<Command.DeviceCommandEntity>(
                        String.format(TOPIC_FORMAT, deviceId.manufacturerId, deviceId.deviceNumber),
                        MessageId.latest
                )
                val manufacturerCommands = pulsarClient.read<Command.DeviceCommandEntity>(
                        String.format(BROADCAST_TOPIC_FORMAT, deviceId.manufacturerId),
                        MessageId.latest
                )

                async { for (command in deviceCommands) {
                    log.debug("Received device command with id: {}", command.commandId)
                    send(command)
                }}
                async { for (command in manufacturerCommands) {
                    log.debug("Received manufacturer command with id: {}", command.commandId)
                    send(command)
                }}
            }

    override suspend fun notifySubscribers(commandEntity: Command.DeviceCommandEntity) {
        val command = commandEntity.command
        val topic =
                if (command.deviceFilter.deviceNumber.number == BROADCAST_DEVICE_NUMBER)
                    String.format(BROADCAST_TOPIC_FORMAT, command.deviceFilter.manufacturerId)
                else
                    String.format(TOPIC_FORMAT, command.deviceFilter.manufacturerId,
                            command.deviceFilter.deviceNumber.number)

        producerCache.getClient(topic).sendAsync(commandEntity).await()

        log.debug("Command with id: {} was sent to the topic '{}'", commandEntity.commandId, topic)
    }

    companion object {
        private val log = logger<PulsarCommandStream>()

        private const val BROADCAST_DEVICE_NUMBER: Long = -1

        private const val TOPIC_FORMAT = "non-persistent://public/device-command/%s-%s"
        private const val BROADCAST_TOPIC_FORMAT = "non-persistent://public/device-command/%s"

        private val DEVICE_COMMAND_SCHEMA = Schema.PROTOBUF(Command.DeviceCommandEntity::class.java)
    }
}
