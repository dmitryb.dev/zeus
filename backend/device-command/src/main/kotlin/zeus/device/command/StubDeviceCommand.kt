package zeus.device.command

import dagger.*
import zeus.device.command.module.*
import zeus.device.command.module.dev.*
import zeus.device.command.module.stub.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    StubInfrastructureModule::class,
    StubInfrastructureInitModule::class,
    DevConfigModule::class
])
interface StubDeviceCommand : DeviceCommand
