package zeus.device.command.module

import com.typesafe.config.*
import dagger.*
import zeus.device.command.entrypoint.*
import zeus.util.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideGrpcServer(
            config: Config,
            commandConsumer: CommandConsumer,
            commandProducer: CommandProducer
    ): GrpcServer {
        return GrpcServer(config.getInt("grpc.server.port"), commandConsumer, commandProducer)
    }

}
