package zeus.device.command.infrastructure.command.queue

import com.datastax.driver.core.*
import com.datastax.driver.core.querybuilder.*
import com.datastax.driver.mapping.*
import com.datastax.driver.mapping.annotations.*
import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.guava.*
import zeus.common.domain.device.*
import zeus.util.log.*
import java.nio.*
import java.util.*
import javax.inject.*
import kotlin.math.*

class CassandraCommandStore @Inject constructor(
        private val session: Session
) : CommandStore {

    private val mapper = MappingManager(session).mapper(Entity::class.java)

    override suspend fun save(entityToStore: Command.DeviceCommandEntity): Command.DeviceCommandEntity {
        log.debug("Storing command with id: {}, for device filter: {} stored", entityToStore.commandId,
                entityToStore.command.deviceFilter.toPrettyString())

        mapper.saveAsync(
                Entity(
                        entityToStore.commandId,
                        entityToStore.command.deviceFilter.manufacturerId,
                        entityToStore.command.deviceFilter.deviceNumber?.number ?: -1L,
                        HashSet(entityToStore.command.deviceFilter.tagsList),
                        entityToStore.command.typeList,
                        entityToStore.command.valuesList.map { value -> ByteBuffer.wrap(value.toByteArray()) },
                        System.currentTimeMillis() + entityToStore.command.timeoutMs
                ),
                Mapper.Option.ttl((entityToStore.command.timeoutMs / 1000).toInt())
        ).await()

        log.debug("Command stored")

        return entityToStore
    }

    override suspend fun find(
            deviceId: DeviceIds.DeviceId,
            startFromCommand: Long
    ): ReceiveChannel<Command.DeviceCommandEntity> = GlobalScope.produce {

        val resultSetFuture = session.executeAsync(QueryBuilder.select()
                .all().from(TABLE)
                .where(QueryBuilder.eq("manufacturer_id", deviceId.manufacturerId))
                .and(QueryBuilder.`in`("device_number", deviceId.deviceNumber, -1))
                .and(QueryBuilder.gte("command_id", startFromCommand))
        )

        for (command in mapper.mapAsync(resultSetFuture).await().all()) {
            send(command.toDomain())
        }
    }

    @Table(name = TABLE)
    private class Entity(
            @PartitionKey @Column(name = "command_id") var commandId: Long,
            @Column(name = "manufacturer_id") var manufacturerId: Long,
            @Column(name = "device_number") var deviceNumber: Long,
            @Column(name = "tags") var tags: Set<String>,
            @Column(name = "type") var type: List<Int>,
            @Column(name = "value") var values: List<ByteBuffer>,
            @Column(name = "deadline") var deadline: Long
    ) {
        private constructor() : this(0, 0, 0, emptySet(), emptyList(), emptyList(), 0)

        fun toDomain(): Command.DeviceCommandEntity = DeviceCommandEntity {
            commandId = this@Entity.commandId
            command {
                deviceFilter {
                    manufacturerId = this@Entity.manufacturerId
                    this@Entity.deviceNumber
                            .takeIf { it != -1L }
                            ?.let { deviceNumber { number = it } }
                    addAllTags(this@Entity.tags)
                }
                addAllType(this@Entity.type)
                addAllValues(values.map { buffer -> Values.Value.parseFrom(buffer) })
                timeoutMs = max(0, deadline - System.currentTimeMillis())
            }
        }
    }

    companion object {
        private const val TABLE = "commands"

        val log = logger<CassandraCommandStore>()
    }
}
