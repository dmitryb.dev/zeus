package zeus.device.command.entrypoint

import device.command.domain.*
import device.command.rpc.*
import kotlinx.coroutines.channels.*
import zeus.common.domain.device.*
import zeus.device.command.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class CommandProducer @Inject constructor(
        private val commandManager: CommandManager
) : DeviceCommandProducerCoroutineGrpc.DeviceCommandProducerImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun push(request: ProducerApi.PushRequest): ProducerApi.PushedResponse = log.uncaught {
        log.debug("Pushing command for filter: {}", request.command.deviceFilter.toPrettyString())

        val command = commandManager.push(request.command)

        log.debug("Command pushed with command id: {}", command.commandId)

        PushedResponse {
            commandId = command.commandId
        }
    }

    override suspend fun allUnackedCdommands(
            request: ProducerApi.AllUnackedCommandsRequest, responseChannel: SendChannel<Command.DeviceCommandEntity>
    ) = log.uncaught {
        commandManager.allUnacked(request.deviceFilter)
                .consumeEach { responseChannel.send(it) }
    }

    companion object {
        private val log = logger<CommandProducer>()
    }
}
