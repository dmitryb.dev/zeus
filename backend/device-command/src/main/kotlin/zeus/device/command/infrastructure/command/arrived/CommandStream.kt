package zeus.device.command.infrastructure.command.arrived

import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.channels.*

interface CommandStream {

    suspend fun commandsStream(deviceId: DeviceIds.DeviceId): ReceiveChannel<Command.DeviceCommandEntity>
    suspend fun notifySubscribers(commandEntity: Command.DeviceCommandEntity)

}
