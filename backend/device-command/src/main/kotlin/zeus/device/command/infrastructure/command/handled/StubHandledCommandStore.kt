package zeus.device.command.infrastructure.command.handled

import common.domain.device.*
import java.util.*
import javax.inject.*

@Singleton
class StubHandledCommandStore @Inject constructor() : HandledCommandStore {

    private val handled = HashMap<Long, HashMap<Long, Long>>()

    override suspend fun saveHandledCommand(deviceId: DeviceIds.DeviceId, commandId: Long) {
        handled.computeIfAbsent(deviceId.manufacturerId) { HashMap() }[deviceId.deviceNumber] = commandId
    }

    override suspend fun getLastHandledCommandId(deviceId: DeviceIds.DeviceId): Long? {
        return handled.computeIfAbsent(deviceId.manufacturerId) { HashMap() }[deviceId.deviceNumber]
    }
}
