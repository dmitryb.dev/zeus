package zeus.device.command.infrastructure.command.queue

import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import java.util.*
import javax.inject.*

@Singleton
class StubCommandStore @Inject constructor() : CommandStore {

    private val commands = HashMap<Long, HashMap<Long, HashMap<Long, Command.DeviceCommandEntity>>>()

    override suspend fun save(entityToStore: Command.DeviceCommandEntity): Command.DeviceCommandEntity {
        getCommandsMap(entityToStore.command.deviceFilter)[entityToStore.commandId] = entityToStore
        return entityToStore
    }

    override suspend fun find(deviceId: DeviceIds.DeviceId, startFromCommand: Long)
            : ReceiveChannel<Command.DeviceCommandEntity> = GlobalScope.produce {

        for (command in getCommandsMap(DeviceFilter {
            manufacturerId = deviceId.manufacturerId
            deviceNumber { number = deviceId.deviceNumber }
        }).values) {
            if (command.commandId >= startFromCommand) {
                send(command)
            }
        }

        for (command in getCommandsMap(DeviceFilter {
            manufacturerId = deviceId.manufacturerId
            deviceNumber { number = -1L }
        }).values) {
            if (command.commandId >= startFromCommand) {
                send(command)
            }
        }
    }

    private fun getCommandsMap(filter: Filters.DeviceFilter): MutableMap<Long, Command.DeviceCommandEntity> {
        return commands.computeIfAbsent(filter.manufacturerId) { HashMap() }
                .computeIfAbsent(filter.deviceNumber?.number ?: -1L) { HashMap() }
    }
}
