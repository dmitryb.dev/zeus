package zeus.device.command.infrastructure.command.queue

import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.channels.*

interface CommandStore {

    suspend fun save(entityToStore: Command.DeviceCommandEntity): Command.DeviceCommandEntity
    suspend fun find(
            deviceId: DeviceIds.DeviceId,
            startFromCommand: Long
    ): ReceiveChannel<Command.DeviceCommandEntity>

}
