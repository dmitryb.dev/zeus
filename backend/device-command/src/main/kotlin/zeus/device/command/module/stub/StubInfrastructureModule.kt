package zeus.device.command.module.stub

import dagger.*
import zeus.device.command.infrastructure.command.arrived.*
import zeus.device.command.infrastructure.command.handled.*
import zeus.device.command.infrastructure.command.queue.*
import zeus.id.generator.local.*
import javax.inject.*

@Module
object StubInfrastructureModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideCommandStore(commandRepository: StubCommandStore): CommandStore =
            commandRepository

    @JvmStatic
    @Provides
    @Singleton
    fun provideHandledCommandStore(
            handledCommandRepository: StubHandledCommandStore
    ): HandledCommandStore =
            handledCommandRepository


    @JvmStatic
    @Provides
    @Named("commandIdGenerator")
    fun provideCommandIdGenerator(idGenerator: StubLocalIdGenerator): LocalIdGenerator = idGenerator

    @JvmStatic
    @Provides
    @Singleton
    fun provideCommandStream(commandObservable: StubCommandStream): CommandStream = commandObservable

}
