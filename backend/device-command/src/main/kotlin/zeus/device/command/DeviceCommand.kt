package zeus.device.command

import zeus.device.command.module.*
import zeus.util.*

interface DeviceCommand {
    fun initializer(): InfrastructureInitializer
    fun grpcServer(): GrpcServer
}
