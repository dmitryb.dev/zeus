package zeus.device.command.module

interface InfrastructureInitializer {
    fun init()
}