package zeus.device.command

import dagger.*
import zeus.device.command.module.*
import zeus.device.command.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    InfrastructureInitModule::class,
    ConfigModule::class
])
interface ProdDeviceCommand : DeviceCommand
