package zeus.device.command.entrypoint

import device.command.rpc.*
import zeus.common.domain.device.*
import zeus.device.command.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class CommandConsumer @Inject constructor(
        private val commandManager: CommandManager
) : DeviceCommandConsumerCoroutineGrpc.DeviceCommandConsumerImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun next(request: ConsumerApi.NextRequest): ConsumerApi.NextResponse = log.uncaught {
        log.debug("Receiving next command for device {}", request.deviceId.toPrettyString())

        val command = commandManager.getNext(request.deviceId)

        log.debug("Received next command with id: {}", command.commandId)

        NextResponse {
            this.command = command
        }
    }

    override suspend fun ack(request: ConsumerApi.AckRequest): ConsumerApi.AckedResponse = log.uncaught {
        log.debug("Acking command with id {} for device {}", request.commandId, request.deviceId.toPrettyString())

        commandManager.ack(request.deviceId, request.commandId)

        log.debug("Command acked")

        AckedResponse {}
    }

    override suspend fun ackAndNext(request: ConsumerApi.AckAndNextRequest): ConsumerApi.NextResponse = log.uncaught {
        log.debug("Handling ack and next request")

        this.ack(AckRequest {
            deviceId = request.deviceId
            commandId = request.commandId
        })
        this.next(NextRequest {
            deviceId = request.deviceId
        })
    }

    companion object {
        private val log = logger<CommandConsumer>()
    }

}
