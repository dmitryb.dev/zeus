package zeus.device.command.infrastructure.command.handled

import com.datastax.driver.core.*
import com.datastax.driver.core.querybuilder.*
import com.datastax.driver.mapping.*
import com.datastax.driver.mapping.annotations.*
import common.domain.device.*
import kotlinx.coroutines.*
import kotlinx.coroutines.guava.*
import zeus.common.domain.device.*
import zeus.util.log.*

class CassandraHandledCommandStore(private val session: Session) : HandledCommandStore {

    private val mapper = MappingManager(session).mapper(Entity::class.java)

    override suspend fun saveHandledCommand(deviceId: DeviceIds.DeviceId, commandId: Long) {
        log.debug("Storing handled command id: {}", commandId)

        mapper.saveAsync(
                Entity(deviceId.manufacturerId, deviceId.deviceNumber, commandId)
        )
                .await()

        log.debug("Handled command stored", commandId)

        GlobalScope.launch { withContext(newTraceContext()) {
            removeOldIds(deviceId, commandId)
        }}
    }

    override suspend fun getLastHandledCommandId(deviceId: DeviceIds.DeviceId): Long? =
            mapper.getAsync(deviceId.manufacturerId, deviceId.deviceNumber)
                    .await()
                    ?.commandId

    private suspend fun removeOldIds(deviceId: DeviceIds.DeviceId, commandId: Long) {
        if (commandId % OLD_ID_DELETE_THRESHOLD == 0L) {
            try {
                log.debug("Removing command ids before id: {}, for device: {} was removed", commandId,
                        deviceId.toPrettyString())

                session.executeAsync(QueryBuilder.delete().from(TABLE)
                        .where(QueryBuilder.eq("manufacturer_id", deviceId.manufacturerId))
                        .and(QueryBuilder.eq("device_number", deviceId.deviceNumber))
                        .and(QueryBuilder.lt("command_id", commandId)))
                        .await()

                log.debug("Handled command ids removed")
            } catch (th: Throwable) {
                // It's just regular task for cleaning, so fail of this is not important
                log.warn("Can't remove old command ids", th)
            }
        }
    }

    @Table(name = TABLE)
    private class Entity(
            @PartitionKey @Column(name = "manufacturer_id") var manufacturerId: Long,
            @PartitionKey(1) @Column(name = "device_number") var deviceNumber: Long,
            @Column(name = "command_id") var commandId: Long
    ) {
        private constructor() : this(0L, 0L, 0)
    }

    companion object {
        private const val TABLE = "handled_commands"

        private const val OLD_ID_DELETE_THRESHOLD = 100

        val log = logger<CassandraHandledCommandStore>()
    }
}
