package zeus.device.command.infrastructure.command.arrived

import com.google.common.collect.*
import common.domain.device.*
import device.command.domain.*
import kotlinx.coroutines.channels.*
import java.util.*
import javax.inject.*

class StubCommandStream @Inject constructor() : CommandStream {

    private val channelsMap = Multimaps.newMultimap<DeviceIds.DeviceId, Channel<Command.DeviceCommandEntity>>(
            HashMap()) { ArrayList() }

    override suspend fun commandsStream(deviceId: DeviceIds.DeviceId): ReceiveChannel<Command.DeviceCommandEntity> {
        val channel = Channel<Command.DeviceCommandEntity>()
        channelsMap.put(DeviceId {
            manufacturerId = deviceId.manufacturerId
            deviceNumber = deviceId.deviceNumber
        }, channel)

        channelsMap.put(DeviceId {
            manufacturerId = deviceId.manufacturerId
            deviceNumber = -1L
        }, channel)

        return channel
    }

    override suspend fun notifySubscribers(commandEntity: Command.DeviceCommandEntity) {
        val deviceId = DeviceId {
            manufacturerId = commandEntity.command.deviceFilter.manufacturerId
            deviceNumber = commandEntity.command.deviceFilter.deviceNumber?.number ?: -1L
        }

        for (channel in channelsMap.get(deviceId)) {
            if (!channel.isClosedForSend) {
                channel.send(commandEntity)
            }
        }

        channelsMap.removeAll(deviceId)
    }
}
