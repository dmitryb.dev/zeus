package zeus.device.command.module.prod

import com.datastax.driver.core.*
import com.typesafe.config.*
import dagger.*
import io.lettuce.core.*
import org.apache.pulsar.client.admin.*
import org.apache.pulsar.client.api.*
import zeus.device.command.infrastructure.command.arrived.*
import zeus.device.command.infrastructure.command.handled.*
import zeus.device.command.infrastructure.command.queue.*
import zeus.id.generator.local.*
import zeus.util.pulsar.*
import javax.inject.*

@Module
object InfrastructureModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideCassandraSession(config: Config): Session = Cluster.builder()
            .addContactPoint(config.getString("cassandra.host"))
            .withPort(config.getInt("cassandra.port"))
            .build()
            .connect(config.getString("cassandra.keyspace"))

    @JvmStatic
    @Provides
    @Singleton
    fun provideRedisClient(config: Config): RedisClient {
        return RedisClient.create(
                RedisURI.builder()
                        .withHost(config.getString("redis.host"))
                        .withPort(config.getInt("redis.port"))
                        .withDatabase(config.getInt("redis.database"))
                        .build()
        )
    }

    @JvmStatic
    @Provides
    fun pulsarAdmin(config: Config): PulsarAdmin {
        return PulsarAdmin.builder()
                .serviceHttpUrl(String.format("http://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.http-port")))
                .enableTlsHostnameVerification(false)
                .build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun providePulsarClient(config: Config, pulsarAdmin: PulsarAdmin): PulsarClient {
        PulsarBootstrap.create(pulsarAdmin, "public/device-command")

        return PulsarClient.builder()
                .serviceUrl(String.format("pulsar://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.port")))
                .build()
    }

    @JvmStatic
    @Provides
    fun provideCommandStore(session: Session): CommandStore = CassandraCommandStore(session)

    @JvmStatic
    @Provides
    fun provideHandledCommandStore(session: Session): HandledCommandStore =
            CassandraHandledCommandStore(session)

    @JvmStatic
    @Provides
    @Named("commandIdGenerator")
    fun provideCommandIdGenerator(session: Session, redisClient: RedisClient): LocalIdGenerator {
        val table = CassandraRedisLocalIdGenerator.CassandraTable(
                "command_ids",
                "manufacturer_id",
                "command_id"
        )
        return CassandraRedisLocalIdGenerator(session, table, redisClient)
    }

    @JvmStatic
    @Provides
    fun provideCommandStream(pulsarClient: PulsarClient): CommandStream = PulsarCommandStream(pulsarClient)

}
