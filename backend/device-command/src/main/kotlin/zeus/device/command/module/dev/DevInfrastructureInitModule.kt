package zeus.device.command.module.dev

import com.datastax.driver.core.*
import com.typesafe.config.*
import dagger.*
import zeus.device.command.module.*
import zeus.device.command.module.prod.InfrastructureInitModule
import javax.inject.*


@Module
object DevInfrastructureInitModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideInfrastructureInitializer(config: Config): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {
            Cluster.builder()
                    .addContactPoint(config.getString("cassandra.host"))
                    .withPort(config.getInt("cassandra.port"))
                    .build()
                    .connect()
                    .execute("CREATE KEYSPACE IF NOT EXISTS ${config.getString("cassandra.keyspace")} WITH REPLICATION = {" +
                            "'class': 'SimpleStrategy'," +
                            "'replication_factor': 1" +
                            "}")

            InfrastructureInitModule.provideInfrastructureInitializer(config).init()
        }
    }

}
