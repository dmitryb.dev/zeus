package template

import dagger.*
import template.module.*
import template.module.dev.*
import template.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    DevConfigModule::class,
    ServiceModule::class,
    InfrastructureModule::class
])
interface StubTemplate : Template
