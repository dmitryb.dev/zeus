package template

import zeus.util.*

object TemplateApp {

    @JvmStatic
    fun main(args: Array<String>) {
        val template = ModeFactory
                .withDefaultMode<Template>("prod", DaggerProdTemplate::create)
                .withMode("dev", DaggerDevTemplate::create)
                .withMode("stub", DaggerStubTemplate::create)
                .create(args)

        template.grpcServer().start()
    }

}
