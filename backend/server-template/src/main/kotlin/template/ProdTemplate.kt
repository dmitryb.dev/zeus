package template

import dagger.*
import template.module.*
import template.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ConfigModule::class,
    ServiceModule::class,
    InfrastructureModule::class
])
interface ProdTemplate : Template
