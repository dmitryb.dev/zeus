package template

import template.module.dev.DevConfigModule
import template.module.EntryPointModule
import dagger.Component
import template.module.prod.*

import javax.inject.Singleton

@Singleton
@Component(modules = [
    EntryPointModule::class,
    DevConfigModule::class,
    ServiceModule::class,
    InfrastructureModule::class
])
interface DevTemplate : Template
