package template

import zeus.util.GrpcServer

interface Template {
    fun grpcServer(): GrpcServer
}
