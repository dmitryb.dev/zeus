package template.module

import com.typesafe.config.*
import dagger.*
import zeus.util.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideGrpcServer(config: Config): GrpcServer {
        return GrpcServer(config.getInt("grpc.server.port"))
    }


}
