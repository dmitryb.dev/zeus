package template.module.prod

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import dagger.Module
import dagger.Provides

@Module
object ConfigModule {

    @JvmStatic
    @Provides
    fun provideConfig() = ConfigFactory.load()

}
