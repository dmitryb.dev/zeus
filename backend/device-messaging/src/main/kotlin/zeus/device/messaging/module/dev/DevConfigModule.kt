package zeus.device.messaging.module.dev

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import dagger.Module
import dagger.Provides

@Module
object DevConfigModule {

    @JvmStatic
    @Provides fun provideConfig(): Config = ConfigFactory
            .load("application-dev.conf")
            .withFallback(ConfigFactory.load("application.conf"))

}
