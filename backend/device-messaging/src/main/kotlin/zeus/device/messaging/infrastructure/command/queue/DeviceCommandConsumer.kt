package zeus.device.messaging.infrastructure.command.queue

import common.domain.device.*
import device.command.domain.*

interface DeviceCommandConsumer {

    suspend fun next(deviceId: DeviceIds.DeviceId): Command.DeviceCommandEntity?
    suspend fun ack(deviceId: DeviceIds.DeviceId, commandId: Long)
    suspend fun ackAndNext(deviceId: DeviceIds.DeviceId, commandId: Long): Command.DeviceCommandEntity?

}
