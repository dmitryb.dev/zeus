package zeus.device.messaging.infrastructure.command.queue

import common.domain.device.*
import common.domain.device.DeviceFilter.DeviceNumberFilter
import device.command.domain.*
import javax.inject.*

@Singleton
class StubDeviceCommandConsumer @Inject constructor() : DeviceCommandConsumer {

    private var id: Long = 0

    override suspend fun next(deviceId: DeviceIds.DeviceId): Command.DeviceCommandEntity =
            DeviceCommandEntity {
                commandId = id
                command {
                    timeoutMs = 5000
                    deviceFilter {
                        manufacturerId = deviceId.manufacturerId
                        deviceNumber = DeviceNumberFilter { number = deviceId.deviceNumber }
                        addAllTags(deviceId.tagsList)
                    }
                    addAllType(arrayOf(1, 2).asIterable())
                    addAllValues(arrayOf(
                            Value { int32 = 42 },
                            Value { float = 42.42f },
                            Value { string = "42" }
                    ).asIterable())
                }
            }

    override suspend fun ack(deviceId: DeviceIds.DeviceId, commandId: Long) {
        id++
    }

    override suspend fun ackAndNext(deviceId: DeviceIds.DeviceId, commandId: Long): Command.DeviceCommandEntity {
        ack(deviceId, commandId)
        return next(deviceId)
    }

}
