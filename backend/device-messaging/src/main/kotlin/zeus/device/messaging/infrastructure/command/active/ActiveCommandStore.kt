package zeus.device.messaging.infrastructure.command.active

import common.domain.device.*

interface ActiveCommandStore {

    suspend fun getActiveCommand(deviceFilter: Filters.SingleDeviceFilter): Long?
    suspend fun setActiveCommand(deviceId: DeviceIds.DeviceId, commandId: Long)
    suspend fun reset(deviceId: DeviceIds.DeviceId, commandId: Long)

}
