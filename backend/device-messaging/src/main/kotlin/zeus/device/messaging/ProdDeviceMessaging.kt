package zeus.device.messaging

import zeus.device.messaging.module.EntryPointModule
import zeus.device.messaging.module.prod.ServiceModule
import zeus.device.messaging.module.prod.ConfigModule
import zeus.device.messaging.module.prod.InfrastructureModule
import dagger.Component

import javax.inject.Singleton

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ServiceModule::class,
    InfrastructureModule::class,
    ConfigModule::class
])
interface ProdDeviceMessaging : DeviceMessaging
