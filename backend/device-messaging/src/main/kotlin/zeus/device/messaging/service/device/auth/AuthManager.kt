package zeus.device.messaging.service.device.auth

import common.domain.device.*

interface AuthManager {

    suspend fun auth(deviceId: DeviceIds.DeviceId, secret: String): DeviceIds.DeviceId?

}
