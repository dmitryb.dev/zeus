package zeus.device.messaging.module.prod

import com.typesafe.config.*
import dagger.*

@Module
object ConfigModule {

    @JvmStatic
    @Provides
    fun provideConfig() = ConfigFactory.load()

}
