package zeus.device.messaging.service.device.auth

import common.domain.device.*
import org.slf4j.*
import javax.inject.*

class StubAuthManager @Inject constructor() : AuthManager {

    override suspend fun auth(deviceId: DeviceIds.DeviceId, secret: String): DeviceIds.DeviceId? = deviceId

    companion object {
        private val log = LoggerFactory.getLogger(StubAuthManager::class.java)
    }
}
