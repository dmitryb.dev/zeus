package zeus.device.messaging

import zeus.device.messaging.entrypoint.device.*
import zeus.util.*

interface DeviceMessaging {
    fun nettyServer(): NettyDeviceServer
    fun grpcServer(): GrpcServer
}
