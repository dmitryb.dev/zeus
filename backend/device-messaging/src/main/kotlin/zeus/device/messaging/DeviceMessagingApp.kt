package zeus.device.messaging

import kotlinx.coroutines.*
import zeus.util.*
import java.util.concurrent.*
import kotlin.system.*

fun main(args: Array<String>) {
    val deviceMessaging = ModeFactory
            .withDefaultMode<DeviceMessaging>("prod", DaggerProdDeviceMessaging::create)
            .withMode("dev", DaggerDevDeviceMessaging::create)
            .withMode("stub", DaggerStubDeviceMessaging::create)
            .create(args)

    val exitOnException = CoroutineExceptionHandler { _, th ->
        th.printStackTrace()
        exitProcess(1)
    }

    GlobalScope.launch(exitOnException + Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
        deviceMessaging.nettyServer().start()
    }

    GlobalScope.launch(exitOnException + Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
        deviceMessaging.grpcServer().start()
    }
}
