package zeus.device.messaging.entrypoint.device

import device.messaging.device.*
import io.netty.bootstrap.*
import io.netty.channel.*
import io.netty.channel.nio.*
import io.netty.channel.socket.*
import io.netty.channel.socket.nio.*
import io.netty.handler.codec.*
import io.netty.handler.codec.protobuf.*
import net.openhft.affinity.*
import org.slf4j.*
import javax.inject.*

class NettyDeviceServer(private val port: Int, private val handlerProvider: Provider<DeviceChannelHandler>) {

    fun start() {
        val bossGroup = NioEventLoopGroup(1)
        val threadFactory = AffinityThreadFactory("work-threads", AffinityStrategies.DIFFERENT_CORE)
        val workerGroup = NioEventLoopGroup(Runtime.getRuntime().availableProcessors(), threadFactory)

        try {
            val serverBootstrap = ServerBootstrap()
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel::class.java)
                    .childHandler(object : ChannelInitializer<SocketChannel>() {
                        public override fun initChannel(channel: SocketChannel) {
                            channel.pipeline().addLast(
                                    LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 2, 0, 2),
                                    ProtobufDecoder(Messages.DeviceOutboundMessage.getDefaultInstance()),
                                    handlerProvider.get()
                            )

                            channel.pipeline().addLast(
                                    LengthFieldPrepender(2),
                                    ProtobufEncoder()
                            )
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)

            val channelFuture = serverBootstrap.bind(port).sync()
            log.info("Netty device server started on {}", port)

            channelFuture.channel().closeFuture().sync()
        } finally {
            workerGroup.shutdownGracefully()
            bossGroup.shutdownGracefully()
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(NettyDeviceServer::class.java)
    }
}
