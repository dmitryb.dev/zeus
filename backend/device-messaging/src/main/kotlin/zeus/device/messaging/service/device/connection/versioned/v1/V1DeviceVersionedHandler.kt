package zeus.device.messaging.service.device.connection.versioned.v1

import common.domain.device.*
import device.messaging.device.*
import device.messaging.device.v1.Outbound.*
import kotlinx.coroutines.*
import org.slf4j.*
import zeus.common.domain.device.*
import zeus.device.messaging.infrastructure.channel.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.command.queue.*
import zeus.device.messaging.infrastructure.device.*
import zeus.device.messaging.infrastructure.event.*
import zeus.device.messaging.service.device.auth.*
import zeus.device.messaging.service.device.connection.util.*
import zeus.device.messaging.service.device.connection.versioned.*
import java.time.*

class V1DeviceVersionedHandler(
        private val eventPublisher: DeviceEventPublisher,
        private val commandConsumer: DeviceCommandConsumer,
        private val activeCommandStore: ActiveCommandStore,
        private val authManager: AuthManager,
        private val channel: DeviceChannel,
        private val onlineDevicesStore: OnlineDevicesStore
) : DeviceVersionedHandler {

    private val pingPong: PingPong = PingPong.start(
            pingAction = { GlobalScope.launch {
                channel.send(InboundConverter.devicePing())
                authorizedDevice?.apply { markAsOnline(deviceId) }
            }},
            onPingFailed = { channel.close() }
    )

    private var authorizedDevice: AuthorizedDevice? = null

    override suspend fun onMessage(message: Messages.DeviceOutboundMessage) {
        if (message.versionCase != Messages.DeviceOutboundMessage.VersionCase.V1) {
            log.error("Unsupported message version: {}", message.versionCase.name)
            return
        }

        when (message.v1.typeCase) {
            OutboundV1.TypeCase.AUTH -> onAuth(message.v1.auth)
            OutboundV1.TypeCase.PONG -> pingPong.onPong()
            OutboundV1.TypeCase.SUBSCRIBE -> onSubscribe()
            OutboundV1.TypeCase.EVENT -> onEvent(message.v1.event)
            OutboundV1.TypeCase.COMMAND_ACK -> onCommandAck(message.v1.commandAck)
            else -> log.warn("Unhandled message {}", message.v1.typeCase.name)
        }
    }

    override fun onDisconnect() {
        pingPong.stop()
        authorizedDevice?.transmitter?.cancelActiveRequest()
    }

    private suspend fun onAuth(auth: Auth) {
        authorizedDevice?.transmitter?.cancelActiveRequest()

        val deviceId = OutboundConverter.serverDeviceId(auth.deviceId)

        authManager.auth(deviceId, auth.secret)?.let {
            val inboundConverter = InboundConverter(auth.forceInt32)
            val outboundConverter = OutboundConverter(auth.deviceId, auth.forceInt32)
            authorizedDevice = AuthorizedDevice(
                    deviceId,
                    CommandTransmitter(commandConsumer, activeCommandStore, deviceId, channel,
                            inboundConverter::deviceCommand),
                    outboundConverter
            )

            markAsOnline(deviceId)

            log.debug("Device authorized: {}:{}, tags: {}", deviceId.manufacturerId, deviceId.deviceNumber,
                    deviceId.tagsList)
        }
                ?:  log.debug("Device authorization failed: {}:{}", auth.deviceId.manufacturerId,
                        auth.deviceId.deviceNumber)

        channel.send(InboundConverter.deviceAuthResult(authorizedDevice != null))
        if (auth.subscribe) {
            onSubscribe()
        }
    }

    private suspend fun onSubscribe() {
        authorizedDevice?.transmitter?.transmitNext()
    }

    private suspend fun onEvent(deviceEvent: Event) = coroutineScope {
        log.debug("Received serverEvent: {} of type {}", deviceEvent.meta.eventId, deviceEvent.typeList)

        authorizedDevice?.let { authorizedDevice ->
            val event = authorizedDevice.outboundConverter.serverEvent(deviceEvent)

            when {
                deviceEvent.meta.command?.ack == true && deviceEvent.meta.subscribe ->
                    authorizedDevice.transmitter.transmitNext(event.meta.command.commandId)

                deviceEvent.meta.command?.ack == true && !deviceEvent.meta.subscribe ->
                    authorizedDevice.transmitter.ack(event.meta.command.commandId)

                deviceEvent.meta.command?.ack != true && deviceEvent.meta.subscribe ->
                    authorizedDevice.transmitter.transmitNext()
            }
            eventPublisher.publish(event)
        }
    }

    private suspend fun onCommandAck(ack: CommandAck) = coroutineScope {
        log.debug("Received ack for device command with id: {}", ack.commandId)

        authorizedDevice?.let { authorizedDevice ->
            val commandIdToAck = authorizedDevice.outboundConverter.serverCommandId(ack.commandId)
            if (ack.subscribe)
                authorizedDevice.transmitter.transmitNext(commandIdToAck)
            else
                authorizedDevice.transmitter.ack(commandIdToAck)
        }
    }

    private suspend fun markAsOnline(deviceId: DeviceIds.DeviceId) = GlobalScope.launch {
        try {
            onlineDevicesStore.markAsOnline(deviceId, Duration.ofMinutes(2).toMillis())
        } catch (e: Exception) {
            log.error("Can't mark device: {} as online", deviceId.toPrettyString(), e)
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(V1DeviceVersionedHandler::class.java)
    }

    private class AuthorizedDevice (
            val deviceId: DeviceIds.DeviceId,
            val transmitter: CommandTransmitter,
            val outboundConverter: OutboundConverter
    )
}
