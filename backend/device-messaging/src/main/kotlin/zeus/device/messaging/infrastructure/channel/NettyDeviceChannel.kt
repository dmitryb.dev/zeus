package zeus.device.messaging.infrastructure.channel

import device.messaging.device.*
import io.netty.channel.*

class NettyDeviceChannel(private val context: ChannelHandlerContext) : DeviceChannel {

    override suspend fun send(msg: Messages.DeviceInboundMessage) {
        context.channel()
                .writeAndFlush(msg)
                .await()
    }

    override fun close() {
        context.channel().close()
    }

}
