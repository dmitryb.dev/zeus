package zeus.device.messaging.infrastructure.command.active

import common.domain.device.*
import io.lettuce.core.*
import io.lettuce.core.api.reactive.*
import kotlinx.coroutines.reactive.*
import zeus.util.redis.*
import javax.inject.*

class RedisActiveCommandStore @Inject constructor(redisClient: RedisClient) : ActiveCommandStore {

    private val redis: RedisReactiveCommands<String, StoredDeviceState> = redisClient
            .connect(Codecs.create(Codecs.String, Codecs.Json(StoredDeviceState::class)))
            .reactive()

    override suspend fun getActiveCommand(deviceFilter: Filters.SingleDeviceFilter): Long? =
        redis.get("a:${deviceFilter.manufacturerId}:${deviceFilter.deviceNumber}")
                .filter { device -> device.tags.containsAll(deviceFilter.tagsList) }
                .map(StoredDeviceState::commandId)
                .awaitFirstOrNull()

    override suspend fun setActiveCommand(deviceId: DeviceIds.DeviceId, commandId: Long) {
        redis.set(
                "a:${deviceId.manufacturerId}:${deviceId.deviceNumber}",
                StoredDeviceState(deviceId.tagsList, commandId)
        )
                .awaitFirst()
    }

    override suspend fun reset(deviceId: DeviceIds.DeviceId, commandId: Long) {
        redis.del("a:${deviceId.manufacturerId}:${deviceId.deviceNumber}")
                .awaitFirst()
    }

    private class StoredDeviceState(val tags: Collection<String>, val commandId: Long)
}
