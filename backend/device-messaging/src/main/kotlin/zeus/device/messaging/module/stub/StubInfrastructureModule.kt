package zeus.device.messaging.module.stub

import dagger.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.command.queue.*
import zeus.device.messaging.infrastructure.device.*
import zeus.device.messaging.infrastructure.event.*
import javax.inject.*

@Module
object StubInfrastructureModule {

    @JvmStatic
    @Provides
    fun provideResponsePublisher(responsePublisher: StubDeviceEventPublisher): DeviceEventPublisher = responsePublisher

    @Singleton
    @JvmStatic
    @Provides
    fun provideDeviceInboundConsumer(inboundConsumer: StubDeviceCommandConsumer): DeviceCommandConsumer = inboundConsumer

    @Singleton
    @JvmStatic
    @Provides
    fun provideActiveRequestsService(
            activeRequestsService: StubActiveCommandStore
    ): ActiveCommandStore = activeRequestsService

    @Singleton
    @JvmStatic
    @Provides
    fun provideOnlineDevicesStore(store: StubOnlineDevicesStore): OnlineDevicesStore = store
}
