package zeus.device.messaging.service.device.connection.versioned.v1

import common.domain.device.Values.Value.TypeCase.*
import device.command.domain.Command
import device.messaging.device.*
import device.messaging.device.v1.*

internal class InboundConverter(private val forceInt32: Boolean) {

    fun deviceCommand(commandEntity: Command.DeviceCommandEntity) = DeviceInboundMessage {
        v1 {
            command {
                meta {
                    commandId = CommandId {
                        if (forceInt32) {
                            pairOfInt32 {
                                high = ((commandEntity.commandId shr 32) and 0xFFFFFFFF).toInt()
                                low = (commandEntity.commandId  and 0xFFFFFFFF).toInt()
                            }
                        } else {
                            int64Value = commandEntity.commandId
                        }
                    }
                }
                addAllType(commandEntity.command.typeList)
                addAllValue(commandEntity.command.valuesList.map { value -> when (value.typeCase) {
                    INT32 -> Value { int32 = value.int32 }
                    FLOAT -> Value { float32 = value.float }
                    else -> Value {}
                }})
            }
        }
    }

    companion object {
        fun devicePing() = DeviceInboundMessage {
            v1 {
                ping {}
            }
        }

        fun deviceAuthResult(isAuthorized: Boolean) = DeviceInboundMessage {
            v1 {
                authResult { this.isAuthorized = isAuthorized }
            }
        }
    }
}
