package zeus.device.messaging.service.device.connection.util

import common.domain.device.*
import device.command.domain.*
import device.messaging.device.*
import io.grpc.*
import kotlinx.coroutines.*
import org.slf4j.*
import zeus.device.messaging.infrastructure.channel.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.command.queue.*
import java.util.concurrent.atomic.*

private const val RETRY_DELAY_MS = 2500L

class CommandTransmitter constructor(
        private val commandConsumer: DeviceCommandConsumer,
        private val activeCommandStore: ActiveCommandStore,
        private val deviceId: DeviceIds.DeviceId,
        private val channel: DeviceChannel,
        private val convertCommand: (Command.DeviceCommandEntity) -> Messages.DeviceInboundMessage
) {

    private val activeRequestJob: AtomicReference<Deferred<Unit>> = AtomicReference()

    suspend fun ack(commandId: Long) = coroutineScope {
        activeRequestJob.get()?.cancel()
        activeRequestJob.set(async {
            while (isActive) {
                try {
                    commandConsumer.ack(deviceId, commandId)
                    activeCommandStore.reset(deviceId, commandId)

                    log.debug("Command: {} for device: {} has been acked", commandId, deviceId)
                    return@async
                } catch (ex: Exception) {
                    if (!(ex is StatusException && ex.status == Status.DEADLINE_EXCEEDED)) {
                        delay(RETRY_DELAY_MS)
                        log.warn("Failed to ack deviceCommand: {} for device: {}, retrying...", commandId, deviceId, ex)
                    }
                }
            }

            log.error("Failed to ack deviceCommand: {} for device: {}", commandId, deviceId)
        })
    }

    suspend fun transmitNext() = coroutineScope {
        activeRequestJob.get()?.cancel()
        activeRequestJob.set(async {
            while(isActive) {
                try {
                    val command = commandConsumer.next(deviceId)
                    if (command != null) {
                        sendCommand(command)
                        setActiveCommand(command)
                        break
                    }
                } catch (ex: Exception) {
                    if (!(ex is StatusException && ex.status == Status.DEADLINE_EXCEEDED)) {
                        delay(RETRY_DELAY_MS)
                        log.warn("Failed to receive and send next deviceCommand for device: {}, retrying...", deviceId, ex)
                    }
                }
            }

            if (!isActive) {
                log.debug("Receive and send next deviceCommand for device: {} failed: request is canceled", deviceId)
            }
        })
    }

    suspend fun transmitNext(commandToAck: Long) = coroutineScope {
        activeRequestJob.get()?.cancel()
        activeRequestJob.set(async {
            var commandAcked = false
            while(isActive) {
                try {
                    val command: Command.DeviceCommandEntity?
                    if (commandAcked) {
                        command = commandConsumer.next(deviceId)
                    } else {
                        activeCommandStore.reset(deviceId, commandToAck)
                        command = commandConsumer.ackAndNext(deviceId, commandToAck)
                        commandAcked = true

                        log.debug("Command: {} for device: {} has been acked", commandToAck, deviceId)
                    }

                    if (command == null) continue

                    sendCommand(command)
                    setActiveCommand(command)
                    break
                } catch (ex: Exception) {
                    if (!(ex is StatusException && ex.status == Status.DEADLINE_EXCEEDED)) {
                        delay(RETRY_DELAY_MS)
                        if (commandAcked) {
                            log.warn("(Command {} was acked) Failed to receive and send next deviceCommand for device: {}, retrying...",
                                    commandToAck, deviceId, ex)
                        } else {
                            log.warn("Failed to ack: {}, receive and send next deviceCommand for device: {}, retrying...",
                                    commandToAck, deviceId, ex)
                        }
                    }
                }
            }

            if (!isActive) {
                log.debug("Ack: {}, receive and send next deviceCommand for device: {} failed: request is canceled",
                        commandToAck, deviceId)
            }
        })
    }

    fun cancelActiveRequest() {
        activeRequestJob.get()?.cancel()
    }

    private suspend fun sendCommand(command: Command.DeviceCommandEntity) {
        channel.send(convertCommand(command))
        log.debug("Command: {} for device: {} has been sent", command.commandId, deviceId)
    }

    private suspend fun setActiveCommand(command: Command.DeviceCommandEntity) {
        try {
            activeCommandStore.setActiveCommand(deviceId, command.commandId)
        } catch (ex: Exception) {
            // Any errors after sending should be ignored to avoid duplicated messages sent to device
            log.warn("Failed to set active deviceCommand: {} for device: {}", command.commandId, deviceId)
        }
    }

    companion object {
        val log = LoggerFactory.getLogger(CommandTransmitter::class.java)!!
    }
}