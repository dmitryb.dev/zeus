package zeus.device.messaging.service.device.connection.versioned

import device.messaging.device.*
import device.messaging.device.Messages.DeviceOutboundMessage.VersionCase.*
import zeus.device.messaging.infrastructure.channel.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.command.queue.*
import zeus.device.messaging.infrastructure.device.*
import zeus.device.messaging.infrastructure.event.*
import zeus.device.messaging.service.device.auth.*
import zeus.device.messaging.service.device.connection.versioned.v1.*
import javax.inject.*

@Singleton
class DeviceHandlerFactory @Inject constructor(
        private val activeCommandStore: ActiveCommandStore,
        private val commandConsumer: DeviceCommandConsumer,
        private val eventPublisher: DeviceEventPublisher,
        private val authManager: AuthManager,
        private val onlineDevicesStore: OnlineDevicesStore
) {

    fun create(message: Messages.DeviceOutboundMessage, channel: DeviceChannel) = when (message.versionCase) {
        V1 -> V1DeviceVersionedHandler(
                eventPublisher,
                commandConsumer,
                activeCommandStore,
                authManager,
                channel,
                onlineDevicesStore
        )
        else -> throw IllegalArgumentException("Unsupported version: " + message.versionCase.name)
    }
}
