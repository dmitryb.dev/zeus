package zeus.device.messaging.infrastructure.command.queue

import common.domain.device.*
import device.command.domain.*
import device.command.rpc.DeviceCommandConsumerCoroutineGrpc.DeviceCommandConsumerCoroutineStub
import io.grpc.*
import java.util.concurrent.*
import javax.inject.*

class GrpcDeviceCommandConsumer @Inject constructor(
        private val client: DeviceCommandConsumerCoroutineStub
) : DeviceCommandConsumer {

    override suspend fun next(
            deviceId: DeviceIds.DeviceId
    ): Command.DeviceCommandEntity = client
            .withDeadline(Deadline.after(15, TimeUnit.MINUTES))
            .next { this.deviceId = deviceId }
            .command

    override suspend fun ack(deviceId: DeviceIds.DeviceId, commandId: Long) {
        client
                .withDeadline(Deadline.after(1, TimeUnit.MINUTES))
                .ack {
                    this.deviceId = deviceId
                    this.commandId = commandId
                }
    }

    override suspend fun ackAndNext(
            deviceId: DeviceIds.DeviceId,
            commandId: Long
    ): Command.DeviceCommandEntity = client
            .withDeadline(Deadline.after(15, TimeUnit.MINUTES))
            .ackAndNext {
                this.deviceId = deviceId
                this.commandId = commandId
            }
            .command
}
