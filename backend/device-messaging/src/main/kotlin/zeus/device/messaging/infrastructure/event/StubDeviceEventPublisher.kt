package zeus.device.messaging.infrastructure.event

import device.event.domain.*
import org.slf4j.*
import javax.inject.*

class StubDeviceEventPublisher @Inject constructor() : DeviceEventPublisher {

    override suspend fun publish(event: Event.DeviceEventValue) {
        log.debug("Publishing serverEvent {} of device {}:{}", event.typeList, event.meta.deviceId.manufacturerId,
                event.meta.deviceId.deviceNumber)
    }

    companion object {
        private val log = LoggerFactory.getLogger(StubDeviceEventPublisher::class.java)
    }
}
