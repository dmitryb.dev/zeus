package zeus.device.messaging.service.device.connection.util

import java.util.concurrent.*
import java.util.concurrent.atomic.*

class PingPong private constructor(
        private val pingAction: () -> Unit,
        private val onPingFailed: () -> Unit,
        private val missedPongsThreshold: Int,
        pingPeriodSeconds: Int
) {

    private val pingScheduleFuture: ScheduledFuture<*> = scheduledExecutor
            .scheduleAtFixedRate({ this.onPing() }, pingPeriodSeconds.toLong(), pingPeriodSeconds.toLong(),
                    TimeUnit.SECONDS)

    private val pingWithoutPongCounter = AtomicInteger()

    fun onPong() {
        pingWithoutPongCounter.set(0)
    }

    fun stop() {
        pingScheduleFuture.cancel(false)
    }

    private fun onPing() {
        if (pingWithoutPongCounter.get() >= missedPongsThreshold) {
            onPingFailed()
            stop()
        } else {
            pingAction()
            pingWithoutPongCounter.incrementAndGet()
        }
    }

    companion object {
        private val scheduledExecutor = Executors.newScheduledThreadPool(
                Runtime.getRuntime().availableProcessors()
        )

        fun start(pingAction: () -> Unit = {}, onPingFailed: () -> Unit = {}, missedPongsThreshold: Int = 2,
                  pingPeriodSeconds: Int = 60
        ) = PingPong(pingAction, onPingFailed, missedPongsThreshold, pingPeriodSeconds)
    }
}
