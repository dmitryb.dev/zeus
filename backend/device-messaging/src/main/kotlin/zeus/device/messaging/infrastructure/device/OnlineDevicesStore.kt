package zeus.device.messaging.infrastructure.device

import common.domain.device.*

interface OnlineDevicesStore {
    suspend fun markAsOnline(deviceId: DeviceIds.DeviceId, timeoutMs: Long)
    suspend fun isOnline(deviceFilter: Filters.SingleDeviceFilter): Boolean?
}