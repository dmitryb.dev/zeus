package zeus.device.messaging.service.device.connection.versioned

import device.messaging.device.*

interface DeviceVersionedHandler {

    suspend fun onMessage(message: Messages.DeviceOutboundMessage)
    fun onDisconnect()
}
