package zeus.device.messaging.service.device.connection

import device.messaging.device.*
import zeus.device.messaging.infrastructure.channel.*
import zeus.device.messaging.service.device.connection.versioned.*
import java.util.concurrent.atomic.*
import javax.inject.*

class DeviceConnectionHandler @Inject constructor(private val deviceHandlerFactory: DeviceHandlerFactory) {
    private val deviceChannel = AtomicReference<DeviceChannel>()
    private val deviceHandler = AtomicReference<DeviceVersionedHandler>()

    fun onConnect(channel: DeviceChannel) {
        this.deviceChannel.set(channel)
    }

    suspend fun onMessage(message: Messages.DeviceOutboundMessage) {
        val handler = deviceHandler.updateAndGet { currentHandler ->
            currentHandler ?: deviceHandlerFactory.create(message, deviceChannel.get())
        }
        handler.onMessage(message)
    }

    fun onDisconnect() {
        deviceHandler.get().onDisconnect()
    }

}
