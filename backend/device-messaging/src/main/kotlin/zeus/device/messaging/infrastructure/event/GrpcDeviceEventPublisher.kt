package zeus.device.messaging.infrastructure.event

import device.event.domain.*
import device.event.rpc.*
import javax.inject.*

class GrpcDeviceEventPublisher @Inject constructor(
        private val client: DeviceEventBrokerCoroutineGrpc.DeviceEventBrokerCoroutineStub
) : DeviceEventPublisher {

    override suspend fun publish(event: Event.DeviceEventValue) {
        client.publish {
            this.event = event
        }
    }
}
