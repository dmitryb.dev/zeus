package zeus.device.messaging.module.prod

import com.typesafe.config.*
import dagger.*
import device.command.rpc.*
import device.event.rpc.*
import io.grpc.*
import io.lettuce.core.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.command.queue.*
import zeus.device.messaging.infrastructure.device.*
import zeus.device.messaging.infrastructure.event.*

@Module
object InfrastructureModule {

    @JvmStatic
    @Provides
    fun provideDeviceCommandClient(config: Config) = DeviceCommandConsumerCoroutineGrpc.newStub(
            ManagedChannelBuilder
                    .forTarget(config.getString("device-command.host") + ":" + config.getString("device-command.port"))
                    .directExecutor()
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideDeviceEventClient(config: Config) = DeviceEventBrokerCoroutineGrpc.newStub(
            ManagedChannelBuilder
                    .forTarget(config.getString("device-event.host") + ":" + config.getString("device-event.port"))
                    .directExecutor()
                    .usePlaintext()
                    .build()
    )

    @JvmStatic
    @Provides
    fun provideDeviceCommandConsumer(consumer: GrpcDeviceCommandConsumer): DeviceCommandConsumer = consumer

    @JvmStatic
    @Provides
    fun provideEventPublisher(publisher: GrpcDeviceEventPublisher): DeviceEventPublisher = publisher

    @JvmStatic
    @Provides
    fun provideRedisClient(config: Config) = RedisClient.create(RedisURI.builder()
            .withHost(config.getString("redis.host"))
            .withPort(config.getInt("redis.port"))
            .withDatabase(config.getInt("redis.database"))
            .build())

    @JvmStatic
    @Provides
    fun provideActiveRequestsStore(store: RedisActiveCommandStore): ActiveCommandStore = store

    @JvmStatic
    @Provides
    fun provideOnlineDevicesStore(store: RedisOnlineDevicesStore): OnlineDevicesStore = store
}
