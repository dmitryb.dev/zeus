package zeus.device.messaging.infrastructure.command.active

import common.domain.device.*
import javax.inject.*

class StubActiveCommandStore @Inject constructor() : ActiveCommandStore {

    private val active: HashMap<Pair<Long, Long>, Pair<Collection<String>, Long>> = HashMap()

    override suspend fun getActiveCommand(deviceFilter: Filters.SingleDeviceFilter): Long? =
            active[Pair(deviceFilter.manufacturerId, deviceFilter.deviceNumber)]
                    .takeIf { pair -> pair?.first?.containsAll(deviceFilter.tagsList) == true }
                    ?.second

    override suspend fun setActiveCommand(deviceId: DeviceIds.DeviceId, commandId: Long) {
        active[Pair(deviceId.manufacturerId, deviceId.deviceNumber)] = Pair(deviceId.tagsList, commandId)
    }

    override suspend fun reset(deviceId: DeviceIds.DeviceId, commandId: Long) {
        active.remove(Pair(deviceId.manufacturerId, deviceId.deviceNumber))
    }
}
