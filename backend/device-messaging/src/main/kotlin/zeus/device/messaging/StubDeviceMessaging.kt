package zeus.device.messaging

import dagger.*
import zeus.device.messaging.module.*
import zeus.device.messaging.module.dev.*
import zeus.device.messaging.module.prod.*
import zeus.device.messaging.module.stub.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ServiceModule::class,
    StubInfrastructureModule::class,
    DevConfigModule::class
])
interface StubDeviceMessaging : DeviceMessaging