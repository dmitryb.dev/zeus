package zeus.device.messaging.infrastructure.device

import common.domain.device.*
import io.lettuce.core.*
import io.lettuce.core.api.reactive.*
import kotlinx.coroutines.reactive.*
import zeus.common.domain.device.*
import zeus.util.log.*
import zeus.util.redis.*
import javax.inject.*

class RedisOnlineDevicesStore @Inject constructor(redisClient: RedisClient) : OnlineDevicesStore {

    @Suppress("UNCHECKED_CAST")
    private val redis: RedisReactiveCommands<String, Collection<String>> = redisClient
            .connect(Codecs.create(Codecs.String, Codecs.Json(Collection::class)))
            .reactive()
            as RedisReactiveCommands<String, Collection<String>>

    override suspend fun markAsOnline(deviceId: DeviceIds.DeviceId, timeoutMs: Long) {
        redis.set(
                "o:${deviceId.manufacturerId}:${deviceId.deviceNumber}",
                deviceId.tagsList,
                SetArgs().px(timeoutMs)
        )
                .awaitFirst()

        log.error("Device: {} marked as online", deviceId.toPrettyString())
    }

    override suspend fun isOnline(deviceFilter: Filters.SingleDeviceFilter): Boolean? {
        val onlineDeviceTags = redis.get("o:${deviceFilter.manufacturerId}:${deviceFilter.deviceNumber}")
                .awaitFirstOrNull() ?: return false

        return if (onlineDeviceTags.containsAll(deviceFilter.tagsList)) true else null
    }

    companion object {
        val log = logger<RedisOnlineDevicesStore>()
    }
}