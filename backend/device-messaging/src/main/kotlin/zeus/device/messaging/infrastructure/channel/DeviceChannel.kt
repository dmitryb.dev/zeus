package zeus.device.messaging.infrastructure.channel

import device.messaging.device.*

interface DeviceChannel {
    suspend fun send(msg: Messages.DeviceInboundMessage)
    fun close()
}
