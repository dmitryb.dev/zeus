package zeus.device.messaging.module

import com.typesafe.config.*
import dagger.*
import zeus.device.messaging.entrypoint.device.*
import zeus.device.messaging.entrypoint.info.*
import zeus.util.*
import javax.inject.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideDeviceServer(
            config: Config,
            handlerProvider: Provider<DeviceChannelHandler>
    ) = NettyDeviceServer(config.getInt("netty.server.port"), handlerProvider)

    @JvmStatic
    @Provides
    fun provideGrpcServer(
            config: Config,
            requests: DeviceStatuses
    ) = GrpcServer(config.getInt("grpc.server.port"), requests)

}
