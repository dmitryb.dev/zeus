package zeus.device.messaging.entrypoint.info

import device.messaging.server.rpc.*
import device.messaging.server.rpc.ActiveCommandResponse.ActiveCommand
import kotlinx.coroutines.channels.*
import zeus.device.messaging.infrastructure.command.active.*
import zeus.device.messaging.infrastructure.device.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class DeviceStatuses @Inject constructor(
        private val activeCommandStore: ActiveCommandStore,
        private val onlineDevicesStore: OnlineDevicesStore
) : DeviceStatusCoroutineGrpc.DeviceStatusImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun getActiveCommand(request: DeviceStatusApi.GetActiveCommandRequest)
            : DeviceStatusApi.ActiveCommandResponse {

        val commandId = activeCommandStore.getActiveCommand(request.deviceId)

        return ActiveCommandResponse {
            commandId?.let { commandId -> setCommand(ActiveCommand {
                this.commandId = commandId
            })}
        }
    }

    override suspend fun getOnlineStatuses(
            requestChannel: ReceiveChannel<DeviceStatusApi.IsDeviceOnlineRequest>,
            responseChannel: SendChannel<DeviceStatusApi.IsDeviceOnlineResponse>
    ) {
        for (request in requestChannel) {
            responseChannel.send(IsDeviceOnlineResponse {
                deviceFilter = request.deviceFilter
                isOnline = onlineDevicesStore.isOnline(deviceFilter) ?: false
            })
        }
    }
}
