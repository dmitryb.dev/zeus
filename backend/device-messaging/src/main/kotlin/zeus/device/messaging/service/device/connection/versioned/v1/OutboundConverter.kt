package zeus.device.messaging.service.device.connection.versioned.v1

import common.domain.device.*
import device.event.domain.*
import device.messaging.device.v1.Common
import device.messaging.device.v1.Common.Value.TypeCase.*
import device.messaging.device.v1.Outbound

internal class OutboundConverter(private val authDeviceId: Outbound.Auth.DeviceId, private val forceInt32: Boolean) {

    fun serverCommandId(commandId: Common.CommandId) = if (forceInt32) {
        (commandId.pairOfInt32.high.toLong() shl 32) or
                commandId.pairOfInt32.low.toLong()
        } else {
            commandId.int64Value
        }

    fun serverEvent(event: Outbound.Event) = DeviceEventValue {
        meta {
            this.deviceId = serverDeviceId(authDeviceId)
            rolesMask = event.meta.rolesMask
            event.meta.command.let { commandAck ->
                Meta@this.command {
                    commandId = serverCommandId(commandAck.commandId)
                }
            }
        }
        addAllType(event.typeList)
        addAllValues(event.valueList.map { v1Value -> when (v1Value.typeCase) {
            INT32 -> Value { int32 = v1Value.int32 }
            FLOAT32 -> Value { float = v1Value.float32 }
            else -> Value {}
        }})
    }

    companion object {
        fun serverDeviceId(deviceId: Outbound.Auth.DeviceId): DeviceIds.DeviceId = DeviceId {
            manufacturerId = deviceId.manufacturerId.toLong()
            deviceNumber = deviceId.deviceNumber.toLong()
            addAllTags(deviceId.tagsList)
        }
    }

}
