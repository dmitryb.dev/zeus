package zeus.device.messaging.infrastructure.event

import device.event.domain.*

interface DeviceEventPublisher {
    suspend fun publish(event: Event.DeviceEventValue)
}
