package zeus.device.messaging.entrypoint.device

import device.messaging.device.*
import io.netty.channel.*
import kotlinx.coroutines.*
import org.slf4j.*
import zeus.device.messaging.infrastructure.channel.*
import zeus.device.messaging.service.device.connection.*
import zeus.util.log.*
import javax.inject.*
import kotlin.math.*

@ObsoleteCoroutinesApi
class DeviceChannelHandler @Inject constructor(
        private val deviceConnectionHandler: DeviceConnectionHandler
) : SimpleChannelInboundHandler<Messages.DeviceOutboundMessage>() {

    override fun channelActive(ctx: ChannelHandlerContext) {
        super.channelActive(ctx)
        setupTraceId()

        log.debug("Device connected: {}", ctx.channel().remoteAddress())
        deviceConnectionHandler.onConnect(NettyDeviceChannel(ctx))
    }

    override fun channelInactive(ctx: ChannelHandlerContext) {
        super.channelInactive(ctx)
        log.debug("Device disconnected: {}", ctx.channel().remoteAddress())
        deviceConnectionHandler.onDisconnect()
    }

    override fun channelRead0(ctx: ChannelHandlerContext, msg: Messages.DeviceOutboundMessage) {
        val dispatcher = dispatchers[abs(
                ctx.channel().id().asShortText().hashCode() % dispatchers.size)
        ]
        GlobalScope.launch(dispatcher + traceContext()) {
            deviceConnectionHandler.onMessage(msg)
        }
    }

    @Suppress("OverridingDeprecatedMember")
    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        log.error("Error during handling channel, {}", cause)
        ctx.close()
    }

    companion object {
        private val log = LoggerFactory.getLogger(DeviceChannelHandler::class.java)

        private val dispatchers = (1 .. (Runtime.getRuntime().availableProcessors()))
                .map { newSingleThreadContext("hashing-dispatcher-$it") }
    }
}