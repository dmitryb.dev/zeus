package zeus.device.messaging.infrastructure.device

import common.domain.device.*
import java.util.concurrent.*
import javax.inject.*

@Singleton
class StubOnlineDevicesStore @Inject constructor(): OnlineDevicesStore {

    val scheduler = Executors.newSingleThreadScheduledExecutor()
    val store = HashMap<Pair<Long, Long>, ScheduledFuture<*>>()

    override suspend fun markAsOnline(deviceId: DeviceIds.DeviceId, timeoutMs: Long) {
        val id = Pair(deviceId.manufacturerId, deviceId.deviceNumber)
        store.get(id)?.cancel(false)
        store.put(id, scheduler.schedule({ store.remove(id) }, timeoutMs, TimeUnit.MILLISECONDS))
    }

    override suspend fun isOnline(deviceFilter: Filters.SingleDeviceFilter): Boolean? =
            store.containsKey(Pair(deviceFilter.manufacturerId, deviceFilter.deviceNumber))
}