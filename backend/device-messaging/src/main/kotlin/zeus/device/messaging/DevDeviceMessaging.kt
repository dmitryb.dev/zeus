package zeus.device.messaging

import zeus.device.messaging.module.EntryPointModule
import zeus.device.messaging.module.prod.InfrastructureModule
import zeus.device.messaging.module.dev.DevConfigModule
import zeus.device.messaging.module.prod.ServiceModule
import dagger.Component

import javax.inject.Singleton

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ServiceModule::class,
    InfrastructureModule::class,
    DevConfigModule::class]
)
interface DevDeviceMessaging : DeviceMessaging
