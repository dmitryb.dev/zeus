package zeus.device.messaging.module.prod

import dagger.*
import zeus.device.messaging.service.device.auth.*

@Module
object ServiceModule {

    @JvmStatic
    @Provides
    fun provideAuthManager(authManager: StubAuthManager): AuthManager = authManager

}
