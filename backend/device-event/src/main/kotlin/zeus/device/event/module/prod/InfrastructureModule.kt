package zeus.device.event.module.prod

import com.datastax.driver.core.*
import com.typesafe.config.*
import dagger.*
import io.lettuce.core.*
import org.apache.pulsar.client.admin.*
import org.apache.pulsar.client.api.*
import zeus.device.event.infrastructure.event.stored.*
import zeus.device.event.infrastructure.event.arrived.*
import zeus.id.generator.local.*
import zeus.util.pulsar.*
import javax.inject.*


@Module
object InfrastructureModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideCassandraSession(config: Config): Session {
        return Cluster.builder()
                .addContactPoint(config.getString("cassandra.host"))
                .withPort(config.getInt("cassandra.port"))
                .build()
                .connect(config.getString("cassandra.keyspace"))
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideRedisClient(config: Config): RedisClient {
        return RedisClient.create(
                RedisURI.builder()
                        .withHost(config.getString("redis.host"))
                        .withPort(config.getInt("redis.port"))
                        .withDatabase(config.getInt("redis.database"))
                        .build()
        )
    }

    @JvmStatic
    @Provides
    fun pulsarAdmin(config: Config): PulsarAdmin {
        return PulsarAdmin.builder()
                .serviceHttpUrl(String.format("http://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.http-port")))
                .enableTlsHostnameVerification(false)
                .build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun providePulsarClient(config: Config, pulsarAdmin: PulsarAdmin): PulsarClient {
        PulsarBootstrap.create(pulsarAdmin, "public/device-event")

        return PulsarClient.builder()
                .serviceUrl(String.format("pulsar://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.port")))
                .build()
    }

    @JvmStatic
    @Provides
    fun provideEventStore(session: Session): EventStore = CassandraEventStore(session)

    @JvmStatic
    @Provides
    @Named("eventIdGenerator")
    fun provideEventIdGenerator(session: Session, redisClient: RedisClient): LocalIdGenerator {
        val table = CassandraRedisLocalIdGenerator.CassandraTable(
                "event_ids",
                "device_id",
                "event_id"
        )
        return CassandraRedisLocalIdGenerator(session, table, redisClient)
    }

    @JvmStatic
    @Provides
    fun provideEventStream(pulsarClient: PulsarClient): EventStream = PulsarEventStream(pulsarClient)

}
