package zeus.device.event.infrastructure.event.stored

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.channels.*

interface EventStore {

    suspend fun save(entityToStore: Event.DeviceEventEntity): Event.DeviceEventEntity
    suspend fun find(
            filter: Filters.SingleDeviceFilter,
            startFromEvent: Long,
            untilEvent: Long = Long.MAX_VALUE
    ): ReceiveChannel<Event.DeviceEventEntity>

}
