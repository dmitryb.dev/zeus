package zeus.device.event

import dagger.*
import zeus.device.event.module.*
import zeus.device.event.module.dev.*
import zeus.device.event.module.dev.DevInfrastructureInitModule
import zeus.device.event.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    DevInfrastructureInitModule::class,
    DevConfigModule::class
])
interface DevDeviceEvent : DeviceEvent
