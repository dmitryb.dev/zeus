package zeus.device.event.infrastructure.event.stored

import com.datastax.driver.core.*
import com.datastax.driver.core.querybuilder.*
import com.datastax.driver.mapping.*
import com.datastax.driver.mapping.annotations.*
import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.guava.*
import zeus.common.domain.device.*
import zeus.util.log.*
import java.nio.*
import java.util.*
import javax.inject.*

class CassandraEventStore @Inject constructor(
        private val session: Session
) : EventStore {

    private val mapper = MappingManager(session).mapper(Entity::class.java)

    override suspend fun save(entityToStore: Event.DeviceEventEntity): Event.DeviceEventEntity {
        log.debug("Storing event with id: {}, for device: {}", entityToStore.eventId,
                entityToStore.event.meta.deviceId.toPrettyString())

        mapper.saveAsync(Entity(
                entityToStore.eventId,
                entityToStore.event.meta.deviceId.manufacturerId,
                entityToStore.event.meta.deviceId.deviceNumber,
                HashSet(entityToStore.event.meta.deviceId.tagsList),
                entityToStore.event.typeList,
                entityToStore.event.valuesList
                        .map { value -> ByteBuffer.wrap(value.toByteArray()) }
        )).await()

        log.debug("Event stored")

        return entityToStore
    }

    override suspend fun find(
            filter: Filters.SingleDeviceFilter,
            startFromEvent: Long,
            untilEvent: Long
    ): ReceiveChannel<Event.DeviceEventEntity> =  GlobalScope.produce {
        val resultSetFuture = session.executeAsync(QueryBuilder.select()
                .all().from(TABLE)
                .where(QueryBuilder.eq("manufacturer_id", filter.manufacturerId))
                .and(QueryBuilder.eq("device_number", filter.deviceNumber))
                .and(QueryBuilder.gte("event_id", startFromEvent))
                .and(QueryBuilder.lte("event_id", untilEvent))
        )

        for (command in mapper.mapAsync(resultSetFuture).await().all()) {
            send(command.toDomain())
        }
    }

    @Table(name = TABLE)
    private class Entity(
            @PartitionKey @Column(name = "event_id") var eventId: Long,
            @Column(name = "manufacturer_id") var manufacturerId: Long,
            @Column(name = "device_number") var deviceNumber: Long,
            @Column(name = "tags") var tags: Set<String>,
            @Column(name = "type") var type: List<Int>,
            @Column(name = "value") var values: List<ByteBuffer>
    ) {
        private constructor() : this(0L, 0L, 0L, emptySet(), emptyList(), emptyList())

        fun toDomain(): Event.DeviceEventEntity = DeviceEventEntity {
            eventId = this@Entity.eventId
            event {
                meta {
                    deviceId {
                        manufacturerId = this@Entity.manufacturerId
                        deviceNumber = deviceNumber
                        addAllTags(this@Entity.tags)
                    }
                }
                addAllType(this@Entity.type)
                addAllValues(values.map { buffer -> Values.Value.parseFrom(buffer) })
            }
        }
    }

    companion object {
        private const val TABLE = "events"

        val log = logger<CassandraEventStore>()
    }
}
