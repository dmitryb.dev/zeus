package zeus.device.event

import dagger.*
import zeus.device.event.module.*
import zeus.device.event.module.dev.*
import zeus.device.event.module.stub.*
import zeus.device.event.module.stub.StubInfrastructureInitModule
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    StubInfrastructureModule::class,
    StubInfrastructureInitModule::class,
    DevConfigModule::class
])
interface StubDeviceEvent : DeviceEvent
