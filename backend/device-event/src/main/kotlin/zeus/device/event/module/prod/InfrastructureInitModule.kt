package zeus.device.event.module.prod

import com.hhandoko.cassandra.migration.*
import com.typesafe.config.*
import dagger.*
import zeus.device.event.module.*
import javax.inject.*


@Module
object InfrastructureInitModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideInfrastructureInitializer(config: Config): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {
            CassandraMigration().apply {
                locations = arrayOf("cql")
                keyspaceConfig.name = config.getString("cassandra.keyspace")
                keyspaceConfig.clusterConfig.contactpoints = arrayOf(config.getString("cassandra.host"))
                keyspaceConfig.clusterConfig.port = config.getInt("cassandra.port")
            }.migrate()
        }
    }

}
