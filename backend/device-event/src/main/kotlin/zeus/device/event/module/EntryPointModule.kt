package zeus.device.event.module

import com.typesafe.config.*
import dagger.*
import zeus.device.event.entrypoint.*
import zeus.util.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideGrpcServer(config: Config, eventApi: Events): GrpcServer = GrpcServer(
            config.getInt("grpc.server.port"),
            eventApi
    )

}
