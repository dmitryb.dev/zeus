package zeus.device.event.infrastructure.event.arrived

import com.google.common.collect.*
import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.channels.*
import java.util.*
import javax.inject.*

class StubEventStream @Inject constructor() : EventStream {

    private val channelsMap = Multimaps.newMultimap<DeviceIds.DeviceId, Channel<Event.DeviceEventEntity>>(
            HashMap()) { ArrayList() }

    override suspend fun eventStream(filter: Filters.SingleDeviceFilter): ReceiveChannel<Event.DeviceEventEntity> {
        val channel = Channel<Event.DeviceEventEntity>()

        channelsMap.put(DeviceId {
            manufacturerId = filter.manufacturerId
            deviceNumber = filter.deviceNumber
        }, channel)

        return channel
    }

    override suspend fun notifySubscribers(eventEntity: Event.DeviceEventEntity) {
        val deviceId = eventEntity.event.meta.deviceId

        for (channel in channelsMap.get(deviceId)) {
            if (!channel.isClosedForSend) {
                channel.send(eventEntity)
            }
        }

        channelsMap.removeAll(deviceId)
    }
}
