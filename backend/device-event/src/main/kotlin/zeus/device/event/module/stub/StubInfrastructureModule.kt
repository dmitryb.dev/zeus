package zeus.device.event.module.stub

import dagger.*
import zeus.device.event.infrastructure.event.arrived.*
import zeus.device.event.infrastructure.event.stored.*
import zeus.id.generator.local.*
import javax.inject.*

@Module
object StubInfrastructureModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideEventStore(eventStore: StubEventStore): EventStore = eventStore

    @JvmStatic
    @Provides
    @Singleton
    fun provideEventStream(eventStream: StubEventStream): EventStream = eventStream

    @JvmStatic
    @Provides
    @Named("eventIdGenerator")
    fun provideEventIdGenerator(localIdGenerator: StubLocalIdGenerator): LocalIdGenerator = localIdGenerator

}
