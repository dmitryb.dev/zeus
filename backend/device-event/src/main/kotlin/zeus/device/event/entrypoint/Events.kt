package zeus.device.event.entrypoint

import device.event.domain.*
import device.event.rpc.*
import device.event.rpc.EventApi.SubscribeRequest.SubscribtionModeCase.*
import kotlinx.coroutines.channels.*
import zeus.common.domain.device.*
import zeus.device.event.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class Events @Inject constructor(
        private val eventManager: EventManager
) : DeviceEventBrokerCoroutineGrpc.DeviceEventBrokerImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun publish(request: EventApi.PublishRequest): EventApi.PublishedResponse = log.uncaught {
        log.debug("Publishing event for device: {}", request.event.meta.deviceId.toPrettyString())

        val entity = eventManager.publish(request.event)

        log.debug("Event published with event id: {}", entity.eventId)

        PublishedResponse {
            eventId = entity.eventId
        }
    }

    override suspend fun subscribe(
            request: EventApi.SubscribeRequest,
            responseChannel: SendChannel<Event.DeviceEventEntity>
    ) = log.uncaught {
        log.debug("Subscribing to events for device: {}", request.filter.toPrettyString())

        if (request.subscribtionModeCase == START_FROM_EVENT_ID) {
            log.debug("Starting from event id: {}", request.startFromEventId)
            for (event in eventManager.subscribe(request.filter, request.startFromEventId)) {
                responseChannel.send(event)
                log.debug("Event with id: {} sent", event.eventId)
            }
        } else {
            for (event in eventManager.subscribe(request.filter)) {
                responseChannel.send(event)
                log.debug("Event with id: {} sent", event.eventId)
            }
        }
    }

    companion object {
        private val log = logger<Events>()
    }
}