package zeus.device.event.infrastructure.event.arrived

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.channels.*

interface EventStream {

    suspend fun eventStream(filter: Filters.SingleDeviceFilter): ReceiveChannel<Event.DeviceEventEntity>
    suspend fun notifySubscribers(eventEntity: Event.DeviceEventEntity)

}
