package zeus.device.event.infrastructure.event.stored

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import java.util.*
import javax.inject.*

@Singleton
class StubEventStore @Inject constructor() : EventStore {

    private val events = HashMap<Long, HashMap<Long, HashMap<Long, Event.DeviceEventEntity>>>()

    override suspend fun save(entityToStore: Event.DeviceEventEntity): Event.DeviceEventEntity {
        getEventsMap(entityToStore.event.meta.deviceId)[entityToStore.eventId] = entityToStore
        return entityToStore
    }

    override suspend fun find(
            filter: Filters.SingleDeviceFilter,
            startFromEvent: Long,
            untilEvent: Long
    ): ReceiveChannel<Event.DeviceEventEntity> = GlobalScope.produce {
        for (event in getEventsMap(DeviceId {
            manufacturerId = filter.manufacturerId
            deviceNumber = filter.deviceNumber
        }).values) {
            if (event.eventId in startFromEvent..untilEvent) {
                send(event)
            }
        }
    }

    private fun getEventsMap(deviceId: DeviceIds.DeviceId): MutableMap<Long, Event.DeviceEventEntity> {
        return events.computeIfAbsent(deviceId.manufacturerId) { HashMap() }
                .computeIfAbsent(deviceId.deviceNumber) { HashMap() }
    }
}
