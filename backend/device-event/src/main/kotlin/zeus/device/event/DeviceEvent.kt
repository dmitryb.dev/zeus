package zeus.device.event

import zeus.device.event.module.*
import zeus.util.*

interface DeviceEvent {
    fun initializer(): InfrastructureInitializer
    fun grpcServer(): GrpcServer
}
