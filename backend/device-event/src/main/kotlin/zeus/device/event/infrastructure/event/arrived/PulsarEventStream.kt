package zeus.device.event.infrastructure.event.arrived

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.future.*
import org.apache.pulsar.client.api.*
import zeus.util.log.*
import zeus.util.pulsar.*
import javax.inject.*
import kotlin.coroutines.*

class PulsarEventStream @Inject constructor(private val pulsarClient: PulsarClient) : EventStream {

    private val producerCache = ProducerCache(pulsarClient, DEVICE_EVENT_SCHEMA)

    override suspend fun eventStream(
            filter: Filters.SingleDeviceFilter
    ): ReceiveChannel<Event.DeviceEventEntity> = GlobalScope.produce(coroutineContext) {
        val deviceEvents = pulsarClient.read<Event.DeviceEventEntity>(
                String.format(TOPIC_FORMAT, filter.manufacturerId, filter.deviceNumber),
                MessageId.latest
        )

        for (event in deviceEvents) {
            log.debug("Received device event with id: {}", event.eventId)
            send(event)
        }
    }

    override suspend fun notifySubscribers(eventEntity: Event.DeviceEventEntity) {
        val event = eventEntity.event
        val topic = String.format(
                TOPIC_FORMAT,
                event.meta.deviceId.manufacturerId,
                event.meta.deviceId.deviceNumber
        )

        log.debug("Sending event with id: {} to the topic '{}'", eventEntity.eventId, topic)

        producerCache.getClient(topic).sendAsync(eventEntity).await()

        log.debug("Event was sent", eventEntity.eventId, topic)
    }

    companion object {
        private val log = logger<PulsarEventStream>()

        private const val TOPIC_FORMAT = "non-persistent://public/device-event/%s-%s"

        private val DEVICE_EVENT_SCHEMA = Schema.PROTOBUF(Event.DeviceEventEntity::class.java)
    }
}
