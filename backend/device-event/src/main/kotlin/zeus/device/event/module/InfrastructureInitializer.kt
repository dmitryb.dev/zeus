package zeus.device.event.module

interface InfrastructureInitializer {
    fun init()
}