package zeus.device.event.module.stub

import dagger.*
import zeus.device.event.module.*
import javax.inject.*

@Module
object StubInfrastructureInitModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideInfrastructureInitializer(): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {}
    }

}
