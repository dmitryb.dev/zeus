package zeus.device.event

import zeus.util.*

fun main(args: Array<String>) {

    val deviceEvent = ModeFactory
            .withDefaultMode<DeviceEvent>("prod", DaggerProdDeviceEvent::create)
            .withMode("dev", DaggerDevDeviceEvent::create)
            .withMode("stub", DaggerStubDeviceEvent::create)
            .create(args)

    deviceEvent.initializer().init()
    deviceEvent.grpcServer().start()
}