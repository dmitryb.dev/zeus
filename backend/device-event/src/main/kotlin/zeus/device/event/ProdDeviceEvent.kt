package zeus.device.event

import dagger.*
import zeus.device.event.module.*
import zeus.device.event.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    InfrastructureInitModule::class,
    ConfigModule::class
])
interface ProdDeviceEvent : DeviceEvent
