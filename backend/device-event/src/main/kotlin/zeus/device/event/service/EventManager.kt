package zeus.device.event.service

import common.domain.device.*
import device.event.domain.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import zeus.device.event.infrastructure.event.arrived.*
import zeus.device.event.infrastructure.event.stored.*
import zeus.id.generator.local.*
import javax.inject.*
import kotlin.coroutines.*

class EventManager @Inject constructor(
        @Named("eventIdGenerator") private val idGenerator: LocalIdGenerator,
        private val eventStore: EventStore,
        private val eventStream: EventStream
) {

    suspend fun publish(event: Event.DeviceEventValue): Event.DeviceEventEntity {
        val eventId = idGenerator.nextLocalId("${event.meta.deviceId.manufacturerId}.${event.meta.deviceId.deviceNumber}")

        val eventEntity = DeviceEventEntity {
            this.eventId = eventId
            this.event = event
        }

        eventStore.save(eventEntity)

        eventStream.notifySubscribers(eventEntity)

        return eventEntity
    }

    suspend fun subscribe(
            filter: Filters.SingleDeviceFilter
    ): ReceiveChannel<Event.DeviceEventEntity> = GlobalScope.produce(coroutineContext) {
        var lastEventId: Long? = null
        for (event in eventStream.eventStream(filter)) {
            if (lastEventId != null) {
                if (event.eventId <= lastEventId) {
                    continue
                }
                if (event.eventId - 1 != lastEventId) {
                    for (lostEvent in eventStore.find(filter, lastEventId + 1, event.eventId - 1)) {
                        send(lostEvent)
                    }
                }
            }
            send(event)
            lastEventId = event.eventId
        }
    }

    suspend fun subscribe(
            filter: Filters.SingleDeviceFilter,
            startFromEventId: Long
    ): ReceiveChannel<Event.DeviceEventEntity> = GlobalScope.produce(coroutineContext) {
        val received = subscribe(filter)

        var lastFoundId = 0L
        for (event in eventStore.find(filter, startFromEventId)) {
            send(event)
            lastFoundId = event.eventId
        }

        for (event in received) {
            if (event.eventId > lastFoundId) {
                send(event)
            }
        }
    }
}
