package zeus.user.account.infrastructure.manufacturer.secret

import zeus.user.account.domain.*

interface ManufacturerSecretStore {

    suspend fun find(userId: Long, manufacturerId: Long): Collection<ManufacturerSecretEntity>
    suspend fun save(userId: Long, secret: ManufacturerSecretValue): ManufacturerSecretEntity

    suspend fun updateSecret(userId: Long, manufacturerId: Long, secretId: Long, newSecretHash: String): Boolean
    suspend fun updateSecretName(userId: Long, manufacturerId: Long, secretId: Long, newName: String): Boolean

    suspend fun remove(userId: Long, manufacturerId: Long, secretId: Long): Boolean

}