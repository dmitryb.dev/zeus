package zeus.user.account.domain

import zeus.util.mysql.mapper.annotation.*

open class ManufacturerSecretValue(
        @get:Column("manufacturer_id") val manufacturerId: Long,
        @get:Column("secret_name") val secretName: String,
        @get:Column("secret_hash") val secretHash: String
)

class ManufacturerSecretEntity @EntityCreator constructor(
        @Column("secret_id") val secretId: Long,
        @Column("manufacturer_id") manufacturerId: Long,
        @Column("secret_name") secretName: String,
        @Column("secret_hash") secretHash: String
) : ManufacturerSecretValue(manufacturerId, secretName, secretHash) {
    constructor(id: Long, secret: ManufacturerSecretValue) :
            this(id, secret.manufacturerId, secret.secretName, secret.secretHash)
}