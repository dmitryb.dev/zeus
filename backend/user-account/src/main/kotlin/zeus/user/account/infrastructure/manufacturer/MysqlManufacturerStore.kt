package zeus.user.account.infrastructure.manufacturer

import com.mysql.cj.xdevapi.*
import zeus.user.account.domain.*
import zeus.user.account.service.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlManufacturerStore @Inject constructor(
        client: Client,
        schemaName: String
) : ManufacturerStore, MysqlStore(client, schemaName, MANUFACTURERS_TABLE) {

    override suspend fun find(userId: Long): ManufacturerEntity? = withTable { table ->
        table
                .select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findOne<ManufacturerEntity>()
    }

    override suspend fun save(manufacturerValue: ManufacturerValue): ManufacturerEntity = try {
        withTable { table ->
            val insertResult = table
                    .insert(manufacturerValue)
                    .await()

            ManufacturerEntity(insertResult.autoIncrementValue, manufacturerValue)
        }
    } catch (e: DuplicatedException) {
        throw ManufacturerManager.ManufacturerAlreadyExistsStoreException()
    }

    override suspend fun updateManufacturerName(
            userId: Long,
            manufacturerId: Long,
            manufacturerName: String
    ): Boolean = withTable { table ->
        val updateResult = table.update()
                .set("manufacturer_name", manufacturerName)
                .where("user_id = :userId AND manufacturer_id = :manufacturerId")
                .bind("userId", userId)
                .bind("manufacturerId", manufacturerId)
                .await()

        updateResult.affectedItemsCount > 0
    }

    companion object {
        const val MANUFACTURERS_TABLE = "manufacturers"
    }
}
