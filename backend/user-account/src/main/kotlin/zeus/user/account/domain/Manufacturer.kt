package zeus.user.account.domain

import zeus.util.mysql.mapper.annotation.*

open class ManufacturerValue(
        @get:Column("user_id") val userId: Long,
        @get:Column("manufacturer_name") val manufacturerName: String)

data class ManufacturerEntity @EntityCreator constructor(
        @Column("manufacturer_id") val manufacturerId: Long,
        @Column("user_id") val userId: Long,
        @Column("manufacturer_name") val manufacturerName: String
) {
    constructor(id: Long, manufacturer: ManufacturerValue) :
            this(id, manufacturer.userId, manufacturer.manufacturerName)
}

class ManufacturerAggregate(val manufacturer: ManufacturerEntity, val secrets: Collection<ManufacturerSecretEntity>)