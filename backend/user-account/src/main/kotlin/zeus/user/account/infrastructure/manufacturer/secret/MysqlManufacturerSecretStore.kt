package zeus.user.account.infrastructure.manufacturer.secret

import com.mysql.cj.xdevapi.*
import zeus.user.account.domain.*
import zeus.user.account.infrastructure.manufacturer.MysqlManufacturerStore.Companion.MANUFACTURERS_TABLE
import zeus.user.account.service.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlManufacturerSecretStore @Inject constructor(
        client: Client,
        schemaName: String
) : ManufacturerSecretStore, MysqlStore(client, schemaName, SECRETS_TABLE) {

    override suspend fun find(
            userId: Long, manufacturerId: Long
    ): Collection<ManufacturerSecretEntity> = withSchema { schema ->
        schema.session.sql(
                "SELECT secret.* " +
                        "FROM $MANUFACTURERS_TABLE as man INNER JOIN $SECRETS_TABLE as secret " +
                        "ON man.manufacturer_id = secret.manufacturer_id " +
                        "WHERE man.user_id = ? AND secret.manufacturer_id = ?"
        )
                .bind(userId, manufacturerId)
                .findAll<ManufacturerSecretEntity>()
    }

    override suspend fun save(
            userId: Long,
            secret: ManufacturerSecretValue
    ): ManufacturerSecretEntity = withSchema { schema ->
        val insertResult = schema.session.sql(
            "INSERT INTO $SECRETS_TABLE (manufacturer_id, secret_name, secret_hash) " +
                    "SELECT ?, ?, ? WHERE" +
                    "(SELECT COUNT(*) FROM $MANUFACTURERS_TABLE WHERE user_id = ? AND manufacturer_id = ?) > 0"
        )
                .bind(secret.manufacturerId, secret.secretName, secret.secretHash, userId, secret.manufacturerId)
                .await()

        if (insertResult.affectedItemsCount == 0L) {
            throw ManufacturerManager.ManufacturerNotFoundException()
        }

        ManufacturerSecretEntity(insertResult.autoIncrementValue, secret)
    }

    override suspend fun updateSecret(
            userId: Long,
            manufacturerId: Long,
            secretId: Long,
            newSecretHash: String
    ): Boolean = withSchema { schema ->
        val updateResult = schema.session.sql(
                "UPDATE $MANUFACTURERS_TABLE as man INNER JOIN $SECRETS_TABLE as secret " +
                        "ON man.manufacturer_id = secret.manufacturer_id " +
                        "SET secret.secret_hash = ? " +
                        "WHERE man.user_id = ? AND man.manufacturer_id = ? AND secret.secret_id = ?"
        )
                .bind(newSecretHash, userId, manufacturerId, secretId)
                .await()

        updateResult.affectedItemsCount > 0
    }

    override suspend fun updateSecretName(
            userId: Long,
            manufacturerId: Long,
            secretId: Long,
            newName: String
    ): Boolean = withSchema { schema ->
        val updateResult = schema.session.sql(
                "UPDATE $MANUFACTURERS_TABLE as man INNER JOIN $SECRETS_TABLE as secret " +
                        "ON man.manufacturer_id = secret.manufacturer_id " +
                        "SET secret.secret_name = ? " +
                        "WHERE man.user_id = ? AND man.manufacturer_id = ? AND secret.secret_id = ?"
        )
                .bind(newName, userId, manufacturerId, secretId)
                .await()

        updateResult.affectedItemsCount > 0
    }

    override suspend fun remove(
            userId: Long,
            manufacturerId: Long,
            secretId: Long
    ): Boolean = withSchema { schema ->
        val deleteResult = schema.session.sql(
                "DELETE secret " +
                        "FROM $MANUFACTURERS_TABLE as man INNER JOIN $SECRETS_TABLE as secret " +
                        "ON man.manufacturer_id = secret.manufacturer_id " +
                        "WHERE man.user_id = ? AND man.manufacturer_id = ? AND secret.secret_id = ?"
        )
                .bind(userId, manufacturerId, secretId)
                .await()

        deleteResult.affectedItemsCount > 0
    }

    companion object {
        private const val SECRETS_TABLE = "manufacturer_secrets"
    }
}