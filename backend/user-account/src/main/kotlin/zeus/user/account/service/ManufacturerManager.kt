package zeus.user.account.service

import org.mindrot.jbcrypt.*
import zeus.infrstructure.transaction.*
import zeus.user.account.domain.*
import zeus.user.account.infrastructure.manufacturer.*
import zeus.user.account.infrastructure.manufacturer.secret.*
import zeus.util.exception.*
import javax.inject.*

class ManufacturerManager @Inject constructor(
        private val manufacturerStore: ManufacturerStore,
        private val secretStore: ManufacturerSecretStore,
        private val transactionProvider: TransactionProvider
) {
    suspend fun create(userId: Long): ManufacturerAggregate = transactionProvider.transactional {
        try {
            val manufacturer = manufacturerStore.save(ManufacturerValue(userId, "New manufacturer #$userId"))
            val secret = createSecret(userId, manufacturer.manufacturerId, "Main secret", generateSecret())

            ManufacturerAggregate(manufacturer, listOf(secret))
        } catch (ex: ManufacturerAlreadyExistsStoreException) {
            throw ManufacturerAlreadyExistsException(getManufacturer(userId))
        }
    }

    suspend fun updateManufacturerName(userId: Long, manufacturerId: Long, newName: String) {
        if (!manufacturerStore.updateManufacturerName(userId, manufacturerId, newName)) {
            throw ManufacturerNotFoundException()
        }
    }

    suspend fun getManufacturer(userId: Long): ManufacturerAggregate {
        val manufacturer = manufacturerStore.find(userId) ?: throw ManufacturerNotFoundException()
        val secrets = secretStore.find(userId, manufacturer.manufacturerId)

        return ManufacturerAggregate(manufacturer, secrets)
    }

    suspend fun createSecret(userId: Long, manufacturerId: Long, secretName: String, secret: String): ManufacturerSecretEntity {
        return secretStore.save(
                userId,
                ManufacturerSecretValue(
                        manufacturerId,
                        secretName,
                        BCrypt.hashpw(secret, BCrypt.gensalt())
                )
        )
    }

    suspend fun updateSecretName(userId: Long, manufacturerId: Long, secretId: Long, newName: String) {
        if (!secretStore.updateSecretName(userId, manufacturerId, secretId, newName)) {
            throw SecretNotFoundException()
        }
    }

    suspend fun removeSecret(userId: Long, manufacturerId: Long, secretId: Long) {
        if (!secretStore.remove(userId, manufacturerId, secretId)) {
            throw SecretNotFoundException()
        }
    }

    private fun generateSecret() = (1..15).map { "abcdefghiklmnopqrstuvwxyz1234567890".random() }.joinToString("")

    open class ManufacturerManagerException : NoStackTraceException()

    class ManufacturerAlreadyExistsException(val manufacturer: ManufacturerAggregate) : ManufacturerManagerException()
    class ManufacturerNotFoundException : ManufacturerManagerException()

    class SecretNotFoundException : ManufacturerManagerException()


    class ManufacturerAlreadyExistsStoreException : NoStackTraceException()

}