package zeus.user.account.infrastructure.manufacturer.secret

import zeus.user.account.domain.*
import javax.inject.*

@Singleton
class StubManufacturerSecretStore @Inject constructor() : ManufacturerSecretStore {

    private var id = 0L
    private val secrets = java.util.HashMap<Long, ManufacturerSecretEntity>()

    override suspend fun find(userId: Long, manufacturerId: Long): Collection<ManufacturerSecretEntity> =
            secrets.values.filter { it.manufacturerId == manufacturerId }

    override suspend fun save(userId: Long, secret: ManufacturerSecretValue): ManufacturerSecretEntity {
        val entity = ManufacturerSecretEntity(id++, secret)

        secrets[entity.secretId] = entity

        return entity
    }

    override suspend fun updateSecret(userId: Long, manufacturerId: Long, secretId: Long, newSecretHash: String): Boolean {
        val entity = secrets[secretId]!!

        secrets[secretId] = ManufacturerSecretEntity(secretId, manufacturerId, entity.secretName, newSecretHash)

        return true
    }

    override suspend fun updateSecretName(userId: Long, manufacturerId: Long, secretId: Long, newName: String): Boolean {
        val entity = secrets[secretId]!!

        secrets[secretId] = ManufacturerSecretEntity(secretId, manufacturerId, newName, entity.secretHash)

        return true
    }

    override suspend fun remove(userId: Long, manufacturerId: Long, secretId: Long): Boolean {
        secrets.remove(secretId)

        return true
    }
}