package zeus.user.account.infrastructure.manufacturer

import kotlinx.coroutines.*
import zeus.user.account.domain.*
import java.util.*
import javax.inject.*

@Singleton
class StubManufacturerStore @Inject constructor() : ManufacturerStore {

    private val manufacturer = HashMap<Long, ManufacturerEntity>()
    private var id: Long = 0

    init {
        GlobalScope.launch { save(ManufacturerValue(1, "Test 1")) }
    }

    override suspend fun find(userId: Long): ManufacturerEntity? = manufacturer[userId]

    override suspend fun save(manufacturerValue: ManufacturerValue): ManufacturerEntity {
        val entity = ManufacturerEntity(id++, manufacturerValue)
        manufacturer[entity.manufacturerId] = entity
        return entity
    }

    override suspend fun updateManufacturerName(userId: Long, manufacturerId: Long, manufacturerName: String): Boolean {
        val entity = manufacturer[userId]!!
        val updated = ManufacturerEntity(entity.manufacturerId, userId, manufacturerName)
        manufacturer[userId] = updated
        return true
    }
}
