package zeus.user.account.infrastructure.device

import zeus.user.account.domain.*
import zeus.user.account.service.*

interface DeviceStore {

    suspend fun findDevices(userId: Long): Collection<DeviceEntity>

    suspend fun findDevice(userId: Long, deviceId: Long): DeviceEntity?

    suspend fun remove(userId: Long, deviceId: Long): Boolean

    suspend fun save(deviceValue: DeviceValue): DeviceEntity

    suspend fun updateDevice(updateRequest: DeviceManager.UpdateDeviceRequest): Boolean
    
}
