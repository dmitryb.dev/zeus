package zeus.user.account.module.prod

import com.mysql.cj.xdevapi.*
import com.typesafe.config.*
import dagger.*
import org.flywaydb.core.*
import zeus.infrstructure.transaction.*
import zeus.user.account.infrastructure.device.*
import zeus.user.account.infrastructure.manufacturer.*
import zeus.user.account.infrastructure.manufacturer.secret.*
import zeus.user.account.module.*
import zeus.util.mysql.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

@Module
object InfrastructureModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideInitializer(config: Config): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {
            Flyway.configure()
                    .dataSource(
                            "jdbc:mysql://${config.getString("mysql.host")}:${config.getString("mysql.port")}/" +
                                    config.getString("mysql.schema") + "?useSSL=false",
                            config.getString("mysql.user"),
                            config.getString("mysql.pass")
                    )
                    .locations("sql")
                    .load()
                    .migrate()
        }
    }

    @JvmStatic
    @Provides
    fun provideTransactionProvider(client: Client): TransactionProvider = MysqlTransactionProvider(client)

    @JvmStatic
    @Singleton
    @Provides
    fun provideMysqlClient(config: Config) = Clients.fromConfig(config)

    @JvmStatic
    @Provides
    fun provideSecretStore(
            config: Config,
            client: Client
    ): ManufacturerSecretStore = MysqlManufacturerSecretStore(client, config.getString("mysql.schema"))

    @JvmStatic
    @Provides
    fun provideManufacturerStore(
            config: Config,
            client: Client
    ): ManufacturerStore = MysqlManufacturerStore(client, config.getString("mysql.schema"))

    @JvmStatic
    @Provides
    fun provideDeviceStore(
            config: Config,
            client: Client
    ): DeviceStore = MysqlDeviceStore(client, config.getString("mysql.schema"))

}
