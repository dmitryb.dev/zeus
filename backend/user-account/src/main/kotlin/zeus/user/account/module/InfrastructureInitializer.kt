package zeus.user.account.module

interface InfrastructureInitializer {
    fun init() {}
}