package zeus.user.account.module.stub

import dagger.*
import zeus.infrstructure.transaction.*
import zeus.user.account.infrastructure.device.*
import zeus.user.account.infrastructure.manufacturer.*
import zeus.user.account.infrastructure.manufacturer.secret.*
import zeus.user.account.module.*
import javax.inject.*

@Module
object StubInfrastructureModule {

    @JvmStatic
    @Provides
    fun provideInitializer(): InfrastructureInitializer = object : InfrastructureInitializer {}

    @JvmStatic
    @Provides
    fun provideTransactionProvider(): TransactionProvider = StubTransactionProvider()

    @JvmStatic
    @Provides
    @Singleton
    fun provideSecretStore(): ManufacturerSecretStore = StubManufacturerSecretStore()

    @JvmStatic
    @Provides
    @Singleton
    fun provideManufacturerStore(manufacturerStore: StubManufacturerStore): ManufacturerStore = manufacturerStore

    @JvmStatic
    @Provides
    @Singleton
    fun provideDeviceRepository(deviceStore: StubDeviceStore): DeviceStore = deviceStore

}
