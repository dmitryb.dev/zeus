package zeus.user.account.entrypoint.util

fun String.mask() = this.replace(".".toRegex(), "*")