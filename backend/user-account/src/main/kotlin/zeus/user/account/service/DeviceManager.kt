package zeus.user.account.service

import org.mindrot.jbcrypt.*
import user.account.dto.*
import zeus.security.device.*
import zeus.user.account.domain.*
import zeus.user.account.infrastructure.device.*
import zeus.util.exception.*
import javax.inject.*

class DeviceManager @Inject constructor(private val deviceStore: DeviceStore) {

    suspend fun findDevices(request: DeviceDtos.FindUserDevicesRequest): Collection<DeviceEntity> =
        deviceStore.findDevices(request.userId).filter { device ->
            (!request.hasManufacturer() || request.manufacturer.id == device.manufacturerId)
                    && (!request.hasDevice() || request.device.number == device.deviceNumber)
                    && (device.tags.split(',').containsAll(request.tagsList))
                    && (!request.hasRole() || request.role.value == device.role)
                    && (!request.hasPass() || BCrypt.checkpw(request.pass.value, device.passHash))
        }

    suspend fun getDevice(userId: Long, userDeviceId: Long): DeviceEntity =
            deviceStore.findDevice(userId, userDeviceId) ?: throw NoSuchDeviceException()

    suspend fun addDevice(request: DeviceDtos.AddDeviceRequest): DeviceEntity {
        val device = DeviceValue(
                request.userId,
                request.filter.manufacturerId,
                request.filter.deviceNumber,
                request.filter.tagsList.joinToString(","),
                request.role,
                DevicePass.hash(request.filter, request.pass),
                request.name
        )
        return deviceStore.save(device)
    }

    suspend fun removeDevice(userId: Long, userDeviceId: Long) {
        if (!deviceStore.remove(userId, userDeviceId)) {
            throw NoSuchDeviceException()
        }
    }

    suspend fun updateDevice(request: UpdateDeviceRequest) {
        if (!deviceStore.updateDevice(request)) {
            throw NoSuchDeviceException()
        }
    }

    open class DeviceManagerException : NoStackTraceException()
    class NoSuchDeviceException : DeviceManagerException()
    class NoSuchManufacturerException : DeviceManagerException()

    class UpdateDeviceRequest(
            val userId: Long,
            val deviceId: Long,
            val manufacturerId: Long,
            val deviceNumber: Long,
            val tags: String,
            val role: String,
            val passHash: String?,
            val name: String
    )

}
