package zeus.user.account

import dagger.*
import zeus.user.account.module.*
import zeus.user.account.module.dev.*
import zeus.user.account.module.stub.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    DevConfigModule::class,
    StubInfrastructureModule::class
])
interface StubUserAccount : UserAccount
