package zeus.user.account.infrastructure.device

import com.mysql.cj.xdevapi.*
import zeus.user.account.domain.*
import zeus.user.account.service.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlDeviceStore @Inject constructor(
        client: Client,
        schemaName: String
) : DeviceStore, MysqlStore(client, schemaName, DEVICES_TABLE) {

    override suspend fun findDevices(userId: Long): Collection<DeviceEntity> = withTable { table ->
        table
                .select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findAll<DeviceEntity>()
    }

    override suspend fun findDevice(userId: Long, deviceId: Long): DeviceEntity? = withTable { table ->
        table
                .select("*")
                .where("user_id = :userId AND device_id = :deviceId")
                .bind("userId", userId)
                .bind("deviceId", deviceId)
                .findOne<DeviceEntity>()
    }


    override suspend fun remove(userId: Long, deviceId: Long): Boolean = withTable { table ->
        val result = table.delete()
                .where("user_id = :userId AND device_id = :deviceId")
                .bind("userId", userId)
                .bind("deviceId", deviceId)
                .await()

        result.affectedItemsCount > 0
    }

    override suspend fun save(deviceValue: DeviceValue): DeviceEntity = try {
        withTable { table ->
            val insertResult = table
                    .insert(deviceValue)
                    .await()

            DeviceEntity(insertResult.autoIncrementValue, deviceValue)
        }
    } catch (ex: ForeignKeyViolated) {
        throw DeviceManager.NoSuchManufacturerException()
    }

    override suspend fun updateDevice(updateRequest: DeviceManager.UpdateDeviceRequest): Boolean = try {
        val values = mutableMapOf<String, Any>(
                "manufacturer_id" to updateRequest.manufacturerId,
                "device_number" to updateRequest.deviceNumber,
                "tags" to updateRequest.tags,
                "role" to updateRequest.role,
                "name" to updateRequest.name
        )

        updateRequest.passHash?.let { values["pass"] = it }

        withTable { table ->
            val updateResult = table
                    .update()
                    .set(values)
                    .where("user_id = :userId AND device_id = :deviceId")
                    .bind("userId", updateRequest.userId)
                    .bind("deviceId", updateRequest.deviceId)
                    .await()

            updateResult.affectedItemsCount > 0
        }
    } catch (ex: ForeignKeyViolated) {
        throw DeviceManager.NoSuchManufacturerException()
    }

    companion object {
        private const val DEVICES_TABLE = "devices"
    }
}
