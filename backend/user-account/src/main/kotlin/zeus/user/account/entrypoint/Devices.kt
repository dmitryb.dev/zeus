package zeus.user.account.entrypoint

import io.grpc.*
import kotlinx.coroutines.channels.*
import user.account.dto.*
import user.account.rpc.*
import zeus.common.domain.device.*
import zeus.security.device.*
import zeus.user.account.entrypoint.util.*
import zeus.user.account.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class Devices @Inject constructor(
        private val deviceManager: DeviceManager
) : DeviceCoroutineGrpc.DeviceImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun findUserDevices(
            request: DeviceDtos.FindUserDevicesRequest,
            responseChannel: SendChannel<DeviceDtos.UserDeviceResponse>
    ) = deviceManager.findDevices(request)
            .map {
                UserDeviceResponse {
                    deviceId = it.deviceId
                    filter {
                        manufacturerId = it.manufacturerId
                        deviceNumber = it.deviceNumber
                        addAllTags(it.tags.split(",").filter(String::isNotBlank))
                    }
                    role = it.role
                    name = it.name
                }
            }
            .forEach { responseChannel.send(it) }

    override suspend fun updateDevice(
            request: DeviceDtos.UpdateDeviceRequest
    ): DeviceApi.DeviceUpdatedResponse = log.uncaught {
        try {
            log.debug("Updating device with id: {} for user id: {}", request.deviceId, request.userId)

            if (request.hasPass()) {
                log.debug("Password will be updated, new pass: {}", request.pass.value.mask())
            } else {
                log.debug("Password won't be updated")
            }

            val requestDto = DeviceManager.UpdateDeviceRequest(
                    request.userId,
                    request.deviceId,
                    request.filter.manufacturerId,
                    request.filter.deviceNumber,
                    request.filter.tagsList.joinToString(","),
                    request.role,
                    if (request.hasPass()) DevicePass.hash(request.filter, request.pass.value) else null,
                    request.name
            )
            
            deviceManager.updateDevice(requestDto)

            log.debug("Device updated")

            DeviceUpdatedResponse {}
        } catch (ex: DeviceManager.NoSuchManufacturerException) {
            log.debug("Manufacturer with such id not found")
            throw Status.FAILED_PRECONDITION.asException()
        } catch (ex: DeviceManager.NoSuchDeviceException) {
            log.debug("Device with such id not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun addDevice(request: DeviceDtos.AddDeviceRequest): DeviceApi.DeviceAddedResponse = log.uncaught {
        try {
            log.debug("Adding device: {} with role: {}, pass: {} for user id: {}", request.filter.toPrettyString(),
                    request.role, request.pass.mask(), request.userId)

            val deviceEntity = deviceManager.addDevice(request)

            log.debug("Device added, id: {}", deviceEntity.deviceId)

            DeviceAddedResponse {
                this.deviceId = deviceEntity.deviceId
            }
        } catch (ex: DeviceManager.NoSuchManufacturerException) {
            log.debug("Manufacturer with such id not found")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    override suspend fun removeDevice(
            request: DeviceApi.RemoveDeviceRequest
    ): DeviceApi.DeviceRemovedResponse = log.uncaught {
        try {
            log.debug("Removing device with id: {} for user id: {}", request.userDeviceId, request.userId)

            deviceManager.removeDevice(request.userId, request.userDeviceId)

            log.debug("Device removed")

            DeviceRemovedResponse {}
        } catch (ex: DeviceManager.NoSuchDeviceException) {
            log.debug("Device not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    companion object {
        val log = logger<Devices>()
    }
}
