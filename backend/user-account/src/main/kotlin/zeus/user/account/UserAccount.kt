package zeus.user.account

import zeus.util.*

interface UserAccount {
    fun grpcServer(): GrpcServer
}
