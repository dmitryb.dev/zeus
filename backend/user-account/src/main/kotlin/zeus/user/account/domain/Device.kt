package zeus.user.account.domain

import zeus.util.mysql.mapper.annotation.*

open class DeviceValue(
        @get:Column("user_id") val userId: Long,
        @get:Column("manufacturer_id") val manufacturerId: Long,
        @get:Column("device_number") val deviceNumber: Long,
        @get:Column("tags") val tags: String,
        @get:Column("role") val role: String,
        @get:Column("pass_hash") val passHash: String,
        @get:Column("name") val name: String
)

open class DeviceEntity @EntityCreator constructor(
        @Column("device_id") @get:Column("device_id") val deviceId: Long,
        @Column("user_id") userId: Long,
        @Column("manufacturer_id") manufacturerId: Long,
        @Column("device_number") deviceNumber: Long,
        @Column("tags") tags: String,
        @Column("role") role: String,
        @Column("pass_hash") passHash: String,
        @Column("name") name: String
) : DeviceValue(userId, manufacturerId, deviceNumber, tags, role, passHash, name) {

    constructor(deviceId: Long, deviceValue: DeviceValue) : this(deviceId, deviceValue.userId,
            deviceValue.manufacturerId, deviceValue.deviceNumber, deviceValue.tags, deviceValue.role,
            deviceValue.passHash, deviceValue.name)
}