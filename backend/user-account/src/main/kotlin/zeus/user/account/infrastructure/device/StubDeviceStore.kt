package zeus.user.account.infrastructure.device

import kotlinx.coroutines.*
import org.mindrot.jbcrypt.*
import zeus.user.account.domain.*
import zeus.user.account.service.*
import java.util.*
import javax.inject.*

@Singleton
class StubDeviceStore @Inject constructor() : DeviceStore {

    private val devices = HashMap<Long, DeviceEntity>()
    private var id: Long = 0

    init {
        runBlocking {
            val pass = BCrypt.hashpw("12345", BCrypt.gensalt())
            save(DeviceEntity(1, DeviceValue(1, 1, 1, "v1", "USER", pass, "Camera")))
            save(DeviceEntity(2, DeviceValue(1, 2, 1, "v1,v2", "ADMIN", pass, "Boiler")))
            save(DeviceEntity(3, DeviceValue(1, 1, 2, "v1,v2,v3", "USER", pass, "Toilet")))
        }
    }

    override suspend fun findDevices(userId: Long): Collection<DeviceEntity> = devices.values
            .filter { device -> device.userId == userId }

    override suspend fun findDevice(userId: Long, deviceId: Long): DeviceEntity? = devices[deviceId]

    override suspend fun remove(userId: Long, deviceId: Long) = devices.remove(deviceId) != null

    override suspend fun save(deviceValue: DeviceValue): DeviceEntity {
        val savedDevice = DeviceEntity(id++, deviceValue)
        devices[savedDevice.deviceId] = savedDevice
        return savedDevice
    }

    override suspend fun updateDevice(updateRequest: DeviceManager.UpdateDeviceRequest): Boolean {
        val newEntity = DeviceEntity(
                updateRequest.deviceId, 
                updateRequest.userId,
                updateRequest.manufacturerId,
                updateRequest.deviceNumber,
                updateRequest.tags,
                updateRequest.role,
                updateRequest.passHash ?: devices[updateRequest.deviceId]!!.passHash,
                updateRequest.name
        )
        devices[updateRequest.deviceId] = newEntity
        return true
    }

}
