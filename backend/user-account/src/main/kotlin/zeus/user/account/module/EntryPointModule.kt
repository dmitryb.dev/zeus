package zeus.user.account.module

import com.typesafe.config.*
import dagger.*
import zeus.user.account.entrypoint.*
import zeus.util.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideGrpcServer(
            config: Config,
            initializer: InfrastructureInitializer,
            manufacturerApi: Manufacturers,
            deviceApi: Devices
    ): GrpcServer {
        initializer.init()
        return GrpcServer(config.getInt("grpc.server.port"), manufacturerApi, deviceApi)
    }

}
