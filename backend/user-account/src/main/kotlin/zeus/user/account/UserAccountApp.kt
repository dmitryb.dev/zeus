package zeus.user.account

import zeus.util.*

fun main(args: Array<String>) {
    val userAccounts = ModeFactory
            .withDefaultMode<UserAccount>("prod", DaggerProdUserAccount::create)
            .withMode("dev", DaggerDevUserAccount::create)
            .withMode("stub", DaggerStubUserAccount::create)
            .create(args)

    userAccounts.grpcServer().start()
}