package zeus.user.account.entrypoint

import io.grpc.*
import io.grpc.protobuf.*
import user.account.rpc.*
import user.account.rpc.ManufacturerInfoResponse.ManufacturerSecret
import zeus.user.account.domain.*
import zeus.user.account.entrypoint.util.*
import zeus.user.account.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class Manufacturers @Inject constructor(
        private val manufacturerManager: ManufacturerManager
) : ManufacturerCoroutineGrpc.ManufacturerImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun createManufacturer(
            request: ManufacturerApi.CreateManufacturerReqeust
    ): ManufacturerApi.ManufacturerInfoResponse = log.uncaught {
        try {
            log.debug("Creating manufacturer for user with id: {}", request.userId)

            val aggregate = manufacturerManager.create(request.userId)

            log.debug("Manufacturer created with id: {}", aggregate.manufacturer.manufacturerId)

            aggregate.toDto()
        } catch (ex: ManufacturerManager.ManufacturerAlreadyExistsException) {
            log.debug("Manufacturer for user already created")
            throw Status.ALREADY_EXISTS.asException(ex.manufacturer.toDto().let { response ->
                Metadata().apply {
                    put(ProtoUtils.keyForProto(response), response)
                }
            })
        }
    }

    override suspend fun updateManufacturerName(
            request: ManufacturerApi.UpdateManufacturerNameRequest
    ): ManufacturerApi.ManufacturerNameUpdatedResponse = log.uncaught {
        try {
            log.debug("Updating manufacturer name for user with id: {}, manufacturer id: {}, new name: {}",
                    request.userId, request.manufacturerId, request.newName)

            manufacturerManager.updateManufacturerName(request.userId, request.manufacturerId, request.newName)

            log.debug("Manufacturer name updated")

            ManufacturerNameUpdatedResponse {}
        } catch (ex: ManufacturerManager.ManufacturerNotFoundException) {
            log.debug("Manufacturer not found/name not changed")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun getManufacturer(
            request: ManufacturerApi.GetManufacturerRequest
    ): ManufacturerApi.ManufacturerInfoResponse = log.uncaught {
        try {
            val aggregate = manufacturerManager.getManufacturer(request.userId)

            aggregate.toDto()
        } catch (ex: ManufacturerManager.ManufacturerNotFoundException) {
            log.debug("Manufacturer not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun createManufacturerSecret(
            request: ManufacturerApi.CreateManufacturerSecretRequest
    ): ManufacturerApi.ManufacturerSecretCreatedResponse = log.uncaught {
        try {
            log.debug("Creating secret for user with id: {}, manufacturer id: {}, secret name: {}, secret: {}",
                    request.userId, request.manufacturerId, request.secretName, request.secret.mask())

            val secret = manufacturerManager.createSecret(request.userId, request.manufacturerId, request.secretName,
                    request.secret)

            log.debug("Secret created with id: {}", secret.secretId)

            ManufacturerSecretCreatedResponse {
                secretId = secret.secretId
            }
        } catch (ex: ManufacturerManager.ManufacturerNotFoundException) {
            log.debug("Manufacturer not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun updateManufacturerSecretName(
            request: ManufacturerApi.UpdateManufacturerSecretNameRequest
    ): ManufacturerApi.ManufacturerSecretNameUpdatedResponse = log.uncaught {
        try {
            log.debug("Updating secret name for user with id: {}, manufacturer id: {}, secret id: {}, new name: {}",
                    request.userId, request.manufacturerId, request.secretId, request.newName)

            manufacturerManager.updateSecretName(request.userId, request.manufacturerId, request.secretId,
                    request.newName)

            log.debug("Secret name updated")

            ManufacturerSecretNameUpdatedResponse {}
        } catch (ex: ManufacturerManager.SecretNotFoundException) {
            log.debug("Secret not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun removeManufacturerSecret(
            request: ManufacturerApi.RemoveManufacturerSecretRequest
    ): ManufacturerApi.ManufacturerSecretRemovedResponse = log.uncaught {
        try {
            log.debug("Removing secret for user with id: {}, manufacturer id: {}, secret id: {}",
                    request.userId, request.manufacturerId, request.secretId)

            manufacturerManager.removeSecret(request.userId, request.manufacturerId, request.secretId)

            log.debug("Secret removed")

            ManufacturerSecretRemovedResponse {}
        } catch (ex: ManufacturerManager.SecretNotFoundException) {
            log.debug("Secret not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    companion object {
        private val log = logger<Manufacturers>()

        private fun ManufacturerAggregate.toDto() = ManufacturerInfoResponse {
            manufacturerId = this@toDto.manufacturer.manufacturerId
            manufacturerName = this@toDto.manufacturer.manufacturerName
            addAllSecrets(this@toDto.secrets.map { secret ->
                ManufacturerSecret {
                    secretId = secret.secretId
                    secretName = secret.secretName
                }
            })
        }
    }
}