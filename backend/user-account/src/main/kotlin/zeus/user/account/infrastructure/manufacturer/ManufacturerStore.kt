package zeus.user.account.infrastructure.manufacturer

import zeus.user.account.domain.*
import zeus.util.exception.*

interface ManufacturerStore {

    suspend fun find(userId: Long): ManufacturerEntity?
    suspend fun save(manufacturerValue: ManufacturerValue): ManufacturerEntity
    suspend fun updateManufacturerName(userId: Long, manufacturerId: Long, manufacturerName: String): Boolean

}
