package zeus.user.account.module.dev

import com.typesafe.config.*
import dagger.*

@Module
object DevConfigModule {

    @JvmStatic
    @Provides
    fun provideConfig(): Config = ConfigFactory.load("application-dev.conf")
            .withFallback(ConfigFactory.load())

}
