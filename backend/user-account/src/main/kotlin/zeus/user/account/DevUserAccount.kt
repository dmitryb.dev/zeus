package zeus.user.account

import dagger.*
import zeus.user.account.module.*
import zeus.user.account.module.dev.*
import zeus.user.account.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    DevConfigModule::class
])
interface DevUserAccount : UserAccount
