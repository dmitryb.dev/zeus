CREATE TABLE manufacturers(
  manufacturer_id bigint AUTO_INCREMENT PRIMARY KEY,
  user_id bigint UNIQUE,
  manufacturer_name varchar(255)
);

CREATE TABLE manufacturer_secrets(
  secret_id bigint AUTO_INCREMENT PRIMARY KEY,
  manufacturer_id bigint,
  secret_name varchar(255),
  secret_hash varchar(255),

  INDEX i__manufacturer_secrets__manufacturer_id (manufacturer_id)
);

CREATE TABLE devices(
  device_id bigint AUTO_INCREMENT PRIMARY KEY,
  user_id bigint,
  manufacturer_id bigint,
  device_number bigint,
  tags varchar(255),
  role varchar(255),
  pass_hash varchar(255),
  name varchar(255)

  INDEX i__devices__user_id (user_id),
  CONSTRAINT fk__devices__manufacturer_id FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(manufacturer_id)
);