CREATE TABLE IF NOT EXISTS device-auth (
  companyId bigint,
  deviceId bigint,
  authData varchar(2048),
  PRIMARY KEY (companyId, deviceId)
);

CREATE TABLE IF NOT EXISTS company-secrets (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  companyId bigint,
  secretName varchar(255)
  secretHash varchar(255)
);