package zeus.deviceauth;

import com.iotui.util.EnvFactory;

public class DeviceAuthApp {

    public static void main(String[] args) {
        DeviceAuth deviceAuth = EnvFactory
                .<DeviceAuth>withDefaultEnv("prod", DaggerProdDeviceAuth::create)
                .withEnv("dev", DaggerDevDeviceAuth::create)
                .withEnv("mock", DaggerProdDeviceAuth::create)
                .create();

        deviceAuth.grpcServer().start();
    }

}
