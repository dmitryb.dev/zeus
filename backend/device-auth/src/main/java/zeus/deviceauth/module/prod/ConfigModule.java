package zeus.deviceauth.module.prod;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import dagger.Module;
import dagger.Provides;

@Module
public class ConfigModule {

    @Provides static Config provideConfig() {
        return ConfigFactory.load();
    }

}
