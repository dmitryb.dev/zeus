package zeus.deviceauth.module.dev;

import zeus.deviceauth.infrastrucutre.repository.company.CompanySecretsRepository;
import zeus.deviceauth.infrastrucutre.repository.company.StubCompanySecretsRepository;
import zeus.deviceauth.infrastrucutre.repository.device.DeviceAuthRepository;
import zeus.deviceauth.infrastrucutre.repository.device.StubDeviceAuthRepository;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class MockRepositoriesModule {

    @Provides @Singleton static CompanySecretsRepository provideCompanySecretsRepository(
            StubCompanySecretsRepository secretsRepository) {

        return secretsRepository;
    }

    @Provides @Singleton static DeviceAuthRepository provideDeviceAuthRepository(
            StubDeviceAuthRepository deviceAuthRepository) {

        return deviceAuthRepository;
    }

}
