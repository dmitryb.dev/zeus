package zeus.deviceauth.module.prod;

import zeus.deviceauth.service.company.BcryptedCompanySecretsManager;
import zeus.deviceauth.service.company.CompanySecretsManager;
import zeus.deviceauth.service.device.BcryptedDeviceAuthManager;
import zeus.deviceauth.service.device.DeviceAuthManager;
import dagger.Module;
import dagger.Provides;

@Module
public class ServicesModule {

    @Provides static CompanySecretsManager provideCompanySecretsManager(BcryptedCompanySecretsManager secretsManager) {
        return secretsManager;
    }

    @Provides static DeviceAuthManager provideDeviceAuthManager(BcryptedDeviceAuthManager authManager) {
        return authManager;
    }

}
