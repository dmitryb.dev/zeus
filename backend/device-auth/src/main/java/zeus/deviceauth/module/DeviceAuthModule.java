package zeus.deviceauth.module;

import zeus.deviceauth.endpoint.CompanyController;
import zeus.deviceauth.endpoint.DeviceAuthController;
import com.iotui.grpc.GrpcServer;
import com.typesafe.config.Config;
import dagger.Module;
import dagger.Provides;

@Module
public class DeviceAuthModule {

    @Provides static GrpcServer provideGrpcServer(Config config,
                                                  CompanyController companyController,
                                                  DeviceAuthController deviceAuthController) {

        return new GrpcServer(config.getInt("grpc.server.port"), companyController, deviceAuthController);
    }


}
