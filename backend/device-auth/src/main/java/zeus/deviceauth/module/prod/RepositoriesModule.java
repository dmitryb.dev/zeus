package zeus.deviceauth.module.prod;

import zeus.deviceauth.infrastrucutre.repository.MysqlBootstrap;
import zeus.deviceauth.infrastrucutre.repository.company.CompanySecretsRepository;
import zeus.deviceauth.infrastrucutre.repository.company.MysqlCompanySecretsRepository;
import zeus.deviceauth.infrastrucutre.repository.device.DeviceAuthRepository;
import zeus.deviceauth.infrastrucutre.repository.device.MysqlDeviceAuthRepository;
import com.iotui.util.mysql.Schemas;
import com.mysql.cj.xdevapi.Schema;
import com.typesafe.config.Config;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class RepositoriesModule {

    @Provides @Singleton static Schema provideSchema(Config config) {
        Schema schema = Schemas.fromConfig(config);
        MysqlBootstrap.create(schema);
        return schema;
    }
    @Provides static CompanySecretsRepository provideCompanySecretsRepository(
            MysqlCompanySecretsRepository secretsRepository) {

        return secretsRepository;
    }

    @Provides static DeviceAuthRepository provideDeviceAuthRepository(MysqlDeviceAuthRepository authRepository) {
        return authRepository;
    }

}
