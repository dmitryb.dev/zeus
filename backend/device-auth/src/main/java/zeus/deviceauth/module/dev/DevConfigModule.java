package zeus.deviceauth.module.dev;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import dagger.Module;
import dagger.Provides;

@Module
public class DevConfigModule {

    @Provides
    static Config provideConfig() {
        return ConfigFactory.load("application-dev.conf");
    }

}
