package zeus.deviceauth;

import zeus.deviceauth.module.dev.DevConfigModule;
import zeus.deviceauth.module.DeviceAuthModule;
import zeus.deviceauth.module.prod.RepositoriesModule;
import zeus.deviceauth.module.prod.ServicesModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        DeviceAuthModule.class,
        ServicesModule.class,
        RepositoriesModule.class,
        DevConfigModule.class
})
public interface DevDeviceAuth extends DeviceAuth {}
