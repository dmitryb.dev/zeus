package zeus.deviceauth;

import zeus.deviceauth.module.DeviceAuthModule;
import zeus.deviceauth.module.dev.DevConfigModule;
import zeus.deviceauth.module.dev.MockRepositoriesModule;
import zeus.deviceauth.module.prod.ServicesModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        DeviceAuthModule.class,
        ServicesModule.class,
        MockRepositoriesModule.class,
        DevConfigModule.class
})
public interface StubDeviceAuth extends DeviceAuth {}
