package zeus.deviceauth;

import com.iotui.grpc.GrpcServer;

public interface DeviceAuth {
    GrpcServer grpcServer();
}
