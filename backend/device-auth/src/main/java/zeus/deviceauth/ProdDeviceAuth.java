package zeus.deviceauth;

import zeus.deviceauth.module.DeviceAuthModule;
import zeus.deviceauth.module.prod.ConfigModule;
import zeus.deviceauth.module.prod.RepositoriesModule;
import zeus.deviceauth.module.prod.ServicesModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        DeviceAuthModule.class,
        ServicesModule.class,
        RepositoriesModule.class,
        ConfigModule.class
})
public interface ProdDeviceAuth extends DeviceAuth {}
