package zeus.deviceauth.service.device;

import com.iotui.util.exception.NoStackTraceException;
import io.reactivex.Completable;
import io.reactivex.Single;
import types.Deviceid.DeviceId;

import java.util.List;

public interface DeviceAuthManager {

    Completable updateDeviceAuth(DeviceId deviceId, List<DeviceAuth> creds);

    Single<VerificationResult> validateDeviceAuth(DeviceId deviceId, String role, String passHash);

    class DeviceAuthManagerException extends NoStackTraceException {}

    class DeviceAuth {
        private final String role;
        private final String pass;

        public DeviceAuth(String role, String pass) {
            this.role = role;
            this.pass = pass;
        }

        public String getRole() {
            return role;
        }

        public String getPass() {
            return pass;
        }
    }

    enum VerificationResult {
        PassCorrect, PassIncorrect, NoSuchRole, NoSuchDevice
    }

}
