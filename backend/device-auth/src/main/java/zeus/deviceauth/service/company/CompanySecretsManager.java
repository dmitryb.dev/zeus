package zeus.deviceauth.service.company;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface CompanySecretsManager {

    Single<SavedSecret> addSecret(long companyId, SecretProps secretProps);
    Completable removeSecret(long companyId, long secretId);
    Observable<SavedSecret> getSecrets(long companyId);

    Single<Boolean> validateSecret(long companyId, String secret);

    class SecretProps {
        private final String name;
        private final String secret;

        public SecretProps(String name, String secret) {
            this.name = name;
            this.secret = secret;
        }

        public String getName() {
            return name;
        }

        public String getSecret() {
            return secret;
        }
    }

    class SavedSecret {
        private final long id;
        private final String name;

        public SavedSecret(long id, String name) {
            this.id = id;
            this.name = name;
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

}
