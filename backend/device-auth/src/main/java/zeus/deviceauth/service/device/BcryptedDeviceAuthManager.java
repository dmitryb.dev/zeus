package zeus.deviceauth.service.device;

import zeus.deviceauth.infrastrucutre.repository.device.DeviceAuthRepository;
import com.iotui.security.signature.DevicePassHash;
import io.reactivex.Completable;
import io.reactivex.Single;
import types.Deviceid;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class BcryptedDeviceAuthManager implements DeviceAuthManager {

    private final DeviceAuthRepository authRepository;

    @Inject
    public BcryptedDeviceAuthManager(DeviceAuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public Completable updateDeviceAuth(Deviceid.DeviceId deviceId, List<DeviceAuth> creds) {
        List<DeviceAuthRepository.DeviceAuth> hashed = creds.stream()
                .map(da -> new DeviceAuthRepository.DeviceAuth(
                        da.getRole(),
                        DevicePassHash.hash(deviceId.getCompanyId(), deviceId.getDeviceId(), da.getRole(), da.getPass())
                ))
                .collect(Collectors.toList());
        return authRepository.saveDeviceAuth(deviceId, hashed);
    }

    @Override
    public Single<VerificationResult> validateDeviceAuth(Deviceid.DeviceId deviceId, String role, String passHash) {
        return authRepository.findDeviceAuths(deviceId)
                .map(availableAuths -> validateDeviceAuth(availableAuths, role, passHash))
                .switchIfEmpty(Single.just(VerificationResult.NoSuchDevice));
    }

    private VerificationResult validateDeviceAuth(List<DeviceAuthRepository.DeviceAuth> auths,
                                                  String requestedRole, String requestedPassHash) {
        List<String> passHashesWithMatchingRole = auths.stream()
                .filter(auth -> requestedRole.equals(auth.getRole()))
                .map(DeviceAuthRepository.DeviceAuth::getPassHash)
                .collect(Collectors.toList());

        if (passHashesWithMatchingRole.isEmpty()) {
            return VerificationResult.NoSuchRole;
        }

        return passHashesWithMatchingRole.stream()
                .anyMatch(passHash -> DevicePassHash.passMatches(passHash, requestedPassHash))
                ? VerificationResult.PassCorrect
                : VerificationResult.PassIncorrect;
    }
}
