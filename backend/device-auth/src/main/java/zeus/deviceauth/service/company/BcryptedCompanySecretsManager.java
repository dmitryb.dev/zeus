package zeus.deviceauth.service.company;

import zeus.deviceauth.infrastrucutre.repository.company.CompanySecretsRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.internal.functions.Functions;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Inject;

public class BcryptedCompanySecretsManager implements CompanySecretsManager {

    private final CompanySecretsRepository companySecretsRepository;

    @Inject
    public BcryptedCompanySecretsManager(CompanySecretsRepository companySecretsRepository) {
        this.companySecretsRepository = companySecretsRepository;
    }

    @Override
    public Single<SavedSecret> addSecret(long companyId, SecretProps secretProps) {
        return companySecretsRepository.saveSecret(new CompanySecretsRepository.SecretProps(
                companyId,
                secretProps.getName(),
                BCrypt.hashpw(secretProps.getSecret(), BCrypt.gensalt())
        ))
                .map(savedSecret -> new SavedSecret(savedSecret.getSecretId(), savedSecret.getName()));
    }

    @Override
    public Completable removeSecret(long companyId, long secretId) {
        return companySecretsRepository.removeSecret(companyId, secretId);
    }

    @Override
    public Observable<SavedSecret> getSecrets(long companyId) {
        return companySecretsRepository.getSecrets(companyId)
                .map(savedSecret -> new SavedSecret(savedSecret.getSecretId(), savedSecret.getName()));
    }

    @Override
    public Single<Boolean> validateSecret(long companyId, String secret) {
        return companySecretsRepository.getSecrets(companyId)
                .filter(savedSecret -> BCrypt.checkpw(secret, savedSecret.getPassHash())) // TODO Optimize this algorithm
                .any(Functions.alwaysTrue());
    }
}
