package zeus.deviceauth.endpoint;

import zeus.deviceauth.service.company.CompanySecretsManager;
import com.iotui.util.rx.Observers;
import deviceauth.CompanyApi;
import deviceauth.CompanyApiServiceGrpc;
import io.grpc.stub.StreamObserver;
import io.reactivex.Single;

import javax.inject.Inject;

public class CompanyController extends CompanyApiServiceGrpc.CompanyApiServiceImplBase {

    private final CompanySecretsManager secretsManager;

    @Inject
    public CompanyController(CompanySecretsManager secretsManager) {
        this.secretsManager = secretsManager;
    }

    @Override
    public void getSecrets(CompanyApi.GetSecretsRequest request,
                           StreamObserver<CompanyApi.CompanySecret> responseObserver) {
        secretsManager.getSecrets(request.getCompanyId())
                .map(this::createCompanySecret)
                .subscribe(Observers.observer(responseObserver));
    }

    @Override
    public void addSecret(CompanyApi.AddSecretRequest request,
                          StreamObserver<CompanyApi.SecretAddedResponse> responseObserver) {
        secretsManager.addSecret(
                request.getCompanyId(),
                new CompanySecretsManager.SecretProps(request.getSecretName(), request.getSecret())
        )
                .map(this::createSecretAddedResponse)
                .subscribe(Observers.singeObserver(responseObserver));
    }

    @Override
    public void removeSecret(CompanyApi.RemoveSecretRequest request,
                             StreamObserver<CompanyApi.SecretRemovedResponse> responseObserver) {
        secretsManager.removeSecret(request.getCompanyId(), request.getSecretId())
                .andThen(Single.just(CompanyApi.SecretRemovedResponse.getDefaultInstance()))
                .subscribe(Observers.singeObserver(responseObserver));
    }

    private CompanyApi.CompanySecret createCompanySecret(CompanySecretsManager.SavedSecret savedSecret) {
        return CompanyApi.CompanySecret.newBuilder()
                .setSecretId(savedSecret.getId())
                .setName(savedSecret.getName())
                .build();
    }

    private CompanyApi.SecretAddedResponse createSecretAddedResponse(CompanySecretsManager.SavedSecret savedSecret) {
        return CompanyApi.SecretAddedResponse.newBuilder()
                .setSecretId(savedSecret.getId())
                .build();
    }
}
