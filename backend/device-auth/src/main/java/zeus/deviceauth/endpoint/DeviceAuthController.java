package zeus.deviceauth.endpoint;

import zeus.deviceauth.service.company.CompanySecretsManager;
import zeus.deviceauth.service.device.DeviceAuthManager;
import com.iotui.util.rx.Observers;
import deviceauth.DeviceAuthApi;
import deviceauth.DeviceAuthApiServiceGrpc;
import io.grpc.stub.StreamObserver;
import io.reactivex.Single;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class DeviceAuthController extends DeviceAuthApiServiceGrpc.DeviceAuthApiServiceImplBase {

    private final DeviceAuthManager deviceAuthManager;
    private final CompanySecretsManager companySecretsManager;

    @Inject
    public DeviceAuthController(DeviceAuthManager deviceAuthManager, CompanySecretsManager companySecretsManager) {
        this.deviceAuthManager = deviceAuthManager;
        this.companySecretsManager = companySecretsManager;
    }

    @Override
    public void updateDeviceAuth(DeviceAuthApi.UpdateAuthRequest request,
                                 StreamObserver<DeviceAuthApi.AuthUpdatedResponse> responseObserver) {
        deviceAuthManager.updateDeviceAuth(request.getDeviceId(), convertCreds(request.getCredsList()))
                .andThen(Single.just(DeviceAuthApi.AuthUpdatedResponse.getDefaultInstance()))
                .subscribe(Observers.singeObserver(responseObserver));
    }

    @Override
    public void verifyDevicePass(DeviceAuthApi.VerifyDevicePassRequest request,
                                 StreamObserver<DeviceAuthApi.DevicePassVerifiedResponse> responseObserver) {
        deviceAuthManager.validateDeviceAuth(request.getDeviceId(), request.getRole(), request.getPassHash())
                .map(this::createAuthVerifiedResponse)
                .subscribe(Observers.singeObserver(responseObserver));
    }

    @Override
    public void verifyCompanySecret(DeviceAuthApi.VerifySecretRequest request,
                                    StreamObserver<DeviceAuthApi.SecretVerifiedResponse> responseObserver) {
        companySecretsManager.validateSecret(request.getCompanyId(), request.getSecret())
                .map(this::createSecretVerfifedResponse)
                .subscribe(Observers.singeObserver(responseObserver));
    }

    private List<DeviceAuthManager.DeviceAuth> convertCreds(List<DeviceAuthApi.UpdateAuthRequest.Creds> creds) {
        return creds.stream()
                .map(value -> new DeviceAuthManager.DeviceAuth(value.getRole(), value.getPass()))
                .collect(Collectors.toList());
    }

    private DeviceAuthApi.DevicePassVerifiedResponse createAuthVerifiedResponse(
            DeviceAuthManager.VerificationResult result) {

        DeviceAuthApi.DevicePassVerifiedResponse.Builder responseBuilder =
                DeviceAuthApi.DevicePassVerifiedResponse.newBuilder();

        switch (result) {
            case PassIncorrect:
                responseBuilder.setError(DeviceAuthApi.DevicePassVerifiedResponse.Error.PASS_INCORRECT);
                break;
            case NoSuchRole:
                responseBuilder.setError(DeviceAuthApi.DevicePassVerifiedResponse.Error.NO_SUCH_ROLE);
                break;
            case NoSuchDevice:
                responseBuilder.setError(DeviceAuthApi.DevicePassVerifiedResponse.Error.NO_SUCH_DEVICE);
                break;
        }

        return responseBuilder.build();
    }

    private DeviceAuthApi.SecretVerifiedResponse createSecretVerfifedResponse(boolean isSecretValid) {
        DeviceAuthApi.SecretVerifiedResponse.Builder responseBuilder = DeviceAuthApi.SecretVerifiedResponse.newBuilder();

        if (!isSecretValid) {
            responseBuilder.setError(DeviceAuthApi.SecretVerifiedResponse.Error.INCORRECT_SECRET);
        }

        return responseBuilder.build();
    }
}
