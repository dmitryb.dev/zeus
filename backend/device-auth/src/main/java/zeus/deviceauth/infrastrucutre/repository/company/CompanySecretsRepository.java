package zeus.deviceauth.infrastrucutre.repository.company;

import com.iotui.util.mysql.mapper.annotation.Column;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface CompanySecretsRepository {

    Single<SavedSecret> saveSecret(SecretProps secret);
    Completable removeSecret(long companyId, long secretId);
    Observable<SavedSecret> getSecrets(long companyId);

    class SecretProps {
        private final long companyId;
        private final String name;
        private final String passHash;

        public SecretProps(long companyId, String name, String passHash) {
            this.companyId = companyId;
            this.name = name;
            this.passHash = passHash;
        }

        @Column("companyId")
        public long getCompanyId() {
            return companyId;
        }

        @Column("companyName")
        public String getName() {
            return name;
        }

        @Column("passHash")
        public String getPassHash() {
            return passHash;
        }
    }

    class SavedSecret extends SecretProps {
        private final long secretId;

        public SavedSecret(long secretId, SecretProps secretProps) {
            super(secretProps.getCompanyId(), secretProps.getName(), secretProps.getPassHash());
            this.secretId = secretId;
        }

        public SavedSecret(@Column("companyId") long companyId,
                           @Column("name") String name,
                           @Column("passHash") String passHash,
                           @Column("secretId") long secretId) {
            super(companyId, name, passHash);
            this.secretId = secretId;
        }

        public long getSecretId() {
            return secretId;
        }
    }

}
