package zeus.deviceauth.infrastrucutre.repository.device;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import types.Deviceid;

import java.util.List;

public interface DeviceAuthRepository {

    Completable saveDeviceAuth(Deviceid.DeviceId deviceId, List<DeviceAuth> creds);
    Maybe<List<DeviceAuth>> findDeviceAuths(Deviceid.DeviceId deviceId);

    class DeviceAuth {
        @JsonProperty("r")
        private final String role;
        @JsonProperty("ph")
        private final String passHash;

        @JsonCreator
        public DeviceAuth(@JsonProperty("r") String role,
                          @JsonProperty("ph") String passHash) {
            this.role = role;
            this.passHash = passHash;
        }

        public String getRole() {
            return role;
        }

        public String getPassHash() {
            return passHash;
        }
    }

}
