package zeus.deviceauth.infrastrucutre.repository.company;

import com.iotui.util.mysql.mapper.XMapper;
import com.iotui.util.rx.Observables;
import com.mysql.cj.xdevapi.Schema;
import com.mysql.cj.xdevapi.Table;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import javax.inject.Inject;

public class MysqlCompanySecretsRepository implements CompanySecretsRepository {

    private static final String SECRETS_TABLE = "company-secrets";

    private final Table table;

    @Inject
    public MysqlCompanySecretsRepository(Schema schema) {
        this.table = schema.getTable(SECRETS_TABLE);
    }

    @Override
    public Single<SavedSecret> saveSecret(SecretProps secret) {
        return Observables.singleFromCompletionStage(() ->
                table
                        .insert(XMapper.columnValues(secret))
                        .executeAsync()
        )
                .map(r -> new SavedSecret(r.getAutoIncrementValue(), secret));
    }

    @Override
    public Completable removeSecret(long companyId, long secretId) {
        return Observables.fromCompletionStage(() ->
                table
                        .delete()
                        .where("companyId = :companyId AND secretId = :secretId")
                        .bind("companyId", companyId)
                        .bind("secretId", secretId)
                        .executeAsync()
        )
                .ignoreElement();
    }

    @Override
    public Observable<SavedSecret> getSecrets(long companyId) {
        return Observables.singleFromCompletionStage(() ->
                table
                        .select("*")
                        .where("companyId = :companyId")
                        .bind("companyId", companyId)
                        .executeAsync()
        )
                .map(rs -> XMapper.readAll(rs, SavedSecret.class))
                .flatMapObservable(Observable::fromIterable);
    }
}
