package zeus.deviceauth.infrastrucutre.repository.device;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.vavr.control.Option;
import types.Deviceid;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class StubDeviceAuthRepository implements DeviceAuthRepository {

    private static final Map<Deviceid.DeviceId, List<DeviceAuth>> auths = new HashMap<>();

    @Inject
    public StubDeviceAuthRepository() {}

    @Override
    public Completable saveDeviceAuth(Deviceid.DeviceId deviceId, List<DeviceAuth> creds) {
        auths.put(deviceId, creds);
        return Completable.complete();
    }

    @Override
    public Maybe<List<DeviceAuth>> findDeviceAuths(Deviceid.DeviceId deviceId) {
        return Option.of(auths.get(deviceId))
                .map(Maybe::just)
                .getOrElse(Maybe.empty());
    }
}
