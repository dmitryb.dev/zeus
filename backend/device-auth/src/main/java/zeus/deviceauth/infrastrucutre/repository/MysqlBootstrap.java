package zeus.deviceauth.infrastrucutre.repository;

import com.iotui.util.resources.Resources;
import com.mysql.cj.xdevapi.Schema;
import com.mysql.cj.xdevapi.Statement;

import java.util.Arrays;

public class MysqlBootstrap {

    public static void create(Schema schema) {
        Resources.getAll("sql").stream()
                .flatMap(sql -> Arrays.stream(sql.split(";")))
                .map(String::trim)
                .map(schema.getSession()::sql)
                .forEach(Statement::execute);
    }

}
