package zeus.deviceauth.infrastrucutre.repository.company;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.stream.Collectors;

@Singleton
public class StubCompanySecretsRepository implements CompanySecretsRepository {

    private final HashMap<Long, SavedSecret> secrets = new HashMap<>();
    private long id = 0;

    @Inject
    public StubCompanySecretsRepository() {}

    @Override
    public Single<SavedSecret> saveSecret(SecretProps secret) {
        SavedSecret savedSecret = new SavedSecret(id++, secret);
        secrets.put(savedSecret.getSecretId(), savedSecret);
        return Single.just(savedSecret);
    }

    @Override
    public Completable removeSecret(long companyId, long secretId) {
        secrets.remove(secretId);
        return Completable.complete();
    }

    @Override
    public Observable<SavedSecret> getSecrets(long companyId) {
        return Observable.fromIterable(
                secrets.values().stream()
                        .filter(secret -> secret.getCompanyId() == companyId)
                        .collect(Collectors.toList())
        );
    }
}
