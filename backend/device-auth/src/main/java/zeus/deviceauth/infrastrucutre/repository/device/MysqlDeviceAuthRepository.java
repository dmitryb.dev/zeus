package zeus.deviceauth.infrastrucutre.repository.device;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iotui.util.rx.Observables;
import com.mysql.cj.xdevapi.Schema;
import com.mysql.cj.xdevapi.Table;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.vavr.control.Try;
import types.Deviceid;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.List;

public class MysqlDeviceAuthRepository implements DeviceAuthRepository {

    private static final String AUTH_TABLE = "device-auth";
    private static final String SAVE_AUTH_SQL = "INSERT INTO device-auth (companyId, deviceId, authData) " +
            "VALUES (%s, %s, %s) " +
            "ON DUPLICATE KEY UPDATE authData = authData";

    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private final Table table;

    @Inject
    public MysqlDeviceAuthRepository(Schema schema) {
        this.table = schema.getTable(AUTH_TABLE);
    }

    @Override
    public Completable saveDeviceAuth(Deviceid.DeviceId deviceId, List<DeviceAuth> creds) {
        return Observables.fromCompletionStage(() ->
                table.getSession()
                        .sql(String.format(SAVE_AUTH_SQL, deviceId.getCompanyId(), deviceId.getDeviceId(),
                                toJson(creds)
                        ))
                        .executeAsync()
        )
                .ignoreElement();
    }

    @Override
    public Maybe<List<DeviceAuth>> findDeviceAuths(Deviceid.DeviceId deviceId) {
        return Observables.singleFromCompletionStage(() ->
                table
                        .select("*")
                        .where("companyId = :companyId AND deviceId = :deviceId")
                        .bind("companyId", deviceId.getCompanyId())
                        .bind("deviceId", deviceId.getDeviceId())
                        .executeAsync()
        )
                .filter(Iterator::hasNext)
                .map(rs -> rs.fetchOne().getString("authData"))
                .map(this::fromJson);
    }


    private String toJson(List<DeviceAuth> auths) {
        return Try.of(() ->
                jsonMapper.writeValueAsString(auths)
        ).get();
    }

    private List<DeviceAuth> fromJson(String json) {
        return Try.of(() ->
                jsonMapper.<List<DeviceAuth>>readValue(
                        json,
                        jsonMapper.getTypeFactory().constructCollectionType(List.class, DeviceAuth.class)
                )
        ).get();
    }
}
