package zeus.user.auth.service

import zeus.infrstructure.transaction.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.entry.facebook.oauth.*
import zeus.user.auth.infrastructure.entry.facebook.store.*
import zeus.user.auth.infrastructure.token.*
import zeus.user.auth.infrastructure.user.*
import zeus.user.auth.service.util.*
import zeus.util.exception.*
import javax.inject.*

class FacebookEntryManager @Inject constructor(
        private val transactionProvider: TransactionProvider,
        private val accountExtractor: FacebookAccountExtractor,
        private val facebookAuthStore: FacebookAuthStore,
        private val userStore: UserStore,
        private val tokenManager: TokenManager,
        private val entryManager: EntryManager
) {

    suspend fun signIn(accessToken: String): SignInResult {
        val account = accountExtractor.getInfo(accessToken)

        class User(val userId: Long, val isNewUser: Boolean)

        suspend fun getUserId() = facebookAuthStore
                .find(account.accountId)
                ?.userId?.let { userId -> User(userId, false) }
                ?: User(createNewUser(account).userId, true)

        val user = try {
            getUserId()
        } catch (ex: FacebookAuthStore.AlreadyExistsException) {
            getUserId() // retry just once, for case, when two concurrent registrations occurs
        }

        return SignInResult(
                tokenManager.signLoggedInUser(user.userId),
                user.userId,
                account,
                user.isNewUser
        )
    }

    suspend fun bind(userId: Long, accessToken: String): ExternalAccount {
        val account = accountExtractor.getInfo(accessToken)

        facebookAuthStore.save(FacebookAccount(account.accountId, account.accountName, userId))

        return account
    }

    suspend fun unbind(userId: Long, facebookAccountId: String) {
        if (entryManager.findLoginEntries(userId).entriesCount <= 1) {
            throw SingleAccountLeftException()
        }
        if (!facebookAuthStore.remove(userId, facebookAccountId)) {
            throw FacebookAccountNotFoundException()
        }
    }

    private suspend fun createNewUser(facebookAccount: ExternalAccount) =
            transactionProvider.transactional {
                val user = userStore.save(UserValue(false))
                facebookAuthStore.save(FacebookAccount(facebookAccount.accountId, facebookAccount.accountName, user.userId))
                user
            }

    abstract class FacebookEntryManagerException : NoStackTraceException()
    class FacebookAccountNotFoundException : FacebookEntryManagerException()
    class SingleAccountLeftException : FacebookEntryManagerException()

    class SignInResult(
            val signInToken: SignInToken,
            val userId: Long,
            val facebookAccount: ExternalAccount,
            val isNewUser: Boolean
    )

}