package zeus.user.auth.infrastructure.user

import zeus.user.auth.domain.*
import javax.inject.*

@Singleton
class StubUserStore @Inject constructor() : UserStore {

    private var id = 1L
    private val users = HashMap<Long, UserEntity>()

    override suspend fun find(userId: Long): UserEntity? = users[userId]

    override suspend fun save(user: UserValue): UserEntity {
        val stored = UserEntity(id++, user)
        users[stored.userId] = stored
        return stored
    }
}