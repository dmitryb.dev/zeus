package zeus.user.auth

import dagger.*
import zeus.user.auth.module.*
import zeus.user.auth.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    ConfigModule::class,
    ServiceModule::class,
    InfrastructureModule::class
])
interface ProdUserAuth : UserAuth
