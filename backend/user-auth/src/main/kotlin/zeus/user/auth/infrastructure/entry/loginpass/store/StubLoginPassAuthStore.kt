package zeus.user.auth.infrastructure.entry.loginpass.store

import kotlinx.coroutines.*
import org.mindrot.jbcrypt.*
import zeus.user.auth.domain.*
import java.util.*
import javax.inject.*

@Singleton
class StubLoginPassAuthStore @Inject constructor() : LoginPassAuthStore {

    private val accounts = HashMap<Long, LoginPassAuth>()

    init {
        val pass = BCrypt.hashpw("12345", BCrypt.gensalt())
        runBlocking {
            save(LoginPassAuth(-3, "test1", "zeus.test1@gmail.com", true, pass))
            save(LoginPassAuth(-2, "test2", "zeus.test2@gmail.com", true, pass))
            save(LoginPassAuth(-1, "zeus.test3@gmail.com", "zeus.test3@gmail.com", true, pass))
        }
    }

    override suspend fun find(userId: Long): LoginPassAuth? = accounts[userId]

    override suspend fun findByEmail(email: String): LoginPassAuth? = accounts.values
            .find { account -> account.email == email }

    override suspend fun findByLogin(login: String): LoginPassAuth? = accounts.values
            .find { account -> account.login == login }

    override suspend fun save(loginPassAuth: LoginPassAuth): LoginPassAuth {
        if (findByEmail(loginPassAuth.email) != null) {
            throw LoginPassAuthStore.AlreadyExistsException()
        }

        accounts[loginPassAuth.id] = loginPassAuth
        return loginPassAuth
    }

    override suspend fun updateEmail(userId: Long, newEmail: String, emailConfirmed: Boolean): Boolean {
        val account = accounts[userId]!!
        val updated = LoginPassAuth(account.id, account.login, newEmail, emailConfirmed, account.passHash)
        accounts[userId] = updated
        return true
    }

    override suspend fun updateLogin(userId: Long, newLogin: String): Boolean {
        val account = accounts[userId]!!
        val updated = LoginPassAuth(account.id, newLogin, account.email, account.emailConfirmed, account.passHash)
        accounts[userId] = updated
        return true
    }

    override suspend fun updateEmailConfirmation(userId: Long, email: String, confirmed: Boolean): Boolean {
        val account = accounts[userId]!!
        val updated = LoginPassAuth(account.id, account.login, email, confirmed, account.passHash)
        accounts[userId] = updated
        return true
    }

    override suspend fun updatePass(userId: Long, newPassHash: String): Boolean {
        val account = accounts[userId]!!
        val updated = LoginPassAuth(account.id, account.login, account.email, account.emailConfirmed, newPassHash)
        accounts[userId] = updated
        return true
    }
}
