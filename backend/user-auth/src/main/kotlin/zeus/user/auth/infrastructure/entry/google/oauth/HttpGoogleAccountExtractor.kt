package zeus.user.auth.infrastructure.entry.google.oauth

import com.fasterxml.jackson.annotation.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.entry.*
import javax.inject.*

class HttpGoogleAccountExtractor @Inject constructor() : GoogleAccountExtractor {

    override suspend fun getInfo(accessToken: String): ExternalAccount {
        val response = try {
            httpClient.request<GoogleMeResponse> {
                url("https://people.googleapis.com/v1/people/me?personFields=names")
                header("Authorization", "Bearer $accessToken")
            }
        } catch (ex: BadResponseStatusException) {
            throw AccountExtractor.IncorrectTokenException()
        }

        val primaryAccount = response.names.find { it.metadata.primary }!!

        return ExternalAccount(primaryAccount.metadata.source.id, primaryAccount.displayName)
    }

    companion object {
        private val httpClient = HttpClient(CIO) {
            install(JsonFeature) {
                serializer = JacksonSerializer()
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private class GoogleMeResponse(val names: Collection<Name>) {
        @JsonIgnoreProperties(ignoreUnknown = true)
        class Name(val displayName: String, val metadata: Metadata)

        @JsonIgnoreProperties(ignoreUnknown = true)
        class Metadata(val primary: Boolean, val source: Source)

        @JsonIgnoreProperties(ignoreUnknown = true)
        class Source(val id: String)
    }
}