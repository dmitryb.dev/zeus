package zeus.user.auth.module.prod

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import dagger.Module
import dagger.Provides

@Module
object ConfigModule {

    @JvmStatic
    @Provides
    fun provideConfig(): Config = ConfigFactory.load()

}
