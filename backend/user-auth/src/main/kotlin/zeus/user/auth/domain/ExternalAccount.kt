package zeus.user.auth.domain

import zeus.util.mysql.mapper.annotation.*

class ExternalAccount(val accountId: String, val accountName: String)

class FacebookAccount @EntityCreator constructor(
        @Column("facebook_account_id") @get:Column("facebook_account_id") val accountId: String,
        @Column("facebook_account_name") @get:Column("facebook_account_name") val accountName: String,
        @Column("user_id") @get:Column("user_id") val userId: Long
)

class GoogleAccount @EntityCreator constructor(
        @Column("google_account_id") @get:Column("google_account_id") val accountId: String,
        @Column("google_account_name") @get:Column("google_account_name") val accountName: String,
        @Column("user_id") @get:Column("user_id") val userId: Long
)