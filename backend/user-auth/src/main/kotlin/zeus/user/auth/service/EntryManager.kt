package zeus.user.auth.service

import kotlinx.coroutines.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.entry.facebook.store.*
import zeus.user.auth.infrastructure.entry.google.store.*
import zeus.user.auth.infrastructure.entry.loginpass.store.*
import javax.inject.*

class EntryManager @Inject constructor(
        private val loginPassAuthStore: LoginPassAuthStore,
        private val facebookAuthStore: FacebookAuthStore,
        private val googleAuthStore: GoogleAuthStore
) {
    suspend fun findLoginEntries(userId: Long): UserAuthEntries = coroutineScope {
        val loginPassDeferred = async { loginPassAuthStore.find(userId) }
        val facebookEntriesDeferred = async { facebookAuthStore.findEntriesOfUser(userId) }
        val googleEntriesDeferred = async { googleAuthStore.findEntriesOfUser(userId) }

        UserAuthEntries(
            loginPassDeferred.await(),
            facebookEntriesDeferred.await(),
            googleEntriesDeferred.await()
        )
    }
}