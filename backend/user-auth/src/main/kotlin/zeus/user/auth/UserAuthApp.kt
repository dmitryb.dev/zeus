package zeus.user.auth

import kotlinx.coroutines.*
import zeus.util.*
import java.util.concurrent.*
import kotlin.system.*

fun main(args: Array<String>) {
    val userAuth = ModeFactory
            .withDefaultMode<UserAuth>("prod", DaggerProdUserAuth::create)
            .withMode("dev", DaggerDevUserAuth::create)
            .withMode("stub", DaggerStubUserAuth::create)
            .create(args)

    val exitOnException = CoroutineExceptionHandler { _, th ->
        th.printStackTrace()
        exitProcess(1)
    }

    GlobalScope.launch(exitOnException + Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
        if (args.contains("--http")) {
            userAuth.ktorServer().start(true)
        }
    }
    GlobalScope.launch(exitOnException + Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
        if (args.contains("--grpc")) {
            userAuth.grpcServer().start()
        }
    }
}