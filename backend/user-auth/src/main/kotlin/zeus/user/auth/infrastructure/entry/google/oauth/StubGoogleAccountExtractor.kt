package zeus.user.auth.infrastructure.entry.google.oauth

import zeus.user.auth.domain.*
import javax.inject.*
import kotlin.random.*

class StubGoogleAccountExtractor @Inject constructor() : GoogleAccountExtractor {

    private val names = listOf("Petia", "Vasia Petrovich", "Ivan Ivanich")

    override suspend fun getInfo(accessToken: String) = ExternalAccount(
            accessToken,
            names[Random.nextInt(0, names.size)]
    )
}