package zeus.user.auth.entrypoint.http

import io.grpc.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import user.auth.*
import user.auth.loginpass.*
import zeus.user.auth.entrypoint.grpc.*
import javax.inject.*

class PublicRoutes @Inject constructor(
        private val userApi: User,
        loginPassAuth: LoginPassAuth,
        facebookAuth: FacebookAuth,
        googleAuth: GoogleAuth
) {
    private val loginPathAuthRoutes = LoginPathAuthRoutes(loginPassAuth)
    private val facebookAuthRoutes = FacebookAuthRoutes(facebookAuth)
    private val googleAuthRoutes = GoogleAuthRoutes(googleAuth)

    fun httpRoute(route: Route) = route.apply {
        authenticate(optional = true) {
            route("api/v1") {
                get("entry") {
                    call.respond(userApi.getAuthEntries(GetAuthEntriesRequest {
                        this.userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                    }))
                }
                route("login-pass") {
                    loginPathAuthRoutes.httpRoute(this)
                }
                route("facebook") {
                    facebookAuthRoutes.httpRoute(this)
                }
                route("google") {
                    googleAuthRoutes.httpRoute(this)
                }
            }
        }
    }
}

internal class LoginPathAuthRoutes(private val loginPassAuth: LoginPassAuth) {
    fun httpRoute(route: Route) = route.apply {
        post("sign-up") {
            val request = call.receive<SignUpJsonRequest>()

            call.respond(loginPassAuth.signUp(SignUpRequest {
                email = request.email
                login = request.login
                pass = request.pass
                langCode = call.request.acceptLanguage() ?: "en"
            }))
        }
        post("bind") {
            val request = call.receive<BindJsonRequest>()

            call.respond(loginPassAuth.bind(BindRequest {
                this.userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                email = request.email
                login = request.login
                pass = request.pass
                langCode = call.request.acceptLanguage() ?: "en"
            }))
        }
        route("confirm-email") {
            post {
                val request = call.receive<ConfirmEmailJsonRequest>()

                call.respond(loginPassAuth.confirmEmail(ConfirmEmailRequest {
                    confirmationToken = request.confirmationToken
                }))
            }
            get {
                call.respond(loginPassAuth.confirmEmail(ConfirmEmailRequest {
                    confirmationToken = call.request.queryParameters["token"]
                }))
            }
        }
        post("resend-email") {
            call.respond(loginPassAuth.resendConfirmationEmail(ResendConfirmationEmailRequest {
                email = call.receive<ResendEmailJsonRequest>().email
                langCode = call.request.acceptLanguage() ?: "en"
            }))
        }
        post("sign-in") {
            val request = call.receive<SignInJsonRequest>()

            call.respond(loginPassAuth.signIn(SignInRequest {
                login = request.login
                pass = request.pass
            }))
        }
        post("forgot-pass") {
            call.respond(loginPassAuth.forgotPass(ForgotPassRequest {
                email = call.receive<ForgotPassJsonRequest>().email
                langCode = call.request.acceptLanguage() ?: "en"
            }))
        }
        post("update-pass") {
            val request = call.receive<UpdatePassJsonRequest>()
            val userId = call.principal<UserPrincipal>()?.userId

            if (request.forgotPassToken == null && userId == null) {
                throw accessDenied
            }

            call.respond(loginPassAuth.updatePass(UpdatePassRequest {
                request.forgotPassToken
                        ?.let { forgotPassToken = request.forgotPassToken }
                        ?: oldPass {
                            this.userId = userId ?: throw accessDenied
                            pass = request.oldPass ?: throw Status.INVALID_ARGUMENT.asException()
                        }
                newPass = request.newPass
            }))
        }
        post("update-login") {
            call.respond(loginPassAuth.updateLogin(UpdateLoginRequest {
                this.userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                newLogin = call.receive<UpdateLoginJsonRequest>().newLogin
            }))
        }
        post("update-email") {
            call.respond(loginPassAuth.updateEmail(UpdateEmailRequest {
                this.userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                newEmail = call.receive<UpdateEmailJsonRequest>().newEmail
                langCode = call.request.acceptLanguage() ?: "en"
            }))
        }
    }

    class SignUpJsonRequest(val login: String, val email: String, val pass: String)
    class BindJsonRequest(val login: String, val email: String, val pass: String)
    class ConfirmEmailJsonRequest(val confirmationToken: String)
    class ResendEmailJsonRequest(val email: String)
    class SignInJsonRequest(val login: String, val pass: String)
    class ForgotPassJsonRequest(val email: String)
    class UpdatePassJsonRequest(val forgotPassToken: String?, val oldPass: String?, val newPass: String)
    class UpdateLoginJsonRequest(val newLogin: String)
    class UpdateEmailJsonRequest(val newEmail: String)
}

internal class FacebookAuthRoutes(private val facebookAuth: FacebookAuth) {
    fun httpRoute(route: Route) = route.apply {
        post("sign-in") {
            call.respond(facebookAuth.signIn(user.auth.facebook.SignInRequest {
                facebookAccessToken = call.receive<SignInJsonRequest>().facebookAccessToken
            }))
        }
        post("bind") {
            call.respond(facebookAuth.bind(user.auth.facebook.BindRequest {
                userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                facebookAccessToken = call.receive<BindJsonRequest>().facebookAccessToken
            }))
        }
        post("unbind") {
            call.respond(facebookAuth.unbind(user.auth.facebook.UnbindRequest {
                userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                facebookAccountId = call.receive<UnbindJsonRequest>().facebookAccountId
            }))
        }
    }

    class SignInJsonRequest(val facebookAccessToken: String)
    class BindJsonRequest(val facebookAccessToken: String)
    class UnbindJsonRequest(val facebookAccountId: String)
}

internal class GoogleAuthRoutes(private val googleAuth: GoogleAuth) {
    fun httpRoute(route: Route) = route.apply {
        post("sign-in") {
            call.respond(googleAuth.signIn(user.auth.google.SignInRequest {
                googleAccessToken = call.receive<SignInJsonRequest>().googleAccessToken
            }))
        }
        post("bind") {
            call.respond(googleAuth.bind(user.auth.google.BindRequest {
                userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                googleAccessToken = call.receive<BindJsonRequest>().googleAccessToken
            }))
        }
        post("unbind") {
            call.respond(googleAuth.unbind(user.auth.google.UnbindRequest {
                userId = call.principal<UserPrincipal>()?.userId ?: throw accessDenied
                googleAccountId = call.receive<UnbindJsonRequest>().googleAccountId
            }))
        }
    }

    class SignInJsonRequest(val googleAccessToken: String)
    class BindJsonRequest(val googleAccessToken: String)
    class UnbindJsonRequest(val googleAccountId: String)
}

private val accessDenied = Status.PERMISSION_DENIED.asException()