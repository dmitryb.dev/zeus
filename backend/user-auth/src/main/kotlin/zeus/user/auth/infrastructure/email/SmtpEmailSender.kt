package zeus.user.auth.infrastructure.email

import io.vertx.core.*
import io.vertx.ext.mail.*
import kotlinx.coroutines.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class SmtpEmailSender @Inject constructor(
        host: String,
        port: Int,
        user: String,
        pass: String,
        private val from: String
) : EmailSender {

    private val mailClient: MailClient

    init {
        val config = MailConfig()
        config.hostname = host
        config.port = port
        config.username = user
        config.password = pass
        config.authMethods = "PLAIN"
        this.mailClient = MailClient.createShared(Vertx.vertx(), config)
    }

    override suspend fun send(email: String, message: EmailSender.Email) {
        val mailMessage = MailMessage()
        mailMessage.setTo(email)
        mailMessage.subject = message.topic
        mailMessage.html = message.text
        mailMessage.from = from

        GlobalScope.launch(coroutineContext) {
            try {
                sendEmail(mailMessage)
                log.debug("Email to $email was sent")
            } catch (th: Throwable) {
                log.error("Email sending to $email failed", th)
            }
        }
    }

    private suspend fun sendEmail(message: MailMessage) {
        suspendCoroutine<Nothing?> { continuation -> mailClient.sendMail(message) { res ->
            if (res.succeeded()) {
                continuation.resume(null)
            } else {
                continuation.resumeWithException(res.cause())
            }
        }}
    }

    companion object {
        val log = logger<SmtpEmailSender>()
    }
}
