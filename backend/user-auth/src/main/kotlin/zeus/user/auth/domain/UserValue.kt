package zeus.user.auth.domain

import zeus.util.mysql.mapper.annotation.*

open class UserValue constructor(
        @get:Column("has_shared_app") val hasSharedApp: Boolean
)

class UserEntity @EntityCreator constructor(
        @Column("user_id") val userId: Long,
        @Column("has_shared_app") hasSharedApp: Boolean
) : UserValue(hasSharedApp) {

    constructor(userId: Long, user: UserValue) : this(userId, user.hasSharedApp)
}

class UserAuthEntries(
        val loginPassAuth: LoginPassAuth?,
        val facebook: Collection<FacebookAccount>,
        val google: Collection<GoogleAccount>
) {
    val entriesCount = (loginPassAuth?.let { 1 } ?: 0) + facebook.size + google.size
}