package zeus.user.auth.infrastructure.entry.loginpass.stream

import kotlinx.coroutines.channels.*
import kotlinx.coroutines.future.*
import org.apache.pulsar.client.api.*
import zeus.user.auth.service.util.*
import zeus.util.log.*
import zeus.util.pulsar.*
import javax.inject.*

class PulsarLoginPassUpdatesStream @Inject constructor(private val pulsarClient: PulsarClient) : LoginPassUpdatesStream {

    private val producerCache = ProducerCache(pulsarClient, Schema.STRING)

    override suspend fun waitForEmailConfirmation(email: String): SignInToken {
        return pulsarClient.read(
                "non-persistent://public/user-auth/email-confirmation:$email",
                MessageId.latest,
                Schema.STRING
        ).first()
    }

    override suspend fun publishEmailConfirmed(email: String, token: SignInToken) {
        log.debug("Publishing email confirmed for email: {}", email)

        producerCache.getClient(
                "non-persistent://public/user-auth/email-confirmation:$email"
        ).sendAsync(token).await()

        log.debug("Email confirmed event published")
    }

    override suspend fun waitForPassChange(email: String) {
        pulsarClient.read(
                "non-persistent://public/user-auth/pass-changed:$email",
                MessageId.latest,
                Schema.STRING
        ).first()
    }

    override suspend fun publishPassChanged(email: String) {
        log.debug("Publishing pass change for email: {}", email)

        producerCache.getClient(
                "non-persistent://public/user-auth/pass-changed:$email"
        ).sendAsync("").await()

        log.debug("Pass changed event published")
    }

    companion object {
        private val log = logger<PulsarLoginPassUpdatesStream>()
    }
}