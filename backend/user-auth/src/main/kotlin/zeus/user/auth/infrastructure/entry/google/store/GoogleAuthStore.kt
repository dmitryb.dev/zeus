package zeus.user.auth.infrastructure.entry.google.store

import zeus.user.auth.domain.*
import zeus.util.exception.*

interface GoogleAuthStore {

    suspend fun find(accountId: String): GoogleAccount?
    suspend fun findEntriesOfUser(userId: Long): Collection<GoogleAccount>
    suspend fun save(googleAccount: GoogleAccount)
    suspend fun remove(userId: Long, accountId: String): Boolean

    class AlreadyExistsException(val googleAccountId: String) : NoStackTraceException()

}
