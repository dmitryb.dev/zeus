package zeus.user.auth.infrastructure.token

import zeus.user.auth.service.util.*

interface TokenManager {

    suspend fun signLoggedInUser(userId: Long): SignInToken
    suspend fun extractUserId(signInToken: SignInToken): Long

    suspend fun signEmailConfirmation(confirmation: EmailConfirmation): EmailConfirmationToken
    suspend fun extractConfirmedEmail(confirmationToken: EmailConfirmationToken): EmailConfirmation

    suspend fun signForgotPass(userId: Long): ForgotPassToken
    suspend fun extractForgotPassUserId(forgotPassToken: ForgotPassToken): Long

    class EmailConfirmation(val userId: Long, val email: String)

}
