package zeus.user.auth.infrastructure.entry.facebook.store

import com.mysql.cj.xdevapi.*
import zeus.user.auth.domain.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlFacebookAuthStore @Inject constructor(
        client: Client,
        @Named("schemaName") private val schemaName: String
) : FacebookAuthStore, MysqlStore(client, schemaName, FB_ENTRY_TABLE) {


    override suspend fun find(accountId: String): FacebookAccount? = withTable { table ->
        table.select("*")
                .where("facebook_account_id = :accountId")
                .bind("accountId", accountId)
                .findOne<FacebookAccount>()
    }

    override suspend fun findEntriesOfUser(userId: Long): Collection<FacebookAccount> = withTable { table ->
        table.select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findAll<FacebookAccount>()
    }

    override suspend fun save(facebookAccount: FacebookAccount) { try {
        withTable { table -> table.save(facebookAccount) }
    } catch (ex: DuplicatedException) {
        throw FacebookAuthStore.AlreadyExistsException(facebookAccount.accountId)
    }}

    override suspend fun remove(userId: Long, accountId: String): Boolean = withTable { table ->
        val removeResult = table.delete()
                .where("user_id = :userId AND facebook_account_id = :accountId")
                .bind("userId", userId)
                .bind("accountId", accountId)
                .await()

        removeResult.affectedItemsCount > 0
    }

    companion object {
        const val FB_ENTRY_TABLE = "facebook_entries"
    }
}
