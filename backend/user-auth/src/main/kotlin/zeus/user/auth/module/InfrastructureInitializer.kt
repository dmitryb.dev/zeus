package zeus.user.auth.module

interface InfrastructureInitializer {
    fun init() {}
}