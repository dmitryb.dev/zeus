package zeus.user.auth.infrastructure.user

import com.mysql.cj.xdevapi.*
import kotlinx.coroutines.future.*
import zeus.user.auth.domain.*
import zeus.util.mysql.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import javax.inject.*

class MysqlUserStore @Inject constructor(
        private val client: Client,
        @Named("schemaName") private val schemaName: String
): UserStore {

    override suspend fun find(userId: Long): UserEntity? = withUserTable { table ->
        table.select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findOne<UserEntity>()
    }

    override suspend fun save(user: UserValue): UserEntity = withUserTable { table ->
        MysqlException.wrapMysqlException {
            val insertResult = table.save(user)

            UserEntity(insertResult.autoIncrementValue, user)
        }
    }


    private suspend fun <T> withUserTable(block: suspend (Table) -> T) = client.withSessionContext { session ->
        block(session.getSchema(schemaName).getTable(USER_TABLE))
    }

    companion object {
        const val USER_TABLE = "users"
    }
}