package zeus.user.auth.entrypoint.grpc

import io.grpc.*
import user.auth.loginpass.*
import user.auth.loginpass.LoginPassAuthApi.UpdatePassRequest.ProofCase.*
import zeus.user.auth.entrypoint.grpc.util.*
import zeus.user.auth.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class LoginPassAuth @Inject constructor(
        private val loginPassManager: LoginPassManager
) : LoginPassAuthCoroutineGrpc.LoginPassAuthImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun signUp(
            request: LoginPassAuthApi.SignUpRequest
    ): LoginPassAuthApi.SignedUpResponse = log.uncaught {
        try {
            log.debug("User is signing up, email: {}, pass: {}", request.email, request.pass.mask())

            loginPassManager.signUp(request.login, request.email, request.pass, request.langCode)

            log.debug("User has signed up")

            SignedUpResponse {}
        } catch (ex: LoginPassManager.EmailAlreadyRegisteredException) {
            log.debug("User with such login/email already exists")
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun bind(request: LoginPassAuthApi.BindRequest): LoginPassAuthApi.BoundResponse = log.uncaught {
        try {
            log.debug("User with id: {} is binding login-pass account, email: {}, pass: {}", request.userId,
                    request.email, request.pass.mask())

            loginPassManager.bind(request.login, request.userId, request.email, request.pass, request.langCode)

            log.debug("Login-pass bound")

            BoundResponse {}
        } catch (ex: LoginPassManager.EmailAlreadyRegisteredException) {
            log.debug("User with such login/email already exists")
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun confirmEmail(
            request: LoginPassAuthApi.ConfirmEmailRequest
    ): LoginPassAuthApi.EmailConfirmedResponse = log.uncaught {
        try {
            log.debug("Confirming email with token")

            val confirmed = loginPassManager.confirmEmail(request.confirmationToken)

            log.debug("Email: {} for user id: {} confirmed", confirmed.email, confirmed.userId)

            EmailConfirmedResponse {
                signInToken = confirmed.singInToken
            }
        } catch (ex: LoginPassManager.NoSuchEmailException) {
            log.debug("Email {} not found / already confirmed", ex.email)
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun resendConfirmationEmail(
            request: LoginPassAuthApi.ResendConfirmationEmailRequest
    ): LoginPassAuthApi.EmailSentResponse = log.uncaught {
        try {
            log.debug("Sending confirmation email to ${request.email}")

            loginPassManager.sendConfirmationEmail(request.email, request.langCode)

            log.debug("Confirmation email sent")

            EmailSentResponse {}
        } catch (ex: LoginPassManager.NoSuchEmailException) {
            log.debug("Email not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.EmailAlreadyConfirmed) {
            log.debug("Email already confirmed")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    override suspend fun signIn(
            request: LoginPassAuthApi.SignInRequest
    ): LoginPassAuthApi.SignedInResponse = log.uncaught {
        try {
            log.debug("User is signing in, login: {}, pass: {}", request.login, request.pass.mask())

            val signInToken = loginPassManager.signIn(request.login, request.pass)

            log.debug("User successfully signed in")

            SignedInResponse {
                this.signInToken = signInToken
            }
        } catch (ex: LoginPassManager.NoSuchLoginException) {
            log.debug("Login not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.IncorrectPassException) {
            log.debug("Incorrect pass")
            throw Status.PERMISSION_DENIED.asException()
        } catch (ex: LoginPassManager.EmailIsNotConfirmed) {
            log.debug("Email is not confirmed")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    override suspend fun forgotPass(
            request: LoginPassAuthApi.ForgotPassRequest
    ): LoginPassAuthApi.ForgotPassResponse = log.uncaught {
        try {
            log.debug("Restoring pass for email: {}", request.email)

            loginPassManager.sendForgotPassEmail(request.email, request.langCode)

            log.debug("Pass restore email sent")

            ForgotPassResponse {}
        } catch (ex: LoginPassManager.NoSuchEmailException) {
            log.debug("Email not found")
            throw Status.NOT_FOUND.asException()
        }
    }

    override suspend fun updatePass(
            request: LoginPassAuthApi.UpdatePassRequest
    ): LoginPassAuthApi.SignedInResponse = log.uncaught {
        try {
            val signInToken = when (request.proofCase) {
                OLD_PASS -> {
                    log.debug("Updating pass using existing pass proof for user: {}, old pass: {}, new pass: {}",
                            request.oldPass.userId, request.oldPass.pass.mask(), request.newPass)
                    loginPassManager.updatePassWithPassProof(request.oldPass.userId, request.oldPass.pass, request.newPass)
                }
                FORGOT_PASS_TOKEN -> {
                    log.debug("Updating pass using forgot pass token, new pass: {}", request.newPass)
                    loginPassManager.updatePassWithForgotPassToken(request.forgotPassToken, request.newPass)
                }
                else -> throw IllegalArgumentException("Unsupported proof ${request.proofCase.name}")
            }

            log.debug("Pass was updated")

            SignedInResponse {
                this.signInToken = signInToken
            }
        } catch (ex: LoginPassManager.IncorrectProofException) {
            log.debug("Proof is incorrect")
            throw Status.PERMISSION_DENIED.asException()
        }
    }

    override suspend fun updateLogin(
            request: LoginPassAuthApi.UpdateLoginRequest
    ): LoginPassAuthApi.LoginUpdatedResponse = log.uncaught {
        try {
            log.debug("Updating login for user id: {}, new login: {}", request.userId, request.newLogin)

            loginPassManager.updateLogin(request.userId, request.newLogin)

            log.debug("Login updated")

            LoginUpdatedResponse {}
        } catch (ex: LoginPassManager.NoSuchUserException) {
            log.debug("User not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.LoginAlreadyRegistered) {
            log.debug("Login already in use")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    override suspend fun updateEmail(
            request: LoginPassAuthApi.UpdateEmailRequest
    ): LoginPassAuthApi.EmailUpdatedResponse = log.uncaught {
        try {
            log.debug("Updating email for user id: {}, new email: {}")

            loginPassManager.updateEmail(request.userId, request.newEmail, request.langCode)

            log.debug("Email updated")

            EmailUpdatedResponse {}
        } catch (ex: LoginPassManager.NoSuchUserException) {
            log.debug("User not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.EmailAlreadyRegisteredException) {
            log.debug("Email already registered")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    override suspend fun waitForEmailConfirmation(
            request: LoginPassAuthApi.WaitForEmailConfirmationReqeust
    ): LoginPassAuthApi.EmailConfirmedResponse = log.uncaught {
        try {
            log.debug("Waiting for email confirmation, login: {}, pass: {}", request.login, request.pass.mask())

            val signInToken = loginPassManager.waitForEmailConfirmation(request.login, request.pass)

            log.debug("Email confirmed")

            EmailConfirmedResponse {
                this.signInToken = signInToken
            }
        } catch (ex: LoginPassManager.NoSuchLoginException) {
            log.debug("Login not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.NoSuchUserException) {
            log.debug("No user with id: {} found", ex.userId)
            throw Status.NOT_FOUND.asException()
        } catch (ex: LoginPassManager.IncorrectPassException) {
            log.debug("Incorrect pass")
            throw Status.PERMISSION_DENIED.asException()
        }
    }

    override suspend fun waitForPassChange(
            request: LoginPassAuthApi.WaitForPassChangeRequest
    ): LoginPassAuthApi.PassChangedResponse = log.uncaught {
        log.debug("Waiting for pass change for email: {}", request.email)

        loginPassManager.waitForPassChange(request.email)

        log.debug("Pass changed")

        PassChangedResponse {}
    }

    companion object {
        val log = logger<LoginPassAuth>()
    }
}
