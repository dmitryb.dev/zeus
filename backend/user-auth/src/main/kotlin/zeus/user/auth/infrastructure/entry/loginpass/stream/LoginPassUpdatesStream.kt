package zeus.user.auth.infrastructure.entry.loginpass.stream

import zeus.user.auth.service.util.*

interface LoginPassUpdatesStream {
    suspend fun waitForEmailConfirmation(email: String): SignInToken
    suspend fun publishEmailConfirmed(email: String, token: SignInToken)

    suspend fun waitForPassChange(email: String)
    suspend fun publishPassChanged(email: String)
}