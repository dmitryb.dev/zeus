package zeus.user.auth.infrastructure.user

import zeus.user.auth.domain.*

interface UserStore {

    suspend fun find(userId: Long): UserEntity?
    suspend fun save(user: UserValue): UserEntity
}