package zeus.user.auth.infrastructure.entry.facebook.store

import com.google.common.collect.*
import zeus.user.auth.domain.*
import javax.inject.*

@Singleton
class StubFacebookAuthStore @Inject constructor() : FacebookAuthStore {

    private val accounts = MultimapBuilder.hashKeys().arrayListValues().build<Long, FacebookAccount>()

    override suspend fun find(accountId: String): FacebookAccount? = accounts.values()
            .find { it.accountId == accountId }

    override suspend fun findEntriesOfUser(userId: Long): Collection<FacebookAccount> = accounts[userId]

    override suspend fun save(facebookAccount: FacebookAccount) {
        accounts.put(facebookAccount.userId, facebookAccount)
    }

    override suspend fun remove(userId: Long, accountId: String): Boolean {
        return accounts
                .get(userId).find { it.accountId == accountId }
                ?.let { accounts.remove(userId, it) } != null
    }
}
