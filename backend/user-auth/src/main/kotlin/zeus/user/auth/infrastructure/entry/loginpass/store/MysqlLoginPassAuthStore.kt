package zeus.user.auth.infrastructure.entry.loginpass.store

import com.mysql.cj.xdevapi.*
import zeus.user.auth.domain.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlLoginPassAuthStore @Inject constructor(
        client: Client,
        @Named("schemaName") private val schemaName: String
) : LoginPassAuthStore, MysqlStore(client, schemaName, LOGIN_PASS_TABLE) {

    override suspend fun find(userId: Long): LoginPassAuth? = withTable { table ->
        table
                .select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findOne<LoginPassAuth>()
    }

    override suspend fun findByEmail(email: String): LoginPassAuth? = withTable { table ->
        table
                .select("*")
                .where("email = :email")
                .bind("email", email)
                .findOne<LoginPassAuth>()
    }

    override suspend fun findByLogin(login: String): LoginPassAuth? = withTable { table ->
        table
                .select("*")
                .where("login = :login")
                .bind("login", login)
                .findOne<LoginPassAuth>()
    }

    override suspend fun save(loginPassAuth: LoginPassAuth): LoginPassAuth = try {
        withTable { table ->
            table.save(loginPassAuth)

            loginPassAuth
        }
    } catch (e: DuplicatedException) {
        throw LoginPassAuthStore.AlreadyExistsException()
    }

    override suspend fun updateEmailConfirmation(
            userId: Long, email: String, confirmed: Boolean
    ): Boolean = withTable { table ->
        val updateResult = table.update()
                .set("email_confirmed", confirmed)
                .where("user_id = :userId AND email = :email")
                .bind("userId", userId)
                .bind("email", email)
                .await()

        updateResult.affectedItemsCount > 0
    }

    override suspend fun updatePass(userId: Long, newPassHash: String): Boolean = withTable { table ->
        val updateResult = table.update()
                .set("pass_hash", newPassHash)
                .where("user_id = :userId")
                .bind("userId", userId)
                .await()

        updateResult.affectedItemsCount > 0
    }

    override suspend fun updateEmail(userId: Long, newEmail: String, emailConfirmed: Boolean): Boolean = try {
        withTable { table ->
            val updateResult = MysqlException.wrapMysqlException {
                table.update()
                        .set("email", newEmail)
                        .set("email_confirmed", emailConfirmed)
                        .where("user_id = :userId")
                        .bind("userId", userId)
                        .await()
            }

            updateResult.affectedItemsCount > 0
        }
    } catch (ex: DuplicatedException) {
        throw LoginPassAuthStore.AlreadyExistsException()
    }

    override suspend fun updateLogin(userId: Long, newLogin: String): Boolean = try {
        withTable { table ->
            val updateResult = MysqlException.wrapMysqlException {
                table.update()
                        .set("login", newLogin)
                        .where("user_id = :userId")
                        .bind("userId", userId)
                        .await()
            }

            updateResult.affectedItemsCount > 0
        }
    } catch (ex: DuplicatedException) {
        throw LoginPassAuthStore.AlreadyExistsException()
    }

    companion object {
        const val LOGIN_PASS_TABLE = "login_pass_entries"
    }
}
