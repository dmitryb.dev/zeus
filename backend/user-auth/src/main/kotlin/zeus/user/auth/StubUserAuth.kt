package zeus.user.auth

import dagger.*
import zeus.user.auth.module.*
import zeus.user.auth.module.dev.*
import zeus.user.auth.module.prod.*
import zeus.user.auth.module.stub.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    DevConfigModule::class,
    ServiceModule::class,
    StubInfrastructureModule::class
])
interface StubUserAuth : UserAuth
