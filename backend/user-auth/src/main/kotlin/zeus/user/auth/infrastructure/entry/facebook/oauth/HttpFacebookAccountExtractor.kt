package zeus.user.auth.infrastructure.entry.facebook.oauth

import com.fasterxml.jackson.annotation.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.entry.*
import javax.inject.*

class HttpFacebookAccountExtractor @Inject constructor() : FacebookAccountExtractor {

    override suspend fun getInfo(accessToken: String): ExternalAccount {
        try {
            val response = httpClient.request<FacebookMeResponse> {
                url("https://graph.facebook.com/me?access_token=$accessToken")
            }
            return ExternalAccount(response.id, response.name)
        } catch (ex: BadResponseStatusException) {
            throw AccountExtractor.IncorrectTokenException()
        }
    }

    companion object {
        private val httpClient = HttpClient(CIO) {
            install(JsonFeature) {
                serializer = JacksonSerializer()
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private class FacebookMeResponse(val id: String, val name: String)
}