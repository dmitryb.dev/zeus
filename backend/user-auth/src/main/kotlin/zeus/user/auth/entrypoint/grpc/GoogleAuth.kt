package zeus.user.auth.entrypoint.grpc

import io.grpc.*
import user.auth.google.*
import zeus.user.auth.infrastructure.entry.*
import zeus.user.auth.infrastructure.entry.google.store.*
import zeus.user.auth.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class GoogleAuth @Inject constructor(
        private val googleEntryManager: GoogleEntryManager
) : GoogleAuthCoroutineGrpc.GoogleAuthImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun signIn(request: GoogleAuthApi.SignInRequest): GoogleAuthApi.SignedInResponse = log.uncaught {
        try {
            log.debug("Signing in with google")

            val result = googleEntryManager.signIn(request.googleAccessToken)

            log.debug("Signed in, user id: {}, account id: {}, account name: {}, is new user: {}",
                    result.userId, result.facebookAccount.accountId, result.facebookAccount.accountName, result.isNewUser)

            SignedInResponse {
                signInToken = result.signInToken
            }
        } catch (ex: AccountExtractor.IncorrectTokenException) {
            log.debug("Incorrect token")
            throw Status.INVALID_ARGUMENT.asException()
        } catch (ex: GoogleAuthStore.AlreadyExistsException) {
            log.debug("Google account with id: {} already exists", ex.googleAccountId)
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun bind(request: GoogleAuthApi.BindRequest): GoogleAuthApi.BoundResponse = log.uncaught {
        try {
            log.debug("Binding google account for user id: {}", request.userId)

            val account = googleEntryManager.bind(request.userId, request.googleAccessToken)

            log.debug("Google account bound, account id: {}, account name: {}", account.accountId, account.accountName)

            BoundResponse {}
        } catch (ex: AccountExtractor.IncorrectTokenException) {
            log.debug("Incorrect token")
            throw Status.INVALID_ARGUMENT.asException()
        } catch (ex: GoogleAuthStore.AlreadyExistsException) {
            log.debug("Google account with id: {} already exists", ex.googleAccountId)
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun unbind(request: GoogleAuthApi.UnbindRequest): GoogleAuthApi.UnboundResponse = log.uncaught {
        try {
            log.debug("Unbinding google account with id: {} for user id: {}", request.googleAccountId, request.userId)

            googleEntryManager.unbind(request.userId, request.googleAccountId)

            log.debug("Google account has been unbound")

            UnboundResponse {}
        } catch (ex: GoogleEntryManager.GoogleAccountNotFoundException) {
            log.debug("Google account not found")
            throw Status.NOT_FOUND.asException()
        } catch (ex: GoogleEntryManager.SingleAccountLeftException) {
            log.debug("Can't unbind account, because user doesn't have any other accounts")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    companion object {
        private val log = logger<FacebookAuth>()
    }
}