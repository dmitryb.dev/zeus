package zeus.user.auth.infrastructure.entry.google.store

import com.google.common.collect.*
import zeus.user.auth.domain.*
import javax.inject.*

@Singleton
class StubGoogleAuthStore @Inject constructor() : GoogleAuthStore {

    private val accounts = MultimapBuilder.hashKeys().arrayListValues().build<Long, GoogleAccount>()

    override suspend fun find(accountId: String): GoogleAccount? = accounts.values()
            .find { it.accountId == accountId }

    override suspend fun findEntriesOfUser(userId: Long): Collection<GoogleAccount> = accounts[userId]

    override suspend fun save(googleAccount: GoogleAccount) {
        accounts.put(googleAccount.userId, googleAccount)
    }

    override suspend fun remove(userId: Long, accountId: String): Boolean {
        return accounts
                .get(userId).find { it.accountId == accountId }
                ?.let { accounts.remove(userId, it) } != null
    }
}
