package zeus.user.auth.infrastructure.entry.facebook.store

import zeus.user.auth.domain.*
import zeus.util.exception.*

interface FacebookAuthStore {

    suspend fun find(accountId: String): FacebookAccount?
    suspend fun findEntriesOfUser(userId: Long): Collection<FacebookAccount>
    suspend fun save(facebookAccount: FacebookAccount)
    suspend fun remove(userId: Long, accountId: String): Boolean

    class AlreadyExistsException(val facebookAccountId: String) : NoStackTraceException()

}
