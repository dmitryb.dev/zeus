package zeus.user.auth.entrypoint.http.util

import com.google.protobuf.*
import com.google.protobuf.util.*
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.io.*
import kotlinx.coroutines.io.jvm.javaio.*
import kotlin.reflect.*
import kotlin.reflect.full.*

class JsonConverter : ContentConverter {
    private val jacksonConverter = JacksonConverter()
    private val builderCache = HashMap<Class<*>, Message.Builder>()

    override suspend fun convertForReceive(context: PipelineContext<ApplicationReceiveRequest, ApplicationCall>): Any? =
            when {
                context.subject.type.isSubclassOf(Message::class) ->
                    parseProto(context.subject.value, context.subject.type, context.call)

                else -> jacksonConverter.convertForReceive(context)
            }

    override suspend fun convertForSend(
            context: PipelineContext<Any, ApplicationCall>,
            contentType: ContentType, value: Any
    ): Any? =
            when (value) {
                is Message -> TextContent(
                        JsonFormat.printer().includingDefaultValueFields().print(value),
                        ContentType.Application.Json
                )
                else -> jacksonConverter.convertForSend(context, contentType, value)
            }

    private fun parseProto(value: Any, type: KClass<*>, call: ApplicationCall): Message? {
        val builder = builderCache
                .computeIfAbsent(type.java) {
                    type.java.getDeclaredMethod("newBuilder").invoke(null) as Message.Builder
                }
                .clone()

        val channel = value as? ByteReadChannel ?: return null
        JsonFormat.parser().merge(
                channel.toInputStream().reader(call.request.contentCharset() ?: Charsets.UTF_8),
                builder
        )
        return builder.build()
    }
}