package zeus.user.auth.service.util

typealias SignInToken = String
typealias EmailConfirmationToken = String
typealias ForgotPassToken = String