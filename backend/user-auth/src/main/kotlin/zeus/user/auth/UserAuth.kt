package zeus.user.auth

import io.ktor.server.engine.*
import zeus.util.*

interface UserAuth {
    fun grpcServer(): GrpcServer
    fun ktorServer(): ApplicationEngine
}
