package zeus.user.auth.entrypoint.grpc

import kotlinx.coroutines.*
import user.auth.*
import user.auth.AuthEntriesResponse.FacebookEntry
import user.auth.AuthEntriesResponse.GoogleEntry
import zeus.user.auth.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class User @Inject constructor(
        private val entryManager: EntryManager
) : UserCoroutineGrpc.UserImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun getAuthEntries(request: UserApi.GetAuthEntriesRequest): UserApi.AuthEntriesResponse = log.uncaught {
        coroutineScope {
            val userEntries = entryManager.findLoginEntries(request.userId)

            AuthEntriesResponse {
                userEntries.loginPassAuth?.let {
                    loginPass {
                        login = it.login
                        email = it.email
                    }
                }
                addAllFacebook(userEntries.facebook.map { FacebookEntry { accountId = it.accountId; accountName = it.accountName } })
                addAllGoogle(userEntries.google.map { GoogleEntry { accountId = it.accountId; accountName = it.accountName } })
            }
        }
    }

    companion object {
        private val log = logger<User>()
    }
}