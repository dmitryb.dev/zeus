package zeus.user.auth.infrastructure.email

interface EmailSender {
    suspend fun send(email: String, message: Email)

    class Email(val topic: String, val text: String)
}
