package zeus.user.auth.entrypoint.grpc

import io.grpc.*
import user.auth.facebook.*
import zeus.user.auth.infrastructure.entry.*
import zeus.user.auth.infrastructure.entry.facebook.store.*
import zeus.user.auth.service.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*
import kotlin.coroutines.*

class FacebookAuth @Inject constructor(
        private val facebookEntryManager: FacebookEntryManager
) : FacebookAuthCoroutineGrpc.FacebookAuthImplBase() {

    override val coroutineContext: CoroutineContext
        get() = traceContext()

    override suspend fun signIn(request: FacebookAuthApi.SignInRequest): FacebookAuthApi.SignedInResponse = log.uncaught {
        try {
            log.debug("Signing in with facebook")

            val result = facebookEntryManager.signIn(request.facebookAccessToken)

            log.debug("Signed in, user id: {}, account id: {}, account name: {}, is new user: {}",
                    result.userId, result.facebookAccount.accountId, result.facebookAccount.accountName, result.isNewUser)

            SignedInResponse {
                signInToken = result.signInToken
            }
        } catch (ex: AccountExtractor.IncorrectTokenException) {
            log.debug("Incorrect token")
            throw Status.INVALID_ARGUMENT.asException()
        } catch (ex: FacebookAuthStore.AlreadyExistsException) {
            log.debug("Facebook account with id: {} already exists", ex.facebookAccountId)
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun bind(request: FacebookAuthApi.BindRequest): FacebookAuthApi.BoundResponse = log.uncaught {
        try {
            log.debug("Binding facebook account for user id: {}", request.userId)

            val account = facebookEntryManager.bind(request.userId, request.facebookAccessToken)

            log.debug("Facebook account bound, account id: {}, account name: {}", account.accountId, account.accountName)

            BoundResponse {}
        } catch (ex: AccountExtractor.IncorrectTokenException) {
            log.debug("Incorrect token")
            throw Status.INVALID_ARGUMENT.asException()
        } catch (ex: FacebookAuthStore.AlreadyExistsException) {
            log.debug("Facebook account with id: {} already exists", ex.facebookAccountId)
            throw Status.ALREADY_EXISTS.asException()
        }
    }

    override suspend fun unbind(request: FacebookAuthApi.UnbindRequest): FacebookAuthApi.UnboundResponse = log.uncaught {
        try {
            log.debug("Unbinding facebook account with id: {} for user id: {}", request.facebookAccountId, request.userId)

            facebookEntryManager.unbind(request.userId, request.facebookAccountId)

            log.debug("Facebook account has been unbound")

            UnboundResponse {}
        } catch (ex: FacebookEntryManager.FacebookAccountNotFoundException) {
            log.debug("Facebook account not found")
            throw Status.NOT_FOUND.asException()
        }  catch (ex: FacebookEntryManager.SingleAccountLeftException) {
            log.debug("Can't unbind account, because user doesn't have any other accounts")
            throw Status.FAILED_PRECONDITION.asException()
        }
    }

    companion object {
        private val log = logger<FacebookAuth>()
    }
}