package zeus.user.auth

import dagger.*
import zeus.user.auth.module.*
import zeus.user.auth.module.dev.*
import zeus.user.auth.module.prod.*
import javax.inject.*

@Singleton
@Component(modules = [
    EntryPointModule::class,
    InfrastructureModule::class,
    ServiceModule::class,
    DevConfigModule::class
])
interface DevUserAuth : UserAuth
