package zeus.user.auth.entrypoint.http

import io.ktor.auth.*

class UserPrincipal(val userId: Long) : Principal