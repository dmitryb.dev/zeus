package zeus.user.auth.infrastructure.entry.loginpass.stream

import com.google.common.collect.*
import zeus.user.auth.service.util.*
import javax.inject.*
import kotlin.coroutines.*

class StubLoginPassUpdatesStream @Inject constructor(): LoginPassUpdatesStream {

    private val emailConfirmationSubscribers: Multimap<String, Continuation<SignInToken>> = MultimapBuilder
            .hashKeys()
            .arrayListValues()
            .build()

    private val passChangedSubscribers: Multimap<String, Continuation<Nothing?>> = MultimapBuilder
            .hashKeys()
            .arrayListValues()
            .build()

    override suspend fun waitForEmailConfirmation(email: String): SignInToken {
        return suspendCoroutine { continuation ->
            emailConfirmationSubscribers.put(email, continuation)
        }
    }

    override suspend fun publishEmailConfirmed(email: String, token: SignInToken) {
        emailConfirmationSubscribers.removeAll(email).forEach { it.resume(token) }
    }

    override suspend fun waitForPassChange(email: String) {
        suspendCoroutine<Nothing?> { continuation ->
            passChangedSubscribers.put(email, continuation)
        }
    }

    override suspend fun publishPassChanged(email: String) {
        passChangedSubscribers.removeAll(email).forEach { it.resume(null) }
    }

}