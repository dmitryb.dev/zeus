package zeus.user.auth.infrastructure.entry.loginpass.store

import zeus.user.auth.domain.*
import zeus.util.exception.*

interface LoginPassAuthStore {

    suspend fun find(userId: Long): LoginPassAuth?
    suspend fun findByEmail(email: String): LoginPassAuth?
    suspend fun findByLogin(login: String): LoginPassAuth?

    suspend fun save(loginPassAuth: LoginPassAuth): LoginPassAuth

    suspend fun updateEmailConfirmation(userId: Long, email: String, confirmed: Boolean): Boolean
    suspend fun updatePass(userId: Long, newPassHash: String): Boolean
    suspend fun updateEmail(userId: Long, newEmail: String, emailConfirmed: Boolean): Boolean
    suspend fun updateLogin(userId: Long, newLogin: String): Boolean

    class AlreadyExistsException : NoStackTraceException()

}
