package zeus.user.auth.service

import zeus.infrstructure.transaction.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.entry.google.oauth.*
import zeus.user.auth.infrastructure.entry.google.store.*
import zeus.user.auth.infrastructure.token.*
import zeus.user.auth.infrastructure.user.*
import zeus.user.auth.service.util.*
import zeus.util.exception.*
import javax.inject.*

class GoogleEntryManager @Inject constructor(
        private val transactionProvider: TransactionProvider,
        private val accountInfoExtractor: GoogleAccountExtractor,
        private val googleAuthStore: GoogleAuthStore,
        private val userStore: UserStore,
        private val tokenManager: TokenManager,
        private val entryManager: EntryManager
) {

    suspend fun signIn(accessToken: String): SignInResult {
        val account = accountInfoExtractor.getInfo(accessToken)

        class User(val userId: Long, val isNewUser: Boolean)

        suspend fun getUserId() = googleAuthStore
                .find(account.accountId)
                ?.userId?.let { userId -> User(userId, false) }
                ?: User(createNewUser(account).userId, true)

        val user = try {
            getUserId()
        } catch (ex: GoogleAuthStore.AlreadyExistsException) {
            getUserId() // retry just once, for case, when two concurrent registrations occurs
        }

        return SignInResult(
                tokenManager.signLoggedInUser(user.userId),
                user.userId,
                account,
                user.isNewUser
        )
    }

    suspend fun bind(userId: Long, accessToken: String): ExternalAccount {
        val account = accountInfoExtractor.getInfo(accessToken)

        googleAuthStore.save(GoogleAccount(account.accountId, account.accountName, userId))

        return account
    }

    suspend fun unbind(userId: Long, googleAccountId: String) {
        if (entryManager.findLoginEntries(userId).entriesCount <= 1) {
            throw SingleAccountLeftException()
        }
        if (!googleAuthStore.remove(userId, googleAccountId)) {
            throw GoogleAccountNotFoundException()
        }
    }

    private suspend fun createNewUser(facebookAccount: ExternalAccount) =
            transactionProvider.transactional {
                val user = userStore.save(UserValue(false))
                googleAuthStore.save(GoogleAccount(facebookAccount.accountId, facebookAccount.accountName, user.userId))
                user
            }

    abstract class GoogleEntryManagerException : NoStackTraceException()
    class GoogleAccountNotFoundException : GoogleEntryManagerException()
    class SingleAccountLeftException : GoogleEntryManagerException()

    class SignInResult(
            val signInToken: SignInToken,
            val userId: Long,
            val facebookAccount: ExternalAccount,
            val isNewUser: Boolean
    )

}