package zeus.user.auth.module.stub

import dagger.*
import zeus.infrstructure.transaction.*
import zeus.user.auth.infrastructure.entry.facebook.oauth.*
import zeus.user.auth.infrastructure.entry.facebook.store.*
import zeus.user.auth.infrastructure.entry.google.oauth.*
import zeus.user.auth.infrastructure.entry.google.store.*
import zeus.user.auth.infrastructure.entry.loginpass.store.*
import zeus.user.auth.infrastructure.entry.loginpass.stream.*
import zeus.user.auth.infrastructure.user.*
import zeus.user.auth.module.*
import javax.inject.*

@Module
object StubInfrastructureModule {

    @JvmStatic
    @Provides
    fun provideInitializer(): InfrastructureInitializer = object : InfrastructureInitializer {}

    @Singleton
    @JvmStatic
    @Provides
    fun provideTransactionProvider(transactionProvider: StubTransactionProvider): TransactionProvider = transactionProvider

    @Singleton
    @JvmStatic
    @Provides
    fun provideLoginPassStore(loginPassStore: StubLoginPassAuthStore): LoginPassAuthStore = loginPassStore

    @Singleton
    @JvmStatic
    @Provides
    fun provideFacebookEntryStore(facebookEntryStore: StubFacebookAuthStore): FacebookAuthStore = facebookEntryStore

    @Singleton
    @JvmStatic
    @Provides
    fun provideGoogleEntryStore(googleEntryStore: StubGoogleAuthStore): GoogleAuthStore = googleEntryStore

    @Singleton
    @JvmStatic
    @Provides
    fun provideUserStore(userStore: StubUserStore): UserStore = userStore

    @Singleton
    @JvmStatic
    @Provides
    fun provideFacebookAccountInfoExtractor(
            infoExtractor: StubFacebookAccountExtractor
    ): FacebookAccountExtractor = infoExtractor

    @Singleton
    @JvmStatic
    @Provides
    fun provideLoginPassUpdatesStream(stream: StubLoginPassUpdatesStream): LoginPassUpdatesStream = stream

    @Singleton
    @JvmStatic
    @Provides
    fun provideGoogleAccountInfoExtractor(
            infoExtractor: StubGoogleAccountExtractor
    ): GoogleAccountExtractor = infoExtractor


}
