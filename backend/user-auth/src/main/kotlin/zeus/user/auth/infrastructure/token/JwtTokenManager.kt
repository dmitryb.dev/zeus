package zeus.user.auth.infrastructure.token

import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import com.auth0.jwt.interfaces.*
import zeus.user.auth.service.util.*
import javax.inject.*

class JwtTokenManager @Inject constructor(private val signAlgorithm: Algorithm) : TokenManager {

    override suspend fun signLoggedInUser(userId: Long): SignInToken {
        return JWT.create()
                .withClaim(TokenField.TYPE, TokenType.SIGN_IN)
                .withClaim(TokenField.USER_ID, userId)
                .sign(signAlgorithm)
    }

    override suspend fun extractUserId(signInToken: SignInToken): Long = parseJwt(signInToken, TokenType.SIGN_IN)
            .claims[TokenField.USER_ID]!!
            .asLong()

    override suspend fun signEmailConfirmation(
            confirmation: TokenManager.EmailConfirmation
    ): EmailConfirmationToken {
        return JWT.create()
                .withClaim(TokenField.TYPE, TokenType.CONFIRM_EMAIL)
                .withClaim(TokenField.USER_ID, confirmation.userId)
                .withClaim(TokenField.EMAIL, confirmation.email)
                .sign(signAlgorithm)
    }

    override suspend fun extractConfirmedEmail(
            confirmationToken: EmailConfirmationToken
    ): TokenManager.EmailConfirmation =
        parseJwt(confirmationToken, TokenType.CONFIRM_EMAIL).let {
            TokenManager.EmailConfirmation(
                    it.claims[TokenField.USER_ID]!!.asLong(),
                    it.claims[TokenField.EMAIL]!!.asString()
            )
        }


    override suspend fun signForgotPass(userId: Long): ForgotPassToken {
        return JWT.create()
                .withClaim(TokenField.TYPE, TokenType.FORGOT_PASS)
                .withClaim(TokenField.USER_ID, userId)
                .sign(signAlgorithm)
    }

    override suspend fun extractForgotPassUserId(forgotPassToken: ForgotPassToken): Long =
            parseJwt(forgotPassToken, TokenType.FORGOT_PASS)
                    .claims[TokenField.USER_ID]!!
                    .asLong()

    private fun parseJwt(token: String, expectedType: String): DecodedJWT =
            JWT.require(signAlgorithm).build()
                    .verify(token)
                    .takeIf { it.claims[TokenField.TYPE]!!.asString() == expectedType }
                    ?: throw IllegalArgumentException("Incorrect token type")

    
}

object TokenType {
    const val SIGN_IN = "signIn"
    const val CONFIRM_EMAIL = "confirmEmail"
    const val FORGOT_PASS = "forgotPass"
}

object TokenField {
    const val TYPE = "type"
    const val USER_ID = "userId"
    const val EMAIL = "email"
}
