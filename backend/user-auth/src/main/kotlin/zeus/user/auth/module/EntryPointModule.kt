package zeus.user.auth.module

import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import com.auth0.jwt.exceptions.*
import com.fasterxml.jackson.core.*
import com.google.protobuf.*
import com.typesafe.config.*
import dagger.*
import io.grpc.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.coroutines.*
import zeus.user.auth.entrypoint.grpc.*
import zeus.user.auth.entrypoint.http.*
import zeus.user.auth.entrypoint.http.util.*
import zeus.user.auth.infrastructure.token.*
import zeus.util.*
import zeus.util.log.*
import javax.inject.*

@Module
object EntryPointModule {

    @JvmStatic
    @Provides
    fun provideGrpcServer(
            config: Config,
            initializer: InfrastructureInitializer,
            loginPassApi: LoginPassAuth,
            facebookAuthApi: FacebookAuth,
            googleEntryApi: GoogleAuth,
            user: User
    ): GrpcServer {
        initializer.init()
        return GrpcServer(config.getInt("grpc.server.port"), loginPassApi, facebookAuthApi, googleEntryApi, user)
    }

    @JvmStatic
    @Provides
    fun provideApiServer(
            config: Config,
            jwtSignAlgorithm: Algorithm,
            publicRoutes: PublicRoutes
    ) : ApplicationEngine = embeddedServer(Netty, config.getInt("http.server.port")) {
        install(Authentication) {
            jwt {
                verifier(JWT.require(jwtSignAlgorithm).build())
                validate { credential ->
                    if (credential.payload.getClaim(TokenField.TYPE).asString() == TokenType.SIGN_IN) {
                        UserPrincipal(credential.payload.getClaim(TokenField.USER_ID).asLong())
                    } else {
                        null
                    }
                }
            }
        }
        install(ContentNegotiation) {
            register(ContentType.Application.Json, JsonConverter())
        }
        install(StatusPages) {
            exception<InvalidProtocolBufferException> { call.respond(HttpStatusCode.BadRequest) }
            exception<JsonProcessingException> { call.respond(HttpStatusCode.BadRequest) }
            exception<JWTDecodeException> { call.respond(HttpStatusCode.BadRequest) }
            exception<StatusException> { when (it.status.code) {
                Status.Code.CANCELLED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.INVALID_ARGUMENT -> call.respond(HttpStatusCode.BadRequest)
                Status.Code.DEADLINE_EXCEEDED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.NOT_FOUND -> call.respond(HttpStatusCode.NotFound)
                Status.Code.ALREADY_EXISTS -> call.respond(HttpStatusCode.Conflict)
                Status.Code.PERMISSION_DENIED -> call.respond(HttpStatusCode.Forbidden)
                Status.Code.FAILED_PRECONDITION -> call.respond(HttpStatusCode.PreconditionFailed)
                Status.Code.ABORTED -> call.respond(HttpStatusCode.RequestTimeout)
                Status.Code.OUT_OF_RANGE -> call.respond(HttpStatusCode.BadRequest)
                Status.Code.UNAUTHENTICATED -> call.respond(HttpStatusCode.Unauthorized)
                else -> call.respond(HttpStatusCode.InternalServerError)
            }}
        }
        intercept(ApplicationCallPipeline.Monitoring) {
            setupTraceId(call.request.header("traceId"))
            withContext(traceContext()) { proceed() }
        }
        routing {
            publicRoutes.httpRoute(this)
        }
    }

}
