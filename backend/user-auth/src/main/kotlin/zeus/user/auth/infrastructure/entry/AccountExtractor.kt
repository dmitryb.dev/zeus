package zeus.user.auth.infrastructure.entry

import zeus.user.auth.domain.*
import zeus.util.exception.*

interface AccountExtractor {

    suspend fun getInfo(accessToken: String): ExternalAccount

    class IncorrectTokenException : NoStackTraceException()

}