package zeus.user.auth.domain

import zeus.util.mysql.mapper.annotation.*

class LoginPassAuth @EntityCreator constructor(
        @Column("user_id") @get:Column("user_id") val id: Long,
        @Column("login") @get:Column("login") val login: String,
        @Column("email") @get:Column("email") val email: String,
        @Column("email_confirmed") @get:Column("email_confirmed") val emailConfirmed: Boolean,
        @Column("pass_hash") @get:Column("pass_hash") val passHash: String
)