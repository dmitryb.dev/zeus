package zeus.user.auth.service

import kotlinx.coroutines.*
import org.mindrot.jbcrypt.*
import zeus.infrstructure.transaction.*
import zeus.user.auth.domain.*
import zeus.user.auth.infrastructure.email.*
import zeus.user.auth.infrastructure.entry.loginpass.store.*
import zeus.user.auth.infrastructure.entry.loginpass.stream.*
import zeus.user.auth.infrastructure.token.*
import zeus.user.auth.infrastructure.user.*
import zeus.user.auth.service.util.*
import zeus.util.exception.*
import javax.inject.*
import kotlin.coroutines.*

class LoginPassManager @Inject constructor(
        private val transactionProvider: TransactionProvider,
        private val userStore: UserStore,
        private val loginPassAuthStore: LoginPassAuthStore,
        private val tokenManager: TokenManager,
        private val emailSender: EmailSender,
        private val emailFactory: EmailFactory,
        private val loginPassUpdatesStream: LoginPassUpdatesStream
) {

    suspend fun signUp(login: String, email: String, pass: String, langCode: String) {
        val passHash = BCrypt.hashpw(pass, BCrypt.gensalt())
        try {
            transactionProvider.transactional {
                val user = userStore.save(UserValue(false))
                loginPassAuthStore.save(LoginPassAuth(user.userId, login, email, false, passHash))

                emailSender.send(
                        email,
                        emailFactory.createConfirmAccountEmail(
                                langCode,
                                tokenManager.signEmailConfirmation(TokenManager.EmailConfirmation(user.userId, email))
                        ))
            }
        } catch (ex: LoginPassAuthStore.AlreadyExistsException) {
            throw EmailAlreadyRegisteredException()
        }
    }

    suspend fun bind(login: String, userId: Long, email: String, pass: String, langCode: String) {
        val passHash = BCrypt.hashpw(pass, BCrypt.gensalt())
        try {
            transactionProvider.transactional {
                loginPassAuthStore.save(LoginPassAuth(userId, login, email, false, passHash))

                emailSender.send(
                        email,
                        emailFactory.createConfirmAccountEmail(
                                langCode,
                                tokenManager.signEmailConfirmation(TokenManager.EmailConfirmation(userId, email))
                        ))
            }
        } catch (ex: LoginPassAuthStore.AlreadyExistsException) {
            throw EmailAlreadyRegisteredException()
        }
    }

    suspend fun confirmEmail(confirmationToken: String): EmailConfirmed {
        val emailConfirmation = tokenManager.extractConfirmedEmail(confirmationToken)

        if (!loginPassAuthStore.updateEmailConfirmation(emailConfirmation.userId, emailConfirmation.email, true)) {
            throw NoSuchEmailException(emailConfirmation.email)
        }

        val signInToken = tokenManager.signLoggedInUser(emailConfirmation.userId)

        GlobalScope.launch {
            loginPassUpdatesStream.publishEmailConfirmed(emailConfirmation.email, signInToken)
        }

        return EmailConfirmed(
                emailConfirmation.userId,
                emailConfirmation.email,
                signInToken
        )
    }

    suspend fun signIn(login: String, pass: String): SignInToken {
        val user = loginPassAuthStore.findByLogin(login) ?: throw NoSuchLoginException()

        if (!BCrypt.checkpw(pass, user.passHash)) {
            throw IncorrectPassException()
        }

        if (!user.emailConfirmed) {
            throw EmailIsNotConfirmed()
        }

        return tokenManager.signLoggedInUser(user.id)
    }

    suspend fun sendConfirmationEmail(email: String, langCode: String) {
        val user = loginPassAuthStore.findByEmail(email) ?: throw NoSuchEmailException(email)

        if (user.emailConfirmed) {
            throw EmailAlreadyConfirmed()
        }

        emailSender.send(
                user.email,
                emailFactory.createConfirmAccountEmail(
                        langCode,
                        tokenManager.signEmailConfirmation(TokenManager.EmailConfirmation(user.id, user.email))
                ))
    }

    suspend fun sendForgotPassEmail(email: String, langCode: String) {
        val user = loginPassAuthStore.findByEmail(email) ?: throw  NoSuchEmailException(email)

        emailSender.send(
                user.email,
                emailFactory.createForgotPasswordEmail(langCode, tokenManager.signForgotPass(user.id))
        )
    }

    suspend fun updatePassWithPassProof(userId: Long, pass: String, newPass: String): SignInToken {
        val user = loginPassAuthStore.find(userId) ?: throw NoSuchUserException(userId)

        if (BCrypt.checkpw(pass, user.passHash)) {
            throw IncorrectProofException()
        }

        loginPassAuthStore.updatePass(userId, BCrypt.hashpw(newPass, BCrypt.gensalt()))

        GlobalScope.launch(coroutineContext) {
            notifyPassChangedSubscribers(userId)
        }

        return tokenManager.signLoggedInUser(user.id)
    }

    suspend fun updatePassWithForgotPassToken(token: String, newPass: String): SignInToken {
        val userId = tokenManager.extractForgotPassUserId(token)

        if (!loginPassAuthStore.updatePass(userId, BCrypt.hashpw(newPass, BCrypt.gensalt()))) {
            throw NoSuchUserException(userId)
        }

        GlobalScope.launch(coroutineContext) {
            notifyPassChangedSubscribers(userId)
        }

        return tokenManager.signLoggedInUser(userId)
    }

    suspend fun updateEmail(userId: Long, newEmail: String, langCode: String) {
        try {
            transactionProvider.transactional {
                if (!loginPassAuthStore.updateEmail(userId, newEmail, false)) {
                    throw NoSuchUserException(userId)
                }

                emailSender.send(
                        newEmail,
                        emailFactory.createConfirmAccountEmail(
                                langCode,
                                tokenManager.signEmailConfirmation(TokenManager.EmailConfirmation(userId, newEmail))
                        ))
            }
        } catch (ex: LoginPassAuthStore.AlreadyExistsException) {
            throw EmailAlreadyRegisteredException()
        }
    }

    suspend fun updateLogin(userId: Long, newLogin: String) {
        try {
            if (!loginPassAuthStore.updateLogin(userId, newLogin)) {
                throw NoSuchUserException(userId)
            }
        } catch (ex: LoginPassAuthStore.AlreadyExistsException) {
            throw LoginAlreadyRegistered()
        }
    }

    suspend fun waitForEmailConfirmation(login: String, pass: String): SignInToken {
        val userBeforeSubscription = loginPassAuthStore.findByLogin(login) ?: throw NoSuchLoginException()

        if (!BCrypt.checkpw(pass, userBeforeSubscription.passHash)) {
            throw IncorrectPassException()
        }
        if (userBeforeSubscription.emailConfirmed) {
            return tokenManager.signLoggedInUser(userBeforeSubscription.id)
        }

        val emailConfirmedDeferred = GlobalScope.async {
            loginPassUpdatesStream.waitForEmailConfirmation(userBeforeSubscription.email)
        }
        val userAfterSubscription = loginPassAuthStore.find(userBeforeSubscription.id)
                ?: throw NoSuchUserException(userBeforeSubscription.id)
        if (userAfterSubscription.emailConfirmed) {
            return tokenManager.signLoggedInUser(userBeforeSubscription.id)
        }
        return emailConfirmedDeferred.await()
    }

    suspend fun waitForPassChange(email: String) {
        return loginPassUpdatesStream.waitForPassChange(email)
    }

    private suspend fun notifyPassChangedSubscribers(userId: Long) {
        val user = loginPassAuthStore.find(userId) ?: throw NoSuchUserException(userId)

        loginPassUpdatesStream.publishPassChanged(user.email)
    }

    class EmailConfirmed(val userId: Long, val email: String, val singInToken: String)

    open class AccountManagerException : NoStackTraceException()

    class EmailAlreadyRegisteredException : AccountManagerException()
    class EmailIsNotConfirmed : AccountManagerException()
    class EmailAlreadyConfirmed : AccountManagerException()
    class NoSuchEmailException(val email: String) : AccountManagerException()
    class NoSuchLoginException : AccountManagerException()

    class LoginAlreadyRegistered : AccountManagerException()

    class IncorrectPassException : AccountManagerException()
    class NoSuchUserException(val userId: Long) : AccountManagerException()
    class IncorrectProofException : AccountManagerException()

}
