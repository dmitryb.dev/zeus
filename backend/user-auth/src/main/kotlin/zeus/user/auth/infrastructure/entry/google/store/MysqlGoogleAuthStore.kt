package zeus.user.auth.infrastructure.entry.google.store

import com.mysql.cj.xdevapi.*
import zeus.user.auth.domain.*
import zeus.util.mysql.exception.*
import zeus.util.mysql.extension.*
import zeus.util.mysql.infrastructure.*
import javax.inject.*

class MysqlGoogleAuthStore @Inject constructor(
        client: Client,
        @Named("schemaName") private val schemaName: String
) : GoogleAuthStore, MysqlStore(client, schemaName, GOOGLE_ENTRY_TABLE) {


    override suspend fun find(accountId: String): GoogleAccount? = withTable { table ->
        table.select("*")
                .where("google_account_id = :accountId")
                .bind("accountId", accountId)
                .findOne<GoogleAccount>()
    }

    override suspend fun findEntriesOfUser(userId: Long): Collection<GoogleAccount> = withTable { table ->
        table.select("*")
                .where("user_id = :userId")
                .bind("userId", userId)
                .findAll<GoogleAccount>()
    }

    override suspend fun save(googleAccount: GoogleAccount) { try {
        withTable { table -> table.save(googleAccount) }
    } catch (ex: DuplicatedException) {
        throw GoogleAuthStore.AlreadyExistsException(googleAccount.accountId)
    }}

    override suspend fun remove(userId: Long, accountId: String): Boolean = withTable { table ->
        val removeResult = table.delete()
                .where("user_id = :userId AND google_account_id = :accountId")
                .bind("userId", userId)
                .bind("accountId", accountId)
                .await()

        removeResult.affectedItemsCount > 0
    }

    companion object {
        const val GOOGLE_ENTRY_TABLE = "google_entries"
    }
}
