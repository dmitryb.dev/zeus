package zeus.user.auth.module.prod

import com.auth0.jwt.algorithms.*
import com.typesafe.config.*
import dagger.*
import zeus.user.auth.infrastructure.email.*
import zeus.user.auth.infrastructure.token.*
import zeus.user.auth.service.*
import java.security.*
import java.security.interfaces.*
import java.security.spec.*
import java.util.*

@Module
object ServiceModule {

    @JvmStatic
    @Provides
    fun provideEmailFactory(config: Config): EmailFactory = EmailFactory(
            config.getString("email.templates"),
            config.getString("email.url.confirm"),
            config.getString("email.url.forgot")
    )

    @JvmStatic
    @Provides
    fun provideSignAlgorithm(config: Config): Algorithm {
        val keyFactory = KeyFactory.getInstance("RSA")
        val privateKey = keyFactory.generatePrivate(
                PKCS8EncodedKeySpec(Base64.getDecoder().decode(
                        config.getString("jwt.private")
                                .replace("\\n".toRegex(), "")
                                .replace("-----BEGIN PRIVATE KEY-----", "")
                                .replace("-----END PRIVATE KEY-----", "")
                                .replace(" ".toRegex(), "")
                ))
        ) as RSAPrivateKey
        val publicKey = keyFactory.generatePublic(
                X509EncodedKeySpec(Base64.getDecoder().decode(
                        config.getString("jwt.public")
                                .replace("\\n".toRegex(), "")
                                .replace("-----BEGIN PUBLIC KEY-----", "")
                                .replace("-----END PUBLIC KEY-----", "")
                                .replace(" ".toRegex(), "")
                ))
        ) as RSAPublicKey

        return Algorithm.RSA256(publicKey, privateKey)
    }

    @JvmStatic
    @Provides
    fun provideTokenManager(tokenManager: JwtTokenManager): TokenManager = tokenManager

    @JvmStatic
    @Provides
    fun provideEmailSender(config: Config): EmailSender = SmtpEmailSender(
            config.getString("smtp.host"),
            config.getInt("smtp.port"),
            config.getString("smtp.user"),
            config.getString("smtp.pass"),
            config.getString("smtp.from")
    )
}
