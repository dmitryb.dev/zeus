package zeus.user.auth.service

import com.typesafe.config.*
import org.jtwig.*
import zeus.user.auth.infrastructure.email.*
import zeus.util.resources.*
import javax.inject.*

class EmailFactory @Inject constructor(
        private val templatesDirectory: String,
        private val confirmEmailUrl: String,
        private val forgotPassUrl: String
) {

    fun createConfirmAccountEmail(localeStr: String, confirmationToken: String): EmailSender.Email {
        val values = cache.computeIfAbsent(localeStr) { ConfigBundle.directory("$templatesDirectory/text", localeStr) }

        val text = template(
                "confirm.twig",
                values.getString("confirm.text"),
                values.getString("confirm.btn"),
                "$confirmEmailUrl?emailConfirmationToken=$confirmationToken"
        )

        return EmailSender.Email(values.getString("confirm.topic"), text)
    }

    fun createForgotPasswordEmail(localeStr: String, forgotPassToken: String): EmailSender.Email {
        val values = cache.computeIfAbsent(localeStr) { ConfigBundle.directory("$templatesDirectory/text", localeStr) }

        val text = template(
                "forgot.twig",
                values.getString("forgot.text"),
                values.getString("forgot.btn"),
                "$forgotPassUrl?forgotPassToken=$forgotPassToken"
        )

        return EmailSender.Email(values.getString("forgot.topic"), text)
    }

    private fun template(templateFile: String, text: String, btn: String, url: String): String {
        val template = if (templatesDirectory.startsWith("resource://")) {
            JtwigTemplate.classpathTemplate("${templatesDirectory.replaceFirst("resource://", "")}/template/$templateFile")
        } else {
            JtwigTemplate.fileTemplate("$templatesDirectory/template/$templateFile")
        }

        val model = JtwigModel.newModel()
                .with("text", text)
                .with("btn", btn)
                .with("url", url)

        return template.render(model)
    }

    companion object {
        private val cache = HashMap<String, Config>()
    }

}
