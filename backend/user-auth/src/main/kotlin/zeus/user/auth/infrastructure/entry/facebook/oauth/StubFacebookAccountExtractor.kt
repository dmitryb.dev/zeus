package zeus.user.auth.infrastructure.entry.facebook.oauth

import zeus.user.auth.domain.*
import javax.inject.*
import kotlin.random.*

class StubFacebookAccountExtractor @Inject constructor() : FacebookAccountExtractor {

    private val names = listOf("John", "Steve Work", "Bill Table")

    override suspend fun getInfo(accessToken: String) = ExternalAccount(
            accessToken,
            names[Random.nextInt(0, names.size)]
    )
}