package zeus.user.auth.entrypoint.grpc.util

fun String.mask() = this.replace(".".toRegex(), "*")