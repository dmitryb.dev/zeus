package zeus.user.auth.module.prod

import com.typesafe.config.*
import dagger.*
import org.apache.pulsar.client.admin.*
import org.apache.pulsar.client.api.*
import org.flywaydb.core.*
import zeus.infrstructure.transaction.*
import zeus.user.auth.infrastructure.entry.facebook.oauth.*
import zeus.user.auth.infrastructure.entry.facebook.store.*
import zeus.user.auth.infrastructure.entry.google.oauth.*
import zeus.user.auth.infrastructure.entry.google.store.*
import zeus.user.auth.infrastructure.entry.loginpass.store.*
import zeus.user.auth.infrastructure.entry.loginpass.stream.*
import zeus.user.auth.infrastructure.user.*
import zeus.user.auth.module.*
import zeus.util.mysql.*
import zeus.util.mysql.infrastructure.*
import zeus.util.pulsar.*
import javax.inject.*

@Module
object InfrastructureModule {

    @JvmStatic
    @Provides
    fun pulsarAdmin(config: Config): PulsarAdmin {
        return PulsarAdmin.builder()
                .serviceHttpUrl(String.format("http://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.http-port")))
                .enableTlsHostnameVerification(false)
                .build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun providePulsarClient(config: Config, pulsarAdmin: PulsarAdmin): PulsarClient {
        PulsarBootstrap.create(pulsarAdmin, "public/user-auth")

        return PulsarClient.builder()
                .serviceUrl(String.format("pulsar://%s:%s",
                        config.getString("pulsar.host"),
                        config.getInt("pulsar.port")))
                .build()
    }

    @Singleton
    @JvmStatic
    @Provides
    fun provideInitializer(config: Config): InfrastructureInitializer = object : InfrastructureInitializer {
        override fun init() {
            Flyway.configure()
                    .dataSource(
                            "jdbc:mysql://${config.getString("mysql.host")}:${config.getString("mysql.port")}/" +
                                    config.getString("mysql.schema") + "?useSSL=false",
                            config.getString("mysql.user"),
                            config.getString("mysql.pass")
                    )
                    .locations("sql")
                    .load()
                    .migrate()
        }
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideMysqlClient(config: Config) = Clients.fromConfig(config)

    @JvmStatic
    @Provides
    @Named("schemaName")
    fun provideSchemaName(config: Config) = config.getString("mysql.schema")!!

    @JvmStatic
    @Provides
    fun provideTransactionProvider(transactionProvider: MysqlTransactionProvider): TransactionProvider = transactionProvider

    @JvmStatic
    @Provides
    fun provideLoginPassStore(loginPassStore: MysqlLoginPassAuthStore): LoginPassAuthStore = loginPassStore

    @JvmStatic
    @Provides
    fun provideFacebookEntryStore(facebookEntryStore: MysqlFacebookAuthStore): FacebookAuthStore = facebookEntryStore

    @JvmStatic
    @Provides
    fun provideGoogleEntryStore(googleEntryStore: MysqlGoogleAuthStore): GoogleAuthStore = googleEntryStore

    @JvmStatic
    @Provides
    fun provideUserStore(userStore: MysqlUserStore): UserStore = userStore

    @JvmStatic
    @Provides
    fun provideLoginPassUpdatesStream(stream: PulsarLoginPassUpdatesStream): LoginPassUpdatesStream = stream

    @JvmStatic
    @Provides
    fun provideFacebookAccountInfoExtractor(
            accountExtractor: HttpFacebookAccountExtractor
    ): FacebookAccountExtractor = accountExtractor

    @JvmStatic
    @Provides
    fun provideGoogleAccountInfoExtractor(
            accountExtractor: HttpGoogleAccountExtractor
    ): GoogleAccountExtractor = accountExtractor

}
