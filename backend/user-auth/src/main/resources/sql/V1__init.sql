CREATE TABLE users(
  user_id bigint AUTO_INCREMENT PRIMARY KEY,
  has_shared_app boolean
);

CREATE TABLE login_pass_entries(
  user_id bigint PRIMARY KEY,
  login varchar(255) UNIQUE,
  email varchar(255) UNIQUE,
  email_confirmed BOOLEAN,
  pass_hash varchar(255)
);

CREATE TABLE facebook_entries(
  facebook_account_id varchar(256) PRIMARY KEY,
  facebook_account_name varchar(255),
  user_id bigint,

  INDEX i__facebook_entries__user_id (user_id)
);

CREATE TABLE google_entries(
  google_account_id varchar(256) PRIMARY KEY,
  google_account_name varchar(255),
  user_id bigint,

  INDEX i__google_entries__user_id (user_id)
);