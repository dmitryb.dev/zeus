import axios from 'axios/index'
import * as HttpStatus from 'http-status-codes/index'

import * as Config from '../../config'
import * as Promise from 'bluebird'

import { EmailSentError } from '@/common/email-sent/service'

Promise.config({ cancellation: true, warnings: false })

export const EmailSentApi = {

  async resendConfirmationEmail(email) {
    await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/login-pass/resend-email`, { email: email })
  },
  async resendPassResetEmail(email) {
    await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/login-pass/forgot-pass`, { email: email })
  },

  waitForEmailConfirmation(email, pass) {
    return requestResponseWebsocket(`${Config.wsApiUrl}/private/api/v1/auth/wait-for-email-confirmation`)
      .request({ login: email, pass: pass })
      .then(response => new Promise((resolve, reject) => {
        if (response.signInToken) {
          resolve(response.signInToken)
        } else {
          response.code === HttpStatus.INTERNAL_SERVER_ERROR ? reject(EmailSentError.ServerError) : reject(response)
        }
      }))
  },
  waitForPassChange(email) {
    return requestResponseWebsocket(`${Config.wsApiUrl}/private/api/v1/auth/wait-for-pass-change`)
      .request({ email: email })
      .then(response => new Promise((resolve, reject) => {
        if (!response.error) {
          resolve()
        } else {
          response.code === HttpStatus.INTERNAL_SERVER_ERROR ? reject(EmailSentError.ServerError) : reject(response)
        }
      }))
  }
}

const requestResponseWebsocket = (url) => ({
  request: (requestMessage) => new Promise((resolve, reject, onCancel) => {
    const webSocket = new WebSocket(url)

    let retryPromise
    let isCancelled = false

    webSocket.onopen = () => {
      webSocket.send(JSON.stringify(requestMessage))
    }

    webSocket.onclose = () => {
      setTimeout(() => {
        if (!isCancelled) {
          retryPromise = requestResponseWebsocket(url).request(requestMessage)
          retryPromise
            .then(resolve)
            .catch(reject)
        }
      }, 2500)
    }

    webSocket.onmessage = ({ data }) => {
      try {
        resolve(data ? JSON.parse(data) : null)
      } catch (e) {
        reject(e)
      } finally {
        isCancelled = true
        webSocket.close()
      }
    }

    onCancel(() => {
      isCancelled = true
      webSocket.close()
      if (retryPromise) {
        retryPromise.cancel()
      }
    })
  })
})