import { delay, TokenManager as TokenManager } from 'zeus-common'
import { EmailSentApi } from './api'

export const EmailSentError = {
  ServerError: 'Server error'
}

export const EmailSentService = {
  waitForEmailConfirmation(email, pass) {
    return EmailSentApi.waitForEmailConfirmation(email, pass)
      .then(TokenManager.saveSignInToken)
      .catch((e) => {
        if (e === EmailSentError.ServerError) {
          return delay(2500).then(() => this.waitForEmailConfirmation(email, pass))
        } else {
          throw e
        }
      })
  },

  waitForPassChange(email) {
    return EmailSentApi.waitForPassChange(email)
      .catch((e) => {
        if (e === EmailSentError.ServerError) {
          delay(2500).then(() => this.waitForPassChange(email))
        } else {
          throw e
        }
      })
  }
}

