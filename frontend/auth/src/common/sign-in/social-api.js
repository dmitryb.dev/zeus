import facebookLogin from 'facebook-login';
import Vue from 'vue'
import { LoaderPlugin } from 'vue-google-login';
import { appUrl, config } from '@/config'

const facebookClientId = '394838444474913'
const googleClientId = '805516939892-d07quaj2u03p9911s8hrpp33v55hlkdn.apps.googleusercontent.com'

Vue.use(LoaderPlugin, {
  client_id: googleClientId
});

export const SocialApi = {
  async facebookLogin() {
    if (isSmallScreen()) {
      location.replace('https://www.facebook.com/v3.3/dialog/oauth?' +
        `client_id=${facebookClientId}` +
        '&display=popup' +
        `&redirect_uri=${appUrl}?calledBy=facebook` +
        `&auth_type=rerequest` +
        `&state=${JSON.stringify(config)}`
      )
    } else {
      return (await facebookLogin({ appId: facebookClientId }).login()).authResponse.accessToken
    }
  },
  async googleLogin() {
    if (isSmallScreen()) {
      location.replace('https://accounts.google.com/o/oauth2/v2/auth?' +
        `client_id=${googleClientId}` +
        '&scope=profile' +
        '&response_type=token' +
        `&redirect_uri=${appUrl}?calledBy=google` +
        `&state=${JSON.stringify(config)}`
      )
    } else {
      const googleAuth = await Vue.GoogleAuth
      const googleUser = await googleAuth.signIn()
      return googleUser.getAuthResponse().id_token
    }
  },
  async getGoogleUser() {
    const googleAuth = await Vue.GoogleAuth
    if (googleAuth.isSignedIn.get()) {
      const googleUser = await googleAuth.currentUser.get()
      return googleUser.getAuthResponse().id_token
    } else {
      return null
    }
  }
}

function isSmallScreen() {
  return window.innerWidth < 600
}