import axios from 'axios/index'
import * as HttpStatus from 'http-status-codes/index'

import { SignInError } from './service'

import * as Config from '../../config'

export const SignInApi = {
  async signIn(login, pass) {
    try {
      const response = await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/login-pass/sign-in`, {
        login: login,
        pass: pass
      })
      return response.data.signInToken
    } catch (error) {
      if (!error.response) {
        throw SignInError.UnknownError
      }
      switch (error.response.status) {
        case HttpStatus.NOT_FOUND: throw SignInError.NoSuchEmail
        case HttpStatus.FORBIDDEN: throw SignInError.IncorrectPassword
        case HttpStatus.PRECONDITION_FAILED: throw SignInError.EmailIsNotConfirmed
        default: throw SignInError.UnknownError
      }
    }
  },
  async signInWithFacebook(accessToken) {
    const response = await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/facebook/sign-in`, {
      facebookAccessToken: accessToken
    })
    return response.data.signInToken
  },
  async signInWithGoogle(accessToken) {
    const response = await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/google/sign-in`, {
      googleAccessToken: accessToken
    })
    return response.data.signInToken
  }
};