import { SignInApi } from './api'
import { TokenManager } from 'zeus-common'
import { SocialApi } from '@/common/sign-in/social-api'

export const SignInService = {
  async signIn(email, pass) {
    const signInToken = await SignInApi.signIn(email, pass)
    TokenManager.saveSignInToken(signInToken)
  },
  async signInWithFacebook(accessToken = null) {
    if (!accessToken) {
      accessToken = await SocialApi.facebookLogin()
    }
    const signInToken = await SignInApi.signInWithFacebook(accessToken)
    TokenManager.saveSignInToken(signInToken)
  },
  async signInWithGoogle(accessToken = null) {
    if (!accessToken) {
      accessToken = await SocialApi.googleLogin()
    }
    const signInToken = await SignInApi.signInWithGoogle(accessToken)
    TokenManager.saveSignInToken(signInToken)
  }
}

export const SignInError = {
  NoSuchEmail: 'Email is not found',
  IncorrectPassword: 'Incorrect password',
  EmailIsNotConfirmed: 'Email is not confirmed',
  UnknownError: 'Server unavailable, retry later'
}