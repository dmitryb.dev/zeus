import { pageBlockingAction, withPageModule } from 'zeus-common'
import { EmailSentApi } from '@/common/email-sent/api'

export default withPageModule({
  namespaced: true,
  state: {
    email: ''
  },
  mutations: {
    setEmail(state, email) {
      state.email = email
    },
    reset(state) {
      state.email = ''
      state.error = null
      state.inProgress = false
    }
  },
  actions: {
    async resendPassResetEmail({ state, commit }) {
      await pageBlockingAction(commit, async () => {
        await EmailSentApi.resendPassResetEmail(state.email)
      })
    }
  }
})