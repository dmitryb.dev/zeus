import { pageBlockingAction, withPageModule } from 'zeus-common'
import { SignInService } from '@/common/sign-in/service'

export default withPageModule({
  namespaced: true,
  actions: {
    async signIn({ commit }, { email, pass, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        try {
          await SignInService.signIn(email, pass)
          onSuccess()
        } catch (error) {
          commit('setError', error)
        }
      })
    },
    async signInWithFacebook({ commit }, onSuccess) {
      await pageBlockingAction(commit, async () => {
        try {
          await SignInService.signInWithFacebook()
          onSuccess()
        } catch (e) {
          // ignore
        }
      })
    },
    async signInWithGoogle({ commit }, onSuccess) {
      await pageBlockingAction(commit, async () => {
        try {
          await SignInService.signInWithGoogle()
          onSuccess()
        } catch (e) {
          // ignore
        }
      })
    }
  }
})