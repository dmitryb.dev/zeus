import axios from 'axios'
import * as HttpStatus from 'http-status-codes'

import * as Config from '../../config'
import { SignUpError } from './service'

export const SignUpApi = {
  async signUp(email, pass) {
    try {
      await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/login-pass/sign-up`, {
        login: email,
        email: email,
        pass: pass
      })
    } catch (error) {
      if (!error.response) {
        throw SignUpError.SERVER_ERROR
      }
      switch (error.response.status) {
        case HttpStatus.CONFLICT: throw SignUpError.EMAIL_ALREADY_EXISTS
        default: throw SignUpError.SERVER_ERROR
      }
    }
  }
}