import { SignUpService } from '@/pages/sign-up/service'
import { pageBlockingAction, withPageModule } from 'zeus-common'

export default withPageModule({
  namespaced: true,
  actions: {
    async signUp({ commit }, { email, pass, passConfirmation, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        try {
          await SignUpService.signUp(email, pass, passConfirmation)
          onSuccess()
        } catch (error) {
          commit('setError', error)
        }
      })
    }
  }
})