import { SignUpApi } from './api'
import { isEmailValid } from 'zeus-common'

export const SignUpService = {
  async signUp(email, pass, passConfirmation) {
    if (!isEmailValid(email)) {
      throw SignUpError.EMAIL_IS_INCORRECT
    }
    if (pass !== passConfirmation) {
      throw SignUpError.PASSWORDS_ARE_DIFFERENT
    }
    return SignUpApi.signUp(email, pass)
  }
}

export const SignUpError = {
  EMAIL_IS_INCORRECT: 'Email is incorrect',
  PASSWORDS_ARE_DIFFERENT: 'Password are different',
  EMAIL_ALREADY_EXISTS: 'Email already exists',
  SERVER_ERROR: 'Server unavailable, retry later'
}