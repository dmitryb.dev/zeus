import axios from 'axios/index'

import * as Config from '../../config'
import { ConfirmingEmailError } from '@/pages/confirming-email/service'

export const ConfirmingEmailApi = {
  async confirmEmail(token) {
    try {
      await axios.post(
        `${Config.httpApiUrl}/private/api/v1/auth/login-pass/confirm-email`,
        { confirmationToken: token }
      )
    } catch (error) {
      if (!error.response || error.response.status >= 500) {
        throw ConfirmingEmailError.SERVER_ERROR
      }
      throw ConfirmingEmailError.CANNOT_CONFIRM_EMAIL
    }
  }
}