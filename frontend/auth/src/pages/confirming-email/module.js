import { pageBlockingAction, withPageModule } from 'zeus-common'
import { ConfirmingEmailService } from '@/pages/confirming-email/service'

export default withPageModule({
  namespaced: true,
  state: {
    confirmed: false
  },
  mutations: {
    emailConfirmed(state) {
      state.confirmed = true
    }
  },
  actions: {
    async confirmEmail({ commit }, token) {
      await pageBlockingAction(commit, async () => {
        try {
          await ConfirmingEmailService.confirmEmail(token)
          commit('emailConfirmed')
        } catch (error) {
          commit('setError', error)
        }
      })
    }
  }
})