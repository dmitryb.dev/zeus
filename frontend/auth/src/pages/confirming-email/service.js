import { ConfirmingEmailApi } from '@/pages/confirming-email/api'

export const ConfirmingEmailError = {
  CANNOT_CONFIRM_EMAIL: 'Can\'t confirm email',
  SERVER_ERROR: 'Server error'
}

export const ConfirmingEmailService = {
  confirmEmail: ConfirmingEmailApi.confirmEmail
}