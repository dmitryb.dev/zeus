import { ChangePassApi } from '@/pages/change-pass/api'
import { TokenManager } from 'zeus-common'

export const ChangePassError = {
  PASSWORDS_ARE_DIFFERENT: 'Password are different',
  CANNOT_UPDATE_PASS: 'Can\'t update password',
  SERVER_ERROR: 'Server error'
}

export const ChangePassService = {
  async changePass(token, pass, passConfirmation) {
    if (pass !== passConfirmation) {
      throw ChangePassError.PASSWORDS_ARE_DIFFERENT
    }

    const result = await ChangePassApi.changePass(token, pass)
    TokenManager.saveSignInToken(result.signInToken)
  }
}