import axios from 'axios/index'

import * as Config from '../../config'
import { ChangePassError } from '@/pages/change-pass/service'

export const ChangePassApi = {
  async changePass(token, pass) {
    try {
      const response = await axios.post(
        `${Config.httpApiUrl}/private/api/v1/auth/login-pass/update-pass`,
        { forgotPassToken: token, newPass: pass }
      )
      return response.data
    } catch (error) {
      if (!error.response || error.response.status >= 500) {
        throw ChangePassError.SERVER_ERROR
      }
      throw ChangePassError.CANNOT_UPDATE_PASS
    }
  }
}