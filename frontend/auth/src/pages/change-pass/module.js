import { pageBlockingAction, withPageModule } from 'zeus-common'
import { ChangePassService } from '@/pages/change-pass/service'

export default withPageModule({
  namespaced: true,
  state: {
    changed: false
  },
  mutations: {
    passChanged(state) {
      state.changed = true
    }
  },
  actions: {
    async changePass({ commit }, { token, pass, passConfirmation }) {
      await pageBlockingAction(commit, async () => {
        try {
          await ChangePassService.changePass(token, pass, passConfirmation)
          commit('passChanged')
        } catch (error) {
          commit('setError', error)
        }
      })
    }
  }
})