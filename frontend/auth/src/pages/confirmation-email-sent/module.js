import { pageBlockingAction, withPageModule } from 'zeus-common'
import { EmailSentApi } from '@/common/email-sent/api'

export default withPageModule({
  namespaced: true,
  state: {
    email: '',
    pass: ''
  },
  mutations: {
    setCredentials(state, { email, pass }) {
      state.email = email
      state.pass = pass
    },
    reset(state) {
      state.email = ''
      state.pass = ''
      state.error = null
      state.inProgress = false
    }
  },
  actions: {
    async resendConfirmationEmail({ state, commit }) {
      await pageBlockingAction(commit, async () => {
        await EmailSentApi.resendConfirmationEmail(state.email)
      })
    }
  }
})