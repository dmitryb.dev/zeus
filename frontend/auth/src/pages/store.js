import Vue from 'vue'
import Vuex from 'vuex'

import SignUpModule from './sign-up/module'
import SignInModule from './sign-in/module'
import ConfirmationEmailSentModule from './confirmation-email-sent/module'
import PassResetEmailSentModule from './pass-reset-email-sent/module'
import ResetPassModule from './reset-pass/module'
import ConfirmingEmailModule from './confirming-email/module'
import ChangePassModule from './change-pass/module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    signUp: SignUpModule,
    signIn: SignInModule,
    confirmationEmailSent: ConfirmationEmailSentModule,
    passResetEmailSent: PassResetEmailSentModule,
    resetPass: ResetPassModule,
    confirmingEmail: ConfirmingEmailModule,
    changePass: ChangePassModule
  }
})