import VueRouter from 'vue-router'
import Vue from 'vue'

import SignUpPage from '@/pages/sign-up/SignUpPage'
import SignInPage from '@/pages/sign-in/SignInPage'
import ConfirmationEmailSentPage from '@/pages/confirmation-email-sent/ConfirmationEmailSentPage'
import ResetPassPage from '@/pages/reset-pass/ResetPassPage'
import PassChangedPage from '@/pages/pass-changed/PassChangedPage'
import EmailConfirmedPage from '@/pages/email-confirmed/EmailConfirmedPage'
import PassResetEmailSentPage from '@/pages/pass-reset-email-sent/PassResetEmailSentPage'
import FacebookCallbackPage from '@/pages/social-callback/FacebookCallbackPage'
import GoogleCallbackPage from '@/pages/social-callback/GoogleCallbackPage'
import ConfirmingEmailPage from '@/pages/confirming-email/ConfirmingEmailPage'
import ChangePassPage from '@/pages/change-pass/ChangePassPage'

import { TokenManager } from 'zeus-common'
import { ButtonSignInFinished } from '@/config'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'abstract',
  routes: [
    { path: '/sign-in', component: SignInPage, beforeEnter: redirectAuthorized },
    { path: '/sign-up', component: SignUpPage, beforeEnter: redirectAuthorized },
    { path: '/reset-pass', component: ResetPassPage, beforeEnter: redirectAuthorized },
    { path: '/confirmation-email-sent', component: ConfirmationEmailSentPage, beforeEnter: redirectAuthorized },
    { path: '/pass-reset-email-sent', component: PassResetEmailSentPage, beforeEnter: redirectAuthorized },
    { path: '/email-confirmed', component: EmailConfirmedPage },
    { path: '/pass-changed/:email', component: PassChangedPage, props: true },
    { path: '/facebook-callback/:accessToken', component: FacebookCallbackPage, props: true },
    { path: '/google-callback/:accessToken', component: GoogleCallbackPage, props: true },
    { path: '/confirming-email/:token', component: ConfirmingEmailPage, props: true },
    { path: '/change-pass/:token', component: ChangePassPage, props: true },

    { path: '*', redirect: '/sign-in' }
  ]
})

function redirectAuthorized(to, from, next) {
  if (TokenManager.getSignInToken()) {
    ButtonSignInFinished.go()
  } else {
    next()
  }
}

export default router