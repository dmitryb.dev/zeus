import axios from 'axios/index'

import * as Config from '../../config'
import * as HttpStatus from 'http-status-codes/index'

import { ResetPassError } from './service'

export const ResetPassApi = {
  async resetPass(email) {
    try {
      await axios.post(`${Config.httpApiUrl}/private/api/v1/auth/login-pass/forgot-pass`, { email: email })
    } catch (error) {
      if (!error.response) {
        throw ResetPassError.UNKNOWN_ERROR
      }
      switch (error.response.status) {
        case HttpStatus.NOT_FOUND: throw ResetPassError.EMAIL_NOT_FOUND
        default: throw ResetPassError.UNKNOWN_ERROR
      }
    }
  }
}