import { pageBlockingAction, withPageModule } from 'zeus-common'
import { ResetPassService } from './service'

export default withPageModule({
  namespaced: true,
  mutations: {
    reset(state) {
      state.error = null
      state.inProgress = false
    }
  },
  actions: {
    async resetPass({ commit }, { email, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        try {
          await ResetPassService.resetPass(email)
          onSuccess()
        } catch (error) {
          commit('setError', error)
        }
      })
    }
  }
})