import { ResetPassApi } from './api'

export const ResetPassError = {
  EMAIL_NOT_FOUND: 'Email not found',
  UNKNOWN_ERROR: 'Server unavailable, retry later'
}

export const ResetPassService = {
  async resetPass(email) {
    await ResetPassApi.resetPass(email)
  }
}

