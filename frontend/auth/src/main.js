import Vue from 'vue'
import Fragment from 'vue-fragment'

import router from './pages/router'
import store from './pages/store'

import App from './App.vue'
import { goToIndexPage } from '@/config'

Vue.config.productionTip = false

Vue.use(Fragment.Plugin)

goToIndexPage(router)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#root')