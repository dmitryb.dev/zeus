import * as queryString from 'query-string'

export const appUrl = location.toString().replace(location.search, "").replace(location.hash, "")
export const httpApiUrl = `http://${process.env.VUE_APP_API_HOST}`
export const wsApiUrl = `ws://${process.env.VUE_APP_API_HOST}`

const propsFromUrl = {
  ...JSON.parse(queryString.parse(location.search).state || '{}'),
  ...JSON.parse(queryString.parse(location.hash).state || '{}'),
  ...queryString.parse(location.search),
  ...queryString.parse(location.hash)
}

export const config = {
  headerText: propsFromUrl.headerText,
  btnFinishText: propsFromUrl.btnFinishText,
  btnFinishUrl: propsFromUrl.btnFinishUrl,
  redirectUrl: propsFromUrl.redirectUrl,
  redirectState: propsFromUrl.redirectState || '',
  darkTheme: propsFromUrl.darkTheme === true || propsFromUrl.darkTheme === 'true'
}

export let header = config.headerText || 'zeus'

const validRedirects = process.env.VUE_APP_VALID_REDIRECTS.split(',')

export let ButtonSignInFinished = {
  text: config.btnFinishText || 'Go to app',
  url: config.redirectUrl && validRedirects.includes(config.redirectUrl)
    ? `${config.redirectUrl}?state=${config.redirectState}`
    : validRedirects[0],
  go() {
    window.location.replace(`${ButtonSignInFinished.url}`)
  }
}

if (config.darkTheme === true) {
  document.body.classList.add('dark-theme')
}

export const goToIndexPage = (router) => {
  if (propsFromUrl.calledBy === 'facebook' && propsFromUrl.code) {
    router.replace(`/facebook-callback/${propsFromUrl.code}`)
    return
  }
  if (propsFromUrl.calledBy === 'google' && propsFromUrl.access_token) {
    router.replace(`/google-callback/${propsFromUrl.access_token}`)
    return
  }
  if (propsFromUrl.emailConfirmationToken) {
    router.replace(`/confirming-email/${propsFromUrl.emailConfirmationToken}`)
    return
  }
  if (propsFromUrl.forgotPassToken) {
    router.replace(`/change-pass/${propsFromUrl.forgotPassToken}`)
    return
  }
  router.replace('/')
}