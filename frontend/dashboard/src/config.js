export const authUrl = process.env.VUE_APP_AUTH_URL

export const appUrl = process.env.VUE_APP_APP_URL
export const apiUrl = `http://${process.env.VUE_APP_API_HOST}`