import { TokenManager } from 'zeus-common'
import { appUrl, authUrl } from '../config'

export const redirectToAuthPage = () => {
  location.replace(`${authUrl}?redirectUrl=${appUrl}`)
}

export const signOut = () => {
  TokenManager.resetSignInToken()
  redirectToAuthPage()
}
