import Vue from 'vue'
import Fragment from 'vue-fragment'
import VueTippy from 'vue-tippy'

import App from './App.vue'
import router from './pages/router'
import store from './pages/store'

Vue.config.productionTip = false

Vue.use(Fragment.Plugin)
Vue.use(VueTippy)

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
