import VueRouter from 'vue-router'
import Vue from 'vue'
import { TokenManager } from 'zeus-common'
import { redirectToAuthPage } from '../common/auth'
import AccountSettingsPage from './settings/account/AccountSettingsPage'
import ManufacturerPage from './settings/manufacturer/ManufacturerPage'

Vue.use(VueRouter)

const router = new VueRouter({
  base: '/dashboard',
  mode: 'history',
  routes: [
    { path: '/account', component: AccountSettingsPage },
    { path: '/manufacturer', component: ManufacturerPage },
    { path: '*', redirect: '/account' }
  ]
})

router.beforeEach((to, from, next) => {
  if (!TokenManager.getSignInToken()) {
    next()
    redirectToAuthPage()
  } else {
    next()
  }
})

export default router