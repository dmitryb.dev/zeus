import { ManufacturerApi } from './api'

export const ManufacturerError = {
  CANNOT_LOAD_MANUFACTURER: 'Can\'t load manufacturer',
  CANNOT_CREATE_MANUFACTURER: 'Can\'t create manufacturer'
}

export const ManufacturerService = {
  getManufacturer: ManufacturerApi.getManufacturer,
  createManufacturer: ManufacturerApi.createManufacturer
}
