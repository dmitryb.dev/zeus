import { pageBlockingAction, withPageModule } from 'zeus-common'
import { ManufacturerService } from './service'

export default withPageModule({
  namespaced: true,
  state: {
    manufacturer: null
  },
  mutations: {
    setManufacturer(state, manufacturer) {
      state.manufacturer = manufacturer
    }
  },
  actions: {
    async loadManufacturer({ commit, state }) {
      if (state.manufacturer === null) {
        await pageBlockingAction(commit, async () => {
          const manufacturer = await ManufacturerService.getManufacturer()
          commit('setManufacturer', manufacturer)
        })
      }
    },
    async becomeManufacturer({ commit, state }) {
      if (state.manufacturer === null) {
        await pageBlockingAction(commit, async () => {
          const manufacturer = await ManufacturerService.createManufacturer()
          commit('setManufacturer', manufacturer)
        })
      }
    }
  }
})