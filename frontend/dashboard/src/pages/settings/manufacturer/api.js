import axios from 'axios/index'
import * as HttpStatus from 'http-status-codes'

import * as Config from '../../../config'
import { TokenManager } from 'zeus-common'
import { ManufacturerError } from './service'

export const ManufacturerApi = {
  async getManufacturer() {
    try {
      const response = await axios.get(
        `${Config.apiUrl}/private/api/v1/manufacturer`,
        {
          headers: {
            'Authorization': `Bearer ${TokenManager.getSignInToken()}`
          }
        }
      )
      return response.data
    } catch (error) {
      if (error.response && error.response.status === HttpStatus.NOT_FOUND) {
        return null
      }
      throw ManufacturerError.CANNOT_LOAD_MANUFACTURER
    }
  },
  async createManufacturer() {
    try {
      const response = await axios.post(
        `${Config.apiUrl}/private/api/v1/manufacturer`,
        {},
        {
          headers: {
            'Authorization': `Bearer ${TokenManager.getSignInToken()}`
          }
        }
      )
      return response.data
    } catch (error) {
      if (error.response && error.response.status === HttpStatus.CONFLICT) {
        return error.response.data
      }
      throw ManufacturerError.CANNOT_CREATE_MANUFACTURER
    }
  }
}