import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import ManufacturerModule from './settings/manufacturer/module'

export default new Vuex.Store({
  modules: {
    manufacturer: ManufacturerModule
  }
})