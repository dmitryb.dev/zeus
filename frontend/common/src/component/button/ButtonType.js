export const ButtonType = {
  INFO: 'info',
  SUCCESS: 'success',
  DANGER: 'danger'
}