export const animate = (element, animationClass) => {
  if (!element.classList.contains(animationClass)) {
    element.classList.add(animationClass)
    element.addEventListener('animationend', () => element.classList.remove(animationClass))
  }
};