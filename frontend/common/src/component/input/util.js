import {animate} from '../util'
import './util.scss'

export const animateError = (element) => animate(element, 'animation-error');