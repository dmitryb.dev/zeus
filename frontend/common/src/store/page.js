export function withPageModule(inner) { return {
  ...inner,
  state: {
    inProgress: false,
    error: null,

    ...inner.state
  },
  mutations: {
    setInProgress(state, value) { state.inProgress = value },

    setError(state, error) { state.error = error },
    resetError(state) { state.error = null },

    ...inner.mutations
  }
}}

export const pageBlockingAction = async (commit, innerAction) => {
  commit('setInProgress', true)
  try {
    await innerAction()
  } catch (error) {
    commit('setError', "Server unavailable, retry later")
  }finally {
    commit('setInProgress', false)
  }
}