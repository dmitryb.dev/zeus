import VButton from './component/button/VButton.vue'
import VButtonLink from './component/button/VButtonLink.vue'
import { ButtonType } from './component/button/ButtonType'

import VPage from './component/page/VPage'
import StatusLabel from './component/StatusLabel'

import FormErrorMessage from './component/input/FormErrorMessage'
import InputNewPass from './component/input/InputNewPass'

import { animate } from './component/util'
import { animateError } from './component/input/util'
import { delay } from './service/util'
import { TokenManager } from './service/token-manager'

import { PassComplexity, getPassComplexity } from './domain/password'
import { isEmailValid } from './domain/email'

import { withPageModule, pageBlockingAction } from './store/page'

export {
  VButton, VButtonLink, ButtonType,
  VPage,
  StatusLabel,
  FormErrorMessage, InputNewPass,
  isEmailValid,

  PassComplexity, getPassComplexity,

  animate, animateError,
  TokenManager,
  delay,

  withPageModule, pageBlockingAction
}