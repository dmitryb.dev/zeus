export const PassComplexity = {
  Low: 'low', Medium: 'medium', High: 'high'
};

export const getPassComplexity = (pass) => {
  if (pass.length === 0) {
    return null
  }
  if (pass.length < 8) {
    return PassComplexity.Low
  }
  if (pass.length < 12) {
    return PassComplexity.Medium
  }
  return PassComplexity.High
};