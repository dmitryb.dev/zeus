import * as Promise from 'bluebird'

Promise.config({ cancellation: true, warnings: false })

export const delay = (millis) => new Promise(resolve => {
  setTimeout(resolve, millis)
})