import * as Cookies from "js-cookie";

const LocalStorageTokenManager = {
  saveSignInToken(token) {
    localStorage.setItem('sign-in-token', token)
  },
  getSignInToken() {
    return localStorage.getItem('sign-in-token')
  },
  resetSignInToken() {
    localStorage.removeItem('sign-in-token')
  }
}

const CookieTokenManager = {
  saveSignInToken(token) {
    Cookies.set('sign-in-token', token)
  },
  getSignInToken() {
    return Cookies.get('sign-in-token')
  },
  resetSignInToken() {
    Cookies.remove('sign-in-token')
  }
}

export const TokenManager = process.env.NODE_ENV === 'production' ? LocalStorageTokenManager : CookieTokenManager