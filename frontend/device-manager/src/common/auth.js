import { TokenManager } from 'zeus-common'
import { appUrl, authUrl, darkTheme, headerText, preset, redirectUrl } from '@/config'

export const redirectToAuthPage = () => {
  const state = { headerText: headerText, ...preset, redirectUrl: redirectUrl, darkTheme: darkTheme }
  location.replace(`${authUrl}?redirectUrl=${appUrl}&redirectState=${JSON.stringify(state)}&darkTheme=${darkTheme}`)
}

export const signOut = () => {
  TokenManager.resetSignInToken()
  redirectToAuthPage()
}
