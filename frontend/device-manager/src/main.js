import Vue from 'vue'
import Fragment from 'vue-fragment'

import App from './App.vue'

import router from './pages/router'
import store from './pages/store'

import * as Promise from "bluebird"

Vue.config.productionTip = false

Vue.use(Fragment.Plugin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#root')

Promise.config({ cancellation: true })

router.replace('/')