import * as queryString from 'query-string'
import Zeus from 'zeus-client/src'

export const authUrl = process.env.VUE_APP_AUTH_URL

export const appUrl = process.env.VUE_APP_APP_URL
export const apiUrl = `http://${process.env.VUE_APP_API_HOST}`

export const ZeusApi = Zeus({ host: process.env.VUE_APP_API_HOST })

const propsFormUrl = queryString.parse(location.search)
const props = {
  ...propsFormUrl,
  ...JSON.parse(propsFormUrl.state || '{}')
}

export const preset = {
  manufacturerId: props.manufacturerId,
  deviceNumber: props.deviceNumber,
  tags: parseTags(props.tags),
  role: props.role,
  pass: props.pass,
}

export const headerText = props.headerText
export const redirectUrl = props.redirectUrl
export const darkTheme = props.darkTheme === true || props.darkTheme === 'true'

if (darkTheme === true) {
  document.body.classList.add('dark-theme')
}

function parseTags(tags) {
  if (Array.isArray(tags)) {
    return tags
  }
  if (typeof tags === 'string') {
    return tags.split(/[,\s]+/g).filter(tag => tag)
  }
  if (tags !== undefined) {
    return [ tags.toString() ]
  }
  return undefined
}