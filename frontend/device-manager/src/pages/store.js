import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import ChooseDeviceModule from '@/pages/choose-device/module'
import AddDeviceModule from '@/pages/device-editor/add-device/module'
import EditDeviceModule from '@/pages/device-editor/edit-device/module'

export default new Vuex.Store({
  modules: {
    chooseDevice: ChooseDeviceModule,
    addDevice: AddDeviceModule,
    editDevice: EditDeviceModule
  }
})