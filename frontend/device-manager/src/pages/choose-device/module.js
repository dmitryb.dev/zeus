import { pageBlockingAction, withPageModule } from 'zeus-common'
import { preset } from '@/config'
import { ChooseDeviceService } from '@/pages/choose-device/service'

export default withPageModule({
  namespaced: true,
  state: {
    loaded: false,
    devices: []
  },
  mutations: {
    setLoadedDevices(state, devices) {
      state.loaded = true
      state.devices = devices
    },
    addDevice(state, device) {
      state.devices.push(device)
    },
    removeDevice(state, deviceId) {
      state.devices = state.devices.filter(device => device.deviceId !== deviceId)
    },
    updateDevice(state, updatedDevice) {
      const index = state.devices.findIndex(device => device.deviceId === updatedDevice.deviceId)
      state.devices[index] = { ...updatedDevice, pass: undefined }
    }
  },
  actions: {
    async loadDevices({ commit, state }) {
      if (!state.loaded) {
        await pageBlockingAction(commit, async () => {
          const devices = await ChooseDeviceService.findDevices(preset)
          commit('setLoadedDevices', devices)
        })
      }
    }
  }
})