import axios from 'axios/index'

import * as Config from '../../config'
import { TokenManager } from 'zeus-common'

export const ChooseDeviceApi = {
  async findDevices(filter) {
    const response = await axios.post(
      `${Config.apiUrl}/private/api/v1/device/find`,
      filter,
      {
        headers: {
        'Authorization': `Bearer ${TokenManager.getSignInToken()}`
        }
      }
    )
    return response.data.devices
  }
}