import { ChooseDeviceApi } from '@/pages/choose-device/api'
import { redirectUrl } from '@/config'

export const ChooseDeviceService = {
  findDevices: ChooseDeviceApi.findDevices,
  chooseDevice(device) {
    if (redirectUrl) {
      const concatQuerySymbol = redirectUrl.includes('?') ? '&' : '?'
      const targetUrl = redirectUrl + concatQuerySymbol + 'zeus_device=' + JSON.stringify(device)
      location.replace(targetUrl)
    } else {
      window.opener.postMessage(device, '*')
      window.close()
    }
  }
}