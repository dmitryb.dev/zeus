import { ZeusApi } from '@/config'

export const DeviceEditorError = {
  MANUFACTURER_ID_IS_NOT_NUMBER: 'Manufacturer Id must be a number',
  DEVICE_NUMBER_IS_NOT_NUMBER: 'Device number is not a number',

  NO_SUCH_MANUFACTURER: 'Manufacturer not found',
  NO_DEVICE_WITH_SUCH_ID: 'Device not found',

  SERVER_ERROR: 'Server unavailable, retry later'
}

export const validateDevice = ({ manufacturerId, deviceNumber }) => {
  if (isNaN(Number(manufacturerId))) {
    throw DeviceEditorError.MANUFACTURER_ID_IS_NOT_NUMBER
  }
  if (isNaN(Number(deviceNumber))) {
    throw DeviceEditorError.DEVICE_NUMBER_IS_NOT_NUMBER
  }
}

export const DeviceService = {
  async getStatus(device) {
    validateDevice(device)

    try {
      return await ZeusApi.device(
        {
          manufacturerId: device.manufacturerId,
          deviceNumber: device.deviceNumber,
          tags: device.tags
        },
        {
          role: device.role,
          pass: device.pass
        }
      ).status()
    } catch (error) {
      throw DeviceEditorError.SERVER_ERROR
    }
  }
}