import { pageBlockingAction, withPageModule } from 'zeus-common'
import { AddDeviceService } from '@/pages/device-editor/add-device/service'
import { DeviceService } from '@/pages/device-editor/common'

export default withPageModule({
  namespaced: true,
  state: {
    hasOtherDevices: false,
    deviceStatus: {}
  },
  mutations: {
    setDeviceCount(state, count) {
      state.hasOtherDevices = count > 0
    },
    setDeviceStatus(state, deviceStatus) {
      state.deviceStatus = deviceStatus
    }
  },
  actions: {
    async loadStatus({ commit }, { device, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        try {
          commit('setDeviceStatus', await DeviceService.getStatus(device))
          onSuccess()
        } catch (error) {
          commit('setError', error)
        }
      })
    },
    async addDevice({ commit }, { device, online, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        const deviceId = await AddDeviceService.addDevice(device)
        commit('chooseDevice/addDevice', { deviceId: deviceId, ...device, online: online }, { root: true })
        onSuccess()
      })
    }
  }
})