import axios from 'axios/index'
import * as HttpStatus from 'http-status-codes'

import * as Config from '../../../config'
import { TokenManager } from 'zeus-common'
import { DeviceEditorError } from '@/pages/device-editor/common'

export const AddDeviceApi = {
  async addDevice(deviceValue) {
    try {
      const response = await axios.post(
        `${Config.apiUrl}/private/api/v1/device`,
        deviceValue,
        {
          headers: {
            'Authorization': `Bearer ${TokenManager.getSignInToken()}`
          }
        }
      )
      return response.data
    } catch (error) {
      if (!error.response) {
        throw DeviceEditorError.SERVER_ERROR
      }
      switch (error.response.status) {
        case HttpStatus.PRECONDITION_FAILED: throw DeviceEditorError.NO_SUCH_MANUFACTURER
        default: throw DeviceEditorError.SERVER_ERROR
      }
    }
  }
}