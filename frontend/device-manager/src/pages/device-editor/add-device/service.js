import { AddDeviceApi } from '@/pages/device-editor/add-device/api'

export const AddDeviceService = {
  async addDevice(device) {
    const addedDevice = await AddDeviceApi.addDevice(device)
    return addedDevice.deviceId
  }
}