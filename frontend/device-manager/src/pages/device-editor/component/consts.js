export const DeviceEditorMode = {
  ADD_NEW: "add-new",
  EDIT_EXISTING: "edit-existing"
}

export const DeviceEditorStep = {
  EDIT_DEVICE: 'Edit device',
  CHOOSE_NAME: 'Choose name'
}

export const CancelButton = {
  BACK: 'back',
  SIGN_OUT: 'sign-out'
}