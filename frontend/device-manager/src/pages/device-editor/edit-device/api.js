import axios from 'axios/index'

import * as Config from '../../../config'
import { TokenManager } from 'zeus-common'
import * as HttpStatus from 'http-status-codes'
import { DeviceEditorError } from '@/pages/device-editor/common'

export const EditDeviceApi = {
  async updateDevice(deviceId, device) {
    await axios.put(
      `${Config.apiUrl}/private/api/v1/device/${deviceId}`,
      device,
      {
        headers: {
          'Authorization': `Bearer ${TokenManager.getSignInToken()}`
        }
      }
    )
  },
  async removeDevice(deviceId) {
    try {
      await axios.delete(
        `${Config.apiUrl}/private/api/v1/device/${deviceId}`,
        {
          headers: {
            'Authorization': `Bearer ${TokenManager.getSignInToken()}`
          }
        }
      )
    } catch (error) {
      if (!error.response) {
        throw DeviceEditorError.SERVER_ERROR
      }
      switch (error.response.status) {
        case HttpStatus.NOT_FOUND: throw DeviceEditorError.NO_DEVICE_WITH_SUCH_ID
        default: throw DeviceEditorError.SERVER_ERROR
      }
    }
  }
}