import { EditDeviceApi } from '@/pages/device-editor/edit-device/api'
import { DeviceEditorError } from '@/pages/device-editor/common'

export const EditDeviceService = {
  async removeDevice(deviceId) {
    try {
      await EditDeviceApi.removeDevice(deviceId)
    } catch (error) {
      if (error !== DeviceEditorError.NO_DEVICE_WITH_SUCH_ID) {
        throw error
      }
    }
  },
  async updateDevice(deviceId, device) {
    await EditDeviceApi.updateDevice(deviceId, {
      manufacturerId: device.manufacturerId,
      deviceNumber: device.deviceNumber,
      tags: device.tags,
      role: device.role,
      pass: device.pass ? device.pass : undefined,
      name: device.name
    })
  }
}