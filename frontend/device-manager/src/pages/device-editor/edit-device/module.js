import { pageBlockingAction, withPageModule } from 'zeus-common'
import { EditDeviceService } from '@/pages/device-editor/edit-device/service'
import { DeviceEditorError, DeviceService } from '@/pages/device-editor/common'

export default withPageModule({
  namespaced: true,
  state: {
    removeInProgress: false,
    device: undefined
  },
  mutations: {
    setRemoveInProgress(state, inProgress) {
      state.removeInProgress = inProgress
    },
    setDeviceStatus(state, status) {
      state.device.online = status.online
    },
    setDevice(state, device) {
      state.device = device
    }
  },
  actions: {
    async loadStatus({ commit }, { device, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        try {
          commit('setDeviceStatus', await DeviceService.getStatus(device))
          onSuccess()
        } catch (error) {
          commit('setError', error)
        }
      })
    },
    async deleteDevice({ commit }, { deviceId, onSuccess }) {
      commit('setRemoveInProgress', true)
      try {
        await EditDeviceService.removeDevice(deviceId)
        commit('chooseDevice/removeDevice', deviceId, { root: true })
        onSuccess()
      } catch (error) {
        commit('setError', DeviceEditorError.SERVER_ERROR)
      } finally {
        commit('setRemoveInProgress', false)
      }
    },
    async updateDevice({ commit }, { deviceId, device, online, onSuccess }) {
      await pageBlockingAction(commit, async () => {
        await EditDeviceService.updateDevice(deviceId, device)
        commit(
          'chooseDevice/updateDevice',
          { deviceId: deviceId, ...device, online: online },
          { root: true }
        )
        onSuccess()
      })
    }
  }
})