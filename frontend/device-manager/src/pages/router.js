import VueRouter from 'vue-router'
import Vue from 'vue'
import ChooseDevicePage from '@/pages/choose-device/ChooseDevicePage'
import AddDevicePage from '@/pages/device-editor/add-device/AddDevicePage'
import EditDevicePage from '@/pages/device-editor/edit-device/EditDevicePage'
import { TokenManager } from 'zeus-common'
import { redirectToAuthPage } from '@/common/auth'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'abstract',
  routes: [
    { path: '/choose-device', component: ChooseDevicePage },
    { path: '/add-device', component: AddDevicePage },
    { path: '/edit-device', component: EditDevicePage },
    { path: '*', redirect: '/choose-device' }
  ]
})

router.beforeEach((to, from, next) => {
  if (!TokenManager.getSignInToken()) {
    redirectToAuthPage()
  } else {
    next()
  }
})

export default router